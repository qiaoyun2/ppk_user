//
//  Copyright © 2020 Tencent. All rights reserved.
//
//  Module: V2TXLive
//

#import <Foundation/Foundation.h>


/// @defgroup V2TXLiveCode_ios V2TXLiveCode
/// 腾讯云直播服务(LVB)错误码和警告码的定义。
/// @{
/**
 * @brief 错误码和警告码。
 */
typedef NS_ENUM(NSInteger, V2TXLiveCode) {

    /// 没有错误
    V2TXLIVE_OK                               =  0,
    /// 暂未归类的通用错误
    V2TXLIVE_ERROR_FAILED                     = -1,
    /// 调用 API 时，传入的参数不合法
    V2TXLIVE_ERROR_INVALID_PARAMETER          = -2,
    /// API 调用被拒绝
    V2TXLIVE_ERROR_REFUSED                    = -3,
    /// 当前 API 不支持调用
    V2TXLIVE_ERROR_NOT_SUPPORTED              = -4,
    /// license 不合法，调用失败
    V2TXLIVE_ERROR_INVALID_LICENSE            = -5,
    /// 超时错误
    V2TXLIVE_ERROR_REQUEST_TIMEOUT            = -6,
    /// 服务器无法处理您的请求
    V2TXLIVE_ERROR_SERVER_PROCESS_FAILED      = -7,
    /// 由于上游带宽太低，导致数据上传被阻塞
    V2TXLIVE_WARNING_NETWORK_BUSY             = 1101,
    /// 视频回放期间出现滞后
    V2TXLIVE_WARNING_VIDEO_BLOCK              = 2105,


    /////////////////////////////////////////////////////////////////////////////////
    //
    //       摄像头相关的警告码
    //
    /////////////////////////////////////////////////////////////////////////////////

    /// 摄像头打开失败
    V2TXLIVE_WARNING_CAMERA_START_FAILED      = -1301,
    /// 摄像头正在被占用中，可尝试打开其他摄像头
    V2TXLIVE_WARNING_CAMERA_OCCUPIED          = -1316,
    /// 摄像头设备未授权，通常在移动设备出现，可能是权限被用户拒绝了
    V2TXLIVE_WARNING_CAMERA_NO_PERMISSION     = -1314,


    /////////////////////////////////////////////////////////////////////////////////
    //
    //       麦克风相关的警告码
    //
    /////////////////////////////////////////////////////////////////////////////////

    /// 麦克风打开失败
    V2TXLIVE_WARNING_MICROPHONE_START_FAILED  = -1302,
    /// 麦克风正在被占用中，例如移动设备正在通话时，打开麦克风会失败
    V2TXLIVE_WARNING_MICROPHONE_OCCUPIED      = -1319,
    /// 麦克风设备未授权，通常在移动设备出现，可能是权限被用户拒绝了
    V2TXLIVE_WARNING_MICROPHONE_NO_PERMISSION = -1317,


    /////////////////////////////////////////////////////////////////////////////////
    //
    //             屏幕分享相关警告码
    //
    /////////////////////////////////////////////////////////////////////////////////
    /// 当前系统不支持屏幕分享
    V2TXLIVE_WARNING_SCREEN_CAPTURE_NOT_SUPPORTED = -1309,
    /// 开始录屏失败，如果在移动设备出现，可能是权限被用户拒绝了
    V2TXLIVE_WARNING_SCREEN_CAPTURE_START_FAILED  = -1308,
    /// 录屏被系统中断
    V2TXLIVE_WARNING_SCREEN_CAPTURE_INTERRUPTED   = -7001,


    /////////////////////////////////////////////////////////////////////////////////
    //
    //             实时音视频(TRTC) 错误码
    //
    /////////////////////////////////////////////////////////////////////////////////
    V2TXLIVE_ERROR_INVALID_ENTER_PARAM           = -3316,   ///< 进房参数错误
    V2TXLIVE_ERROR_INVALID_SDK_APP_ID            = -3317,   ///< 进房参数 sdkAppId 错误
    V2TXLIVE_ERROR_INVALID_ROOM_ID               = -3318,   ///< 进房参数 roomId 错误
    V2TXLIVE_ERROR_INVALID_USER_ID               = -3319,   ///< 进房参数 userId 错误
    V2TXLIVE_ERROR_INVALID_USER_SIG              = -3320,   ///< 进房参数 userSig 错误
    V2TXLIVE_ERROR_ENTER_ROOM_FAILED             = -3301,   ///< 通用进房失败
    V2TXLIVE_ERROR_ENTER_ROOM_TIMEOUT            = -3308,   ///< 网络不佳，进房超时
    V2TXLIVE_ERROR_SERVICE_SUSPENDED             = -100013, ///< 服务不可用，欠费

    V2TXLIVE_ERROR_ROOM_DISMISSED                = -2010,   ///< 房间被解散
    V2TXLIVE_ERROR_USER_KICKOUT                  = -2011,   ///< 用户被踢出
};

typedef NS_ENUM(NSUInteger, V2TXLiveMode) {
    V2TXLiveMode_RTMP,    ///< RTMP 协议
    V2TXLiveMode_RTC     ///< TRTC 协议
};

/// @}
