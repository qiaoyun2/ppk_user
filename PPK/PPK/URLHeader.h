//
//  URLHeader.h
//  HOOLA
//
//  Created by null on 2018/9/18.
//  Copyright © 2018 null. All rights reserved.
//

#ifndef URLHeader_h
#define URLHeader_h

#define SUCCESS  1
#define TokenError  2
#define RequestServerError @"您的网络不稳定，请稍后重试"

#define kDomainUrl @"https://hello.pinpinkan.vip/pinpinkan-app/api/"
#define kServerUrl [NSString stringWithFormat:@"%@v1/", kDomainUrl]
#define kServerUrlV2 [NSString stringWithFormat:@"%@v2/", kDomainUrl]

#define kBaseImageUrl @"https://pinpinkans.oss-cn-hangzhou.aliyuncs.com/"

#define kImageUrl(url) [url hasPrefix:@"http"] ? [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] : [NSString stringWithFormat:@"%@%@",kBaseImageUrl,[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]


#pragma mark ===== 通用 =====
/** 文件上传 **/
#define kUploadFileURL [NSString stringWithFormat:@"%@common/uploadImagesAli", kServerUrl]
/** 发送验证码  注册:register； 登录：login; 忘记密码：forget； 绑定微信qq:bindThird **/
#define kGetCodeURL [NSString stringWithFormat:@"%@sms/aliSend", kServerUrl]
/** 获取配置信息  registerAgreement：注册协议； privacyPolicy：隐私政策l; logo:logo; companyPicture:公司图片; personalPicture:个人图片; officialMobile:官方电话**/
#define kConfigNameURL [NSString stringWithFormat:@"%@config/config/queryValueByConfigName", kServerUrl]
/** 留言建议 **/
#define kFeedbackURL [NSString stringWithFormat:@"%@marketFeedback/add",kServerUrl]

#pragma mark ===== 登录注册 =====
/** 注册 **/
#define kRegisterURL [NSString stringWithFormat:@"%user/register", kServerUrl]
/** 账号密码登录 **/
#define kMobileLoginURL [NSString stringWithFormat:@"%@user/login", kServerUrl]
/** 手机验证码登录 **/
#define kCodeLoginURL [NSString stringWithFormat:@"%@user/loginOrRegister", kServerUrl]
/** 忘记密码 **/
#define kForgetPwdURL [NSString stringWithFormat:@"%@user/forgetPwd", kServerUrl]
/** qq、微信一键登录 **/
#define kThirdLoginURL [NSString stringWithFormat:@"%@user/mobileThirdLogin", kServerUrl]
/** qq、微信绑定手机号 **/
#define kThirdBindURL [NSString stringWithFormat:@"%@user/bindOpenId", kServerUrl]


#pragma mark - 外卖
/** 首页搜索条件 **/
#define kTOBannerListURL [NSString stringWithFormat:@"%@takeoutBanner/takeoutBanner/list", kServerUrl]
/** 一级分类 **/
#define kTOShopCategoryURL [NSString stringWithFormat:@"%@takeoutShopCategory/takeoutShopCategory/firstLevelList", kServerUrl]
/** 二级分类 **/
#define kTOShopChildCategoryURL [NSString stringWithFormat:@"%@takeoutShopCategory/takeoutShopCategory/childLevelList", kServerUrl]
/** 搜索发现提示词 **/
#define kTOSearchHotListURL [NSString stringWithFormat:@"%@takeoutSearchHot/takeoutSearchHot/list", kServerUrl]
/** 商铺列表 **/
#define kTOShopListURL [NSString stringWithFormat:@"%@takeoutShop/takeoutShop/first/list", kServerUrl]
/** 店铺详情 **/
#define kTOShopDetailURL [NSString stringWithFormat:@"%@takeoutShop/takeoutShop/queryById", kServerUrl]
/** 商品分类的商品 **/
#define kTOFoodListURL [NSString stringWithFormat:@"%@takeoutFood/takeoutFood/list", kServerUrl]
/** 店铺内搜索 **/
#define kTOFoodSearchURL [NSString stringWithFormat:@"%@takeoutShop/takeoutShop/search", kServerUrl]
/** 商品详情 **/
#define kTOFoodDetailURL [NSString stringWithFormat:@"%@takeoutFood/takeoutFood/getById", kServerUrl]

/** 商铺收藏 - 新增 **/
#define kTOCollectionAddURL [NSString stringWithFormat:@"%@takeoutShopCollect/takeoutShopCollect/add", kServerUrl]
/** 商铺收藏 - 列表 **/
#define kTOCollectionListURL [NSString stringWithFormat:@"%@takeoutShopCollect/takeoutShopCollect/list", kServerUrl]
/** 商铺收藏 - 删除 **/
#define kTOCollectionCancelURL [NSString stringWithFormat:@"%@takeoutShopCollect/takeoutShopCollect/deleteBatch", kServerUrl]

/** 购物车 - 新增 **/
#define kTOCardAddURL [NSString stringWithFormat:@"%@takeoutCart/takeoutCart/add", kServerUrl]
/** 购物车 - 编辑数量 **/
#define kTOCardEditURL [NSString stringWithFormat:@"%@takeoutCart/takeoutCart/edit", kServerUrl]
/** 购物车 - 删除 **/
#define kTOCardDeleteURL [NSString stringWithFormat:@"%@takeoutCart/takeoutCart/delete", kServerUrl]
/** 购物车 - 批量删除 **/
#define kTOCardDeleteBatchURL [NSString stringWithFormat:@"%@takeoutCart/takeoutCart/deleteBatch", kServerUrl]
/** 购物车 - 清空 **/
#define kTOCardDeleteAllURL [NSString stringWithFormat:@"%@takeoutCart/takeoutCart/deleteAll", kServerUrl]
/** 购物车 - 列表 **/
#define kTOCardListAllURL [NSString stringWithFormat:@"%@takeoutCart/takeoutCart/list", kServerUrl]

/** 配送时间 **/
#define kTOTimeConfigURL [NSString stringWithFormat:@"%@takeoutTimeConfig/takeoutTimeConfig/list", kServerUrl]

/** 外卖订单 - 下单 **/
#define kTOOrderAddURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/add", kServerUrl]
/** 外卖订单 - 订单列表 **/
#define kTOOrderListURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/list", kServerUrl]
/** 外卖订单 - 订单详情 **/
#define kTOOrderDetailURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/queryById", kServerUrl]
/** 外卖订单 - 调起支付 **/
#define kTOOrderPayURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/pay", kServerUrl]
/** 外卖订单 - 取消订单 **/
#define kTOOrderCancelURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/cancel", kServerUrl]
/** 外卖订单 - 退款 **/
#define kTOOrderRefundURL [NSString stringWithFormat:@"%@takeoutOrderRefund/takeoutOrderRefund", kServerUrl]
/** 外卖订单 - 删除 **/
#define kTOOrderDeleteURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/delete", kServerUrl]
/** 外卖订单 - 评价 **/
#define kTOOrderCommentAddURL [NSString stringWithFormat:@"%@takeoutEvaluate/takeoutEvaluate/add", kServerUrl]
/** 外卖订单 - 评价统计 **/
#define kTOOrderCommentCountURL [NSString stringWithFormat:@"%@takeoutEvaluate/takeoutEvaluate/count", kServerUrl]
/** 外卖订单 - 评价列表 **/
#define kTOOrderCommentListURL [NSString stringWithFormat:@"%@takeoutEvaluate/takeoutEvaluate/list", kServerUrl]

#pragma mark - 房产租赁
/** 首页搜索条件 **/
#define kHLFilterInfoURL [NSString stringWithFormat:@"%@house/house/homeSearchMsg", kServerUrl]
/** 有房发布类型选择 **/
#define kHLHouseTypeListURL [NSString stringWithFormat:@"%@houseType/houseType/list", kServerUrl]
/** 租赁信息-新增 **/
#define kHLAddHouseLeaseURL [NSString stringWithFormat:@"%@house/house/add", kServerUrl]
/** 租赁信息-编辑 **/
#define kHLEditHouseLeaseURL [NSString stringWithFormat:@"%@house/house/edit", kServerUrl]
/** 租赁信息-详情 **/
#define kHLHouseLeaseDetailURL [NSString stringWithFormat:@"%@house/house/queryById", kServerUrl]
/** 租赁信息-删除 **/
#define kHLDeleteHouseLeaseURL [NSString stringWithFormat:@"%@house/house/delete", kServerUrl]

/** 房产收藏-新增或者取消 **/
#define kHLHouseLeaseCollectionURL [NSString stringWithFormat:@"%@houseUserFavorites/houseUserFavorites/addOrCancel", kServerUrl]
/** 房产收藏-列表 **/
#define kHLCollectionListURL [NSString stringWithFormat:@"%@houseUserFavorites/houseUserFavorites/list", kServerUrl]
/** 房产收藏-批量删除 **/
#define kHLDeleteCollectionURL [NSString stringWithFormat:@"%@houseUserFavorites/houseUserFavorites/deleteBatch", kServerUrl]

/** 完善用户信息-下拉框数据 **/
#define kHLInitSelectDataURL [NSString stringWithFormat:@"%@houseUserInfo/houseUserInfo/initSelectData", kServerUrl]
/** 完善用户信息-新增 **/
#define kHLEditUserInfoURL [NSString stringWithFormat:@"%@houseUserInfo/houseUserInfo/add", kServerUrl]
/** 获取个人信息 **/
#define kHLUserInfoURL [NSString stringWithFormat:@"%@houseUserInfo/houseUserInfo/queryUser", kServerUrl]

/** 搜索列表 **/
#define kHLHouseLeaseListURL [NSString stringWithFormat:@"%@house/house/list", kServerUrl]
/** 发布记录 **/
#define kHLReleaseListURL [NSString stringWithFormat:@"%@house/house/myReleaseList", kServerUrl]
/** 举报类型列表 **/
#define kHLReportReasonURL [NSString stringWithFormat:@"%@houseReportType/houseReportType/list", kServerUrl]
/** 举报提交 **/
#define kHLAddReportURL [NSString stringWithFormat:@"%@houseReport/houseReport/add", kServerUrl]

/** 留言-新增 **/
#define kHLAddCommentURL [NSString stringWithFormat:@"%@houseComment/houseComment/add", kServerUrl]
/** 留言-删除 **/
#define kHLDeleteCommentURL [NSString stringWithFormat:@"%@houseComment/houseComment/delete", kServerUrl]
/** 留言列表 **/
#define kHLCommentListURL [NSString stringWithFormat:@"%@houseComment/houseComment/list", kServerUrl]

#pragma mark - 维修

/** 一级分类 **/
#define kMTRepairClassifyURL [NSString stringWithFormat:@"%@repairClassify/repairClassify/list", kServerUrl]
/** 维修订单 **/
#define kMTRepairOrderLIstURL [NSString stringWithFormat:@"%@repairOrder/repairOrder/list", kServerUrl]
/** 维修订单 - 预约维修 **/
#define kMTRepairAddURL [NSString stringWithFormat:@"%@repairOrder/repairOrder/add", kServerUrl]
/** 维修订单 - 保证金金额 **/
#define kMTRepairBailAmountURL [NSString stringWithFormat:@"%@repairOrder/repairOrder/bailAmount", kServerUrl]

#pragma mark - 生活预约
/** 一级分类 **/
#define kRWClassifyFirstListURL [NSString stringWithFormat:@"%@recovery/recoveryClassify/first/list", kServerUrl]
/** 二级分类 **/
#define kRWClassifyChildListURL [NSString stringWithFormat:@"%@recovery/recoveryClassify/child/list", kServerUrl]
/** 分类详情 **/
#define kRWClassifyDetailURL [NSString stringWithFormat:@"%@recovery/recoveryClassify/queryById", kServerUrl]

#pragma mark ===== 回收订单 =====
/** 预约回收 **/
#define kRWAddOrderURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/add", kServerUrl]
/** 订单列表 **/
#define kRWOrderListURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/list", kServerUrl]
/** 取消订单 **/
#define kRWCancelOrderURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/cancel", kServerUrl]
/** 删除订单 **/
#define kRWDeleteOrderURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/delete", kServerUrl]
/** 订单详情 **/
#define kRWOrderDetailURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/queryById", kServerUrl]
/** 预约时间段 **/
#define kRWReserveDateListURL [NSString stringWithFormat:@"%@dict/queryListByCode", kServerUrl]
/** 取消接单原因列表 **/
#define kRWCancelReasonURL [NSString stringWithFormat:@"%@recoveryOrderCancel/list", kServerUrl]
/** 评论 **/
#define kRWAddCommentURL [NSString stringWithFormat:@"%@recovery/recoveryOrderComments/add", kServerUrl]
/** 根据订单号获取师傅信息 **/
#define kRWMasterInfoURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/queryMasterMsg", kServerUrl]
/** 师傅的评价 **/
#define kRWMasterCommentListURL [NSString stringWithFormat:@"%@recovery/recoveryOrderComments/list", kServerUrl]

#pragma mark ===== 地址管理 =====
/** 新增地址 **/
#define kAddAddressURL [NSString stringWithFormat:@"%@user/userAddress/add", kServerUrl]
/** 编辑地址**/
#define kEditAddressURL [NSString stringWithFormat:@"%@user/userAddress/edit", kServerUrl]
/** 删除地址**/
#define kDeleteAddressURL [NSString stringWithFormat:@"%@user/userAddress/delete", kServerUrl]
/** 批量删除**/
#define kMultipleDeleteAddressURL [NSString stringWithFormat:@"%@user/userAddress/deleteBatch", kServerUrl]
/** 地址详情**/
#define kAddressDetailURL [NSString stringWithFormat:@"%@user/userAddress/queryById", kServerUrl]
/** 地列列表**/
#define kAddressListURL [NSString stringWithFormat:@"%@user/userAddress/list", kServerUrl]
/** 设置默认收货地址**/
#define kSetDefaultAddressURL [NSString stringWithFormat:@"%@user/userAddress/editDefaultAddress", kServerUrl]

#pragma mark - 人力市场
/** 搜索关键字列表 **/
#define kSearchKeywordsURL [NSString stringWithFormat:@"%@marketSearchKeywords/list", kServerUrl]
/** 工种选择 **/
#define kWorkTypeURL [NSString stringWithFormat:@"%@marketWorkType/list", kServerUrl]
/** 收藏或者取消收藏 **/
#define kMarketColletionURL [NSString stringWithFormat:@"%@marketColletion/collectionOrCancle", kServerUrl]
/** 我的收藏列表 **/
#define kMarketColletionListURL [NSString stringWithFormat:@"%@marketColletion/list", kServerUrl]
/** 批量删除我的收藏 **/
#define kDeleteMarketColletionURL [NSString stringWithFormat:@"%@marketColletion/deleteByIds", kServerUrl]
/** 首页上半部数据 **/
#define kMarketTopInfoURL [NSString stringWithFormat:@"%@marketWorkType/queryIndex", kServerUrl]
/** 随机获取一条广告 **/
#define kGetRandomAdURL [NSString stringWithFormat:@"%@advertisement/queryRandomAd", kServerUrl]

#pragma mark ===== 找工作 =====
/** 发布招工 **/
#define kPublishMarketJobURL [NSString stringWithFormat:@"%@marketJob/add", kServerUrl]
/** 我要置顶（急招） **/
#define kTopMyJobURL [NSString stringWithFormat:@"%@marketJob/topMyJob", kServerUrl]
/** 招工列表**/
#define kMarketJobListURL [NSString stringWithFormat:@"%@marketJob/list", kServerUrl]
/** 招工详情**/
#define kMarketJobDetailURL [NSString stringWithFormat:@"%@marketJob/queryById", kServerUrl]
/** 我的发布记录**/
#define kMarketJobMyReleaseURL [NSString stringWithFormat:@"%@marketJob/myReleaseList", kServerUrl]
/** 上架/下架**/
#define kMarketJobUpOrDownURL [NSString stringWithFormat:@"%@marketJob/upOrDown", kServerUrl]
/** 删除招工**/
#define kMarketJobDeleteURL [NSString stringWithFormat:@"%@marketJob/delete", kServerUrl]
/** 批量删除**/
#define kMarketJobDeleteBatchURL [NSString stringWithFormat:@"%@marketJob/deleteBatch", kServerUrl]
/** 修改**/
#define kMarketJobEditURL [NSString stringWithFormat:@"%@marketJob/edit", kServerUrl]

#pragma mark ===== 找工人 =====
/** 发布找活**/
#define kPublishMarketWorkerURL [NSString stringWithFormat:@"%@marketWorker/add", kServerUrl]
/** 我要置顶（急找）**/
#define kTopMyWorkerURL [NSString stringWithFormat:@"%@marketWorker/topMyJob", kServerUrl]
/** 找活列表**/
#define kMarketWorkerListURL [NSString stringWithFormat:@"%@marketWorker/list", kServerUrl]
/** 找活详情**/
#define kMarketWorkerDetailURL [NSString stringWithFormat:@"%@marketWorker/queryById", kServerUrl]
/** 我的发布记录**/
#define kMarketWorkerMyReleaseURL [NSString stringWithFormat:@"%@marketWorker/myReleaseList", kServerUrl]
/** 上架/下架**/
#define kMarketWorkerUpOrDownURL [NSString stringWithFormat:@"%@marketWorker/upOrDown", kServerUrl]
/** 删除**/
#define kMarketWorkerDeleteURL [NSString stringWithFormat:@"%@marketWorker/delete", kServerUrl]
/** 批量删除**/
#define kMarketWorkerDeleteBatchURL [NSString stringWithFormat:@"%@marketWorker/deleteBatch", kServerUrl]
/** 修改**/
#define kMarketWorkerEditURL [NSString stringWithFormat:@"%@marketWorker/edit", kServerUrl]

#pragma mark ===== 会员 =====
/** 会员类型**/
#define kMarketViplistURL [NSString stringWithFormat:@"%@vipType/marketViplist", kServerUrl]
/** 单独获取次卡价格**/
#define kMarketOneTimeCardPriceURL [NSString stringWithFormat:@"%@vipType/queryOneTimeCardPrice", kServerUrl]
/** 购买会员支付**/
#define kMarketBuyVipURL [NSString stringWithFormat:@"%@userVip/pay", kServerUrl]
/** 购买记录**/
#define kMarkePayLogURL [NSString stringWithFormat:@"%@userVip/list", kServerUrl]
/** 实名认证-个人**/
#define kAuthenticationPersonURL [NSString stringWithFormat:@"%@user/authenticationPerson", kServerUrl]
/** 实名认证-企业**/
#define kAuthenticationCompanyURL [NSString stringWithFormat:@"%@user/authenticationCompany", kServerUrl]
/** 内购**/
#define kInAppPurchaseURL [NSString stringWithFormat:@"%@vipRecharge/applePayUser", kServerUrl]




#pragma mark ===== 我的 =====
/** 获取用户信息 **/
#define kGetUserInfoURL [NSString stringWithFormat:@"%@user/queryUser", kServerUrl]
/** 修改用户信息 **/
#define kEditUserInfoURL [NSString stringWithFormat:@"%@user/modifyUser", kServerUrl]
/** 注销账号 **/
#define kDelUserURL [NSString stringWithFormat:@"%@user/delUser", kServerUrl]



#endif /* URLHeader_h */
