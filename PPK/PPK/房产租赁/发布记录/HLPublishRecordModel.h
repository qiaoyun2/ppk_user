//
//  HLPublishRecordModel.h
//  PPK
//
//  Created by null on 2022/6/12.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HLPublishRecordModel : BaseModel


@property (nonatomic, strong) NSString *adContent; //
@property (nonatomic, strong) NSString *adPicture; //
@property (nonatomic, strong) NSString *adTitle; //
@property (nonatomic, strong) NSString *adType; //
@property (nonatomic, strong) NSString *ID; //
@property (nonatomic, strong) NSString *latitude; //
@property (nonatomic, strong) NSString *longitude; //
@property (nonatomic, strong) NSString *picture; //
@property (nonatomic, strong) NSString *position; //
@property (nonatomic, strong) NSString *rent; //
@property (nonatomic, strong) NSString *rentSaleType; // 1租房 2买卖房
@property (nonatomic, strong) NSString *status; // 状态，0下架 1审核通过-上架 2审核中 3审核失败
@property (nonatomic, strong) NSString *title; //
@property (nonatomic, strong) NSString *type; // 租赁发布类型：1-有房；2-无房
@property (nonatomic, strong) NSString *typeName; //
@property (nonatomic, strong) NSString *url; //

@end

NS_ASSUME_NONNULL_END
