//
//  HLHasRoomPickerView.m
//  PPK
//
//  Created by null on 2022/5/26.
//

#import "HLHasRoomPickerView.h"

@implementation HLHasRoomPickerView

- (void)awakeFromNib
{
    [super awakeFromNib];
    CDKViewGradientTopToBottom(self.leftView, @[UIColorFromRGB(0x81D1F4),UIColorFromRGB(0x00A2EA)]);
    CDKViewGradientTopToBottom(self.rightView, @[UIColorFromRGB(0xFDC557),UIColorFromRGB(0xFE8F21)]);
}

//展示出现
-(void)showPopView{
    /*从中间出现的popview*/
    self.hidden = NO;
    CGAffineTransform transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0,1.0);
    self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.2,0.2);
    self.contentView.alpha = 0;
    [UIView animateWithDuration:0.3 delay:0.1 usingSpringWithDamping:0.5 initialSpringVelocity:10 options:UIViewAnimationOptionCurveLinear animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.4f];
        self.contentView.transform = transform;
        self.contentView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}

//隐藏出现
-(void)dismissPopView{
    [UIView animateWithDuration:0.3 animations:^{
        self.contentView.transform=CGAffineTransformMakeScale(0.02, 0.02);
    } completion:^(BOOL finished) {
        self.hidden = YES;
        [self removeFromSuperview];
        //        [UIView animateWithDuration:0.08 animations:^{
        //            self.LucyPopView.transform=CGAffineTransformMakeScale(0.25, 0.25);
        //        } completion:^(BOOL finished) {
        //            [self removeFromSuperview];
        //        }];
    }];
}


- (IBAction)onButtonsClick:(UIButton *)sender {
    [self dismissPopView];
    if (self.onSelected) {
        self.onSelected(sender.tag);
    }
}


- (IBAction)closeButtonAction:(id)sender {
    [self dismissPopView];
}

@end
