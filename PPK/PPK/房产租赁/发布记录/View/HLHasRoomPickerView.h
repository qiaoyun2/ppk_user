//
//  HLHasRoomPickerView.h
//  PPK
//
//  Created by null on 2022/5/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HLHasRoomPickerView : UIView

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIView *rightView;


@property (nonatomic, copy) void(^onSelected)(BOOL hasRoom);



//展示出现
-(void)showPopView;
//隐藏出现
-(void)dismissPopView;

@end

NS_ASSUME_NONNULL_END
