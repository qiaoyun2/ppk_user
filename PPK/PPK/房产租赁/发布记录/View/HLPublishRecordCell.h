//
//  HLPublishRecordCell.h
//  PPK
//
//  Created by null on 2022/5/26.
//

#import <UIKit/UIKit.h>
#import "HLPublishRecordModel.h"
#import "HLCollectionModel.h"



NS_ASSUME_NONNULL_BEGIN

@interface HLPublishRecordCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitLabel;
@property (weak, nonatomic) IBOutlet UIButton *tagButton;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;

@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewLeading;


@property (nonatomic, strong) HLPublishRecordModel *model;
@property (nonatomic, strong) HLCollectionModel *collectionModel;

@property (nonatomic, copy) void(^onSelectButtonClick)(BOOL selected);

@end

NS_ASSUME_NONNULL_END
