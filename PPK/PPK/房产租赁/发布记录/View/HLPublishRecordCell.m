//
//  HLPublishRecordCell.m
//  PPK
//
//  Created by null on 2022/5/26.
//

#import "HLPublishRecordCell.h"

@implementation HLPublishRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentViewLeading.constant = 16;
    self.selectButton.hidden = YES;
    self.statusButton.hidden = YES;
}

- (void)setModel:(HLPublishRecordModel *)model
{
    _model = model;
    NSArray *images = [model.picture componentsSeparatedByString:@","];
    if (images.count) {
        [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(images[0])] placeholderImage:DefaultImgWidth];
    }
    self.titleLabel.text = model.title;
    self.addressLabel.text = model.position;
    self.priceLabel.text = model.rent;
    self.unitLabel.text = model.rentSaleType.integerValue == 1 ? @"元/月":@"元/平米";
    [self.tagButton setTitle:model.typeName forState:UIControlStateNormal];
    
    self.statusButton.hidden = NO;
    NSString *str = @"";
    if (model.status.integerValue==0) {
        str = @"下架";
    }else if (model.status.integerValue==1) {
        str = @"审核通过";
    }else if (model.status.integerValue==2) {
        str = @"审核中";
    }else {
        str = @"审核失败";
    }
    [self.statusButton setTitle:str forState:UIControlStateNormal];
}

- (void)setCollectionModel:(HLCollectionModel *)collectionModel
{
    _collectionModel = collectionModel;
    if (collectionModel.isEdit) {
        self.contentViewLeading.constant = 50;
        self.selectButton.hidden = NO;
    }else {
        self.contentViewLeading.constant = 16;
        self.selectButton.hidden = YES;
    }
    self.selectButton.selected = collectionModel.isSelected;
    NSArray *images = [collectionModel.picture componentsSeparatedByString:@","];
    if (images.count) {
        [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(images[0])] placeholderImage:DefaultImgWidth];
    }
    self.titleLabel.text = collectionModel.title;
    self.addressLabel.text = collectionModel.position;
    self.priceLabel.text = collectionModel.rent;
    self.unitLabel.text = collectionModel.rentSaleType.integerValue == 1 ? @"元/月":@"元/平米";
    [self.tagButton setTitle:collectionModel.typeName forState:UIControlStateNormal];
}

- (IBAction)selectButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.onSelectButtonClick) {
        self.onSelectButtonClick(sender.selected);
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
