//
//  HLPublishRecordModel.m
//  PPK
//
//  Created by null on 2022/6/12.
//

#import "HLPublishRecordModel.h"

@implementation HLPublishRecordModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
}

@end
