//
//  HLPublishRecordController.m
//  PPK
//
//  Created by null on 2022/5/26.
//

#import "HLPublishRecordController.h"
#import "HLPublishRecordCell.h"
#import "HLHasRoomPickerView.h"
#import "HLPublishTypeController.h"
#import "HLUserModel.h"
#import "HLHouseDetailController.h"

@interface HLPublishRecordController () <UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) HLUserModel *userModel;
@property (nonatomic, assign) NSInteger page;

@end

@implementation HLPublishRecordController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self requestForUserInfo];
    [self refresh];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"发布记录";
    [self setNavigationRightBarButtonWithTitle:@"立即发布" color:MainColor];
    
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
}

- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(15);
    WeakSelf
    [NetworkingTool getWithUrl:kHLReleaseListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                HLPublishRecordModel *model = [[HLPublishRecordModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

/// 获取用户信息
- (void)requestForUserInfo
{
    [NetworkingTool getWithUrl:kHLUserInfoURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.userModel = [[HLUserModel alloc] initWithDictionary:dataDic];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HLPublishRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLPublishRecordCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HLPublishRecordCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    HLPublishRecordModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HLPublishRecordModel *model = self.dataArray[indexPath.row];
    HLHouseDetailController *vc = [[HLHouseDetailController alloc] init];
    vc.houseId = model.ID;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Function
- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender
{
    if (!self.userModel.ageRangeId.length || !self.userModel.starsId.length || !self.userModel.jobName.length) {
        [UIAlertController alertViewNormalWithTitle:@"提示" message:@"尊敬的用户发布找房需要先完善信息哦" titlesArry:@[@"去完善"] indexBlock:^(NSInteger index, id obj) {
            self.navigationController.tabBarController.selectedIndex = 3;
        } okColor:MainColor cancleColor:[UIColor lightGrayColor] isHaveCancel:YES];
        return;
    }
    
    HLHasRoomPickerView *view = [[[NSBundle mainBundle] loadNibNamed:@"HLHasRoomPickerView" owner:nil options:nil] firstObject];
    view.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].keyWindow addSubview:view];
    [view showPopView];
    WeakSelf
    [view setOnSelected:^(BOOL hasRoom) {
        HLPublishTypeController *vc = [[HLPublishTypeController alloc] init];
        vc.type = hasRoom ? 1:2;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
