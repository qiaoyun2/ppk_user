//
//  HLPublishTypeCell.m
//  PPK
//
//  Created by null on 2022/6/11.
//

#import "HLPublishTypeCell.h"

@implementation HLPublishTypeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(HLPublishTypeModel *)model
{
    [self.thumbImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.picture)] placeholderImage:DefaultImgWidth];
    self.titleLabel.text = model.name;
    self.subTitleLabel.text = model.descrip;
    self.selectButton.selected = model.selected;
}

- (IBAction)selectButtonAction:(UIButton *)sender {
//    sender.selected = !sender.selected;
//    if (self.onSelectButtonClick) {
//        self.onSelectButtonClick(sender.selected);
//    }
}


@end
