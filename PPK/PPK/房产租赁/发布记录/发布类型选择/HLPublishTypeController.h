//
//  HLPublishTypeController.h
//  PPK
//
//  Created by null on 2022/6/11.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HLPublishTypeController : BaseViewController

@property (nonatomic, assign) NSInteger type; // 1:有房 2:无房

@end

NS_ASSUME_NONNULL_END
