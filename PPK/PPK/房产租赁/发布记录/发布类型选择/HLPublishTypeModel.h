//
//  HLPublishTypeModel.h
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HLPublishTypeModel : BaseModel

@property (nonatomic, strong) NSString *ID; //
@property (nonatomic, strong) NSString *name; //标题
@property (nonatomic, strong) NSString *picture; // 图片
@property (nonatomic, strong) NSString *descrip; // 详情描述
@property (nonatomic, strong) NSString *rentSaleType; //1租房 2买卖房

@property (nonatomic, assign) BOOL selected; //

@end

NS_ASSUME_NONNULL_END
