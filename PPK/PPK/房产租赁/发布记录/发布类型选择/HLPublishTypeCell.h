//
//  HLPublishTypeCell.h
//  PPK
//
//  Created by null on 2022/6/11.
//

#import <UIKit/UIKit.h>
#import "HLPublishTypeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HLPublishTypeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@property (nonatomic, copy) void(^onSelectButtonClick)(BOOL selected);
@property (nonatomic, strong) HLPublishTypeModel *model;

@end

NS_ASSUME_NONNULL_END
