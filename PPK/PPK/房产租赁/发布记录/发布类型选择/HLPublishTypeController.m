//
//  HLPublishTypeController.m
//  PPK
//
//  Created by null on 2022/6/11.
//

#import "HLPublishTypeController.h"
#import "HLPublishTypeCell.h"
#import "HLPublishController.h"


@interface HLPublishTypeController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, assign) NSInteger index;

@end

@implementation HLPublishTypeController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"发布类型选择";
    self.index = 0;
    [self requestForDataList];
}

- (void)requestForDataList
{
    [NetworkingTool getWithUrl:kHLHouseTypeListURL params:@{@"type":@(self.type)} success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.dataArray removeAllObjects];
        if ([responseObject[@"code"] integerValue]==1) {
            NSArray *dataArray = responseObject[@"data"];
            for (NSDictionary *obj in dataArray) {
                HLPublishTypeModel *model = [[HLPublishTypeModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HLPublishTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HLPublishTypeCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HLPublishTypeCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    HLPublishTypeModel *model = self.dataArray[indexPath.row];
    model.selected = indexPath.row == self.index;
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.index = indexPath.row;
    [self.tableView reloadData];
}

- (IBAction)nextButtonAction:(UIButton *)sender {
    HLPublishTypeModel *model = self.dataArray[self.index];
    HLPublishController *vc = [[HLPublishController alloc] init];
    vc.typeModel = model;
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
