//
//  HLPublishTypeModel.m
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "HLPublishTypeModel.h"

@implementation HLPublishTypeModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"description"]) {
        self.descrip = value;
    }
    if ([key isEqual:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
}

@end
