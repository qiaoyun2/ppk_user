//
//  HLPublishController.m
//  PPK
//
//  Created by null on 2022/6/11.
//

#import "HLPublishController.h"
#import "HXPhotoView.h"
#import "HXAssetManager.h"
#import "SelectAddressViewController.h"
#import "UploadManager.h"
#import "ImageCell.h"

#import <HXPhotoPicker/HXPhotoPicker.h>

@interface HLPublishController ()<HXPhotoViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *titleField;
@property (weak, nonatomic) IBOutlet UITextField *moneyField;
@property (weak, nonatomic) IBOutlet UILabel *unitLabel;
@property (weak, nonatomic) IBOutlet UILabel *rentTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (weak, nonatomic) IBOutlet UIView *limiteView;
@property (weak, nonatomic) IBOutlet UILabel *limiteLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoViewHeight;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;


@property (nonatomic, strong) NSMutableArray<UIImage *> *photos;
@property (nonatomic, strong) NSMutableArray *imagePathArray;
@property (strong, nonatomic) HXPhotoManager *manager;


@property (nonatomic, assign) CGFloat latitude;
@property (nonatomic, assign) CGFloat longitude;
@property (nonatomic, strong) NSString *province;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *district;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *confine; // 限制0不限；1男；2女


@end

@implementation HLPublishController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = self.typeModel.name;
    
    self.photos = [NSMutableArray array];
    self.imagePathArray = [NSMutableArray array];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"ImageCell" bundle:nil] forCellWithReuseIdentifier:@"ImageCell"];

    
    //1租房 2买卖房
    if (self.typeModel.rentSaleType.integerValue==1) {
        self.limiteView.hidden = NO;
        self.rentTitleLabel.text = @"租      金";
        self.unitLabel.text = @"元/月";
    }else {
        self.limiteView.hidden = YES;
        self.rentTitleLabel.text = @"房      价";
        self.unitLabel.text = @"元/平米";
    }
    
    if (self.detailModel.ID.length) {
        NSLog(@"====");

        self.latitude = [self.detailModel.latitude floatValue];
        self.longitude = [self.detailModel.longitude floatValue];
        self.province = @"";
        self.city = @"";
        self.district = @"";
        self.address = self.detailModel.street;
        self.confine = self.detailModel.confine;
        self.titleField.text = self.detailModel.title;
        self.moneyField.text = self.detailModel.rent;
        self.addressLabel.text = self.detailModel.street;
        self.addressLabel.textColor = UIColorFromRGB(0x333333);
        if (self.detailModel.confine.length) {
            self.limiteLabel.textColor = UIColorFromRGB(0x333333);
            if (self.detailModel.confine.integerValue==0) {
                self.limiteLabel.text = @"不限";
            }else if (self.detailModel.confine.integerValue==1) {
                self.limiteLabel.text = @"男";
            }else {
                self.limiteLabel.text = @"女";
            }
        }
        self.textView.text = self.detailModel.descrip;
//        self.photos.
        if (self.detailModel.picture.length) {
            self.imagePathArray = [NSMutableArray arrayWithArray:[_detailModel.picture componentsSeparatedByString:@","]];
        }
    }
    [self.collectionView reloadData];
    [self updateCollectionViewHeight];
}

#pragma mark - Network
- (void)requestForSubmit
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (self.detailModel.ID.length) {
        params[@"id"] = self.detailModel.ID;
    }else {
        params[@"typeId"] = self.typeModel.ID;
    }
    params[@"title"] = self.titleField.text;
    params[@"rent"] = self.moneyField.text;
    
    params[@"position"] = [NSString stringWithFormat:@"%@%@%@",self.province,self.city,self.district];
    params[@"longitude"] = @(self.longitude); // 经度
    params[@"latitude"] = @(self.latitude); // 纬度
    params[@"province"] = self.province; // 省
    params[@"city"] = self.city; // 市
    params[@"area"] = self.district; // 区
    params[@"street"] = self.address; // 区

    params[@"description"] = self.textView.text;
    
    if (self.imagePathArray.count) {
        params[@"picture"] = [self.imagePathArray componentsJoinedByString:@","];
    }
    
    if (self.typeModel.rentSaleType.integerValue==1) {
        // 租房
        params[@"isSale"] = @"2";
        if (self.confine.length) {
            params[@"confine"] = self.confine;
        }
    }else {
        // 卖房
        params[@"isSale"] = @"1";
    }
    NSString *url = self.detailModel.ID.length ? kHLEditHouseLeaseURL : kHLAddHouseLeaseURL;
    [NetworkingTool postWithUrl:url params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSString *str = self.detailModel.ID.length ? @"编辑成功" : @"发布成功";
            [LJTools showText:str delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                if (self.onEditSuccess) {
//                    self.onEditSuccess();
//                }
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForUploadImages
{    
    WeakSelf
    [UploadManager uploadImageArray:self.photos block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
        NSArray *array = [imageUrl componentsSeparatedByString:@","];
        [weakSelf.imagePathArray addObjectsFromArray:array];
        [weakSelf.collectionView reloadData];
        [weakSelf updateCollectionViewHeight];
    }];
}



#pragma mark - HXPhotoViewDelegate
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {
//    if (videos.count>0) {
//        self.isVideo = YES;
//        self.videoModel = [videos firstObject];
//        [self.photos removeAllObjects];
//        [self.imagePathArray removeAllObjects];
//        [self.videoModel getImageWithSuccess:^(UIImage * _Nullable image, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
//            [self.photos addObject:image];
//        } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
//
//        }];
//        NSLog(@"%@",self.videoModel.videoURL);
//    }
//    else {
//        self.isVideo = NO;
//        self.videoModel = nil;
//        [self.photos removeAllObjects];
//        [self.imagePathArray removeAllObjects];
//        for (HXPhotoModel *model in photos) {
//            [self.photos addObject:[HXAssetManager originImageForAsset:model.asset]];
//        }
//    }
}

#pragma mark - UICollectionViewDelegate
// 拿到view变化的高度
- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame {
    self.photoViewHeight.constant = frame.size.height;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger max = 9;
    if (self.imagePathArray.count>=max) {
        return max;
    }
    return self.imagePathArray.count+1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCell" forIndexPath:indexPath];
    if (indexPath.row < self.imagePathArray.count) {
        cell.errorBtn.hidden = NO;
        [cell.thumbImageV sd_setImageWithURL:[NSURL URLWithString:kImageUrl(self.imagePathArray[indexPath.row])] placeholderImage:DefaultImgWidth];
        WeakSelf
        [cell setDeleteBlock:^{
            [weakSelf.imagePathArray removeObjectAtIndex:indexPath.row];
            [weakSelf.collectionView reloadData];
            [weakSelf updateCollectionViewHeight];
        }];
    }
    else {
        cell.errorBtn.hidden = YES;
        cell.thumbImageV.image = [UIImage imageNamed:@"组 50936"];
        return cell;
    }
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.imagePathArray.count) {
        [self.manager clearSelectedList];
        [self hx_presentSelectPhotoControllerWithManager:self.manager didDone:^(NSArray<HXPhotoModel *> * _Nullable allList, NSArray<HXPhotoModel *> * _Nullable photos, NSArray<HXPhotoModel *> * _Nullable videos, BOOL isOriginal, UIViewController * _Nullable viewController, HXPhotoManager * _Nullable manager) {
            [self.photos removeAllObjects];
            for (HXPhotoModel *model in photos) {
                [self.photos addObject:[HXAssetManager originImageForAsset:model.asset]];
            }
            [self requestForUploadImages];
        } cancel:^(UIViewController * _Nullable viewController, HXPhotoManager * _Nullable manager) {
            
        }];

    }else {
         /// 图片浏览
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH-88-18-20)/3, (SCREEN_WIDTH-88-18-20)/3-1);
}

#pragma mark - Function
- (void)updateCollectionViewHeight
{
    NSInteger max = 9;
    NSInteger num = self.imagePathArray.count == max ? self.imagePathArray.count : self.imagePathArray.count+1;
    NSInteger row = num%3 == 0 ? num/3 : num/3+1;
    self.photoViewHeight.constant = row*(SCREEN_WIDTH-88-18-20)/3 + (row-1)*10;
}

#pragma mark - XibFunction
- (IBAction)addressViewTap:(id)sender {
    [self.view endEditing:YES];
    WeakSelf
    SelectAddressViewController *vc = [[SelectAddressViewController alloc] init];
    if (self.longitude) {
        vc.coordinate = CLLocationCoordinate2DMake(self.latitude, self.longitude);
    }
    if (self.city.length) {
        vc.city = self.city;
    }
    vc.Block = ^(AMapPOI * _Nonnull model,NSString *detailAddress) {
        weakSelf.province = model.province;
        weakSelf.city = model.city;
        weakSelf.district = model.district;
        weakSelf.address = detailAddress;
        weakSelf.latitude = model.location.latitude;
        weakSelf.longitude = model.location.longitude;
        weakSelf.addressLabel.text = detailAddress;
        weakSelf.addressLabel.textColor = UIColorFromRGB(0x333333);
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)limiteViewTap:(id)sender {
    [self.view endEditing:YES];
    WeakSelf
    [ZJNormalPickerView zj_showStringPickerWithTitle:@"选择性别" dataSource:@[@"不限",@"男",@"女"] defaultSelValue:nil isAutoSelect:NO resultBlock:^(id selectValue, NSInteger index) {
        weakSelf.limiteLabel.text = selectValue;
        weakSelf.limiteLabel.textColor = UIColorFromRGB(0x333333);
        weakSelf.confine = [NSString stringWithFormat:@"%ld",index];
    } cancelBlock:^{
        
    }];
}

- (IBAction)publishButtonAction:(UIButton *)sender {
    if (!self.titleField.text.length) {
        [LJTools showText:@"请输入标题" delay:1.5];
        return;
    }
    if (!self.moneyField.text.length) {
        NSString *str = self.typeModel.rentSaleType.integerValue == 1 ? @"请输入租金":@"请输入房价";
        [LJTools showText:str delay:1.5];
        return;
    }

    [self requestForSubmit];

}
#pragma mark - Lazy Load
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _manager.configuration.type = HXConfigurationTypeWXChat;
        _manager.configuration.singleSelected = NO;
        _manager.configuration.lookGifPhoto = NO;
        _manager.configuration.useWxPhotoEdit = YES;
        _manager.configuration.selectTogether=NO;
        _manager.configuration.photoEditConfigur.onlyCliping = YES;
        _manager.configuration.showOriginalBytes = NO;
        _manager.configuration.videoMaxNum=1;
        _manager.configuration.photoMaxNum=9;
        _manager.configuration.requestImageAfterFinishingSelection = YES;
        _manager.configuration.photoEditConfigur.aspectRatio = HXPhotoEditAspectRatioType_None;
//
        _manager.configuration.saveSystemAblum = YES;
        
//        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
//        //        _manager.openCamera = NO;
//        _manager.configuration.saveSystemAblum = YES;
//        _manager.configuration.photoMaxNum = 9; //
//        _manager.configuration.videoMaxNum = 5;  //
//        _manager.configuration.maxNum = 14;

    }
    return _manager;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
