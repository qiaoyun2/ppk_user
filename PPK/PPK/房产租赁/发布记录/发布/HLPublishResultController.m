//
//  HLPublishResultController.m
//  PPK
//
//  Created by null on 2022/6/11.
//

#import "HLPublishResultController.h"
#import "HLHouseDetailController.h"

@interface HLPublishResultController ()

@end

@implementation HLPublishResultController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"发布结果";
}

- (IBAction)lookButtonAction:(id)sender {
    HLHouseDetailController *vc = [[HLHouseDetailController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
