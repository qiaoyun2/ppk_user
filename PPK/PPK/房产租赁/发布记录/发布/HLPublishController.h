//
//  HLPublishController.h
//  PPK
//
//  Created by null on 2022/6/11.
//

#import "BaseViewController.h"
#import "HLPublishTypeModel.h"
#import "HouseDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HLPublishController : BaseViewController

@property (nonatomic, strong) HLPublishTypeModel *typeModel;

@property (nonatomic, strong) HouseDetailModel *detailModel;

@end

NS_ASSUME_NONNULL_END
