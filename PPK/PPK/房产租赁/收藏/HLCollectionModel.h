//
//  HLCollectionModel.h
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HLCollectionModel : BaseModel

@property (nonatomic, strong) NSString *favoritesId; // 收藏id
@property (nonatomic, strong) NSString *title; //
@property (nonatomic, strong) NSString *picture; //
@property (nonatomic, strong) NSString *rent; //
@property (nonatomic, strong) NSString *position; //
@property (nonatomic, strong) NSString *typeName; //
@property (nonatomic, strong) NSString *ID; // 房屋租赁表id
@property (nonatomic, strong) NSString *rentSaleType; // 1租房 2买卖房

@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL isEdit;

@end

NS_ASSUME_NONNULL_END
