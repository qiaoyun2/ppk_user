//
//  CityViewController.h
//  wonderfulgoods
//
//  Created by null on 2020/1/9.
//  Copyright © 2020 null. All rights reserved.
//

#import "BaseViewController.h"
@class SDCityModel;
NS_ASSUME_NONNULL_BEGIN

@interface CityViewController : BaseViewController
@property (nonatomic,copy)void (^cityPickerBlock)(SDCityModel *city);
@property (nonatomic,strong)NSMutableArray *dataArr;
@end

NS_ASSUME_NONNULL_END
