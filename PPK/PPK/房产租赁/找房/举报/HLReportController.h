//
//  HLReportController.h
//  PPK
//
//  Created by null on 2022/6/12.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HLReportController : BaseViewController

@property (nonatomic, strong) NSString *houseId;
@property (nonatomic, strong) NSString *userId;

@end

NS_ASSUME_NONNULL_END
