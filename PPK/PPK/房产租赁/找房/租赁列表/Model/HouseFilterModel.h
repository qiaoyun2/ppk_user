//
//  HouseFilterModel.h
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HouseFilterRentModel : BaseModel

@property (nonatomic, strong) NSString *highPrice;
@property (nonatomic, strong) NSString *lowPrice;
@property (nonatomic, strong) NSString *rentName;

@end

@interface HouseFilterTagModel : BaseModel

@property (nonatomic, strong) NSString *searchName;
@property (nonatomic, strong) NSString *showName;

@end

@interface HouseFilterTypeModel : BaseModel

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *searchName;

@end


@interface HouseFilterModel : BaseModel


@property (nonatomic, strong) NSArray *rentModels;
@property (nonatomic, strong) NSArray *tagModels;
@property (nonatomic, strong) NSArray *typeModels;



@end

NS_ASSUME_NONNULL_END
