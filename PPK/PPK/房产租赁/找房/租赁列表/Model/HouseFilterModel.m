//
//  HouseFilterModel.m
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "HouseFilterModel.h"

@implementation HouseFilterRentModel

@end

@implementation HouseFilterTagModel

@end

@implementation HouseFilterTypeModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
}
@end

@implementation HouseFilterModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"houseRentConfigVOList"]) {
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *obj in value) {
            HouseFilterRentModel *model = [[HouseFilterRentModel alloc] initWithDictionary:obj];
            [tempArray addObject:model];
        }
        self.rentModels = [NSArray arrayWithArray:tempArray];
    }
    
    if ([key isEqual:@"houseTagVOList"]) {
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *obj in value) {
            HouseFilterTagModel *model = [[HouseFilterTagModel alloc] initWithDictionary:obj];
            [tempArray addObject:model];
        }
        self.tagModels = [NSArray arrayWithArray:tempArray];
    }
    
    if ([key isEqual:@"houseTypeVOList"]) {
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *obj in value) {
            HouseFilterTypeModel *model = [[HouseFilterTypeModel alloc] initWithDictionary:obj];
            [tempArray addObject:model];
        }
        self.typeModels = [NSArray arrayWithArray:tempArray];
    }
}

@end
