//
//  HouseListModel.h
//  PPK
//
//  Created by null on 2022/6/12.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HouseListModel : BaseModel

@property (nonatomic, strong) NSString *avatar; //
@property (nonatomic, strong) NSString *createTime; // 发布时间
@property (nonatomic, strong) NSString *distance; // 距离
@property (nonatomic, strong) NSString *nickName; // 昵称
@property (nonatomic, strong) NSString *position; // 位置
@property (nonatomic, strong) NSString *picture; //
@property (nonatomic, strong) NSString *rent; // 租金
@property (nonatomic, strong) NSString *title; // 标题
@property (nonatomic, strong) NSString *typeName; // 类型名称
@property (nonatomic, strong) NSString *adTitle; //
@property (nonatomic, strong) NSString *adContent; //
@property (nonatomic, strong) NSString *adPicture; //
@property (nonatomic, strong) NSString *adType; //
@property (nonatomic, strong) NSString *url; //
@property (nonatomic, strong) NSString *rentSaleType; // 1租房 2买卖房
@property (nonatomic, strong) NSString *descrip; // 
@property (nonatomic, strong) NSString *ID; //

@property (nonatomic, strong) NSString *searchStr; //


@end

NS_ASSUME_NONNULL_END
