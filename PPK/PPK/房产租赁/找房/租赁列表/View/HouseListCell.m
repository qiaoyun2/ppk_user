//
//  HouseListCell.m
//  PPK
//
//  Created by null on 2022/6/11.
//

#import "HouseListCell.h"

@implementation HouseListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(HouseListModel *)model
{
    _model = model;
    
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.avatar)] placeholderImage:DefaultImgWidth];
    self.nameLabel.text = model.nickName;
    self.timeLabel.text = model.createTime;
    self.titleLabel.text = model.title;
    self.contentLabel.text = model.descrip;
    [self.typeButton setTitle:model.typeName forState:UIControlStateNormal];
    if (model.rentSaleType.integerValue==1) {
        self.moneyLabel.text = [NSString stringWithFormat:@"租金 %@元/月",model.rent];
    }else {
        self.moneyLabel.text = [NSString stringWithFormat:@"房价 %@元/平",model.rent];
    }
    self.addressLabel.text = model.position;
    self.distanceLabel.text = [NSString stringWithFormat:@"%@km",model.distance];
    if (model.picture.length>0) {
        self.imgsView.hidden = NO;
        NSArray *images = [model.picture componentsSeparatedByString:@","];
        self.imgsView.imageArray= images;
    }else {
        self.imgsView.hidden = YES;
    }
    
    if (model.searchStr.length>0) {
        if ([model.title containsString:model.searchStr]) {
            NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:model.title];
            NSRange range = [model.title rangeOfString:model.searchStr];
            // 改变文字颜色
            [noteStr addAttribute:NSForegroundColorAttributeName value:MainColor range:range];
            // 为label添加Attributed
            [self.titleLabel setAttributedText:noteStr];
        }
        if ([model.descrip containsString:model.searchStr]) {
            NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:model.descrip];
            NSRange range = [model.descrip rangeOfString:model.searchStr];
            // 改变文字颜色
            [noteStr addAttribute:NSForegroundColorAttributeName value:MainColor range:range];
            // 为label添加Attributed
            [self.contentLabel setAttributedText:noteStr];
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
