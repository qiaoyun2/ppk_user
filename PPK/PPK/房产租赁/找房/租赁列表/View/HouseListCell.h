//
//  HouseListCell.h
//  PPK
//
//  Created by null on 2022/6/11.
//

#import <UIKit/UIKit.h>
#import "FoundListImage9TypographyView.h"
#import "HouseListModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface HouseListCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *typeButton;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;
@property (nonatomic, strong) HouseListModel *model;

@end

NS_ASSUME_NONNULL_END
