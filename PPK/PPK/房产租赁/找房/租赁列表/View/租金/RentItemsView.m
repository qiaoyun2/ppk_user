//
//  RentItemsView.m
//  PPK
//
//  Created by null on 2022/6/12.
//

#import "RentItemsView.h"
#import <Masonry.h>

@interface RentItemsView ()

@property (nonatomic, assign) CGFloat viewHeight;
@property (nonatomic, assign) UIButton *lastButton;

@end

@implementation RentItemsView

// 1、代码加载方式
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.colSpace = 12;
    }
    return self;
}


// 2.xib 加载方式
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if(self = [super initWithCoder:aDecoder]){
        self.colSpace = 12;
    }
    return self;
}

- (void)setDataArray:(NSArray *)dataArray
{
    _dataArray = dataArray;
    [self relaodSubViews];
}

- (void)relaodSubViews
{
    [self removeAllSubviews];
    
//    CGFloat left = 16;
    CGFloat x = 16;
    CGFloat y = 0;
    
    CGFloat lineSpace = 10;
    CGFloat colSpace = self.colSpace;
    CGFloat height = 17;

    for (int i = 0; i<self.dataArray.count; i++) {
        CGFloat width  = [[NSString stringWithFormat:@"%@",self.dataArray[i]] commonStringWidthForFont:14]+1;
        if (width > self.bounds.size.width) {
            width = self.bounds.size.width;
        }
        if (x + width +16 > (self.bounds.size.width)) {
            x = 16;
            y += height + lineSpace;
        }
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
        [button setTitleColor:MainColor forState:UIControlStateSelected];
        [button setTitle:self.dataArray[i] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        button.backgroundColor = [UIColor whiteColor];
        button.tag = i;
        [button addTarget:self action:@selector(buttonsAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
        if (self.defaultSelect) {
            if (i==0) {
                button.selected = YES;
                self.lastButton = button;
            }
        }

        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(y);
            make.left.mas_equalTo(x);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            if (i==self.dataArray.count-1) {
                make.bottom.equalTo(self);
            }
        }];
//        label.frame = CGRectMake(x, y, width, height);
//        NSLog(@"------%@------",NSStringFromCGRect(button.frame));
        x += width + colSpace;
    }
    self.viewHeight = y + height;
    NSLog(@"%f-----------------",self.viewHeight);
}

- (void)buttonsAction:(UIButton *)sender
{
    self.lastButton.selected = NO;
    sender.selected = YES;
    self.lastButton = sender;
    if (self.onButtonsClick) {
        self.onButtonsClick(sender.tag, self.dataArray[sender.tag]);
    }
}

- (CGFloat)getViewHeight
{
    return self.viewHeight;
}


@end
