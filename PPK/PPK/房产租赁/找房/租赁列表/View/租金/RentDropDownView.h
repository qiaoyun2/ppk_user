//
//  RentDropDownView.h
//  PPK
//
//  Created by null on 2022/6/12.
//

#import <UIKit/UIKit.h>
#import "HouseFilterModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RentDropDownView : UIView

@property (nonatomic, copy) void(^didSelectBlock)(NSString *min, NSString *max);/**<选择的返回类型回调 */
@property (nonatomic, copy) void(^hiddenBlock)(void);

@property (nonatomic, strong) HouseFilterModel *model;

@property (nonatomic, assign) BOOL hiddenTabbar;

- (void)showDropDown; // 显示下拉菜单
- (void)hideDropDown; // 隐藏下拉菜单


@end

NS_ASSUME_NONNULL_END
