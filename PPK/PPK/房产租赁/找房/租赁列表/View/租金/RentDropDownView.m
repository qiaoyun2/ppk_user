//
//  RentDropDownView.m
//  PPK
//
//  Created by null on 2022/6/12.
//

#import "RentDropDownView.h"
#import "RentItemsView.h"

@interface RentDropDownView ()<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *minField;
@property (weak, nonatomic) IBOutlet UITextField *maxField;
@property (weak, nonatomic) IBOutlet RentItemsView *rentItemView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTop;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, assign) CGFloat contentViewHeight;

@property (nonatomic, strong) NSString *min;
@property (nonatomic, strong) NSString *max;

@property (nonatomic, strong) NSArray *dataArray;

@end

@implementation RentDropDownView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.clipsToBounds = YES;

    CGFloat top = NavAndStatusHight + 43;
    self.contentViewHeight = SCREEN_HEIGHT - top - TAB_BAR_HEIGHT;
    self.frame = CGRectMake(0, top, SCREEN_WIDTH, self.contentViewHeight);
    self.contentViewTop.constant = - self.contentViewHeight;
    
    self.min = @"";
    self.max = @"";
    
    self.rentItemView.width = SCREEN_WIDTH;
    self.rentItemView.defaultSelect = NO;
    self.rentItemView.colSpace = 40;
    WeakSelf
    [self.rentItemView setOnButtonsClick:^(NSInteger tag, NSString * _Nonnull selectedStr) {
        HouseFilterRentModel *model = weakSelf.dataArray[tag];
        self.minField.text = model.lowPrice;
        self.maxField.text = model.highPrice;
    }];

//    [self addTapGestureRecognizerToSelf];
}

- (void)setModel:(HouseFilterModel *)model
{
    _model = model;
    if (!self.dataArray.count) {
        self.dataArray = [NSArray arrayWithArray:model.rentModels];
        NSMutableArray *titles = [NSMutableArray array];
        for (HouseFilterRentModel *model in self.dataArray) {
            [titles addObject:model.rentName];
        }
        self.rentItemView.dataArray = titles;
    }
}

- (void)setHiddenTabbar:(BOOL)hiddenTabbar
{
    _hiddenTabbar = hiddenTabbar;
    CGFloat top = NavAndStatusHight + 43;
    if (hiddenTabbar) {
        self.contentViewHeight = SCREEN_HEIGHT - top;
    }else {
        self.contentViewHeight = SCREEN_HEIGHT - top - TAB_BAR_HEIGHT;
    }
    self.frame = CGRectMake(0, top, SCREEN_WIDTH, self.contentViewHeight);
    self.contentViewTop.constant = - self.contentViewHeight;
}

#pragma mark - UIGestureRecognizerDelegate
/*
 解决didSelectRowAtIndexPath不能响应事件：
 UITapGestureRecognizer吞掉了touch事件，导致didSelectRowAtIndexPath方法无法响应。
 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch  {
    // 输出点击的view的类名
    NSLog(@"%@", NSStringFromClass([touch.view class]));
    // 若为UITableViewCellContentView（即点击了tableViewCell），则不截获Touch事件
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    return  YES;
}


#pragma mark - Function
//添加手势
- (void)addTapGestureRecognizerToSelf {
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapMaskView:)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
}

- (void)tapMaskView: (UITapGestureRecognizer *)tap {
    NSLog(@"收起");
    [self hideDropDown];
}

- (void)showDropDown {
    self.backgroundColor = [UIColor clearColor];
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        self.contentViewTop.constant = 0;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

- (void)hideDropDown {
    if (self.hiddenBlock) {
        self.hiddenBlock();
    }
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [UIColor clearColor];
        self.contentViewTop.constant = -self.contentViewHeight;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}


#pragma mark - XibFunction
- (IBAction)confirmButtonAction:(id)sender {
    [self hideDropDown];
    if (self.didSelectBlock) {
        self.didSelectBlock(self.minField.text,self.maxField.text);
    }
}



@end
