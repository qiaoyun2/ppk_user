//
//  RentItemsView.h
//  PPK
//
//  Created by null on 2022/6/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RentItemsView : UIView

@property (nonatomic, strong) NSArray *dataArray;
/** 列间距 */
@property (nonatomic, assign) CGFloat colSpace;
@property (nonatomic, assign) BOOL defaultSelect;

@property (nonatomic, copy) void(^onButtonsClick)(NSInteger tag, NSString *selectedStr);


- (CGFloat)getViewHeight;

@end

NS_ASSUME_NONNULL_END
