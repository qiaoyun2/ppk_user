//
//  FilterDropDownView.m
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "FilterDropDownView.h"
#import "RentItemsView.h"

@interface FilterDropDownView ()<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet RentItemsView *sexItemView;
@property (weak, nonatomic) IBOutlet RentItemsView *typeItemView;
@property (weak, nonatomic) IBOutlet RentItemsView *tagItemView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTop;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, assign) CGFloat contentViewHeight;

@property (nonatomic, strong) NSString *sex;
@property (nonatomic, strong) NSString *typeId;
@property (nonatomic, strong) NSString *tagSearchName;

@property (nonatomic, strong) NSArray *tagArray;
@property (nonatomic, strong) NSArray *typeArray;

@end

@implementation FilterDropDownView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.clipsToBounds = YES;

    CGFloat top = NavAndStatusHight + 43;
    self.contentViewHeight = SCREEN_HEIGHT - top - TAB_BAR_HEIGHT;
    self.frame = CGRectMake(0, top, SCREEN_WIDTH, self.contentViewHeight);
    self.contentViewTop.constant = - self.contentViewHeight;
    
    WeakSelf
    self.sexItemView.width = SCREEN_WIDTH;
    self.sexItemView.defaultSelect = YES;
    self.sexItemView.colSpace = 24;
    self.sexItemView.dataArray = @[@"不限",@"限男性",@"限女性"];
    [self.sexItemView setOnButtonsClick:^(NSInteger tag, NSString * _Nonnull selectedStr) {
        weakSelf.sex = [NSString stringWithFormat:@"%ld",tag];
    }];
    
    
    self.typeItemView.width = SCREEN_WIDTH;
    self.typeItemView.defaultSelect = NO;
    self.typeItemView.colSpace = 24;
    [self.typeItemView setOnButtonsClick:^(NSInteger tag, NSString * _Nonnull selectedStr) {
        HouseFilterTypeModel *model = self.typeArray[tag];
        weakSelf.typeId = model.ID;
    }];
    
    self.tagItemView.width = SCREEN_WIDTH;
    self.tagItemView.defaultSelect = NO;
    self.tagItemView.colSpace = 24;
    [self.tagItemView setOnButtonsClick:^(NSInteger tag, NSString * _Nonnull selectedStr) {
        HouseFilterTagModel *model = self.tagArray[tag];
        weakSelf.tagSearchName = model.searchName;
    }];

//    [self addTapGestureRecognizerToSelf];
}

- (void)setModel:(HouseFilterModel *)model
{
    _model = model;
    if (!self.tagArray) {
        self.tagArray = [NSArray arrayWithArray:model.tagModels];
        NSMutableArray *titles = [NSMutableArray array];
        for (HouseFilterTagModel *model in self.tagArray) {
            [titles addObject:model.showName];
        }
        self.tagItemView.dataArray = titles;
    }
    if (!self.typeArray) {
        self.typeArray = [NSArray arrayWithArray:model.typeModels];
        NSMutableArray *titles = [NSMutableArray array];
        for (HouseFilterTypeModel *model in self.typeArray) {
            [titles addObject:model.searchName];
        }
        self.typeItemView.dataArray = titles;
    }
}

- (void)setHiddenTabbar:(BOOL)hiddenTabbar
{
    _hiddenTabbar = hiddenTabbar;
    CGFloat top = NavAndStatusHight + 43;
    if (hiddenTabbar) {
        self.contentViewHeight = SCREEN_HEIGHT - top;
    }else {
        self.contentViewHeight = SCREEN_HEIGHT - top - TAB_BAR_HEIGHT;
    }
    self.frame = CGRectMake(0, top, SCREEN_WIDTH, self.contentViewHeight);
    self.contentViewTop.constant = - self.contentViewHeight;
}

#pragma mark - UIGestureRecognizerDelegate
/*
 解决didSelectRowAtIndexPath不能响应事件：
 UITapGestureRecognizer吞掉了touch事件，导致didSelectRowAtIndexPath方法无法响应。
 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch  {
    // 输出点击的view的类名
    NSLog(@"%@", NSStringFromClass([touch.view class]));
    // 若为UITableViewCellContentView（即点击了tableViewCell），则不截获Touch事件
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    return  YES;
}


#pragma mark - Function
//添加手势
- (void)addTapGestureRecognizerToSelf {
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapMaskView:)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
}

- (void)tapMaskView: (UITapGestureRecognizer *)tap {
    NSLog(@"收起");
    [self hideDropDown];
}

- (void)showDropDown {
    self.backgroundColor = [UIColor clearColor];
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        self.contentViewTop.constant = 0;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

- (void)hideDropDown {
    if (self.hiddenBlock) {
        self.hiddenBlock();
    }
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [UIColor clearColor];
        self.contentViewTop.constant = -self.contentViewHeight;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}


#pragma mark - XibFunction
- (IBAction)confirmButtonAction:(id)sender {
    [self hideDropDown];
    if (self.didSelectBlock) {
        self.didSelectBlock(self.sex,self.typeId,self.tagSearchName);
    }
}

@end
