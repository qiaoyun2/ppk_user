//
//  HLHomeViewController.m
//  PPK
//
//  Created by null on 2022/6/12.
//

#import "HLHomeViewController.h"
#import "ReLayoutButton.h"
#import "HouseListCell.h"
#import "CityDropDownView.h"
#import "SortDownView.h"
#import "RentDropDownView.h"
#import "FilterDropDownView.h"
#import "LMAdvertiseCell.h"
#import "HouseFilterModel.h"
#import "HLHouseDetailController.h"
#import "CityViewController.h"
#import "SDCityModel.h"
#import "LMSearchViewController.h"

@interface HLHomeViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UIButton *houseTitleButton;
@property (weak, nonatomic) IBOutlet UIButton *peopleTitleButton;
@property (weak, nonatomic) IBOutlet UIImageView *line1;
@property (weak, nonatomic) IBOutlet UIImageView *line2;
@property (weak, nonatomic) IBOutlet ReLayoutButton *cityButton;
@property (weak, nonatomic) IBOutlet ReLayoutButton *rentButton;
@property (weak, nonatomic) IBOutlet ReLayoutButton *filterButton;
@property (weak, nonatomic) IBOutlet ReLayoutButton *sortButton;

@property (nonatomic, strong) CityDropDownView *cityView;
@property (nonatomic, strong) SortDownView *sortView;
@property (nonatomic, strong) RentDropDownView *rentView;
@property (nonatomic, strong) FilterDropDownView *filterView;

@property (nonatomic, strong) NSString *sortType; // 1:最新排序；2:距离排序；3：实名认证；4：企业认证
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *district;
@property (nonatomic, strong) NSString *min;
@property (nonatomic, strong) NSString *max;
@property (nonatomic, strong) NSString *sex; // 性别限值
@property (nonatomic, strong) NSString *typeId; // 租赁发布类型id
@property (nonatomic, strong) NSString *tagSearchName; // 标签


@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger type; // 1-找房；2-找人

@property (nonatomic, strong) HouseFilterModel *filterModel;

@end

@implementation HLHomeViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.topViewHeight.constant = NavAndStatusHight;
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 153);
    self.titleLabel.text = [LJTools getAppDelegate].city;
    self.type = 1;
    self.sex = @"0";
    self.city = [LJTools getAppDelegate].city;
    self.district = @"";
    self.sortType = @"1";
    self.typeId = @"";
    self.tagSearchName = @"";
    self.min = @"";
    self.max = @"";

    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationChangeSuccess:) name:LocationChangeSuccess object:nil];
    [self refresh];
    [self requestForFilterItem];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Network
- (void)requestForFilterItem
{
    [NetworkingTool getWithUrl:kHLFilterInfoURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.filterModel = [[HouseFilterModel alloc] initWithDictionary:dataDic];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

- (void)refresh {
    _page = 1;
    if (!self.city.length) {
        return;
    }
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(15);
    params[@"city"] = self.city;
    params[@"area"] = self.district;
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度
    params[@"sort"] = self.sortType; // 1:最新排序；2:距离排序；3:实名认证；4:企业认证
    params[@"type"] = @(self.type);
    
    if (self.searchField.text.length) {
        params[@"keywords"] = self.searchField.text;
    }
    if (self.sex.length) {
        params[@"confine"] = self.sex;
    }
    if (self.typeId.length) {
        params[@"typeId"] = self.typeId;
    }
    if (self.tagSearchName.length) {
        params[@"tagSearchName"] = self.tagSearchName;
    }
    if (self.min.length) {
        params[@"lowPrice"] = self.min;
        params[@"highPrice"] = self.max;
    }
    
    WeakSelf
    [NetworkingTool getWithUrl:kHLHouseLeaseListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                HouseListModel *model = [[HouseListModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView withY:200];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}


#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HouseListModel *model = self.dataArray[indexPath.row];
    if (model.adPicture.length>0) {
        LMAdvertiseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LMAdvertiseCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"LMAdvertiseCell" owner:nil options:nil] firstObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [cell.adImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.adPicture)] placeholderImage:DefaultImgWidth];
        return cell;
    }
    
    HouseListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HouseListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HouseListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    HouseListModel *model = self.dataArray[indexPath.row];
    HLHouseDetailController *vc = [[HLHouseDetailController alloc] init];
    vc.houseId = model.ID;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 153;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self refresh];
    return YES;
}

#pragma mark - Function
// 将所有弹窗消失
- (void)hiddenAllDropDownView
{
    [self.cityView hideDropDown];
    [self.rentView hideDropDown];
    [self.filterView hideDropDown];
    [self.sortView hideDropDown];
    
    self.cityButton.selected = NO;
    self.rentButton.selected = NO;
    self.filterButton.selected = NO;
    self.sortButton.selected = NO;
}

// 选择城市
- (void)showCityDropDownView
{
    if (!self.cityView) {
        self.cityView = [[[NSBundle mainBundle] loadNibNamed:@"CityDropDownView" owner:nil options:nil] firstObject];
        [self.view addSubview:self.cityView];
    }
    self.cityView.isShowArea = YES;
    [self.cityView showDropDown];
    WeakSelf
    [self.cityView setDidSelectBlock:^(NSString * _Nonnull city, NSString * _Nonnull district) {
        NSString *str = district.length>0?district:city;
        [weakSelf.cityButton setTitle:str forState:UIControlStateNormal];
        weakSelf.city = city;
        weakSelf.district = district;
        weakSelf.titleLabel.text = str;
        [weakSelf refresh];
    }];
    [self.cityView setHiddenBlock:^{
        weakSelf.cityButton.selected = NO;
    }];
}

// 租金
- (void)showRentDropDownView
{
    if (!self.rentView) {
        self.rentView = [[[NSBundle mainBundle] loadNibNamed:@"RentDropDownView" owner:nil options:nil] firstObject];
        [self.view addSubview:self.rentView];
    }
    self.rentView.model = self.filterModel;
    [self.rentView showDropDown];
    WeakSelf
    [self.rentView setDidSelectBlock:^(NSString * _Nonnull min, NSString * _Nonnull max) {
        weakSelf.min = min;
        weakSelf.max = max;
        [weakSelf refresh];
    }];
    [self.rentView setHiddenBlock:^{
        weakSelf.rentButton.selected = NO;
    }];
}

// 筛选
- (void)showFilterDropDownView
{
    if (!self.filterView) {
        self.filterView = [[[NSBundle mainBundle] loadNibNamed:@"FilterDropDownView" owner:nil options:nil] firstObject];
        [self.view addSubview:self.filterView];
    }
    self.filterView.model = self.filterModel;
    [self.filterView showDropDown];
    WeakSelf
    [self.filterView setDidSelectBlock:^(NSString * _Nonnull sex, NSString * _Nonnull typeId, NSString * _Nonnull tagSearchName) {
        weakSelf.sex = sex;
        weakSelf.typeId = typeId;
        weakSelf.tagSearchName = tagSearchName;
        [weakSelf refresh];
    }];
    [self.filterView setHiddenBlock:^{
        weakSelf.filterButton.selected = NO;
    }];
}


// 排序
- (void)showSortDropDownView
{
    if (!self.sortView) {
        self.sortView = [[[NSBundle mainBundle] loadNibNamed:@"SortDownView" owner:nil options:nil] firstObject];
        [self.view addSubview:self.sortView];
    }

    [self.sortView showDropDown];
    WeakSelf
    [self.sortView setDidSelectBlock:^(NSInteger index, NSString * _Nonnull selectedStr) {
        [weakSelf.sortButton setTitle:selectedStr forState:UIControlStateNormal];
        weakSelf.sortType = [NSString stringWithFormat:@"%ld",index+1];
        [weakSelf refresh];
    }];
    [self.sortView setHiddenBlock:^{
        weakSelf.sortButton.selected = NO;
    }];
}

- (void)locationChangeSuccess:(NSNotification *)noti
{
    self.city = [LJTools getAppDelegate].city;
    [self.cityButton setTitle:self.city forState:UIControlStateNormal];
    self.titleLabel.text = self.city;
    [self refresh];
}

#pragma mark - XibFunction
- (IBAction)backButtonAction:(UIButton *)sender {
    [self.tabBarController.navigationController popViewControllerAnimated:YES];
}


- (IBAction)titleViewTap:(id)sender {
    CityViewController *vc = [[CityViewController alloc] init];
    WeakSelf
    vc.cityPickerBlock = ^(SDCityModel * _Nonnull cityModel) {
        weakSelf.city = cityModel.name;
        weakSelf.titleLabel.text = cityModel.name;
        [weakSelf.cityButton setTitle:cityModel.name forState:UIControlStateNormal];
        [weakSelf refresh];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)houseTitleButtonAction:(UIButton *)sender {
    sender.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    self.peopleTitleButton.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
    self.line1.hidden = NO;
    self.line2.hidden = YES;
    self.type = 1;
    [self refresh];
}

- (IBAction)peopleTitleButtonAction:(UIButton *)sender {
    sender.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    self.houseTitleButton.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
    self.line1.hidden = YES;
    self.line2.hidden = NO;
    self.type = 2;
    [self refresh];
}

- (IBAction)sortButtonsAction:(UIButton *)sender {
    [self.view endEditing:YES];
    if (!sender.selected) {
        [self hiddenAllDropDownView];
        CGFloat time = 0.0;
        if (self.tableView.contentOffset.y<110) {
            [self.tableView setContentOffset:CGPointMake(0,110) animated:YES];
            time = 0.25;
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(time * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (sender.tag==0) {
                [self showCityDropDownView];
            }
            else if (sender.tag==1) {
                [self showRentDropDownView];
            }
            else if (sender.tag==2) {
                [self showFilterDropDownView];
            }
            else {
                [self showSortDropDownView];
            }
        });
        sender.selected = !sender.selected;
    }
    else {
        [self hiddenAllDropDownView];
    }
}

- (IBAction)topButtonAction:(UIButton *)sender {
    [self.tableView setContentOffset:CGPointMake(0,0) animated:YES];
}

- (IBAction)searchButtonAction:(id)sender {
    LMSearchViewController *vc = [[LMSearchViewController alloc] init];
    vc.city = self.city;
    vc.type = [NSString stringWithFormat:@"%ld",self.type+10];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
