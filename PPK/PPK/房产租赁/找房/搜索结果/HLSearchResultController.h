//
//  HLSearchResultController.h
//  PPK
//
//  Created by null on 2022/6/29.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HLSearchResultController : BaseViewController

@property (nonatomic, copy) NSString *city; 
@property (nonatomic, copy) NSString *type; // 10房源  11寻房
@property (nonatomic, copy) NSString *searchStr;

@end

NS_ASSUME_NONNULL_END
