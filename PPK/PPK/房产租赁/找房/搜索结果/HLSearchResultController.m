//
//  HLSearchResultController.m
//  PPK
//
//  Created by null on 2022/6/29.
//

#import "HLSearchResultController.h"
#import "HouseListCell.h"
#import "HLHouseDetailController.h"
#import "HomeSearchManager.h"

@interface HLSearchResultController ()<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *textTF;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navHeight;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, assign) NSInteger page;


@end

@implementation HLSearchResultController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navHeight.constant = NavAndStatusHight;
    self.textTF.text = self.searchStr;
    
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];

    [self refresh];
}

- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(15);
    params[@"city"] = self.city;
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度
    params[@"type"] = @(self.type.integerValue-10);
    params[@"keywords"] = self.textTF.text;

//    if (self.sex.length) {
//        params[@"confine"] = self.sex;
//    }
//    if (self.typeId.length) {
//        params[@"typeId"] = self.typeId;
//    }
//    if (self.tagSearchName.length) {
//        params[@"tagSearchName"] = self.tagSearchName;
//    }

    
    WeakSelf
    [NetworkingTool getWithUrl:kHLHouseLeaseListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                HouseListModel *model = [[HouseListModel alloc] initWithDictionary:obj];
                model.searchStr = self.textTF.text;
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView withY:200];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self saveSearchKeyword];
    return YES;
}


#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    HouseListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HouseListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HouseListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    HouseListModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    HouseListModel *model = self.dataArray[indexPath.row];
    HLHouseDetailController *vc = [[HLHouseDetailController alloc] init];
    vc.houseId = model.ID;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Function
- (void)saveSearchKeyword {
    NSString *key = self.textTF.text;
    [self.textTF endEditing:YES];
    [self.view endEditing:YES];
    if (key.length == 0) {
        [LJTools showText:@"请输入内容后再搜索" delay:1.5];
        return;
    }
    if (key.length > 8) {
        [LJTools showText:@"输入内容最多8个字符" delay:1.5];
        return;
    }
    
//    /// 本地缓存
    [[HomeSearchManager shareManager] saveOrUpdateByColumnName:key type:self.type];
    self.searchStr = key;
    [self refresh];
}



#pragma mark - UI
- (IBAction)btn:(UIButton *)sender {
    [self.view endEditing:YES];
    switch (sender.tag) {
        case 1://返回
        {
            [self backItemClicked];
        }
            break;
        case 2://搜索
        {
            [self saveSearchKeyword];
        }
            break;
    }
}

@end
