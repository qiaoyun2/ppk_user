//
//  HLWriteCommentController.h
//  PPK
//
//  Created by null on 2022/6/12.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HLWriteCommentController : BaseViewController

@property (nonatomic, strong) NSString *houseId;
@property (nonatomic, copy) void(^onCommentSuccess)(void);


@end

NS_ASSUME_NONNULL_END
