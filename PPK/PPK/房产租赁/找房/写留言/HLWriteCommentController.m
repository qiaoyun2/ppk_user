//
//  HLWriteCommentController.m
//  PPK
//
//  Created by null on 2022/6/12.
//

#import "HLWriteCommentController.h"

@interface HLWriteCommentController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *anonymousButton;

@end

@implementation HLWriteCommentController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"写留言";
    [self setNavigationRightBarButtonWithTitle:@"发送"];
}

- (void)requestForSubmit
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"houseId"] = self.houseId;
    params[@"pid"] = @"";
    params[@"content"] = self.textView.text; //
    [NetworkingTool postWithUrl:kHLAddCommentURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
//            [LJTools showText:@"留言成功" delay:1.5];
            if (self.onCommentSuccess) {
                self.onCommentSuccess();
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender
{
    if (!self.textView.text.length) {
        [LJTools showText:@"请输入留言内容" delay:1.5];
        return;
    }
    [self requestForSubmit];

}

- (IBAction)anonymousButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
