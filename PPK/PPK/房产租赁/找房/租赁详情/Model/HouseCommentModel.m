//
//  HouseCommentModel.m
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "HouseCommentModel.h"

@implementation HouseCommentModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
    
}

@end
