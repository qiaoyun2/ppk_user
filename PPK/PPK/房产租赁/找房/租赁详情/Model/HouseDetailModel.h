//
//  HouseDetailModel.h
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "BaseModel.h"
#import "HouseListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HouseDetailModel : BaseModel

@property (nonatomic, strong) NSString *ageRange; // 年龄
@property (nonatomic, strong) NSString *apartmentLayout; // 年龄

@property (nonatomic, strong) NSString *avatar; //
@property (nonatomic, strong) NSString *descrip; //
@property (nonatomic, strong) NSString *gender; // 性别：0-女；1男
@property (nonatomic, strong) NSString *job; // 职业
@property (nonatomic, strong) NSString *nickName; // 昵称
@property (nonatomic, strong) NSString *phone; // 联系电话
@property (nonatomic, strong) NSString *picture; //
@property (nonatomic, strong) NSString *publishTime; // 发布时间
@property (nonatomic, strong) NSString *rent; // 租金
@property (nonatomic, strong) NSString *confine; // 限制：0-不限；1-男；2-女
@property (nonatomic, strong) NSString *stars; // 星座
@property (nonatomic, strong) NSString *title; // 标题
@property (nonatomic, strong) NSString *typeName; // 发布类型
@property (nonatomic, strong) NSString *status; // 状态：0下架 1审核通过-上架 2审核中 3审核失败
@property (nonatomic, strong) NSString *auditOpinion; // 审核意见
@property (nonatomic, strong) NSString *longitude; // 经度
@property (nonatomic, strong) NSString *latitude; // 纬度
@property (nonatomic, strong) NSString *isCollection; // 是否收藏0否1是
@property (nonatomic, strong) NSString *rentSaleType; // 1租房 2买卖房
@property (nonatomic, strong) NSString *ID; //
@property (nonatomic, strong) NSString *isMine; //
@property (nonatomic, strong) NSString *square; //
@property (nonatomic, strong) NSString *position; //
@property (nonatomic, strong) NSString *street; //
@property (nonatomic, strong) NSString *userId; //

@property (nonatomic, strong) NSArray *recommendList; //



@end

NS_ASSUME_NONNULL_END
