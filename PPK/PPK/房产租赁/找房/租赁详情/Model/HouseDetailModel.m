//
//  HouseDetailModel.m
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "HouseDetailModel.h"

@implementation HouseDetailModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"description"]) {
        self.descrip = value;
    }
    if ([key isEqual:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
    if ([key isEqual:@"recommendList"]) {
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *obj in value) {
            HouseListModel *model = [[HouseListModel alloc] initWithDictionary:obj];
            [array addObject:model];
        }
        self.recommendList = [NSArray arrayWithArray:array];
    }
}


@end

