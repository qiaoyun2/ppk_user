//
//  HouseCommentModel.h
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HouseCommentModel : BaseModel


@property (nonatomic, strong) NSString *avatar; //
@property (nonatomic, strong) NSString *content; //
@property (nonatomic, strong) NSString *createTime; //
@property (nonatomic, strong) NSString *firstPid; //
@property (nonatomic, strong) NSString *ID; //
@property (nonatomic, strong) NSString *isMine; //
@property (nonatomic, strong) NSString *nickname; // 昵称
@property (nonatomic, strong) NSString *pid; //
@property (nonatomic, strong) NSString *toNickname; //
@property (nonatomic, strong) NSString *userId; //
@property (nonatomic, strong) NSArray *childList; 


@end

NS_ASSUME_NONNULL_END
