//
//  HouseDetailCell.h
//  PPK
//
//  Created by null on 2022/6/9.
//

#import <UIKit/UIKit.h>
#import "FoundListImage9TypographyView.h"
#import "HouseDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HouseDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *typeButton;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *sexLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;
@property (weak, nonatomic) IBOutlet UIView *recommentView;
@property (weak, nonatomic) IBOutlet UIStackView *recommentStackView;

@property (weak, nonatomic) IBOutlet UILabel *commentNumLabel;
@property (nonatomic, strong) HouseDetailModel *model;



@end

NS_ASSUME_NONNULL_END
