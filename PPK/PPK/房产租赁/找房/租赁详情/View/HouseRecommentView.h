//
//  HouseRecommentView.h
//  PPK
//
//  Created by null on 2022/6/13.
//

#import <UIKit/UIKit.h>
#import "HouseListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HouseRecommentView : UIView
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *thumbImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *rentLabel;
@property (weak, nonatomic) IBOutlet UIButton *typeButton;
@property (weak, nonatomic) IBOutlet UILabel *unitLabel;

@property (nonatomic, strong) HouseListModel *model;

@end

NS_ASSUME_NONNULL_END
