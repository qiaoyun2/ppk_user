//
//  HouseCommentCell.h
//  PPK
//
//  Created by null on 2022/6/9.
//

#import <UIKit/UIKit.h>
#import "HouseCommentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HouseCommentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;


@property (nonatomic, strong) HouseCommentModel *model;
@property (nonatomic, copy) void(^onDeleteButtonClick)(void);

@end

NS_ASSUME_NONNULL_END
