//
//  HouseRecommentView.m
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "HouseRecommentView.h"

@implementation HouseRecommentView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.contentView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.06].CGColor;
    self.contentView.layer.shadowOffset = CGSizeMake(0,1.5);
    self.contentView.layer.shadowRadius = 6;
    self.contentView.layer.shadowOpacity = 1;
    self.contentView.layer.cornerRadius = 12;
}

- (void)setModel:(HouseListModel *)model
{
    _model = model;
    NSArray *images = [model.picture componentsSeparatedByString:@","];
    if (images.count) {
        [self.thumbImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl([images firstObject])] placeholderImage:DefaultImgWidth];
    }
    self.titleLabel.text = model.title;
    self.addressLabel.text = model.position;
    self.rentLabel.text = model.rent;
    [self.typeButton setTitle:model.typeName forState:UIControlStateNormal];
//    if (model.rentSaleType.integerValue==1) {
//        self.moneyLabel.text = [NSString stringWithFormat:@"租金 %@元/月",model.rent];
//    }else {
//        self.moneyLabel.text = [NSString stringWithFormat:@"房价 %@元/平",model.rent];
//    }

}

@end
