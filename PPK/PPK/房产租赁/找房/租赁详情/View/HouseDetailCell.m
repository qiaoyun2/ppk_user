//
//  HouseDetailCell.m
//  PPK
//
//  Created by null on 2022/6/9.
//

#import "HouseDetailCell.h"
#import "HouseRecommentView.h"
#import "HLHouseDetailController.h"
#import "HLReportController.h"
#import "LMMapViewController.h"

@implementation HouseDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(HouseDetailModel *)model
{
    _model = model;
    self.titleLabel.text = model.title;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.avatar)] placeholderImage:DefaultImgWidth];
    self.nameLabel.text = model.nickName;
    self.timeLabel.text = model.publishTime;
    [self.typeButton setTitle:model.typeName forState:UIControlStateNormal];
    NSString *sex = model.gender.integerValue == 0 ? @"女":@"男";
    self.infoLabel.text = [NSString stringWithFormat:@"性别:%@ · 年领:%@ · 星座:%@ · 工作:%@",sex,model.ageRange,model.stars,model.job];
    
    if (model.rentSaleType.integerValue==1) {
        self.moneyLabel.text = [NSString stringWithFormat:@"%@元/月",model.rent];
        self.moneyTitleLabel.text = @"租金";
    }else {
        self.moneyLabel.text = [NSString stringWithFormat:@"%@元/平",model.rent];
        self.moneyTitleLabel.text = @"房价";
    }
    NSString *limit = @"不限男女";
    if (model.confine.integerValue==1) {
        limit = @"限男生";
    }else if (model.confine.integerValue==2) {
        limit = @"限男生";
    }
    self.sexLabel.text = limit;
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@",model.position,model.street];
    self.contentLabel.text = model.descrip;
    if (model.picture.length>0) {
        self.imgsView.hidden = NO;
        NSArray *images = [model.picture componentsSeparatedByString:@","];
        self.imgsView.imageArray= images;
    }else {
        self.imgsView.hidden = YES;
    }
    
    NSArray *array = self.recommentStackView.arrangedSubviews;
    for (HouseRecommentView *view in array) {
        [self.recommentStackView removeArrangedSubview:view];
        [view removeFromSuperview];
    }
    
    [model.recommendList enumerateObjectsUsingBlock:^(HouseListModel *listModel, NSUInteger idx, BOOL * _Nonnull stop) {
        HouseRecommentView *view = [[[NSBundle mainBundle] loadNibNamed:@"HouseRecommentView" owner:nil options:nil] firstObject];
        view.tag = 100+idx;
        view.model = listModel;
        [self.recommentStackView addArrangedSubview:view];
        [view jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            HLHouseDetailController *vc = [[HLHouseDetailController alloc] init];
            vc.houseId = listModel.ID;
            [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
        }];
    }];
    
}

- (IBAction)reportButtonAction:(UIButton *)sender {
    HLReportController *vc = [[HLReportController alloc] init];
    vc.houseId = _model.ID;
    vc.userId = _model.userId;
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

- (IBAction)addressViewTap:(id)sender {
    LMMapViewController *vc = [[LMMapViewController alloc] init];
    vc.coordinate = CLLocationCoordinate2DMake([_model.latitude floatValue], [_model.longitude floatValue]);
    vc.city = _model.position;
//    vc.district = _model
    vc.address = _model.street;
//    vc.distance = _model.dis;
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

@end
