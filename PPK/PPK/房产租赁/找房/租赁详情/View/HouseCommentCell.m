//
//  HouseCommentCell.m
//  PPK
//
//  Created by null on 2022/6/9.
//

#import "HouseCommentCell.h"

@implementation HouseCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(HouseCommentModel *)model
{
    _model = model;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.avatar)] placeholderImage:DefaultImgHeader];
    self.nameLabel.text = model.nickname;
    self.timeLabel.text = model.createTime;
    self.contentLabel.text = model.content;
    self.deleteButton.hidden = !model.isMine.boolValue;
}

- (IBAction)deleteButtonAction:(UIButton *)sender {
    if (self.onDeleteButtonClick) {
        self.onDeleteButtonClick();
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
