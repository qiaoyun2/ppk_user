//
//  HLHouseDetailController.m
//  PPK
//
//  Created by null on 2022/6/9.
//

#import "HLHouseDetailController.h"
#import "HouseCommentCell.h"
#import "HouseDetailCell.h"
#import "HLWriteCommentController.h"
#import "HLPublishController.h"
#import "ShareView.h"

@interface HLHouseDetailController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *mineView;
@property (weak, nonatomic) IBOutlet UIView *otherView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeight;
@property (weak, nonatomic) IBOutlet ReLayoutButton *collectionButton;

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger commentNum;

@property (nonatomic, strong) HouseDetailModel *detailModel;



@end

@implementation HLHouseDetailController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self refresh];
    [self requestForHouseDetail];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"租赁详情";
    self.bottomViewHeight.constant = 60+bottomBarH;
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
}

#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(15);
    params[@"houseId"] = self.houseId;
    WeakSelf
    [NetworkingTool getWithUrl:kHLCommentListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            self.commentNum = [dataDic[@"total"] integerValue];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                HouseCommentModel *model = [[HouseCommentModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
//        [self addBlankOnView:self.tableView];
//        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}


- (void)requestForDeleteComment:(HouseCommentModel *)model
{
    [NetworkingTool postWithUrl:kHLDeleteCommentURL params:@{@"id":model.ID} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            self.commentNum -= 1;
            [self.dataArray removeObject:model];
            [self.tableView reloadData];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForHouseDetail
{
    [NetworkingTool getWithUrl:kHLHouseLeaseDetailURL params:@{@"id":self.houseId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.detailModel = [[HouseDetailModel alloc] initWithDictionary:dataDic];
            [self.tableView reloadData];
            self.collectionButton.selected = self.detailModel.isCollection.integerValue;
            if (self.detailModel.isMine.integerValue) {
                self.mineView.hidden = NO;
                self.otherView.hidden = YES;
            }else {
                self.mineView.hidden = YES;
                self.otherView.hidden = NO;
            }
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

- (void)requestForCollection:(UIButton *)sender
{
    [NetworkingTool postWithUrl:kHLHouseLeaseCollectionURL params:@{@"houseId":self.houseId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            sender.selected = !sender.selected;
        }
        [LJTools showText:responseObject[@"msg"] delay:1.5];

    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForDeleteHouse
{
    [NetworkingTool postWithUrl:kHLDeleteHouseLeaseURL params:@{@"id":self.houseId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 1;
    }
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        HouseDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HouseDetailCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"HouseDetailCell" owner:nil options:nil] firstObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (self.detailModel.ID) {
            cell.model = self.detailModel;
            cell.commentNumLabel.text = [NSString stringWithFormat:@"全部留言(%ld)",self.commentNum];
        }
        return cell;
    }
    HouseCommentModel *model = self.dataArray[indexPath.row];
    HouseCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HouseCommentCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HouseCommentCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.model = model;
    WeakSelf
    [cell setOnDeleteButtonClick:^{
        [UIAlertController alertViewNormalWithTitle:@"提示" message:@"删除后不能恢复" titlesArry:@[@"立即删除"] indexBlock:^(NSInteger index, id obj) {
            [weakSelf requestForDeleteComment:model];
        } okColor:UIColorFromRGB(0xFF6464) cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
    }];
    return cell;
}


#pragma mark - Function
- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType title:(NSString*)title content:(NSString*)content ImageUrl:(NSString*)imageUrl url:(NSString *)url
{
    //创建分享消息对象
    UMSocialMessageObject*messageObject =[UMSocialMessageObject messageObject];
//    NSData *dateImg = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
    
    //创建网页内容对象
    UMShareWebpageObject*shareObject =[UMShareWebpageObject shareObjectWithTitle:title descr:content thumImage:[UIImage imageNamed:@"ppklogo"]];
    //设置网页地址
    shareObject.webpageUrl =url;
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data,NSError*error){
        if(error){
            NSLog(@"************Share fail with error %@*********",error);
        }else{
            NSLog(@"response data is %@",data);
        }
    }];
}

#pragma mark - XibFunction
- (IBAction)shareButtonAction:(UIButton *)sender {
    ShareView *shareView = [[[NSBundle mainBundle] loadNibNamed:@"ShareView" owner:nil options:nil] firstObject];
    shareView.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].keyWindow addSubview:shareView];
    [shareView show];
    WeakSelf
    [shareView setOnPlatformDidSelected:^(NSInteger tag) {
        [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"downLoadUrl"} success:^(NSURLSessionDataTask *task, id responseObject) {
            [LJTools hideHud];
            if ([responseObject[@"code"] intValue] == SUCCESS) {
                NSString *downLoadUrl = (NSString *)responseObject[@"data"];
                [weakSelf shareWebPageToPlatformType:tag+1 title:@"拼拼看" content:@"推荐给好友" ImageUrl:nil url:downLoadUrl];
            } else {
                [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            [LJTools showNOHud:RequestServerError delay:1.0];
        } IsNeedHub:YES];
        
        
    }];
}

- (IBAction)contactButtonAction:(UIButton *)sender {
    [LJTools call:self.detailModel.phone];
}

- (IBAction)deleteButtonAction:(id)sender {
    WeakSelf
    [UIAlertController alertViewNormalWithTitle:@"提示" message:@"删除后不能恢复" titlesArry:@[@"立即删除"] indexBlock:^(NSInteger index, id obj) {
        [weakSelf requestForDeleteHouse];
    } okColor:UIColorFromRGB(0xFF6464) cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
}
- (IBAction)editButtonAction:(UIButton *)sender {
    HLPublishTypeModel *typeModel = [[HLPublishTypeModel alloc] init];
    typeModel.name = self.detailModel.typeName;
    typeModel.ID = self.detailModel.ID;
    typeModel.rentSaleType = self.detailModel.rentSaleType;
    
    HLPublishController *vc = [[HLPublishController alloc] init];
    vc.typeModel = typeModel;
    vc.detailModel = self.detailModel;
    [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)commentButtonActon:(id)sender {
    HLWriteCommentController *vc = [[HLWriteCommentController alloc] init];
    vc.houseId = self.houseId;
    WeakSelf
    [vc setOnCommentSuccess:^{
        [weakSelf refresh];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)collectionButtonAction:(UIButton *)sender {
    [self requestForCollection:sender];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
