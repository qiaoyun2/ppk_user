//
//  HLUserConfig.m
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "HLUserConfig.h"

@implementation HLUserConfigItem

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
}

@end

@implementation HLUserConfig

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"starsList"]) {
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *obj in value) {
            HLUserConfigItem *item = [[HLUserConfigItem alloc] initWithDictionary:obj];
            [tempArray addObject:item];
        }
        self.starsList = [NSArray arrayWithArray:tempArray];
    }
    
    if ([key isEqual:@"jobList"]) {
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *obj in value) {
            HLUserConfigItem *item = [[HLUserConfigItem alloc] initWithDictionary:obj];
            [tempArray addObject:item];
        }
        self.jobList = [NSArray arrayWithArray:tempArray];
    }
    
    if ([key isEqual:@"ageList"]) {
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *obj in value) {
            HLUserConfigItem *item = [[HLUserConfigItem alloc] initWithDictionary:obj];
            [tempArray addObject:item];
        }
        self.ageList = [NSArray arrayWithArray:tempArray];
    }
}

@end
