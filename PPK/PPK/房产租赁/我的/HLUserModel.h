//
//  HLUserModel.h
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HLUserModel : BaseModel

@property (nonatomic, strong) NSString *ageRangeId;
@property (nonatomic, strong) NSString *ageRangeName;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *jobId;
@property (nonatomic, strong) NSString *jobName;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *starsId;
@property (nonatomic, strong) NSString *starsName;


@end

NS_ASSUME_NONNULL_END
