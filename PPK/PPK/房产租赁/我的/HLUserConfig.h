//
//  HLUserConfig.h
//  PPK
//
//  Created by null on 2022/6/13.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HLUserConfigItem : BaseModel

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *name;

@end


@interface HLUserConfig : BaseModel

@property (nonatomic, strong) NSArray *starsList; // 星座
@property (nonatomic, strong) NSArray *jobList; // 职业
@property (nonatomic, strong) NSArray *ageList; // 年龄段



@end

NS_ASSUME_NONNULL_END
