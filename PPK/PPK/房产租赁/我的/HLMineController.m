//
//  HLMineController.m
//  PPK
//
//  Created by null on 2022/6/12.
//

#import "HLMineController.h"
#import "ZJPickerView.h"
#import "HLUserConfig.h"
#import "HLUserModel.h"

@interface HLMineController ()
@property (weak, nonatomic) IBOutlet UILabel *sexLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *professionLabel;
@property (weak, nonatomic) IBOutlet UILabel *constellationLabel;

@property (weak, nonatomic) IBOutlet UITextField *mobileField;
@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UITextField *codeField;
@property (weak, nonatomic) IBOutlet UIButton *codeButton;

@property (nonatomic, strong) HLUserConfig *config;
@property (nonatomic, strong) HLUserModel *userModel;


@property (nonatomic, strong) NSString *gender; // 性别：0-女；1男
@property (nonatomic, strong) NSString *ageId; // 年龄id
@property (nonatomic, strong) NSString *jobId; // 年龄id
@property (nonatomic, strong) NSString *starsId; // 星座id

@end

@implementation HLMineController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self requestForUserInfo];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"个人";
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    self.mobileField.text = [User getUser].mobile;
    self.gender = [User getUser].sex;
    self.sexLabel.text = [User getUser].sex.integerValue == 1 ? @"男":@"女";
    [self setNavigationRightBarButtonWithTitle:@"保存" color:MainColor];
    [self.mobileField addTarget:self action:@selector(mobileFieldEditChanged:) forControlEvents:UIControlEventEditingChanged];
    [self requestForConfig];
}

#pragma mark - 网络请求
/// 获取用户信息
- (void)requestForUserInfo
{
    [NetworkingTool getWithUrl:kHLUserInfoURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.userModel = [[HLUserModel alloc] initWithDictionary:dataDic];
            self.gender = self.userModel.gender;
            self.ageId = self.userModel.ageRangeId;
            self.jobId = self.userModel.jobId;
            self.starsId = self.userModel.starsId;
            if (self.ageId.length) {
                self.ageLabel.text = self.userModel.ageRangeName;
            }
            if (self.starsId.length) {
                self.constellationLabel.text = self.userModel.starsName;
            }
            if (self.jobId.length) {
                self.professionLabel.text = self.userModel.jobName;
            }
        }else {
            
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

/// 编辑用户信息
- (void)requestForSaveUserInfo
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (self.gender.length) {
        params[@"gender"] = self.gender;
    }
    if (self.ageId.length) {
        params[@"ageId"] = self.ageId;
    }
    if (self.jobId.length) {
        params[@"jobId"] = self.jobId;
    }
    if (self.starsId.length) {
        params[@"starsId"] = self.starsId;
    }
    if (self.mobileField.text.length) {
        params[@"phone"] = self.mobileField.text;
    }
    if (![self.mobileField.text isEqualToString:[User getUser].mobile]) {
        params[@"event"] = @"verifyPhone";
        params[@"captcha"] = self.codeField.text;
    }
    [NetworkingTool postWithUrl:kHLEditUserInfoURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"保存成功" delay:1.5];
            self.codeView.hidden = YES;
            [self.codeButton cancelCountdownWithEndString:@"获取验证码"];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForConfig
{
    [NetworkingTool getWithUrl:kHLInitSelectDataURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.config = [[HLUserConfig alloc] initWithDictionary:dataDic];
        }else {
            
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

- (void)requestForSendCode:(UIButton *)sender
{
    NSDictionary *params = @{@"mobile":_mobileField.text,@"event":@"verifyPhone"};
    [NetworkingTool postWithUrl:kGetCodeURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [sender setCountdown:60 WithStartString:@"" WithEndString:@"获取验证码"];
        }
        [LJTools showOKHud:responseObject[@"msg"] delay:1];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

#pragma mark - Function
- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender
{
    if (!self.ageId.length) {
        [LJTools showText:@"请选择年龄" delay:1.5];
        return;
    }
    if (!self.gender.length) {
        [LJTools showText:@"请选择性别" delay:1.5];
        return;
    }
    if (!self.jobId.length) {
        [LJTools showText:@"请选择职业" delay:1.5];
        return;
    }
    if (!self.starsId.length) {
        [LJTools showText:@"请选择星座" delay:1.5];
        return;
    }
    
    if (!self.mobileField.text.length) {
        [LJTools showText:@"请输入手机号" delay:1.5];
        return;
    }
    if (![self.mobileField.text isTelephone]) {
        [LJTools showText:@"请输入正确的手机号" delay:1.5];
        return;
    }
    
    if (![self.mobileField.text isEqualToString:[User getUser].mobile]) {
        if (self.codeField.text.length==0) {
            [LJTools showText:@"请输入验证码" delay:1.5];
            return;
        }
    }
    
    [self requestForSaveUserInfo];
}

- (void)mobileFieldEditChanged:(UITextField *)field
{
    if ([field.text isEqualToString:[User getUser].mobile]) {
        self.codeView.hidden = YES;
        self.codeButton.hidden = YES;
    }else {
        self.codeView.hidden = NO;
        self.codeButton.hidden = NO;
    }
}

#pragma mark - XibFunction
- (IBAction)sexButtonAction:(UIButton *)sender {
    WeakSelf
    [ZJNormalPickerView zj_showStringPickerWithTitle:@"选择性别" dataSource:@[@"女",@"男"] defaultSelValue:nil isAutoSelect:NO resultBlock:^(id selectValue, NSInteger index) {
        weakSelf.sexLabel.text = selectValue;
        weakSelf.gender = [NSString stringWithFormat:@"%ld",index];
    } cancelBlock:^{
        
    }];
}

- (IBAction)ageButtonAction:(UIButton *)sender {
    NSMutableArray *titles = [NSMutableArray array];
    for (HLUserConfigItem *item in self.config.ageList) {
        [titles addObject:item.name];
    }
    WeakSelf
    [ZJNormalPickerView zj_showStringPickerWithTitle:@"选择年龄" dataSource:titles defaultSelValue:nil isAutoSelect:NO resultBlock:^(id selectValue, NSInteger index) {
        weakSelf.ageLabel.text = selectValue;
        HLUserConfigItem *item = self.config.ageList[index];
        weakSelf.ageId = item.ID;
    } cancelBlock:^{
        
    }];
    
}

- (IBAction)professionButtonAction:(UIButton *)sender {
    NSMutableArray *titles = [NSMutableArray array];
    for (HLUserConfigItem *item in self.config.jobList) {
        [titles addObject:item.name];
    }
    WeakSelf
    [ZJNormalPickerView zj_showStringPickerWithTitle:@"选择职业" dataSource:titles defaultSelValue:nil isAutoSelect:NO resultBlock:^(id selectValue, NSInteger index) {
        weakSelf.professionLabel.text = selectValue;
        HLUserConfigItem *item = self.config.jobList[index];
        weakSelf.jobId = item.ID;
    } cancelBlock:^{
        
    }];
}

- (IBAction)constellationButtonAction:(UIButton *)sender {
    NSMutableArray *titles = [NSMutableArray array];
    for (HLUserConfigItem *item in self.config.starsList) {
        [titles addObject:item.name];
    }
    WeakSelf
    [ZJNormalPickerView zj_showStringPickerWithTitle:@"选择星座" dataSource:titles defaultSelValue:nil isAutoSelect:NO resultBlock:^(id selectValue, NSInteger index) {
        weakSelf.constellationLabel.text = selectValue;
        HLUserConfigItem *item = self.config.starsList[index];
        weakSelf.starsId = item.ID;
    } cancelBlock:^{
        
    }];
}

- (IBAction)codeButtonAction:(UIButton *)sender {
    if (self.mobileField.text.length==0) {
        [LJTools showText:@"请输入手机号" delay:1.5];
        return;
    }
    
    if (![self.mobileField.text isTelephone]) {
        [LJTools showText:@"请输入正确的手机号" delay:1.5];
        return;
    }
    [self requestForSendCode:sender];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
