//
//  MasterCenterController.m
//  PPK
//
//  Created by null on 2022/4/30.
//

#import "MasterCenterController.h"
#import "GBStarRateView.h"
#import "MyCommentCell.h"

@interface MasterCenterController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet GBStarRateView *starView;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;

@property (nonatomic, assign) NSInteger page;


@end

@implementation MasterCenterController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"师傅中心";
    self.view.backgroundColor = UIColorFromRGB(0xF6F7F9);
    self.starView.allowClickScore = NO;
    self.starView.allowSlideScore = NO;
    self.starView.spacingBetweenStars = 2;
    self.starView.starSize = CGSizeMake(10, 10);
    self.starView.starImage = [UIImage imageNamed:@"路径 86768"];
    self.starView.currentStarImage = [UIImage imageNamed:@"路径 54422"];
    self.starView.style = GBStarRateViewStyleIncompleteStar;
    self.starView.isAnimation = NO;
    self.starView.currentStarRate = 4.0;
    
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    [self refresh];
    [self requestForMasterInfo];
}

#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
    params[@"orderId"] = self.orderId;
//    params[@"startDate"] = self.start_data;
//    params[@"endDate"] = self.end_data;
    WeakSelf
    [NetworkingTool getWithUrl:kRWMasterCommentListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                RWOrderCommentModel *model = [[RWOrderCommentModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}


- (void)requestForMasterInfo
{
    [NetworkingTool getWithUrl:kRWMasterInfoURL params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(dataDic[@"avatar"])] placeholderImage:DefaultImgHeader];
            self.nameLabel.text = dataDic[@"nickname"];
            self.numLabel.text = [NSString stringWithFormat:@"%@单",dataDic[@"orderNums"]];
            self.starView.currentStarRate = [dataDic[@"score"] floatValue];
            self.scoreLabel.text = [NSString stringWithFormat:@"%.1f分",[dataDic[@"score"] floatValue]];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCommentCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MyCommentCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    RWOrderCommentModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH-24, 50);
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 17.5, 3, 15)];
    imageView.image = [UIImage imageNamed:@"矩形 21797"];
    [headerView addSubview:imageView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(27, 0, 150, 50)];
    label.text = @"他的评价";
    label.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    label.textColor = UIColorFromRGB(0x333333);
    label.textAlignment = NSTextAlignmentLeft;
    [headerView addSubview:label];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(12, 49.5, SCREEN_WIDTH-48, 0.5)];
    line.backgroundColor = UIColorFromRGB(0xEEEEEE);
    [headerView addSubview:line];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
