//
//  MyCommentCell.m
//  PPKMaster
//
//  Created by null on 2022/4/18.
//

#import "MyCommentCell.h"

@implementation MyCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.starView.allowClickScore = NO;
    self.starView.allowSlideScore = NO;
    self.starView.spacingBetweenStars = 2;
    self.starView.starSize = CGSizeMake(10, 10);
    self.starView.starImage = [UIImage imageNamed:@"路径 86768"];
    self.starView.currentStarImage = [UIImage imageNamed:@"路径 54422"];
    self.starView.style = GBStarRateViewStyleIncompleteStar;
    self.starView.isAnimation = NO;
    self.starView.currentStarRate = 4;
}

- (void)setModel:(RWOrderCommentModel *)model
{
    _model = model;
    
    self.contentLabel.text = model.content;
    self.timeLabel.text = model.createTime;
    if (model.video.length>0) {
        self.imgsView.videoPath = model.video;
        self.imgsView.imageArray= @[model.videoPicture];
    }
    else {
        if (model.picture.length>0) {
            NSArray *images = [model.picture componentsSeparatedByString:@","];
            self.imgsView.imageArray= images;
        }
    }
    self.starView.currentStarRate = [model.starNum floatValue];
    if (model.isAnonymous.integerValue==1) {
        self.headImageView.image = DefaultImgHeader;
        self.nameLabel.text = @"匿名用户";
    }else {
        [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.avatar)] placeholderImage:DefaultImgHeader];
        self.nameLabel.text = model.nickname;
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
