//
//  MyCommentCell.h
//  PPKMaster
//
//  Created by null on 2022/4/18.
//

#import <UIKit/UIKit.h>
#import "GBStarRateView.h"
#import "FoundListImage9TypographyView.h"
#import "RWOrderCommentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyCommentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet GBStarRateView *starView;
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;

@property (nonatomic, strong) RWOrderCommentModel *model;

@end

NS_ASSUME_NONNULL_END
