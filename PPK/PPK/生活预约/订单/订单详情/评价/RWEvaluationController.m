//
//  RWEvaluationController.m
//  PPK
//
//  Created by null on 2022/4/13.
//

#import "RWEvaluationController.h"
#import <HXPhotoPicker/HXPhotoPicker.h>
#import "HXAssetManager.h"
#import "UploadManager.h"
#import "GBStarRateView.h"

@interface RWEvaluationController () <HXPhotoViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagLabel;

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet HXPhotoView *photoView;
@property (weak, nonatomic) IBOutlet UIButton *anonymousButton;

@property (weak, nonatomic) IBOutlet GBStarRateView *averageStarView;
@property (weak, nonatomic) IBOutlet GBStarRateView *starView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoViewHeight;

@property (nonatomic, assign) BOOL isVideo;
@property (nonatomic, strong) NSMutableArray<UIImage *> *photos;
@property (nonatomic, strong) HXPhotoModel *videoModel;
@property (nonatomic, strong) NSString *videoPath;
@property (nonatomic, strong) NSMutableArray *imagePathArray;
@property (strong, nonatomic) HXPhotoManager *manager;

@end

@implementation RWEvaluationController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"评价";
    self.view.backgroundColor = UIColorFromRGB(0xF6F7F9);
    
    self.photos = [NSMutableArray array];
    self.imagePathArray = [NSMutableArray array];
    
    self.starView.allowClickScore = YES;
    self.starView.allowSlideScore = YES;
    self.starView.spacingBetweenStars = 10;
    self.starView.starSize = CGSizeMake(17, 17);
    self.starView.starImage = [UIImage imageNamed:@"路径 17051"];
    self.starView.currentStarImage = [UIImage imageNamed:@"路径 17047"];
    self.starView.style = GBStarRateViewStyleWholeStar;
    self.starView.isAnimation = NO;
    self.starView.currentStarRate = 5;
    
    self.averageStarView.allowClickScore = NO;
    self.averageStarView.allowSlideScore = NO;
    self.averageStarView.spacingBetweenStars = 2;
    self.averageStarView.starSize = CGSizeMake(10, 10);
    self.averageStarView.starImage = [UIImage imageNamed:@"路径 86768"];
    self.averageStarView.currentStarImage = [UIImage imageNamed:@"路径 54422"];
    self.averageStarView.style = GBStarRateViewStyleIncompleteStar;
    self.averageStarView.isAnimation = NO;
    self.averageStarView.currentStarRate = 4.0;
    
    self.photoView.spacing = 10.f;
    self.photoView.delegate = self;
    self.photoView.deleteCellShowAlert = YES;
    self.photoView.outerCamera = YES;
    self.photoView.previewShowDeleteButton = YES;
    self.photoView.addImageName = @"组 51192";
    self.photoView.lineCount = 4;
    self.photoView.manager = self.manager;
    [self requestForMasterInfo];
}

#pragma mark - Network
- (void)requestForUploadVideo
{
    NSData *data = [NSData dataWithContentsOfURL:self.videoModel.videoURL];
    WeakSelf
    [UploadManager uploadWithVideoData:data block:^(NSString * _Nonnull videoUrl) {
        NSLog(@"%@",videoUrl);
        weakSelf.videoPath = videoUrl;
    }];
}

- (void)requestForUploadImages
{
    WeakSelf
    [UploadManager uploadImageArray:self.photos block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
        NSArray *array = [imageUrl componentsSeparatedByString:@","];
        [weakSelf.imagePathArray addObjectsFromArray:array];
        [weakSelf requestForSubmit];
    }];
}

- (void)requestForSubmit
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"content"] = self.textView.text;
    params[@"orderId"] = self.orderId;
    params[@"resourceType"] = @"1";
    params[@"video"] = @"";
    params[@"videoPicture"] = @"";
    if (self.imagePathArray.count) {
        params[@"picture"] = [self.imagePathArray componentsJoinedByString:@","];
    }else {
        params[@"picture"] = @"";
    }
    params[@"starNum"] = @(self.starView.currentStarRate);
    [NetworkingTool postWithUrl:kRWAddCommentURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"评论成功" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForMasterInfo
{
    [NetworkingTool getWithUrl:kRWMasterInfoURL params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(dataDic[@"avatar"])] placeholderImage:DefaultImgHeader];
            self.nameLabel.text = dataDic[@"nickname"];
            self.numLabel.text = [NSString stringWithFormat:@"%@单",dataDic[@"orderNums"]];
            self.averageStarView.currentStarRate = [dataDic[@"score"] floatValue];
            self.scoreLabel.text = [NSString stringWithFormat:@"%.1f分",[dataDic[@"score"] floatValue]];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - HXPhotoViewDelegate
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {
    if (videos.count>0) {
        self.isVideo = YES;
        self.videoModel = [videos firstObject];
        [self.photos removeAllObjects];
        WeakSelf
        [self.videoModel getImageWithSuccess:^(UIImage * _Nullable image, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
            [weakSelf.photos addObject:image];
        } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
            
        }];
        NSLog(@"%@",self.videoModel.videoURL);
    }
    else {
        self.isVideo = NO;
        self.videoModel = nil;
        [self.photos removeAllObjects];
        for (HXPhotoModel *model in photos) {
            [self.photos addObject:[HXAssetManager originImageForAsset:model.asset]];
        }
    }
}
// 拿到view变化的高度
- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame {
    self.photoViewHeight.constant = frame.size.height;
}

- (IBAction)contactButtonAction:(id)sender {
    
}

- (IBAction)anonymousButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (IBAction)submitButtonAction:(UIButton *)sender {
    if (!self.textView.text.length) {
        [LJTools showText:@"请输入评论内容" delay:1.5];
        return;
    }
    if (self.photos.count) {
        [self requestForUploadImages];
    }else {
        [self requestForSubmit];
    }
}

#pragma mark - Lazy Load
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _manager.configuration.type = HXConfigurationTypeWXChat;
        _manager.configuration.singleSelected = NO;
        _manager.configuration.lookGifPhoto = NO;
        _manager.configuration.useWxPhotoEdit = YES;
        _manager.configuration.selectTogether=NO;
        _manager.configuration.photoEditConfigur.onlyCliping = YES;
        _manager.configuration.showOriginalBytes = NO;
        _manager.configuration.videoMaxNum=1;
        _manager.configuration.photoMaxNum=9;
        _manager.configuration.requestImageAfterFinishingSelection = YES;
        _manager.configuration.photoEditConfigur.aspectRatio = HXPhotoEditAspectRatioType_None;
    }
    return _manager;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
