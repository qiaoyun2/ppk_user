//
//  RWEvaluationController.h
//  PPK
//
//  Created by null on 2022/4/13.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RWEvaluationController : BaseViewController

@property (nonatomic, strong) NSString *orderId;

@end

NS_ASSUME_NONNULL_END
