//
//  RWOrderDetailController.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "RWOrderDetailController.h"
#import "FoundListImage9TypographyView.h"
#import "RWEvaluationController.h"
#import "RWOrderDetailModel.h"
#import "MasterCenterController.h"

@interface RWOrderDetailController ()
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel; // 订单号
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel; // 联系方
@property (weak, nonatomic) IBOutlet UILabel *addressLabel; // 上门地址

@property (weak, nonatomic) IBOutlet UILabel *titleLabel; //
@property (weak, nonatomic) IBOutlet UILabel *introduceLbel; // 描述


@property (weak, nonatomic) IBOutlet UIView *imgsBgView;
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;
@property (weak, nonatomic) IBOutlet UILabel *reserveTimeLabel; // 预约上门时间
@property (weak, nonatomic) IBOutlet UILabel *createTimeLabel; // 下单时间
@property (weak, nonatomic) IBOutlet UIView *masterView; // 回收员
@property (weak, nonatomic) IBOutlet UILabel *masterMobileLabel; // 回收员

@property (weak, nonatomic) IBOutlet UILabel *typeLabel; // 类型
@property (weak, nonatomic) IBOutlet UILabel *numLabel; // 数量
@property (weak, nonatomic) IBOutlet UIView *moneyView; // 回收价格
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton; // 取消
@property (weak, nonatomic) IBOutlet UIButton *contactButton; // 拨打电话
@property (weak, nonatomic) IBOutlet UIButton *commentButton; // 评论
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;  // 删除
@property (weak, nonatomic) IBOutlet UIStackView *buttonsStackView;
@property (weak, nonatomic) IBOutlet UIView *bottomMasterView;
@property (weak, nonatomic) IBOutlet UIImageView *masterHeadImageView;
@property (weak, nonatomic) IBOutlet UILabel *masterNameLabel;


@property (nonatomic, strong) RWOrderDetailModel *detailModel;

@end

@implementation RWOrderDetailController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self requestForOrderDetail];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单详情";
    for (UIButton *button in self.buttonsStackView.arrangedSubviews) {
        button.hidden = YES;
    }
    WeakSelf
    [self.bottomMasterView jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        MasterCenterController *vc = [[MasterCenterController alloc] init];
        vc.orderId = weakSelf.orderId;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
}

#pragma mark - Network
- (void)requestForOrderDetail
{
    [NetworkingTool getWithUrl:kRWOrderDetailURL params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.detailModel = [[RWOrderDetailModel alloc] initWithDictionary:dataDic];
            [self updateSubviews];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}



- (void)requestForCancelOrder
{
    [NetworkingTool postWithUrl:kRWCancelOrderURL params:@{@"orderId":self.orderId, @"cancelId":@""} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"取消成功" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 *NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestForOrderDetail];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForDeleteOrder
{
    [NetworkingTool postWithUrl:kRWDeleteOrderURL params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - Function
- (void)updateSubviews
{
    NSInteger status = [self.detailModel.status integerValue];
    self.statusLabel.text = [self getStatusFormat:status];
    self.orderNumLabel.text = _detailModel.orderId;
    self.mobileLabel.text = [NSString stringWithFormat:@"%@ %@",_detailModel.receiver, _detailModel.telphone];
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",_detailModel.province,_detailModel.city,_detailModel.area,_detailModel.street];
    self.titleLabel.text = [NSString stringWithFormat:@"%@ %@",_detailModel.firstClassify, _detailModel.secondClassify];
    self.introduceLbel.text = _detailModel.remark;
    self.imgsView.hidden = NO;
    if (_detailModel.video.length>0) {
        self.imgsView.videoPath = _detailModel.video;
        self.imgsView.imageArray= @[_detailModel.videoPicture];
    }
    else {
        if (_detailModel.picture.length>0) {
            NSArray *images = [_detailModel.picture componentsSeparatedByString:@","];
            self.imgsView.imageArray= images;
        }
        else {
            self.imgsBgView.hidden = YES;
        }
    }
    
    
    self.reserveTimeLabel.text = _detailModel.reserveTime;
    self.createTimeLabel.text = _detailModel.createTime;
    self.masterMobileLabel.text = [NSString stringWithFormat:@"%@ %@",_detailModel.receiveUserName, _detailModel.receiveUserTelPhone];
    
    self.typeLabel.text = [NSString stringWithFormat:@"%@ %@",_detailModel.firstClassify, _detailModel.secondClassify];
    self.numLabel.text = [NSString stringWithFormat:@"%@",_detailModel.goodsNum];

    [self.masterHeadImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(_detailModel.receiveUserAvatar)] placeholderImage:DefaultImgHeader];
    self.masterNameLabel.text = _detailModel.receiveUserName;
    
    for (UIButton *button in self.buttonsStackView.arrangedSubviews) {
        button.hidden = YES;
    }
    if (status==1) {
        // 预约中
        self.cancelButton.hidden = NO;
        self.masterView.hidden = YES;
        self.moneyView.hidden = YES;
        self.bottomMasterView.hidden = YES;
    }
    else if (status==2) {
        // 待上门
        self.cancelButton.hidden = [_detailModel.cancelTime integerValue] <= 0;
        self.contactButton.hidden = NO;
        self.masterView.hidden = NO;
        self.moneyView.hidden = YES;
        self.bottomMasterView.hidden = NO;
    }
    else if (status==3) {
        // 已完成
        self.deleteButton.hidden = NO;
        self.commentButton.hidden = [_detailModel.isComment integerValue];
        self.masterView.hidden = NO;
        self.moneyView.hidden = YES;
        self.bottomMasterView.hidden = NO;
    }
    else {
        // 已取消
        self.deleteButton.hidden = NO;
        self.masterView.hidden = YES;
        self.moneyView.hidden = YES;
        self.bottomMasterView.hidden = YES;

    }
}

- (NSString *)getStatusFormat:(NSInteger)status
{
    if (status==1) {
        return @"等待系统接单";
    }
    if (status==2) {
        return @"等待回收员上门回收";
    }
    if (status==3) {
        return @"订单已完成";
    }
    return @"订单已取消";
}


#pragma mark - XibFunction
- (IBAction)buttonsAction:(UIButton *)sender {
//
//    RWEvaluationController *vc = [[RWEvaluationController alloc] init];
//    vc.orderId = self.orderId;
//    [self.navigationController pushViewController:vc animated:YES];
//    return;

    NSInteger tag = sender.tag;
    /// 0:取消  1:联系  2:评论  3:删除
    if (tag==0) {
        WeakSelf
        [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认取消订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
            [weakSelf requestForCancelOrder];
        } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
    }
    else if (tag==1) {
        [LJTools call:_detailModel.receiveUserTelPhone];
    }
    else if (tag==2) {
        RWEvaluationController *vc = [[RWEvaluationController alloc] init];
        vc.orderId = self.orderId;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else {
        WeakSelf
        [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
            [weakSelf requestForDeleteOrder];
        } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
