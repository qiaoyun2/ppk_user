//
//  RWOrderController.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "RWOrderController.h"
#import "RWOrderListController.h"
#import "YNPageViewController.h"
#import "UIView+YNPageExtend.h"
#import "ReLayoutButton.h"

@interface RWOrderController ()<YNPageViewControllerDataSource, YNPageViewControllerDelegate>

@property (nonatomic, strong) NSArray *titles;


@end

@implementation RWOrderController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单";
    self.titles = @[@"全部",@"预约中",@"待上门",@"已完成",@"已取消"];
    [self setupPageVC];
}

- (void)setupPageVC {
    YNPageConfigration *configration = [YNPageConfigration defaultConfig];
    configration.pageStyle = YNPageStyleTop;
    /// 控制tabbar 和 nav
    configration.showTabbar = YES;
    configration.showNavigation = YES;
    //scrollMenu = NO,aligmentModeCenter = NO 会变成平分
    configration.scrollMenu = NO;
    configration.aligmentModeCenter = NO;
    configration.showBottomLine = NO;
    configration.showScrollLine = NO;
    configration.itemMargin = 0;
//    configration.bottomLineBgColor = RGBA(218, 98, 57, 1);
//    configration.scrollViewBackgroundColor = UIColorFromRGB(0xF5F5F5);
    configration.menuHeight = 50;
    configration.normalItemColor = RGBA(51, 51, 51, 1);
    configration.selectedItemColor = RGBA(51, 51, 51, 1);
    configration.itemFont = [UIFont systemFontOfSize:16];
    configration.selectedItemFont = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    configration.cutOutHeight = 0;
    
    NSMutableArray *buttonArrayM = [NSMutableArray array];
    for (int i = 0; i<self.titles.count; i++) {
        ReLayoutButton *button = [ReLayoutButton buttonWithType:UIButtonTypeCustom];
        button.layoutType = 3;
        button.margin = 3;
        UIImage *selectImage = [UIImage imageNamed:@"矩形 1884"];
        [button setImage:[UIImage imageNamed:@"矩形 1884-1"] forState:UIControlStateNormal];
        [button setImage:selectImage forState:UIControlStateSelected];
        [buttonArrayM addObject:button];
    }
    configration.buttonArray = buttonArrayM;
    
    YNPageViewController *vc = [YNPageViewController pageViewControllerWithControllers:self.getArrayVCs titles:self.titles  config:configration];
    vc.dataSource = self;
    vc.delegate = self;
    /// 指定默认选择index 页面
    vc.pageIndex = 0;
    /// 作为自控制器加入到当前控制器
    [vc addSelfToParentViewController:self];
}

- (NSArray *)getArrayVCs {
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSInteger i = 0; i<5; i++) {
        RWOrderListController *vc = [[RWOrderListController alloc] init];
        vc.status = i;
        [tempArray addObject:vc];
    }
    return [tempArray copy];
}

#pragma mark - YNPageViewControllerDataSource
- (UIScrollView *)pageViewController:(YNPageViewController *)pageViewController pageForIndex:(NSInteger)index {
    UIViewController *vc = pageViewController.controllersM[index];
    return [(RWOrderListController *)vc tableView];
}

#pragma mark - YNPageViewControllerDelegate
- (void)pageViewController:(YNPageViewController *)pageViewController
            contentOffsetY:(CGFloat)contentOffset
                  progress:(CGFloat)progress {
}

- (void)pageViewController:(YNPageViewController *)pageViewController
                 didScroll:(UIScrollView *)scrollView
                  progress:(CGFloat)progress
                 formIndex:(NSInteger)fromIndex
                   toIndex:(NSInteger)toIndex
{

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
