//
//  RWOrderListCell.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "RWOrderListCell.h"

static dispatch_source_t _timer;

@implementation RWOrderListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    for (UIButton *button in self.buttonsStackView.arrangedSubviews) {
        button.hidden = YES;
    }
}

- (void)setModel:(RWOrderListModel *)model
{
    _model = model;
    
    for (UIButton *button in self.buttonsStackView.arrangedSubviews) {
        button.hidden = YES;
    }
    self.countDownView.hidden = YES;
    
    self.orderNumLabel.text = model.orderId;
    self.reserveTimeLabel.text = model.reserveTime;
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",model.province,model.city,model.area,model.street];
    NSInteger status = [model.status integerValue];
    self.statusLabel.text = [LJTools getRWStatusFormat:status];

    if (status==1) {
        // 预约中
        self.cancelButton.hidden = NO;
        self.countDownView.hidden = YES;
    }
    else if (status==2) {
        // 待上门
        self.contactButton.hidden = NO;
        if (model.cancelTime.intValue>0) {
            self.countDownView.hidden = NO;
            self.cancelButton.hidden = NO;
            [self startTimer:model.cancelTime.intValue];
        }else {
            self.countDownView.hidden = YES;
            self.cancelButton.hidden = YES;
        }
    }
    else if (status==3) {
        // 已完成
        self.deleteButton.hidden = NO;
        self.commentButton.hidden = [model.isComment integerValue];
    }
    else {
        // 已取消
        self.deleteButton.hidden = NO;
    }
}

- (void)startTimer:(int)time
{
    __block int timeout=time;
    //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0);
    //每秒执行
    
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_async(dispatch_get_main_queue(), ^{
                self.countDownView.hidden = YES;
                self.cancelButton.hidden = YES;
            });
        }else{
            int seconds = timeout;
            //format of minute
            NSString *str_minute = [NSString stringWithFormat:@"%02d",(seconds%3600)/60];
            //format of second
            NSString *str_second = [NSString stringWithFormat:@"%02d",seconds%60];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.countDownLabel.text = [NSString stringWithFormat:@"%@:%@",str_minute,str_second];
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

- (IBAction)buttonsAction:(UIButton *)sender {
    if (self.onButtonsClick) {
        self.onButtonsClick(sender.tag);
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
