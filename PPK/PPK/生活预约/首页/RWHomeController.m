//
//  RWHomeController.m
//  PPK
//
//  Created by null on 2022/3/30.
//

#import "RWHomeController.h"
#import "RWCateButton.h"
#import "YNPageConfigration.h"
#import "YNPageScrollMenuView.h"
#import "AddressListView.h"
#import "RWMapViewController.h"
#import "RWReserveController.h"
#import "RWCateModel.h"
#import "AddressModel.h"

@interface RWHomeController ()<YNPageScrollMenuViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *cateView;
@property (weak, nonatomic) IBOutlet UIView *subCateView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (nonatomic, strong) UIScrollView *cateScrollView;
@property (nonatomic, strong) RWCateButton *lastButton;
@property (nonatomic, strong) AddressListView *addressView;

@property (nonatomic, strong) NSMutableArray *subDataArray;
@property (nonatomic, strong) RWCateModel *cateModel;
@property (nonatomic, strong) RWCateModel *subCateModel;
@property (nonatomic, strong) AddressModel *addressModel;


@end

@implementation RWHomeController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.topViewHeight.constant = NavAndStatusHight;
    [self.subCateView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(12, 12) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, 58)];
    [self requestForFirstCateList];
}

//设置状态栏颜色
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - UI
- (void)setupCateView
{
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 16, SCREEN_WIDTH, 64)];
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    [self.cateView addSubview:scrollView];
    
    CGFloat left = 16;
    CGFloat height = 64;
    CGFloat space = 15;
    
    for (int i = 0; i<self.dataArray.count; i++) {
        RWCateModel *model = self.dataArray[i];
        RWCateButton *button = [[[NSBundle mainBundle] loadNibNamed:@"RWCateButton" owner:nil options:nil] firstObject];
        CGFloat width = [model.name commonStringWidthForFont:16]+24;
        button.frame = CGRectMake(left, 0, width, height);
        button.nameLabel.backgroundColor = [UIColor clearColor];
        button.nameLabel.textColor = [UIColor whiteColor];
        [button.iconImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.picture)] placeholderImage:DefaultImgWidth];
        button.nameLabel.text = model.name;
        button.alpha = 0.8;
        button.tag = 100+i;
        [button addTarget:self action:@selector(cateButtonsAction:) forControlEvents:UIControlEventTouchUpInside];
        if (i==0) {
            button.alpha = 1;
            button.nameLabel.backgroundColor = [UIColor whiteColor];
            button.nameLabel.textColor = MainColor;
            self.lastButton = button;
        }
        [scrollView addSubview:button];
        left += width + space;
        NSLog(@"%@",NSStringFromCGRect(button.frame));
    }
    scrollView.contentSize = CGSizeMake(left-space, 64);
}

- (void)setupSubCateView
{
    [self.subCateView removeAllSubviews];
    if (!self.subDataArray.count) {
        return;
    }
    YNPageConfigration *configration = [YNPageConfigration defaultConfig];
    //scrollMenu = NO,aligmentModeCenter = NO 会变成平分
    configration.scrollMenu = YES;
    configration.aligmentModeCenter = NO;
    configration.showBottomLine = NO;
    configration.showScrollLine = NO;
    configration.menuHeight = 58;
    configration.normalItemColor = RGBA(51, 51, 51, 1);
    configration.selectedItemColor = RGBA(51, 51, 51, 1);
    configration.itemFont = [UIFont systemFontOfSize:16];
    configration.selectedItemFont = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    
    NSMutableArray *titleArray = [NSMutableArray array];
    for (RWCateModel *model in self.subDataArray) {
        [titleArray addObject:model.name];
    }
    NSMutableArray *buttonArrayM = [NSMutableArray array];
    for (int i = 0; i<titleArray.count; i++) {
        ReLayoutButton *button = [ReLayoutButton buttonWithType:UIButtonTypeCustom];
        button.layoutType = 3;
        button.margin = 5;
        UIImage *selectImage = [UIImage imageNamed:@"矩形 1884"];
        [button setImage:[UIImage imageNamed:@"矩形 1884-1"] forState:UIControlStateNormal];
        [button setImage:selectImage forState:UIControlStateSelected];
        [buttonArrayM addObject:button];
    }
    configration.buttonArray = buttonArrayM;
    
    YNPageScrollMenuView *menuView = [YNPageScrollMenuView pagescrollMenuViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 58) titles:titleArray configration:configration delegate:self currentIndex:0];
    [self.subCateView addSubview:menuView];
}

#pragma mark - Network
- (void)requestForFirstCateList
{
    [NetworkingTool getWithUrl:kRWClassifyFirstListURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        self.dataArray = [NSMutableArray array];
        if ([responseObject[@"code"] integerValue]==1) {
            [responseObject[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                RWCateModel *model = [[RWCateModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
                if (idx==0) {
                    self.cateModel = model;
                    [self requestForSubCateList];
                }
            }];
            [self setupCateView];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

- (void)requestForSubCateList
{
    [NetworkingTool getWithUrl:kRWClassifyChildListURL params:@{@"parentId":self.cateModel.ID} success:^(NSURLSessionDataTask *task, id responseObject) {
        self.subDataArray = [NSMutableArray array];
        if ([responseObject[@"code"] integerValue]==1) {
            [responseObject[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                RWCateModel *model = [[RWCateModel alloc] initWithDictionary:obj];
                [self.subDataArray addObject:model];
                if (idx==0) {
                    self.subCateModel = model;
                }
            }];
            [self setupSubCateView];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

#pragma mark - YNPageScrollMenuViewDelegate
/// 点击item
- (void)pagescrollMenuViewItemOnClick:(UIButton *)button index:(NSInteger)index
{
    self.subCateModel = self.subDataArray[index];
}

#pragma mark - Function
- (void)cateButtonsAction:(RWCateButton *)button
{
    if (button==self.lastButton) {
        return;
    }
    self.lastButton.alpha = 0.7;
    self.lastButton.nameLabel.backgroundColor = [UIColor clearColor];
    self.lastButton.nameLabel.textColor = [UIColor whiteColor];
    
    button.alpha = 1;
    button.nameLabel.backgroundColor = [UIColor whiteColor];
    button.nameLabel.textColor = MainColor;
    
    self.lastButton = button;
    self.cateModel = self.dataArray[button.tag-100];
    self.subCateModel = nil;
    [self requestForSubCateList];
}

#pragma mark - XibFunction
- (IBAction)backButtonAction:(id)sender {
    [self.tabBarController.navigationController popViewControllerAnimated:YES];
}

- (IBAction)mapButtonAction:(UIButton *)sender {
    RWMapViewController *vc = [[RWMapViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    WeakSelf
    [vc setDidSelectBlock:^(AddressModel * _Nonnull addressModel) {
        weakSelf.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",addressModel.province,addressModel.city,addressModel.area,addressModel.street];
        weakSelf.addressLabel.textColor = UIColorFromRGB(0x333333);
        weakSelf.addressModel = addressModel;
    }];
}


- (IBAction)addressButtonAction:(id)sender {
    if (!self.addressView) {
        self.addressView = [[[NSBundle mainBundle] loadNibNamed:@"AddressListView" owner:nil options:nil] firstObject];
        self.addressView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.addressView];
    }
    [self.addressView show];
    WeakSelf
    [self.addressView setOnSelectedAddress:^(AddressModel * _Nonnull addressModel) {
        weakSelf.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",addressModel.province,addressModel.city,addressModel.area,addressModel.street];
        weakSelf.addressLabel.textColor = UIColorFromRGB(0x333333);
        weakSelf.addressModel = addressModel;
    }];
}


- (IBAction)appointmentButtonAction:(UIButton *)sender {
    if (!self.addressModel.province.length) {
        [LJTools showText:@"请选择地址" delay:1.5];
        return;
    }
    if (!self.subCateModel) {
        [LJTools showText:@"请选择二级分类" delay:1.5];
        return;
    }
    RWReserveController *vc = [[RWReserveController alloc] init];
    vc.addressModel = self.addressModel;
    vc.cateModel = self.cateModel;
    vc.subCateModel = self.subCateModel;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
