//
//  RWSubCateView.h
//  PPK
//
//  Created by null on 2022/4/12.
//

#import <UIKit/UIKit.h>
#import "RWCateModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RWSubCateView : UIView

@property (nonatomic, copy) void(^didSelectBlock)(RWCateModel *__nullable subModel);
@property (nonatomic, strong) RWCateModel *firstModel;
///0 生活预约 1，维修预约
@property (nonatomic, assign) NSInteger type;

- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
