//
//  RWSubCateView.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "RWSubCateView.h"
#import "RWSubCateCell.h"

@interface RWSubCateView ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *cateNameLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTrailing;

@property (nonatomic, strong) NSMutableArray *subDataArray;
@property (nonatomic, strong) RWCateModel *subModel;

@end

@implementation RWSubCateView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.topViewHeight.constant = StatusHight + 43;
    self.contentViewWidth.constant = SCREEN_WIDTH*0.7;
    self.contentViewTrailing.constant = - self.contentViewWidth.constant;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

#pragma mark -Network
- (void)requestForSubCateList
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSString *url = @"";
    if ( self.type == 1) {
        dict[@"parentId"] = _firstModel.ID;
        dict[@"layer"] = @"2";
        url = kMTRepairClassifyURL;
    }else{
        dict[@"parentId"] = _firstModel.ID;
        url = kRWClassifyChildListURL;
    }
    [NetworkingTool getWithUrl:url params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        self.subDataArray = [NSMutableArray array];
        if ([responseObject[@"code"] integerValue]==1) {
            [responseObject[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                RWCateModel *model = [[RWCateModel alloc] initWithDictionary:obj];
                [self.subDataArray addObject:model];
            }];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.subDataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RWSubCateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RWSubCateCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"RWSubCateCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    RWCateModel *model = self.subDataArray[indexPath.row];
    cell.cateNameLabel.text = model.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RWCateModel *model = self.subDataArray[indexPath.row];
    if (self.didSelectBlock) {
        self.didSelectBlock(model);
    }
    [self dismiss];
}

#pragma mark - Function
- (void)show
{
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0.4);
        self.contentViewTrailing.constant = 0;
        [self layoutIfNeeded];
    }];
}

- (void)dismiss
{
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0);
        self.contentViewTrailing.constant = - self.contentViewWidth.constant;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

- (void)setFirstModel:(RWCateModel *)firstModel
{
    _firstModel = firstModel;
    self.cateNameLabel.text = firstModel.name;
    [self requestForSubCateList];
}
-(void)setType:(NSInteger)type{
    _type = type;
}

- (IBAction)closeButtonAction:(id)sender {
    [self dismiss];
}


@end
