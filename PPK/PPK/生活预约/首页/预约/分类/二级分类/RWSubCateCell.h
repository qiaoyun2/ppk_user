//
//  RWSubCateCell.h
//  PPK
//
//  Created by null on 2022/4/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RWSubCateCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cateNameLabel;

@end

NS_ASSUME_NONNULL_END
