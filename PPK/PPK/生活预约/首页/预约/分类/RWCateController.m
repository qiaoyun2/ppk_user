//
//  RWCateController.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "RWCateController.h"
#import "RWCateCell.h"
#import "RWSubCateView.h"

@interface RWCateController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) RWSubCateView *subCateView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation RWCateController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"选择分类";
    [self.collectionView registerNib:[UINib nibWithNibName:@"RWCateCell" bundle:nil] forCellWithReuseIdentifier:@"RWCateCell"];
    [self requestForFirstCateList];
}

#pragma mark - Network
- (void)requestForFirstCateList
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSString *url = @"";
    if (self.type == 1) {
        url = kMTRepairClassifyURL;
        dict[@"layer"] = @"1";
        dict[@"parentId"] = @"0";
    }else{
        url = kRWClassifyFirstListURL;
    }
    [NetworkingTool getWithUrl:url params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        self.dataArray = [NSMutableArray array];
        if ([responseObject[@"code"] integerValue]==1) {
            [responseObject[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                RWCateModel *model = [[RWCateModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.collectionView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

#pragma mark - Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RWCateCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RWCateCell" forIndexPath:indexPath];
    RWCateModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    if (!self.subCateView) {
//        self.subCateView = [[[NSBundle mainBundle] loadNibNamed:@"RWSubCateView" owner:nil options:nil] firstObject];
//        self.subCateView.frame = [UIScreen mainScreen].bounds;
//        [[UIApplication sharedApplication].keyWindow addSubview:self.subCateView];
//    }
//    self.subCateView.firstModel = model;
//    [self.subCateView show];
//    WeakSelf
//    [self.subCateView setDidSelectBlock:^(RWCateModel * _Nullable subCateModel) {
//        if (weakSelf.didSelectBlock) {
//            weakSelf.didSelectBlock(model, subCateModel);
//        }
//        [weakSelf.navigationController popViewControllerAnimated:YES];
//    }];
    RWCateModel *model = self.dataArray[indexPath.row];
    if (self.didSelectBlock) {
        self.didSelectBlock(model);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH-105)/3, (SCREEN_WIDTH-105)/3+25);
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
