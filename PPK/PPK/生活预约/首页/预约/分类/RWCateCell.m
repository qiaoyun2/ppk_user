//
//  RWCateCell.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "RWCateCell.h"

@implementation RWCateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(RWCateModel *)model
{
    _model = model;
    [self.thumbImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.picture)] placeholderImage:DefaultImgWidth];
    self.titleLabel.text = model.name;
}

@end
