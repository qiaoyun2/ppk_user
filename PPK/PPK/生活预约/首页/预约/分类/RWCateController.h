//
//  RWCateController.h
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "BaseViewController.h"
#import "RWCateModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RWCateController : BaseViewController
///0 生活预约 1，维修预约
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, copy) void(^didSelectBlock)(RWCateModel *__nullable cateModel);


@end

NS_ASSUME_NONNULL_END
