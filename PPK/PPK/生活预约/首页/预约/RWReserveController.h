//
//  RWReserveController.h
//  PPK
//
//  Created by null on 2022/4/11.
//

#import "BaseViewController.h"
#import "AddressModel.h"
#import "RWCateModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RWReserveController : BaseViewController

@property (nonatomic, strong) AddressModel *addressModel;
@property (nonatomic, strong) RWCateModel *cateModel;
@property (nonatomic, strong) RWCateModel *__nullable subCateModel;

@end

NS_ASSUME_NONNULL_END
