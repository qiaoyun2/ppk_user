//
//  RWReserveController.m
//  PPK
//
//  Created by null on 2022/4/11.
//

#import "RWReserveController.h"
#import <HXPhotoPicker/HXPhotoPicker.h>
#import "HXAssetManager.h"
#import "UploadManager.h"
#import "RWCateController.h"
#import "RWSubCateView.h"
#import "RWReserveResultController.h"
#import "AddressListView.h"
#import "RWReserveTimeView.h"

@interface RWReserveController ()<HXPhotoViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *cateButton;
@property (weak, nonatomic) IBOutlet UIButton *subCateButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UITextField *numField;
@property (weak, nonatomic) IBOutlet UITextField *weightField;

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet HXPhotoView *photoView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *noticeLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@property (weak, nonatomic) IBOutlet UITextField *codeField;
@property (weak, nonatomic) IBOutlet UIButton *codeButton;
@property (weak, nonatomic) IBOutlet UIView *codeView;


@property (nonatomic, assign) BOOL isVideo;
@property (nonatomic, strong) NSMutableArray<UIImage *> *photos;
@property (nonatomic, strong) HXPhotoModel *videoModel;
@property (nonatomic, strong) NSString *videoPath;
@property (nonatomic, strong) NSMutableArray *imagePathArray;
@property (strong, nonatomic) HXPhotoManager *manager;
@property (nonatomic, strong) NSString *reserveTime;

@property (strong, nonatomic) RWSubCateView *subCateView;
@property (nonatomic, strong) AddressListView *addressView;
@property (nonatomic, strong) RWReserveTimeView *timeView;





@end

@implementation RWReserveController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"预约";
    self.textView.jk_placeHolderTextView.text = @"请输入备注内容...";
    NSString *string = @"回收须知：所有用户在下单时需要支付￥50作为订单保证金，本金额只作为维修员上门时的路费（需用户确认到达后支付），师傅确认出发前双方均可无责退单，师傅出发后需要用户向平台申请退款（需用户确认到达后支付）";
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:string];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [attriString addAttributes:@{NSForegroundColorAttributeName: MainColor} range:NSMakeRange(0, 5)];
    [attriString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [string length])];
    [self.noticeLabel setAttributedText:attriString];
    
    self.photos = [NSMutableArray array];
    self.imagePathArray = [NSMutableArray array];
    
    self.photoView.spacing = 10.f;
    self.photoView.delegate = self;
    self.photoView.deleteCellShowAlert = YES;
    self.photoView.outerCamera = YES;
    self.photoView.previewShowDeleteButton = YES;
    self.photoView.addImageName = @"组 50938";
    self.photoView.lineCount = 4;
    self.photoView.manager = self.manager;
    
    [self.cateButton setTitle:self.cateModel.name forState:UIControlStateNormal];
    [self.subCateButton setTitle:self.subCateModel.name forState:UIControlStateNormal];
    [self updateAddressView];
    self.codeView.hidden = [self.addressModel.telphone isEqual:[User getUser].mobile];
//    [self requestForDateList];

}



#pragma mark - Network

- (void)requestForReserve
{

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"firstClassifyId"] = self.cateModel.ID;
    params[@"secondClassifyId"] = self.subCateModel.ID;
    params[@"goodsNum"] = self.numField.text;
    params[@"goodsWeight"] = self.weightField.text;
    if (self.videoModel) {
        params[@"resourceType"] = @"2";
        params[@"video"] = self.videoPath;
        params[@"videoPicture"] = [self.imagePathArray firstObject];
        params[@"picture"] = @"";
    }else if (self.imagePathArray.count) {
        params[@"resourceType"] = @"1";
        params[@"picture"] = [self.imagePathArray componentsJoinedByString:@","];
        params[@"video"] = @"";
        params[@"videoPicture"] = @"";
    }
    else {
        params[@"resourceType"] = @"";
        params[@"picture"] = @"";
        params[@"video"] = @"";
        params[@"videoPicture"] = @"";
    }
    params[@"remark"] = self.textView.text;
    params[@"reserveTime"] = self.reserveTime;
    
    
    if (self.addressModel.ID.intValue>0) {
        params[@"addressId"] = self.addressModel.ID;
    }
    else{
        params[@"addressId"] = @"";
        params[@"longitude"] = self.addressModel.longitude;
        params[@"latitude"] = self.addressModel.latitude;
        params[@"country"] = self.addressModel.country;
        params[@"province"] = self.addressModel.province;
        params[@"city"] = self.addressModel.city;
        params[@"area"] = self.addressModel.area;
        params[@"street"] = self.addressModel.street;
        params[@"nickname"] = self.addressModel.receiver;
        params[@"mobile"] = self.addressModel.telphone;
    }
    params[@"captcha"] = self.codeField.text;

    [NetworkingTool postWithUrl:kRWAddOrderURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            RWReserveResultController *vc = [[RWReserveResultController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            
            NSMutableArray *controllers = [[NSMutableArray alloc]initWithArray:self.navigationController.viewControllers];

            for (UIViewController *vc in controllers) {
                if ([vc isKindOfClass:[RWReserveController class]]) {
                    [controllers removeObject:vc];
                    break;
                }
            }
            self.navigationController.viewControllers = controllers;
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}


- (void)requestForUploadVideo
{
    NSData *data = [NSData dataWithContentsOfURL:self.videoModel.videoURL];
    WeakSelf
    [UploadManager uploadWithVideoData:data block:^(NSString * _Nonnull videoUrl) {
        NSLog(@"%@",videoUrl);
        weakSelf.videoPath = videoUrl;
        [weakSelf requestForUploadImages];
    }];
}

- (void)requestForUploadImages
{
    WeakSelf
    [UploadManager uploadImageArray:self.photos block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
        NSArray *array = [imageUrl componentsSeparatedByString:@","];
        [weakSelf.imagePathArray addObjectsFromArray:array];
        [weakSelf requestForReserve];

    }];
}


#pragma mark - HXPhotoViewDelegate
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {

    if (videos.count>0) {
        self.isVideo = YES;
        self.videoModel = [videos firstObject];
        [self.photos removeAllObjects];
        [self.imagePathArray removeAllObjects];
        [self.videoModel getImageWithSuccess:^(UIImage * _Nullable image, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
            [self.photos addObject:image];
        } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
            
        }];
        NSLog(@"%@",self.videoModel.videoURL);
    }
    else {
        self.isVideo = NO;
        self.videoModel = nil;
        [self.photos removeAllObjects];
        [self.imagePathArray removeAllObjects];
        for (HXPhotoModel *model in photos) {
            [self.photos addObject:[HXAssetManager originImageForAsset:model.asset]];
        }
//        [self requestForUploadImages];
    }
//    self.selectList = allList;
}
// 拿到view变化的高度
- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame {
    self.photoViewHeight.constant = frame.size.height;
}

#pragma mark - Function
- (void)updateAddressView
{
//    self.nameLabel.text = [NSString stringWithFormat:@"%@(%@)",self.addressModel.receiver,[self.addressModel.gender integerValue]==1?@"男士":@"女士"];
    self.nameLabel.text = [NSString stringWithFormat:@"%@",self.addressModel.receiver];
    self.mobileLabel.text = self.addressModel.telphone;
    self.addressLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@",_addressModel.province,_addressModel.city,_addressModel.area,_addressModel.street];
}

/// 地址
- (IBAction)addressViewTap:(id)sender {
    if (!self.addressView) {
        self.addressView = [[[NSBundle mainBundle] loadNibNamed:@"AddressListView" owner:nil options:nil] firstObject];
        self.addressView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.addressView];
    }
    [self.addressView show];
    WeakSelf
    [self.addressView setOnSelectedAddress:^(AddressModel * _Nonnull addressModel) {
        weakSelf.addressModel = addressModel;
        weakSelf.codeField.text = @"";
        weakSelf.codeView.hidden = [addressModel.telphone isEqual:[User getUser].mobile];
        [weakSelf updateAddressView];
    }];

}

/// 预约时间
- (IBAction)timeViewTap:(id)sender {
    if (!self.timeView) {
        self.timeView = [[[NSBundle mainBundle] loadNibNamed:@"RWReserveTimeView" owner:nil options:nil] firstObject];
        self.timeView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.timeView];
    }
    [self.timeView show];
    WeakSelf
    [self.timeView setDidSelectBlock:^(NSString * _Nonnull selectedStr) {
        weakSelf.reserveTime = selectedStr;
        weakSelf.timeLabel.text = selectedStr;
        weakSelf.timeLabel.textColor = UIColorFromRGB(0x333333);
    }];

}

/// 一级分类
- (IBAction)cateButtonAction:(id)sender {
    RWCateController *vc = [[RWCateController alloc] init];
    WeakSelf
    [vc setDidSelectBlock:^(RWCateModel * _Nullable cateModel) {
        weakSelf.cateModel = cateModel;
        weakSelf.subCateModel = nil;
        [weakSelf.cateButton setTitle:cateModel.name forState:UIControlStateNormal];
        [weakSelf.subCateButton setTitle:@"请选择二级分类" forState:UIControlStateNormal];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

/// 二级分类
- (IBAction)subCateButtonAction:(id)sender {
    if (!self.subCateView) {
        self.subCateView = [[[NSBundle mainBundle] loadNibNamed:@"RWSubCateView" owner:nil options:nil] firstObject];
        self.subCateView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.subCateView];
    }
    self.subCateView.firstModel = self.cateModel;
    [self.subCateView show];
    WeakSelf
    [self.subCateView setDidSelectBlock:^(RWCateModel * _Nullable subModel) {
        weakSelf.subCateModel = subModel;
        [weakSelf.subCateButton setTitle:subModel.name forState:UIControlStateNormal];
    }];
}

/// 勾选按钮
- (IBAction)selectButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (IBAction)agreementButtonAction:(UIButton *)sender {
    
    /// 隐私政策
    [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"recoveryKnow"} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSString *privacy = (NSString *)responseObject[@"data"];
            WKWebViewController *vc = [WKWebViewController new];
            vc.titleStr = @"回收须知";
            vc.contentStr = privacy;
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
    
}

- (IBAction)codeButtonAction:(UIButton *)sender {
    NSDictionary *params = @{@"mobile":self.addressModel.telphone,@"event":@"verifyPhone"};
    [NetworkingTool postWithUrl:kGetCodeURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [sender setCountdown:60 WithStartString:@"" WithEndString:@"获取验证码"];
        }
        [LJTools showOKHud:responseObject[@"msg"] delay:1];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}


/// 预约
- (IBAction)reservationButtonAction:(UIButton *)sender {
    
    if (!self.reserveTime.length) {
        [LJTools showText:@"请选择预约时间" delay:1.5];
        return;
    }
    
    if (!self.selectButton.selected) {
        [LJTools showText:@"请阅读并同意回收须知" delay:1.5];
        return;
    }
    
    if (!self.subCateModel) {
        [LJTools showText:@"请选择二级分类" delay:1.5];
        return;
    }
    
    if (![self.addressModel.telphone isEqual:[User getUser].mobile] && self.codeField.text.length==0) {
        [LJTools showText:@"请输入验证码" delay:1.5];
        return;
    }

    if (self.videoModel) {
        [self requestForUploadVideo];
    }
    else if (self.photos.count) {
        [self requestForUploadImages];
    }
    else {
        [self requestForReserve];
    }
    
//    RWReserveResultController *vc = [[RWReserveResultController alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Lazy Load
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
        _manager.configuration.type = HXConfigurationTypeWXChat;
        _manager.configuration.singleSelected = NO;
        _manager.configuration.lookGifPhoto = NO;
        _manager.configuration.useWxPhotoEdit = YES;
        _manager.configuration.selectTogether=NO;
        _manager.configuration.photoEditConfigur.onlyCliping = YES;
        _manager.configuration.showOriginalBytes = NO;
        _manager.configuration.videoMaxNum=1;
        _manager.configuration.photoMaxNum=9;
        _manager.configuration.requestImageAfterFinishingSelection = YES;
        _manager.configuration.photoEditConfigur.aspectRatio = HXPhotoEditAspectRatioType_None;
    }
    return _manager;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
