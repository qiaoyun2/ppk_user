//
//  RWCateModel.h
//  PPK
//
//  Created by null on 2022/4/19.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RWCateModel : BaseModel

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *parentId;
@property (nonatomic, strong) NSString *layer;
@property (nonatomic, strong) NSString *picture;

@end

NS_ASSUME_NONNULL_END
