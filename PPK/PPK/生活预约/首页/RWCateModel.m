//
//  RWCateModel.m
//  PPK
//
//  Created by null on 2022/4/19.
//

#import "RWCateModel.h"

@implementation RWCateModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
}

@end
