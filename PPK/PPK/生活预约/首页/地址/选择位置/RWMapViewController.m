//
//  RWMapViewController.m
//  PPK
//
//  Created by null on 2022/4/1.
//

#import "RWMapViewController.h"

#import "LocationManager.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import "SearchTableViewCell.h"
#import "SelectAddressTableViewCell.h"
#import "CityDropDownView.h"

@interface RWMapViewController () <AMapSearchDelegate,MAMapViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *searchTableView;
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UIView *mapBGView;
@property (weak, nonatomic) IBOutlet UIImageView *downImageView;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;


@property (weak, nonatomic) IBOutlet UILabel *districtLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *doorNumField;
@property (weak, nonatomic) IBOutlet UITextField *mobileField;


@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) AMapSearchAPI *search;
@property (nonatomic, strong) AMapPOIAroundSearchRequest *aroundRequest;
@property (nonatomic, strong) AMapPOIKeywordsSearchRequest *keywordsRequest;
@property (nonatomic, strong) AMapLocationManager *locationManager;
@property (nonatomic, strong) MAPointAnnotation *pointAnnotation;

@property (nonatomic, strong) NSMutableArray *searchArray;
@property (nonatomic, strong) CityDropDownView *cityView;
@property (nonatomic, assign) BOOL showCityView;

@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *district;
@property (nonatomic, strong) NSString *address;

@property (nonatomic, strong) AMapPOI *poi;

@property (nonatomic) CLLocationCoordinate2D coordinate;

@end

@implementation RWMapViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"选择位置";
    self.searchArray = [NSMutableArray array];
    if (!self.city) {
        self.city = [LJTools getAppDelegate].city;
    }
    self.downImageView.transform = CGAffineTransformRotate(self.view.transform, M_PI);//旋转180
    self.searchTableView.hidden = YES;
    [self.tableView registerNib:[UINib nibWithNibName:@"SelectAddressTableViewCell" bundle:nil] forCellReuseIdentifier:@"SelectAddressTableViewCell"];
    [self.searchTableView registerNib:[UINib nibWithNibName:@"SearchTableViewCell" bundle:nil] forCellReuseIdentifier:@"SearchTableViewCell"];
    [self.searchField addTarget:self action:@selector(searchFieldEditChanged:) forControlEvents:UIControlEventEditingChanged];
    
    if (self.coordinate.longitude && self.city) {
        self.cityLabel.text = self.city;
        [self initMapView];
        [self searchRequest];
    }
    else {
        [self startLocation];
    }
    
//    if ([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
//        NSLog(@"定位服务已启用");
//        [[LocationManager shareManager]startLocation:YES withlocationManageBlock:^(BOOL isLocation, CLLocation * _Nonnull nowLocation, NSString * _Nonnull province, NSString * _Nonnull city, NSString * _Nonnull area, NSString * _Nonnull address, double lat, double lng) {
//            if (isLocation) {
//                [self initMapView];
//            }
//        }];
//    } else {
//        [LJTools showNOHud:@"请开启定位:设置 > 隐私 > 位置 > 定位服务" delay:1.0];
//    }
//    [self initMapView];
}

#pragma mark - UI
-(void)initMapView
{
    ///地图需要v4.5.0及以上版本才必须要打开此选项（v4.5.0以下版本，需要手动配置info.plist）
    [AMapServices sharedServices].enableHTTPS = YES;
    ///初始化地图
    self.mapView = [[MAMapView alloc] initWithFrame:self.mapBGView.bounds];
    [self.mapView setDelegate:self];
    self.mapView.zoomLevel = 17;
    ///把地图添加至view
    [self.mapBGView addSubview:self.mapView];
    ///如果您需要进入地图就显示定位小蓝点，则需要下面两行代码
//    self.mapView.userLocationVisible = YES;
//    self.mapView.centerCoordinate = CLLocationCoordinate2DMake([LJTools getAppDelegate].latitude , [LJTools getAppDelegate].longitude);
//    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
//    self.mapView.showsUserLocation = NO;
    self.mapView.centerCoordinate = self.coordinate;

}

- (void)searchRequest
{
    self.search = [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
    
    self.aroundRequest = [[AMapPOIAroundSearchRequest alloc] init];
    self.aroundRequest.location = [AMapGeoPoint locationWithLatitude:self.coordinate.latitude longitude:self.coordinate.longitude];
    self.aroundRequest.radius = 1000;
    [self.search AMapPOIAroundSearch:self.aroundRequest];
    
    //关键字搜索
    self.keywordsRequest = [[AMapPOIKeywordsSearchRequest alloc] init];
    self.keywordsRequest.city = self.city;
    self.keywordsRequest.cityLimit = YES;

}


#pragma mark - Network
-(void)setPoint:(AMapPOI *)model
{
    CLLocationCoordinate2D coor;
    coor.latitude = model.location.latitude;
    coor.longitude = model.location.longitude;
    if (_pointAnnotation==nil) {
        _pointAnnotation = [[MAPointAnnotation alloc] init];
    }
    _pointAnnotation.coordinate = coor;
    //设置地图的定位中心点坐标
    [self.mapView setCenterCoordinate:coor animated:YES];
    //将点添加到地图上，即所谓的大头针
    [self.mapView addAnnotation:_pointAnnotation];
}

#pragma mark - MAMapViewDelegate
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id)annotation {
    //大头针标注
    if ([annotation isKindOfClass:[MAPointAnnotation class]]) {//判断是否是自己的定位气泡，如果是自己的定位气泡，不做任何设置，显示为蓝点，如果不是自己的定位气泡，比如大头针就会进入
        static NSString *pointReuseIndentifier = @"pointReuseIndentifier";
        MAAnnotationView*annotationView = (MAAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
        if (annotationView == nil) {
            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
            annotationView.frame = CGRectMake(0, 0, 100, 100);
            annotationView.canShowCallout= NO;
            //设置大头针显示的图片
            annotationView.image = [UIImage imageNamed:@"redPin"];
        }
        return annotationView;
    }
    return nil;
    
}
- (void)mapViewDidFailLoadingMap:(MAMapView *)mapView withError:(NSError *)error
{
    NSLog(@"error======%@",error);
    [LJTools showNOHud:@"定位失败" delay:1.0];
}
- (void)mapViewWillStartLocatingUser:(MAMapView *)mapView
{
    self.aroundRequest.location = [AMapGeoPoint locationWithLatitude:mapView.centerCoordinate.latitude longitude:mapView.centerCoordinate.longitude];
    [self.search AMapPOIAroundSearch:self.aroundRequest];
}
- (void)mapView:(MAMapView *)mapView regionDidChangeAnimated:(BOOL)animated wasUserAction:(BOOL)wasUserAction
{
    self.aroundRequest.location = [AMapGeoPoint locationWithLatitude:mapView.centerCoordinate.latitude longitude:mapView.centerCoordinate.longitude];
    [self.search AMapPOIAroundSearch:self.aroundRequest];
}

#pragma mark - AMapSearchDelegate
/* POI 搜索回调. */
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    NSArray *array = response.pois;
    if ([request isEqual:self.keywordsRequest]) {
        [self.searchArray removeAllObjects];
        [self.searchArray addObjectsFromArray:array];
        [self.searchTableView reloadData];
        return;
    }
    
    if (array.count) {
        AMapPOI *poi = array[0];
        self.poi = poi;
        self.city = poi.city;
        self.cityLabel.text = self.city;
        self.districtLabel.text = poi.name;
        self.addressLabel.text = poi.address;
    }
    [self.dataArray setArray:array];
    [self.tableView reloadData];
}

- (void)onGeocodeSearchDone:(AMapGeocodeSearchRequest *)request response:(AMapGeocodeSearchResponse *)response
{
    if (response.geocodes.count == 0)
    {
        return;
    }
    AMapGeocode *geocode = response.geocodes[0];

    [self.mapView setCenterCoordinate:CLLocationCoordinate2DMake(geocode.location.latitude, geocode.location.longitude) animated:YES];
    self.aroundRequest.location = geocode.location;
    [self.search AMapPOIAroundSearch:self.aroundRequest];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.searchTableView) {
        return self.searchArray.count;
    }
    return self.dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView==self.searchTableView) {
        SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchTableViewCell" forIndexPath:indexPath];
        AMapPOI *model = self.searchArray[indexPath.row];
        [cell.titleLabel setText:model.name];
        [cell.detaiLabel setText:model.address];
        return cell;
    }
    
    SelectAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectAddressTableViewCell" forIndexPath:indexPath];
    AMapPOI *model = self.dataArray[indexPath.row];
    [cell.titleLabel setText:model.name];
    [cell.detaiLabel setText:model.address];
    [cell.distanceLabel setText:[NSString stringWithFormat:@"%ldm",model.distance]];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.searchField endEditing:YES];
    self.searchField.text = @"";
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray *dataArray = tableView==self.searchTableView ? self.searchArray : self.dataArray;
    AMapPOI *model = dataArray[indexPath.row];
    self.poi = model;
    self.city = model.city;
    self.cityLabel.text = model.city;
    self.districtLabel.text = model.name;
    self.addressLabel.text = model.address;
    self.inputView.hidden = NO;
    self.tableView.hidden = YES;
    self.searchTableView.hidden = YES;
}
#pragma mark - 搜索
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    self.searchField.text = @"";
    return YES;
}
//-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    [self.search cancelAllRequests];
//    self.keywordsRequest.keywords = textField.text;
//    [self.search AMapPOIKeywordsSearch:self.keywordsRequest];
//    return YES;
//}

#pragma mark - Function
- (void)startLocation
{
    if ([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        NSLog(@"定位服务已启用");
        
        self.locationManager = [[AMapLocationManager alloc] init];
        // 带逆地理信息的一次定位（返回坐标和地址信息）
        [self.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
        //   定位超时时间，最低2s，此处设置为2s
        self.locationManager.locationTimeout =2;
        //   逆地理请求超时时间，最低2s，此处设置为2s
        self.locationManager.reGeocodeTimeout = 2;
        
        [self.locationManager requestLocationWithReGeocode:YES completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
            if (error)
            {
                NSLog(@"locError:{%ld - %@};", (long)error.code, error.localizedDescription);
                
                if (error.code == AMapLocationErrorLocateFailed)
                {
                    return;
                }
            }
            if (regeocode)
            {
                NSLog(@"reGeocode:%@", regeocode);
                self.city = regeocode.city;
                self.coordinate = location.coordinate;
                self.cityLabel.text = regeocode.city;
                [self initMapView];
                [self searchRequest];
            }
        }];
    } else {
        [LJTools showNOHud:@"请开启定位:设置 > 隐私 > 位置 > 定位服务" delay:1.0];
    }
}

// 选择城市
- (void)showCityDropDownView
{
    self.showCityView = YES;
    if (!self.cityView) {
        self.cityView = [[[NSBundle mainBundle] loadNibNamed:@"CityDropDownView" owner:nil options:nil] firstObject];
        self.cityView.hiddenTabbar = YES;
        [[UIApplication sharedApplication].keyWindow addSubview:self.cityView];
    }
    self.cityView.selectedStr = self.city;
    [self.cityView showDropDown];
    WeakSelf
    [self.cityView setDidSelectBlock:^(NSString * _Nonnull city, NSString * _Nonnull district) {
        AMapGeocodeSearchRequest *geo = [[AMapGeocodeSearchRequest alloc] init];
        geo.city = city;
        geo.address = city;
        [weakSelf.search AMapGeocodeSearch:geo];
        weakSelf.city = city;
        weakSelf.cityLabel.text = city;
    }];
    [self.cityView setHiddenBlock:^{
        weakSelf.showCityView = NO;
    }];
}

-(void)rightButtonTouchUpInside:(UIBarButtonItem *)sender
{

}

- (void)searchFieldEditChanged:(UITextField *)textField
{
    if (self.showCityView) {
        [self.cityView hideDropDownAnimation:NO];
    }
    self.searchTableView.hidden = NO;
    [self.search cancelAllRequests];
    self.keywordsRequest.keywords = textField.text;
    self.keywordsRequest.city = self.city;
    self.keywordsRequest.cityLimit = YES;
    [self.search AMapPOIKeywordsSearch:self.keywordsRequest];
}

- (void)backItemClicked
{
    [self.view endEditing:YES];
    if (self.cityView) {
        [self.cityView hideDropDownAnimation:NO];
        [self.cityView removeFromSuperview];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (AddressModel *)getAddressModel
{
    AddressModel *model = [[AddressModel alloc] init];
    model.ID = @"0";
    model.receiver = self.nameField.text;
    model.tag = @"";
    model.gender = @"";
    model.telphone = self.mobileField.text;
    model.country = @"中国";
    model.province = self.poi.province;
    model.city = self.poi.city;
    model.area = self.poi.district;
    model.street = [NSString stringWithFormat:@"%@%@",self.poi.address,self.doorNumField.text];
    model.longitude = [NSString stringWithFormat:@"%f",self.poi.location.longitude];
    model.latitude = [NSString stringWithFormat:@"%f",self.poi.location.latitude];
    model.defaultFlag = @"";
    return model;
}

- (IBAction)confirmButtonAction:(UIButton *)sender {
    if (!self.poi) {
        [LJTools showText:@"请选择或搜索地址" delay:1.5];
        return;
    }
    
    if (!self.nameField.text.length) {
        [LJTools showText:@"请输入联系人姓名" delay:1.5];
        return;
    }
//    if (!self.doorNumField.text.length) {
//        [LJTools showText:@"请输入详细地址" delay:1.5];
//        return;
//    }
    if (!self.mobileField.text.length) {
        [LJTools showText:@"请输入电话" delay:1.5];
        return;
    }
    if (![self.mobileField.text isTelephone]) {
        [LJTools showText:@"请输入正确的电话" delay:1.5];
        return;
    }
    
    if (self.didSelectBlock) {
        self.didSelectBlock([self getAddressModel]);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelButtonAction:(id)sender {
    [self.view endEditing:YES];
    if (self.showCityView) {
        [self.cityView hideDropDownAnimation:NO];
    }
    if (self.searchTableView.hidden) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        self.searchTableView.hidden = YES;
        self.searchField.text = @"";
    }
}

- (IBAction)cityButtonAction:(UIButton *)sender {
    if (self.searchField.editing) {
        [self.view endEditing:YES];
        self.searchTableView.hidden = YES;
        self.searchField.text = @"";
    }
    if (self.showCityView) {
        [self.cityView hideDropDown];
    }
    else {
        [self showCityDropDownView];
    }
}


- (IBAction)editButtonAction:(id)sender {
    self.inputView.hidden = YES;
    self.tableView.hidden = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
