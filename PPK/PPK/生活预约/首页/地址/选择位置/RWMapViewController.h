//
//  RWMapViewController.h
//  PPK
//
//  Created by null on 2022/4/1.
//

#import "BaseViewController.h"
#import "AddressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RWMapViewController : BaseViewController


@property (nonatomic, copy) void(^didSelectBlock)(AddressModel *model);



@end

NS_ASSUME_NONNULL_END
