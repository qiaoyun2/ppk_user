//
//  AddressListController.m
//  null
//
//  Created by null on 2021/5/10.
//

#import "AddressListController.h"
#import "MyAddressCell.h"
#import "EditAddressController.h"

@interface AddressListController ()<MyAddressCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation AddressListController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"地址管理";
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    [self initUI];
    [self requestForAddressList];
}

#pragma mark - UI
- (void)initUI{
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerNib:[UINib nibWithNibName:@"MyAddressCell" bundle:nil] forCellReuseIdentifier:@"MyAddressCell"];
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        NSLog(@"下拉刷新");
        [weakSelf requestForAddressList];
    }];
//    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        NSLog(@"上拉加载更多");
//        [weakSelf loadMore];
//    }];
}

#pragma mark - Network
// 地址列表
- (void)requestForAddressList{
    [NetworkingTool getWithUrl:kAddressListURL params:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.dataArray removeAllObjects];
        if ([responseObject[@"code"]intValue]==SUCCESS) {
            NSArray *dataList = responseObject[@"data"];
            for (NSDictionary *dict in dataList) {
                AddressModel *model = [[AddressModel alloc]initWithDictionary:dict];
                [self.dataArray addObject:model];
            }
        }else{
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = self.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        
    } IsNeedHub:NO];
}

// 删除地址
- (void)requestForDeleteAddress:(AddressModel *)model
{
    [NetworkingTool postWithUrl:kDeleteAddressURL params:@{@"id":model.ID} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"]intValue]==SUCCESS) {
            [self.dataArray removeObject:model];
        }
        [self.tableView reloadData];
        [LJTools showText:responseObject[@"msg"] delay:1.5];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {

    } IsNeedHub:YES];
}

// 设为默认地址
- (void)requestForSetDefault:(AddressModel *)model
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:model.mj_keyValues];
    params[@"id"] = model.ID;
    // 设为默认地址
    [NetworkingTool postWithUrl:kSetDefaultAddressURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"]intValue]==SUCCESS) {
            for (AddressModel *addressModel in self.dataArray) {
                addressModel.defaultFlag = @"0";
            }
            model.defaultFlag = @"1";
        }else{
            [LJTools showText:responseObject[@"msg"] delay:1];
        }
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showText:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger num = self.dataArray.count;
    return num;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyAddressCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    AddressModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    cell.delegate = self;
    return cell;
}

- (void)addressListCell:(MyAddressCell *)cell buttonsAction:(UIButton *)sender
{
    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    AddressModel *model = self.dataArray[indexPath.row];
    if (sender.tag == 1) {
        if (model.defaultFlag.integerValue==1) {
            return;
        }
        // 设为默认
        [self requestForSetDefault:model];
    }
    else if(sender.tag == 2){
        // 编辑
        EditAddressController *vc = [EditAddressController new];
        vc.model = model;
        [self.navigationController pushViewController:vc animated:YES];
        WeakSelf
        [vc setOnEditAddress:^(AddressModel * _Nonnull result) {
            [weakSelf requestForAddressList];
        }];
    }
    else{
        // 删除
        [self requestForDeleteAddress:model];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AddressModel *model = self.dataArray[indexPath.row];
    if (self.selectAddressBlock){
        self.selectAddressBlock(model);
    }
    [self.navigationController popViewControllerAnimated:true];
}
#pragma mark - Function
#pragma mark - XibFunction
- (IBAction)addButtonAction:(id)sender {
    EditAddressController * vc = [EditAddressController new];
    vc.isNew = YES;
    [self.navigationController pushViewController:vc animated:YES];
    WeakSelf
    [vc setOnAddAddress:^{
        [weakSelf requestForAddressList];
    }];
    [vc setOnEditAddress:^(AddressModel * _Nonnull model) {
        [weakSelf requestForAddressList];
    }];
    [vc setOnDeleteAddress:^(AddressModel * _Nonnull model) {
        [weakSelf.dataArray removeObject:model];
        [weakSelf.tableView reloadData];
    }];
}
#pragma mark – lazy load

@end
