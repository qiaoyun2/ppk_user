//
//  MyAddressCell.h
//  null
//
//  Created by null on 2020/9/15.
//  Copyright © 2020 null. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MyAddressCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *tagButton; // 标签
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (weak, nonatomic) IBOutlet UIButton *defaultButton; // 默认地址

@property (nonatomic, strong) AddressModel *model;
@property (nonatomic, weak) id delegate;

@end

@protocol MyAddressCellDelegate <NSObject>

@optional

- (void)addressListCell:(MyAddressCell*)cell buttonsAction:(UIButton*)sender;

@end
NS_ASSUME_NONNULL_END
