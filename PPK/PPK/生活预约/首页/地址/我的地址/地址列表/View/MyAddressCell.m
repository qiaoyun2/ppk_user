//
//  MyAddressCell.m
//  null
//
//  Created by null on 2020/9/15.
//  Copyright © 2020 null. All rights reserved.
//

#import "MyAddressCell.h"


@implementation MyAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(AddressModel *)model{
    _model = model;
    NSString *sex = model.gender.integerValue == 1 ? @"(男士)" : @"(女士)";
    _nameLabel.text = [NSString stringWithFormat:@"%@%@",model.receiver,sex];
    _mobileLabel.text = model.telphone;
    _addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",model.province,model.city,model.area,model.street];
    _defaultButton.selected = model.defaultFlag.integerValue == 1;
    if (model.tag.length>0) {
        [self.tagButton setTitle:model.tag forState:UIControlStateNormal];
        self.tagButton.hidden = NO;
    }else{
        self.tagButton.hidden = YES;
    }
}

- (IBAction)buttonsAction:(UIButton *)sender {
    // 1:设为默认  2:编辑  3:删除
    if (_delegate && [_delegate respondsToSelector:@selector(addressListCell:buttonsAction:)]) {
        [_delegate addressListCell:self buttonsAction:sender];
    }
}

@end
