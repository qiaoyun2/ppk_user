//
//  AddressListController.h
//  null
//
//  Created by null on 2021/5/10.
//

#import "BaseViewController.h"
#import "AddressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddressListController : BaseViewController
@property (nonatomic ,copy) void (^selectAddressBlock)(AddressModel *addressModel);
@end

NS_ASSUME_NONNULL_END
