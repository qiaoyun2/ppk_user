//
//  AddressModel.h
//  Mei
//
//  Created by null on 2019/4/12.
//  Copyright © 2019 null. All rights reserved.
//


#import "BaseModel.h"
NS_ASSUME_NONNULL_BEGIN


@interface AddressModel :BaseModel


@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *tag;  // 标签：公司；家；学校
@property (nonatomic, strong) NSString *receiver;
@property (nonatomic, strong) NSString *gender; // 0-女；1-男
@property (nonatomic, copy) NSString *telphone;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *area;
@property (nonatomic, copy) NSString *street;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *defaultFlag;



@end

NS_ASSUME_NONNULL_END
