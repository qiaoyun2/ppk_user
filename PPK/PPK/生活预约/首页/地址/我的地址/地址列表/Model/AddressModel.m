//
//  AddressModel.m
//  Mei
//
//  Created by null on 2019/4/12.
//  Copyright © 2019 null. All rights reserved.
//

#import "AddressModel.h"
#import  <YYText.h>
@implementation AddressModel
//+ (NSDictionary *)mj_replacedKeyFromPropertyName
//{
//    return @{@"address_id":@"id"};
//}

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
}

@end
