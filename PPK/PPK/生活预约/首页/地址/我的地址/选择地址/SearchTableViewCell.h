//
//  SearchTableViewCell.h
//  PPK
//
//  Created by null on 2022/3/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detaiLabel;

@end

NS_ASSUME_NONNULL_END
