//
//  SelectAddressViewController.h
//  null
//
//  Created by null on 2020/12/16.
//

#import "BaseViewController.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <MAMapKit/MAMapKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelectAddressViewController : BaseViewController

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic) NSString *city;

@property (nonatomic, copy) void(^Block)(AMapPOI *model,NSString *detailAddress);
@end

NS_ASSUME_NONNULL_END
