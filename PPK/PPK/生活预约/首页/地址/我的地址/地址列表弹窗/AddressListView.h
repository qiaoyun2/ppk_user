//
//  AddressListView.h
//  PPK
//
//  Created by null on 2022/4/1.
//

#import <UIKit/UIKit.h>
#import "AddressModel.h"

NS_ASSUME_NONNULL_BEGIN



@interface AddressListView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, copy) void(^onSelectedAddress)(AddressModel *address);
- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
