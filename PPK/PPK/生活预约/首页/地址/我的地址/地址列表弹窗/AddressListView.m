//
//  AddressListView.m
//  PPK
//
//  Created by null on 2022/4/1.
//

#import "AddressListView.h"
#import "AddressListCell.h"
#import "EditAddressController.h"

@interface AddressListView ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation AddressListView


- (void)awakeFromNib
{
    [super awakeFromNib];
    self.index = -1;
    self.contentViewHeight.constant = 310+bottomBarH;
    self.contentViewBottom.constant = - self.contentViewHeight.constant;
    [self.contentView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(12, 12) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, self.contentViewHeight.constant)];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self requestForAddressList];
    
}

#pragma mark - Network
- (void)requestForAddressList
{
    [NetworkingTool getWithUrl:kAddressListURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            self.dataArray = [NSMutableArray array];
            for (NSDictionary *obj in responseObject[@"data"]) {
                AddressModel *model = [[AddressModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}


#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!self.dataArray.count) {
        tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_empty"]];
        tableView.contentMode = UIViewContentModeCenter;
    }
    else {
        tableView.backgroundView = nil;
    }
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddressListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddressListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"AddressListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.row == self.index) {
        cell.iconImageView.image = [UIImage imageNamed:@"路径 71405"];
        cell.addressLabel.textColor = UIColorFromRGB(0x333333);
    }else {
        cell.iconImageView.image = [UIImage imageNamed:@"路径 19609"];
        cell.addressLabel.textColor = UIColorFromRGB(0x999999);
    }
    AddressModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.index = indexPath.row;
    AddressModel *model = self.dataArray[indexPath.row];
    if (self.onSelectedAddress) {
        self.onSelectedAddress(model);
    }
    [self.tableView reloadData];
    [self dismiss];
}

#pragma mark - Function
- (void)show
{
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0.4);
        self.contentViewBottom.constant = 0;
        [self layoutIfNeeded];
    }];
}

- (void)dismiss
{
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0);
        self.contentViewBottom.constant = - self.contentViewHeight.constant;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

#pragma mark - XibFunction
- (IBAction)addButtonAction:(UIButton *)sender {
    [self dismiss];
    EditAddressController *vc = [[EditAddressController alloc] init];
    vc.isNew = YES;
    WeakSelf
    [vc setOnAddAddress:^{
        [weakSelf requestForAddressList];
    }];
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

- (IBAction)closeButtonAction:(UIButton *)sender {
    [self dismiss];
}
@end
