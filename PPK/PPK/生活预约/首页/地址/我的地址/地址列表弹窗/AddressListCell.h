//
//  AddressListCell.h
//  PPK
//
//  Created by null on 2022/4/1.
//

#import <UIKit/UIKit.h>
#import "AddressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddressListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (nonatomic, strong) AddressModel *model;
@end

NS_ASSUME_NONNULL_END
