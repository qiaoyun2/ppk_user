//
//  LMTabBarController.m
//  PPK
//
//  Created by null on 2022/3/6.
//

#import "LMTabBarController.h"
#import "NavigationController.h"
#import "LMJobListController.h"
#import "LMWorkerListController.h"
#import "LMCollectionController.h"
#import "LMVipCenterController.h"


@interface LMTabBarController ()<UITabBarControllerDelegate, UITabBarDelegate>

@end

@implementation LMTabBarController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBar.tintColor = MainColor;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    // Do any additional setup after loading the view.
    self.tabBar.translucent = NO;

    [self setTabBar];
    self.selectedIndex = 0;
    self.delegate = self;
    // 设置一个自定义 View,大小等于 tabBar 的大小
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    // 给自定义 View 设置颜色
    bgView.backgroundColor = RGB(255, 255, 255);
    // 将自定义 View 添加到 tabBar 上
    [self.tabBar insertSubview:bgView atIndex:0];
    // 未选择颜色设置
    [self.tabBar setUnselectedItemTintColor:UIColorFromRGB(0xBFBFBF)];
    // 设置选择颜色
    self.tabBar.tintColor = MainColor;
//    [self.tabBar setBackgroundImage:[UIImage new]];
//    [self.tabBar setShadowImage:[UIImage new]];
//    self.tabBar.layer.shadowColor = [UIColor lightGrayColor].CGColor;
//    self.tabBar.layer.shadowOffset = CGSizeMake(0, -1);
//    self.tabBar.layer.shadowOpacity = 0.3;
}
- (void)setTabBar {
    
    /**** 添加子控制器 ****/
    [self setupOneChildViewController:[LMJobListController new] title:@"找工作" image:@"组 14956" selectedImage:@"组 14956-1"];
    [self setupOneChildViewController:[LMWorkerListController new] title:@"找工人" image:@"组 14957" selectedImage:@"组 14957-1"];
    [self setupOneChildViewController:[LMCollectionController new] title:@"收藏" image:@"组 14958" selectedImage:@"组 14958-1"];
    [self setupOneChildViewController:[LMVipCenterController new] title:@"会员" image:@"组 14959" selectedImage:@"组 14959-1"];
}

- (void)setupOneChildViewController:(UIViewController *)vc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage {
    if (title.length) { // 图片名有具体值，判断图片传入值是空还是nil
        UIImage *tabImage = [UIImage imageNamed:image];
        tabImage = [tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.image = tabImage;
        UIImage *selecttabImage = [UIImage imageNamed:selectedImage];
        selecttabImage = [selecttabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = selecttabImage;
        
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        //颜色属性
        attributes[NSForegroundColorAttributeName] = UIColorFromRGB(0xBFBFBF);
        //字体大小属性
        //还有一些其他属性的key可以去NSAttributedString.h文件里去找
        attributes[NSFontAttributeName] = [UIFont systemFontOfSize:10];
        
        NSMutableDictionary *selectAttri = [NSMutableDictionary dictionary];
        selectAttri[NSForegroundColorAttributeName] = MainColor;
        selectAttri[NSFontAttributeName] = [UIFont systemFontOfSize:10];
        
        vc.tabBarItem.title = title;
        //设置为选中状态的文字属性
        [vc.tabBarItem setTitleTextAttributes:attributes forState:UIControlStateNormal];
        //设置选中状态的属性
        [vc.tabBarItem setTitleTextAttributes:selectAttri forState:UIControlStateSelected];
        
//        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(-4, 0, 4, 0);
//        [vc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -9)];
    }
    
    NavigationController *nav = [[NavigationController alloc] initWithRootViewController:vc];
    [self addChildViewController:nav];
}

//判断是否跳转
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    if ([tabBarController.tabBar.selectedItem.title isEqualToString:@"收藏"] || [tabBarController.tabBar.selectedItem.title isEqualToString:@"会员"]) {
        NavigationController *nav = tabBarController.selectedViewController;
        if ([LJTools panduanLoginWithViewContorller:nav.viewControllers[0] isHidden:NO]){
            return YES;
        }
        return NO;
    }else{
        return YES;
    }
}

@end
