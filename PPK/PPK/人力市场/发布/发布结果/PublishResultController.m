//
//  PublishResultController.m
//  PPK
//
//  Created by null on 2022/3/10.
//

#import "PublishResultController.h"

@interface PublishResultController ()
@property (weak, nonatomic) IBOutlet UILabel *successLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *realNameButton;
@property (weak, nonatomic) IBOutlet UIButton *topButton;

@end

@implementation PublishResultController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"结果";
    
    BOOL isVip = YES;
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"实名认证会提升联系机率哦"];
    [string addAttributes:@{NSForegroundColorAttributeName: MainColor} range:NSMakeRange(0, 4)];
    [self.realNameButton setAttributedTitle:string forState:UIControlStateNormal];
    self.realNameButton.hidden = !isVip;
    
    if (self.type==0) {
        // 发布招工
        self.successLabel.text = isVip ? @"尊贵的会员，您的招工信息发布成功且享受会员特权已自动置顶！":@"您的招工信息发布成功!";
    }else {
        // 发布找活
        self.successLabel.text = isVip ? @"尊贵的会员，您的找活信息发布成功且享受会员特权已自动置顶！":@"您的找活信息发布成功!!";
    }
}

- (IBAction)topButtonAction:(UIButton *)sender {
}

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
