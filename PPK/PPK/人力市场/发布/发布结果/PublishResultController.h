//
//  PublishResultController.h
//  PPK
//
//  Created by null on 2022/3/10.
//

#import "NavigationController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PublishResultController : BaseViewController

@property (nonatomic, assign) NSInteger type; // 0：发布招工  1：发布找活

@end

NS_ASSUME_NONNULL_END
