//
//  CitySheetView.h
//  PPK
//
//  Created by null on 2022/3/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CitySheetView : UIView

@property (nonatomic, strong) NSString *selectedStr;
@property (nonatomic, copy) void(^didSelectBlock)(NSString *selectedStr);/**<选择的返回类型回调 */

- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
