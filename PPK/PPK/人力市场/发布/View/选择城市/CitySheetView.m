//
//  CitySheetView.m
//  PPK
//
//  Created by null on 2022/3/10.
//

#import "CitySheetView.h"
#import "ProvinceCell.h"
#import "ProvinceModel.h"

@interface CitySheetView ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@property (weak, nonatomic) IBOutlet UITableView *provinceTableView;
@property (weak, nonatomic) IBOutlet UITableView *cityTableView;
@property (weak, nonatomic) IBOutlet UITableView *districtTableView;

@property (nonatomic, strong) NSMutableArray *provinces;
@property (nonatomic, strong) NSMutableArray *selectedCitys;
@property (nonatomic, strong) ProvinceModel *lastProvince;

//@property (nonatomic, strong) CityModel *lastCity;

@end

@implementation CitySheetView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.hidden = YES;
    self.selectedCitys = [NSMutableArray array];
    self.contentViewHeight.constant = 560+bottomBarH;
    self.contentViewBottom.constant = - self.contentViewHeight.constant;
//    [self.contentView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(12, 12) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, self.contentViewHeight.constant)];
    
    self.provinceTableView.delegate = self;
    self.provinceTableView.dataSource = self;
    
    self.cityTableView.delegate = self;
    self.cityTableView.dataSource = self;
    
//    self.districtTableView.delegate = self;
//    self.districtTableView.dataSource = self;

    NSString *mainBundleDirectory=[[NSBundle mainBundle] bundlePath];
    NSString *path=[mainBundleDirectory stringByAppendingPathComponent:@"province.json"];
    NSURL *url=[NSURL fileURLWithPath:path];
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    self.provinces = [NSMutableArray array];
    for (NSDictionary *obj in dataArray) {
         ProvinceModel *model = [[ProvinceModel alloc] initWithDictionary:obj];
        [self.provinces addObject:model];
    }
}

#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.provinceTableView) {
        return self.provinces.count;
    }
    
    if (self.lastProvince) {
        return self.lastProvince.citys.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProvinceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProvinceCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ProvinceCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if (tableView == self.provinceTableView) {
        ProvinceModel *model = self.provinces[indexPath.row];
        cell.nameLabel.text = model.name;
        cell.badgeView.hidden = !model.selected;
        if (model == self.lastProvince) {
            cell.lineImageView.hidden = NO;
            cell.nameLabel.textColor = MainColor;
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }
        else {
            cell.lineImageView.hidden = YES;
            cell.nameLabel.textColor = UIColorFromRGB(0x333333);
            cell.contentView.backgroundColor = UIColorFromRGB(0xF6F7F9);
        }
        return cell;
    }
    
    if (tableView == self.cityTableView) {
        CityModel *cityModel = self.lastProvince.citys[indexPath.row];
        cell.nameLabel.text = cityModel.name;
        cell.nameLabel.textColor = [self.selectedCitys containsObject:cityModel] ? MainColor : UIColorFromRGB(0x333333);
        return cell;
    }

    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.provinceTableView) {
        self.lastProvince = self.provinces[indexPath.row];
        [self.provinceTableView reloadData];
        [self.cityTableView reloadData];
        return;
    }

    CityModel *cityModel = self.lastProvince.citys[indexPath.row];
    if ([self.selectedCitys containsObject:cityModel]) {
        [self.selectedCitys removeObject:cityModel];
    }
    else {
        if (self.selectedCitys.count>=3) {
            [LJTools showText:@"最多选择三个城市" delay:1.5];
            return;
        }
        [self.selectedCitys addObject:cityModel];
    }
    self.lastProvince.selected = [self checkProvinceIsSelected];
    [self.provinceTableView reloadData];
    [self.cityTableView reloadData];
}

#pragma mark - Function
- (BOOL)checkProvinceIsSelected
{
    for (CityModel *model in self.selectedCitys) {
        if ([model.pname isEqualToString:self.lastProvince.name]) {
            return YES;
        }
    }
    return NO;
}

- (void)show
{
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0.4);
        self.contentViewBottom.constant = 0;
        [self layoutIfNeeded];
    }];
}

- (void)dismiss
{
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0);
        self.contentViewBottom.constant = - self.contentViewHeight.constant;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

- (void)setSelectedStr:(NSString *)selectedStr
{
    _selectedStr = selectedStr;
    [self.selectedCitys removeAllObjects];
    NSArray *citys = [selectedStr componentsSeparatedByString:@","];
    for (ProvinceModel *model in self.provinces) {
        NSArray *cityArray = model.citys;
        for (CityModel *cityModel in cityArray) {
            if ([citys containsObject:cityModel.name]) {
                [self.selectedCitys addObject:cityModel];
                model.selected = YES;
            }
        }
    }
    [self.provinceTableView reloadData];
    [self.cityTableView reloadData];
}

#pragma mark - XibFunction
- (IBAction)confirmButtonClick:(UIButton *)sender {
    if (!self.selectedCitys.count) {
        [LJTools showText:@"请选择期望工作地" delay:1.5];
        return;
    }

    NSMutableArray *citys = [NSMutableArray array];
    for (CityModel *model in self.selectedCitys) {
        [citys addObject:model.name];
    }
    if (self.didSelectBlock) {
        self.didSelectBlock([citys componentsJoinedByString:@","]);
    }
    [self dismiss];
}

- (IBAction)closeButtonClick:(id)sender {
    [self dismiss];
}

@end
