//
//  PayResultView.h
//  PPK
//
//  Created by null on 2022/3/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PayResultView : UIView
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *payStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *payTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *payTimeLabel;

@property (nonatomic, copy) void(^onClosed)(void);

//展示出现
-(void)showPopView;
//隐藏出现
-(void)dismissPopView;

@end

NS_ASSUME_NONNULL_END
