//
//  LMPublishTipView.h
//  PPK
//
//  Created by null on 2022/3/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LMPublishTipView : UIView
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;

@property (nonatomic, copy) void(^onPublishButtonClick)(void);

//展示出现
-(void)showPopView;
//隐藏出现
-(void)dismissPopView;

@end

NS_ASSUME_NONNULL_END
