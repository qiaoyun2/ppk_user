//
//  JobTypeSheetView.h
//  PPK
//
//  Created by null on 2022/3/10.
//

#import <UIKit/UIKit.h>
#import "LMJobTypeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface JobTypeSheetView : UIView


@property (nonatomic, copy) void(^didSelectedType)(LMJobTypeModel *typeModel,NSArray *childModels);

- (void)setFirstId:(NSString *)firstId typeIds:(NSString *)typeIds;


- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
