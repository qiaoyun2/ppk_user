//
//  JobTypeSheetView.m
//  PPK
//
//  Created by null on 2022/3/10.
//

#import "JobTypeSheetView.h"
#import "ProvinceCell.h"
#import "LMJobTypeModel.h"
#import "FeedbackViewController.h"

@interface JobTypeSheetView ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;


@property (weak, nonatomic) IBOutlet UITableView *cateTableView;
@property (weak, nonatomic) IBOutlet UITableView *subCateTableView;

@property (nonatomic, assign) NSInteger cateIndex;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableArray *seletedSubModels; // 已选择二级分类

@property (nonatomic, strong) NSString *firstId;
@property (nonatomic, strong) NSString *typeIds;


@end


@implementation JobTypeSheetView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.hidden = YES;
    self.contentViewHeight.constant = 560+bottomBarH;
    self.contentViewBottom.constant = - self.contentViewHeight.constant;
//    [self.contentView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(12, 12) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, self.contentViewHeight.constant)];
    
    self.cateTableView.delegate = self;
    self.cateTableView.dataSource = self;
    
    self.subCateTableView.delegate = self;
    self.subCateTableView.dataSource = self;
    
    self.dataArray = [NSMutableArray array];
    self.seletedSubModels = [NSMutableArray array];
    self.cateIndex = 0;
    
    [self requestForWorkType];
}

#pragma mark - Network
- (void)requestForWorkType
{
    [NetworkingTool getWithUrl:kWorkTypeURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        self.dataArray = [NSMutableArray array];
        if ([responseObject[@"code"] integerValue]==1) {
            NSArray *dataArray = responseObject[@"data"];
//            [self insertAllMenu];
            [dataArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                LMJobTypeModel *model = [[LMJobTypeModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.cateTableView reloadData];
        [self.subCateTableView reloadData];
        [self defaultSelected];

    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.dataArray.count==0) {
        return 0;
    }
    if (tableView==self.cateTableView) {
        return self.dataArray.count;
    }
    LMJobTypeModel *model = self.dataArray[self.cateIndex];
    return model.childList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProvinceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProvinceCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ProvinceCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.nameLabel.textAlignment = NSTextAlignmentLeft;
    
    // 一级分类
    if (tableView == self.cateTableView) {
        LMJobTypeModel *model = self.dataArray[indexPath.row];
        cell.nameLabel.text = model.workName;
        if (indexPath.row == self.cateIndex) {
            cell.lineImageView.hidden = NO;
            cell.nameLabel.textColor = MainColor;
            cell.contentView.backgroundColor = UIColorFromRGB(0xF6F7F9);
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }else {
            cell.lineImageView.hidden = YES;
            cell.nameLabel.textColor = UIColorFromRGB(0x333333);
            cell.contentView.backgroundColor = UIColorFromRGB(0xF6F7F9);
        }
        return cell;
    }
    
    // 二级分类
    LMJobTypeModel *model = self.dataArray[self.cateIndex];
    LMJobTypeModel *subModel = model.childList[indexPath.row];
    if (tableView == self.subCateTableView) {
        cell.nameLabel.text = subModel.workName;
        if ([self.seletedSubModels containsObject:subModel]) {
            cell.nameLabel.textColor = MainColor;
        }else {
            cell.nameLabel.textColor = UIColorFromRGB(0x333333);
        }
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 一级分类
    if (tableView == self.cateTableView) {
        if (indexPath.row != self.cateIndex) {
            self.cateIndex = indexPath.row;
            // 切换1级分类是否重置选择
            [self.seletedSubModels removeAllObjects];
            [self.cateTableView reloadData];
            [self.subCateTableView reloadData];
        }
        return;
    }
    
    // 二级分类
    LMJobTypeModel *model = self.dataArray[self.cateIndex];
    LMJobTypeModel *subModel = model.childList[indexPath.row];
    
    if ([self.seletedSubModels containsObject:subModel]) {
        [self.seletedSubModels removeObject:subModel];
    }else {
        [self.seletedSubModels addObject:subModel];
        if (self.seletedSubModels.count>3) {
            [self.seletedSubModels removeObjectAtIndex:0];
        }
    }
    [self.subCateTableView reloadData];

}

#pragma mark - Function
- (void)show
{
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0.4);
        self.contentViewBottom.constant = 0;
        [self layoutIfNeeded];
    }];
}

- (void)dismiss
{
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0);
        self.contentViewBottom.constant = - self.contentViewHeight.constant;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

- (void)setFirstId:(NSString *)firstId typeIds:(NSString *)typeIds
{
    self.firstId = firstId;
    self.typeIds = typeIds;
    [self defaultSelected];
}

- (void)defaultSelected
{
    if (!self.firstId) {
        return;
    }
    if (!self.typeIds) {
        return;
    }
    if (!self.dataArray.count) {
        return;
    }
    [self.seletedSubModels removeAllObjects];
    [self.dataArray enumerateObjectsUsingBlock:^(LMJobTypeModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([model.ID isEqualToString:self.firstId]) {
            NSArray *subModels = model.childList;
            NSArray *idArray = [self.typeIds componentsSeparatedByString:@","];
            [subModels enumerateObjectsUsingBlock:^(LMJobTypeModel *subModel, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([idArray containsObject:subModel.ID]) {
                    [self.seletedSubModels addObject:subModel];
                }
            }];
            self.cateIndex = idx;
        }
    }];
    [self.cateTableView reloadData];
    [self.subCateTableView reloadData];
}

#pragma mark - XibFunction
- (IBAction)addJobButtonClick:(id)sender {
    [self dismiss];
    FeedbackViewController *vc = [[FeedbackViewController alloc] init];
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

- (IBAction)confirmButtonClick:(UIButton *)sender {
    if (!self.seletedSubModels.count) {
        [LJTools showText:@"请选择工种" delay:1.5];
        return;
    }
    
    LMJobTypeModel *model = self.dataArray[self.cateIndex];
    if (self.didSelectedType) {
        self.didSelectedType(model, self.seletedSubModels);
    }
    [self dismiss];
}

- (IBAction)closeButtonClick:(id)sender {
    [self dismiss];
}

@end
