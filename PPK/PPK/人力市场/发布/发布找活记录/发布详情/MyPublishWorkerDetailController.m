//
//  MyPublishWorkerDetailController.m
//  PPK
//
//  Created by null on 2022/3/22.
//

#import "MyPublishWorkerDetailController.h"
#import "LMWorkerDetailCell.h"
#import "LMPublishApplyJobController.h"

@interface MyPublishWorkerDetailController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *managerView;
@property (weak, nonatomic) IBOutlet UIButton *blueEditButton;
@property (weak, nonatomic) IBOutlet UIButton *whiteEditButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;
@property (weak, nonatomic) IBOutlet UIButton *upButton;

@property (nonatomic, strong) LMWorkerDetailModel *detailModel;

@end

@implementation MyPublishWorkerDetailController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"发布详情";
    if (self.status==1) {
        self.blueEditButton.hidden = self.downButton.hidden = NO;
        self.whiteEditButton.hidden = self.upButton.hidden = YES;
    }else {
        self.blueEditButton.hidden = self.downButton.hidden = YES;
        self.whiteEditButton.hidden = self.upButton.hidden = NO;
    }
    [self requestForWorkerDetail];
}

#pragma mark - Network
- (void)requestForWorkerDetail
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"workerId"] = self.workerId;
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度
    [NetworkingTool getWithUrl:kMarketWorkerDetailURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.detailModel = [[LMWorkerDetailModel alloc] initWithDictionary:dataDic];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForDownOrUp
{
    [NetworkingTool postWithUrl:kMarketWorkerUpOrDownURL params:@{@"workerId":self.workerId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        [LJTools showText:responseObject[@"msg"] delay:1.5];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LMWorkerDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LMWorkerDetailCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"LMWorkerDetailCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.model = self.detailModel;
    cell.tipView.hidden = YES;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

#pragma mark - XibFunction
- (IBAction)editButtonAction:(id)sender {
    LMPublishApplyJobController *vc = [[LMPublishApplyJobController alloc] init];
    vc.workerId = self.workerId;
    WeakSelf
    [vc setOnEditSuccess:^{
        [weakSelf requestForWorkerDetail];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)downOrUpButtonAction:(UIButton *)sender {
    [self requestForDownOrUp];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
