//
//  MyPublishWorkerDetailController.h
//  PPK
//
//  Created by null on 2022/3/22.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyPublishWorkerDetailController : BaseViewController

@property (nonatomic, strong) NSString *workerId;
@property (nonatomic, assign) NSInteger status;

@end

NS_ASSUME_NONNULL_END
