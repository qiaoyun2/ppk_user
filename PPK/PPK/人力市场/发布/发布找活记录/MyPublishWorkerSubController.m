//
//  MyPublishWorkerSubController.m
//  PPK
//
//  Created by null on 2022/3/12.
//

#import "MyPublishWorkerSubController.h"
#import "LMWorkerListCell.h"
#import "LMPublishApplyJobController.h" // 修改
#import "MyPublishWorkerDetailController.h" // 详情

@interface MyPublishWorkerSubController () <UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, assign) NSInteger page;

@end

@implementation MyPublishWorkerSubController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refresh];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
}

#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(15);
    params[@"status"] = @(self.status); // 状态，0已下架 1审核通过-上架 2审核中 3审核失败
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度

    WeakSelf
    [NetworkingTool getWithUrl:kMarketWorkerMyReleaseURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                LMWorkerListModel *model = [[LMWorkerListModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

- (void)requestForDownOrUp:(LMWorkerListModel *)model
{
    [NetworkingTool postWithUrl:kMarketWorkerUpOrDownURL params:@{@"workerId":model.ID} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self.dataArray removeObject:model];
            [self.tableView reloadData];
        }
        [LJTools showText:responseObject[@"msg"] delay:1.5];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForTop:(LMWorkerListModel *)model
{
    [NetworkingTool postWithUrl:kTopMyWorkerURL params:@{@"workerId":model.ID} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            model.isTop = @"1";
            [self.tableView reloadData];
        }
        [LJTools showText:responseObject[@"msg"] delay:1.5];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForDelete:(LMWorkerListModel *)model
{
    [NetworkingTool postWithUrl:kMarketWorkerDeleteURL params:@{@"id":model.ID} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self.dataArray removeObject:model];
            [self.tableView reloadData];
        }
        [LJTools showText:responseObject[@"msg"] delay:1.5];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LMWorkerListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LMWorkerListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"LMWorkerListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    LMWorkerListModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    cell.managerView.hidden = NO;
    cell.viewNumButton.hidden = YES;
    if (self.status==1) {
        cell.blueEditButton.hidden = cell.downButton.hidden = NO;
        cell.whiteEditButton.hidden = cell.upButton.hidden = cell.deleteButton.hidden = YES;
    }else {
        cell.blueEditButton.hidden = cell.downButton.hidden = YES;
        cell.whiteEditButton.hidden = cell.upButton.hidden = cell.deleteButton.hidden = NO;
    }
    WeakSelf
    [cell setOnEditButtonClick:^{
        [weakSelf pushToEditController:model];
    }];
    [cell setOnDownOrUpButtonClick:^{
        [weakSelf requestForDownOrUp:model];
    }];
    [cell setOnTopButtonClick:^{
        [weakSelf requestForTop:model];
    }];
    [cell setOnDeleteButtonClick:^{
        [UIAlertController alertViewNormalWithTitle:@"温馨提示" message:@"确定要删除吗？" titlesArry:@[@"确定"] indexBlock:^(NSInteger index, id obj) {
            [weakSelf requestForDelete:model];
        } okColor:MainColor cancleColor:[UIColor lightGrayColor] isHaveCancel:YES];
    }];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LMWorkerListModel *model = self.dataArray[indexPath.row];
    MyPublishWorkerDetailController *vc = [[MyPublishWorkerDetailController alloc] init];
    vc.workerId = model.ID;
    vc.status = self.status;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)pushToEditController:(LMWorkerListModel *)model
{
    LMPublishApplyJobController *vc = [[LMPublishApplyJobController alloc] init];
    vc.workerId = model.ID;
    [self.navigationController pushViewController:vc animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
