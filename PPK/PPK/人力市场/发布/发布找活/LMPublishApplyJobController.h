//
//  LMPublishApplyJobController.h
//  PPK
//
//  Created by null on 2022/3/10.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMPublishApplyJobController : BaseViewController

@property (nonatomic, copy) void(^onEditSuccess)(void);

@property (nonatomic, strong) NSString *workerId;

@end

NS_ASSUME_NONNULL_END
