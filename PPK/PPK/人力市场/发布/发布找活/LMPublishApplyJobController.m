//
//  LMPublishApplyJobController.m
//  PPK
//
//  Created by null on 2022/3/10.
//

#import "LMPublishApplyJobController.h"
#import <HXPhotoPicker/HXPhotoPicker.h>
#import "HXAssetManager.h"
#import "UploadManager.h"
#import "JobTypeSheetView.h"
#import "CitySheetView.h"
#import "SelectAddressViewController.h"
#import "ImageCell.h"
#import "LMWorkerDetailModel.h"

@interface LMPublishApplyJobController () <HXPhotoViewDelegate,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIStackView *typeStackView;
@property (weak, nonatomic) IBOutlet UILabel *workCityLabel;
@property (weak, nonatomic) IBOutlet UILabel *liveCityLabel;
@property (weak, nonatomic) IBOutlet UIView *descriptionView;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *mobileField;
@property (weak, nonatomic) IBOutlet UIButton *codeButton;
@property (weak, nonatomic) IBOutlet UITextField *codeField;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoViewHeight;
@property (weak, nonatomic) IBOutlet UIView *codeView;

@property (nonatomic, strong) JobTypeSheetView *typeView; // 工种类型
@property (nonatomic, strong) CitySheetView *cityView;

//@property (nonatomic, strong) LMJobTypeModel *lastTypeModel;
//@property (nonatomic, strong) NSArray *subTypeModels;

@property (nonatomic, assign) BOOL isVideo;
@property (nonatomic, strong) NSMutableArray<UIImage *> *photos;
@property (nonatomic, strong) HXPhotoModel *videoModel;
@property (nonatomic, strong) NSString *videoPath;
@property (nonatomic, strong) NSMutableArray *imagePathArray;

@property (nonatomic, strong) NSString *workTypeIdFirst;
@property (nonatomic, strong) NSString *workTypeIds;
@property (nonatomic, strong) NSArray *keywords;
@property (nonatomic, strong) NSString *liveCity; // 当前所在地
@property (nonatomic, strong) NSString *workCity; // 期望工作的城市
@property (nonatomic, strong) NSString *liveDetailAddress; // 当前所在地详细地址
//@property (nonatomic, strong) NSString *workContent;
//@property (nonatomic, strong) NSString *contactsName;
//@property (nonatomic, strong) NSString *contactsMobile;

@property (nonatomic, assign) CGFloat latitude;
@property (nonatomic, assign) CGFloat longitude;
@property (nonatomic, strong) NSString *district;

@property (strong, nonatomic) HXPhotoManager *manager;
@property (nonatomic, strong) LMWorkerDetailModel *detailModel;

@end

@implementation LMPublishApplyJobController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"发布找活";
    [self.collectionView registerNib:[UINib nibWithNibName:@"ImageCell" bundle:nil] forCellWithReuseIdentifier:@"ImageCell"];
    self.textView.jk_placeHolderTextView.text = @"请输入自我介绍...";
    self.photos = [NSMutableArray array];
    self.imagePathArray = [NSMutableArray array];
    [self updateCollectionViewHeight];
    self.codeView.hidden = YES;
    self.codeButton.hidden = YES;
    self.mobileField.text = [User getUser].mobile;
    [self.mobileField addTarget:self action:@selector(mobileFieldEditChanged:) forControlEvents:UIControlEventEditingChanged];
    if (self.workerId.length>0) {
        [self requestForWorkerDetail];
    }
}

#pragma mark - 网络请求
- (void)requestForSendCode:(UIButton *)sender
{
    NSDictionary *params = @{@"mobile":_mobileField.text,@"event":@"verifyPhone"};
    [NetworkingTool postWithUrl:kGetCodeURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [sender setCountdown:60 WithStartString:@"" WithEndString:@"获取验证码"];
        }
        [LJTools showOKHud:responseObject[@"msg"] delay:1];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

- (void)requestForWorkerDetail
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"workerId"] = self.workerId;
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度
    [NetworkingTool getWithUrl:kMarketWorkerDetailURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.detailModel = [[LMWorkerDetailModel alloc] initWithDictionary:dataDic];
            [self updateSubviews];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}


- (void)requestForSubmit
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"workTypeIdFirst"] = self.workTypeIdFirst;
    params[@"workTypeIds"] = self.workTypeIds;
    params[@"workCity"] = self.workCity;
    params[@"workDetailAddress"] = @"";
    params[@"liveCity"] = self.liveCity;
    params[@"liveDetailAddress"] = self.liveDetailAddress;
    params[@"longitude"] = @(self.longitude); // 经度
    params[@"latitude"] = @(self.latitude); // 纬度
    
    params[@"contactsName"] = self.nameField.text;
    params[@"contactsMobile"] = self.mobileField.text;
    params[@"selfIntroduce"] = self.textView.text;
    params[@"event"] = @"verifyPhone";
    params[@"captcha"] = self.codeField.text;
    params[@"isTop"] = @"0";

    if (self.workerId.length>0) {
        params[@"workerId"] = self.workerId;
    }
    
    if (self.isVideo) {
        params[@"isVideo"] = @"1";
        params[@"videoPath"] = self.videoPath;
        params[@"videoPicture"] = [self.imagePathArray firstObject];
        params[@"picture"] = @"";
    }else {
        if (self.imagePathArray.count) {
            params[@"isVideo"] = @"0";
            params[@"videoPath"] = @"";
            params[@"videoPicture"] = @"";
            params[@"picture"] = [self.imagePathArray componentsJoinedByString:@","];
        }
    }
    NSString *url = self.workerId.length>0 ? kMarketWorkerEditURL:kPublishMarketWorkerURL;
    [NetworkingTool postWithUrl:url params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSString *str = self.workerId.length>0 ? @"修改成功":@"发布成功";
            [LJTools showText:str delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (self.onEditSuccess) {
                    self.onEditSuccess();
                }
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
    
}

- (void)requestForUploadVideo
{
    NSData *data = [NSData dataWithContentsOfURL:self.videoModel.videoURL];
    WeakSelf
    [UploadManager uploadWithVideoData:data block:^(NSString * _Nonnull videoUrl) {
        NSLog(@"%@",videoUrl);
        weakSelf.videoPath = videoUrl;
    }];
}

- (void)requestForUploadImages
{
    WeakSelf
    [UploadManager uploadImageArray:self.photos block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
        NSArray *array = [imageUrl componentsSeparatedByString:@","];
        [weakSelf.imagePathArray addObjectsFromArray:array];
        [weakSelf.collectionView reloadData];
        [weakSelf updateCollectionViewHeight];
    }];
}

#pragma mark - Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger max = self.isVideo ? 1 : 9;
    if (self.imagePathArray.count>=max) {
        return max;
    }
    return self.imagePathArray.count+1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCell" forIndexPath:indexPath];
    if (indexPath.row < self.imagePathArray.count) {
        cell.errorBtn.hidden = NO;
        [cell.thumbImageV sd_setImageWithURL:[NSURL URLWithString:kImageUrl(self.imagePathArray[indexPath.row])] placeholderImage:DefaultImgWidth];
        WeakSelf
        [cell setDeleteBlock:^{
            if (weakSelf.isVideo) {
                weakSelf.isVideo = NO;
                weakSelf.videoPath = @"";
            }
            [weakSelf.imagePathArray removeObjectAtIndex:indexPath.row];
            [weakSelf.collectionView reloadData];
            [weakSelf updateCollectionViewHeight];
        }];
    }
    else {
        cell.errorBtn.hidden = YES;
        cell.thumbImageV.image = [UIImage imageNamed:@"组 50936"];
        return cell;
    }
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.imagePathArray.count) {
        
        if (self.isVideo) {
            self.manager.type = HXPhotoManagerSelectedTypeVideo;
        }
        else {
            if (self.imagePathArray.count==0) {
                self.manager.type = HXPhotoManagerSelectedTypePhotoAndVideo;
                self.manager.configuration.photoMaxNum=9;
            }
            else {
                self.manager.type = HXPhotoManagerSelectedTypePhoto;
                self.manager.configuration.photoMaxNum=9-self.imagePathArray.count;
            }
        }
        
        [self.manager clearSelectedList];
        [self hx_presentSelectPhotoControllerWithManager:self.manager didDone:^(NSArray<HXPhotoModel *> * _Nullable allList, NSArray<HXPhotoModel *> * _Nullable photos, NSArray<HXPhotoModel *> * _Nullable videos, BOOL isOriginal, UIViewController * _Nullable viewController, HXPhotoManager * _Nullable manager) {
            if (videos.count>0) {
                self.isVideo = YES;
                self.videoModel = [videos firstObject];
                [self.photos removeAllObjects];
                WeakSelf
                [self.videoModel getImageWithSuccess:^(UIImage * _Nullable image, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
                    [weakSelf.photos addObject:image];
                    [weakSelf requestForUploadImages];
                    [weakSelf requestForUploadVideo];
                } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                    
                }];
            }
            else {
                self.isVideo = NO;
                self.videoModel = nil;
                [self.photos removeAllObjects];
                for (HXPhotoModel *model in photos) {
                    [self.photos addObject:[HXAssetManager originImageForAsset:model.asset]];
                }
                [self requestForUploadImages];
            }
        } cancel:^(UIViewController * _Nullable viewController, HXPhotoManager * _Nullable manager) {
            
        }];

    }else {
         /// 图片浏览
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH-88-18-20)/3, (SCREEN_WIDTH-88-18-20)/3-1);
}



#pragma mark - Function
- (void)reloadTypeStackView
{
    NSArray *array = self.typeStackView.arrangedSubviews;
    for (UILabel *label in array) {
        [self.typeStackView removeArrangedSubview:label];
        [label removeFromSuperview];
    }
    [self.keywords enumerateObjectsUsingBlock:^(NSString *name, NSUInteger idx, BOOL * _Nonnull stop) {
        UILabel *label = [[UILabel alloc] init];
        label.textColor = UIColorFromRGB(0x333333);
        label.font = [UIFont systemFontOfSize:14];
        label.text = name;
//        label.height = 17;
        [self.typeStackView addArrangedSubview:label];
    }];
}

- (void)mobileFieldEditChanged:(UITextField *)field
{
    if ([field.text isEqualToString:[User getUser].mobile]) {
        self.codeView.hidden = YES;
        self.codeButton.hidden = YES;
    }else {
        self.codeView.hidden = NO;
        self.codeButton.hidden = NO;
    }
}

- (void)updateSubviews
{
    self.workTypeIdFirst = _detailModel.workTypeIdFirst;
    self.workTypeIds = _detailModel.workTypeIds;
    self.keywords = _detailModel.keywords;
    self.longitude = [_detailModel.longitude floatValue];
    self.latitude = [_detailModel.latitude floatValue];

    self.isVideo = [_detailModel.isVideo integerValue];
    
    self.workCity = _detailModel.workCity;
    self.liveCity = _detailModel.liveCity;
    self.liveDetailAddress = _detailModel.liveDetailAddress;
//    self.workContent = _detailModel.workContent;
//    self.contactsName = _detailModel.contactsName;
//    self.contactsMobile = _detailModel.contactsMobile;
    if (_detailModel.picture.length>0) {
        self.imagePathArray = [NSMutableArray arrayWithArray:[_detailModel.picture componentsSeparatedByString:@","]];
    }
    else {
        if ([_detailModel.isVideo integerValue]==1 && _detailModel.videoPicture.length>0) {
            self.imagePathArray = [NSMutableArray arrayWithArray:@[_detailModel.videoPicture]];
        }
    }
    [self.collectionView reloadData];
    [self updateCollectionViewHeight];
    [self reloadTypeStackView];
    self.workCityLabel.text = _detailModel.workCity;
    self.workCityLabel.textColor = UIColorFromRGB(0x333333);
    self.liveCityLabel.text = _detailModel.liveCity;
    self.liveCityLabel.textColor = UIColorFromRGB(0x333333);
    self.textView.text = _detailModel.selfIntroduce;
    self.nameField.text = _detailModel.contactsName;
    self.mobileField.text = _detailModel.contactsMobile;
    if ([self.mobileField.text isEqualToString:[User getUser].mobile]) {
        self.codeView.hidden = YES;
        self.codeButton.hidden = YES;
    }else {
        self.codeView.hidden = NO;
        self.codeButton.hidden = NO;
    }
}

- (void)updateCollectionViewHeight
{
    NSInteger max = self.isVideo ? 1:9;
    NSInteger num = self.imagePathArray.count == max ? self.imagePathArray.count : self.imagePathArray.count+1;
    NSInteger row = num%3 == 0 ? num/3 : num/3+1;
    self.photoViewHeight.constant = row*(SCREEN_WIDTH-88-18-20)/3 + (row-1)*10;
}

#pragma mark - XibFunction
- (IBAction)typeViewTap:(id)sender {
    if (!self.typeView) {
        JobTypeSheetView *typeView = [[[NSBundle mainBundle] loadNibNamed:@"JobTypeSheetView" owner:nil options:nil] firstObject];
        typeView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:typeView];
        self.typeView = typeView;
    }
    // 设置默认选中数据
    [self.typeView setFirstId:self.workTypeIdFirst typeIds:self.workTypeIds];
    [self.typeView show];
    WeakSelf
    [self.typeView setDidSelectedType:^(LMJobTypeModel * _Nonnull typeModel, NSArray * _Nonnull childModels) {
        NSMutableArray *subIdArray = [NSMutableArray array];
        NSMutableArray *titleArray = [NSMutableArray array];
        for (LMJobTypeModel *model in childModels) {
            [subIdArray addObject:model.ID];
            [titleArray addObject:model.workName];
        }
        weakSelf.workTypeIdFirst = typeModel.ID;
        weakSelf.workTypeIds = [subIdArray componentsJoinedByString:@","];
        weakSelf.keywords = [NSArray arrayWithArray:titleArray];
        [weakSelf reloadTypeStackView];
    }];
}

- (IBAction)workCityViewTap:(id)sender {
    if (!self.cityView) {
        CitySheetView *cityView = [[[NSBundle mainBundle] loadNibNamed:@"CitySheetView" owner:nil options:nil] firstObject];
        cityView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:cityView];
        self.cityView = cityView;
    }
    if (self.workCity.length>0) {
        self.cityView.selectedStr = self.workCity;
    }
    [self.cityView show];
    WeakSelf
    [self.cityView setDidSelectBlock:^(NSString * _Nonnull selectedStr) {
        weakSelf.workCity = selectedStr;
        weakSelf.workCityLabel.text = selectedStr;
        weakSelf.workCityLabel.textColor = UIColorFromRGB(0x333333);
    }];
}

- (IBAction)liveCityViewTap:(id)sender {
    WeakSelf
    SelectAddressViewController *vc = [[SelectAddressViewController alloc] init];
    if (self.longitude) {
        vc.coordinate = CLLocationCoordinate2DMake(self.latitude, self.longitude);
    }
    if (self.workCity) {
        vc.city = self.liveCity;
    }
    vc.Block = ^(AMapPOI * _Nonnull model,NSString *detailAddress) {
        weakSelf.liveCity = model.city;
        weakSelf.district = model.district;
        weakSelf.liveDetailAddress = detailAddress;
        weakSelf.latitude = model.location.latitude;
        weakSelf.longitude = model.location.longitude;
        weakSelf.liveCityLabel.text = model.city;
        weakSelf.liveCityLabel.textColor = UIColorFromRGB(0x333333);
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)codeButtonAction:(UIButton *)sender {
    if (self.mobileField.text.length==0) {
        [LJTools showText:@"请输入手机号" delay:1.5];
        return;
    }
    
    if (![self.mobileField.text isTelephone]) {
        [LJTools showText:@"请输入正确的手机号" delay:1.5];
        return;
    }
    [self requestForSendCode:sender];
}

- (IBAction)publishButtonAction:(UIButton *)sender {
    if (self.keywords.count==0) {
        [LJTools showText:@"请选择工种" delay:1.5];
        return;
    }
    if (!self.workCity.length) {
        [LJTools showText:@"请选择期望工作地" delay:1.5];
        return;
    }
    if (!self.liveCity.length) {
        [LJTools showText:@"请选择当前所在地" delay:1.5];
        return;
    }
    
    if (self.nameField.text.length==0) {
        [LJTools showText:@"请输入联系人姓名" delay:1.5];
        return;
    }

    if (self.mobileField.text.length==0) {
        [LJTools showText:@"请输入手机号" delay:1.5];
        return;
    }
    
    if (![self.mobileField.text isTelephone]) {
        [LJTools showText:@"请输入正确的手机号" delay:1.5];
        return;
    }
    
    if (![self.mobileField.text isEqualToString:[User getUser].mobile]) {
        if (self.codeField.text.length==0) {
            [LJTools showText:@"请输入验证码" delay:1.5];
            return;
        }
    }

    if (self.textView.text.length==0) {
        [LJTools showText:@"请输入自我介绍" delay:1.5];
        return;
    }
    
//    if (!self.imagePathArray.count) {
//        [LJTools showText:@"请上传图片或者视频" delay:1.5];
//        return;
//    }
    
    [self requestForSubmit];

}

#pragma mark - Lazy Load
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
        _manager.configuration.type = HXConfigurationTypeWXChat;
        _manager.configuration.singleSelected = NO;
        _manager.configuration.lookGifPhoto = NO;
        _manager.configuration.useWxPhotoEdit = YES;
        _manager.configuration.selectTogether=NO;
        _manager.configuration.photoEditConfigur.onlyCliping = YES;
        _manager.configuration.showOriginalBytes = NO;
        _manager.configuration.videoMaxNum=1;
        _manager.configuration.photoMaxNum=9;
        _manager.configuration.videoAutoPlayType = HXVideoAutoPlayTypeNormal;
        _manager.configuration.requestImageAfterFinishingSelection = YES;
        _manager.configuration.photoEditConfigur.aspectRatio = HXPhotoEditAspectRatioType_None;
    }
    return _manager;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
