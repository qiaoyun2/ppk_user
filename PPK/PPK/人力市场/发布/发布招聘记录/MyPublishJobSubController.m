//
//  MyPublishJobSubController.m
//  PPK
//
//  Created by null on 2022/3/12.
//

#import "MyPublishJobSubController.h"
#import "MyPublishJobDetailController.h"
#import "LMJobListCell.h"
#import "LMPublishJobController.h"

@interface MyPublishJobSubController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign) NSInteger page;

@end

@implementation MyPublishJobSubController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refresh];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];

}

#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(15);
    params[@"status"] = @(self.status); // 状态，0已下架 1审核通过-上架 2审核中 3审核失败
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度

    WeakSelf
    [NetworkingTool getWithUrl:kMarketJobMyReleaseURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                LMJobListModel *model = [[LMJobListModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

- (void)requestForDownOrUp:(LMJobListModel *)model
{
    [NetworkingTool postWithUrl:kMarketJobUpOrDownURL params:@{@"jobId":model.ID} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self.dataArray removeObject:model];
            [self.tableView reloadData];
        }
        [LJTools showText:responseObject[@"msg"] delay:1.5];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForTop:(LMJobListModel *)model
{
    [NetworkingTool postWithUrl:kTopMyJobURL params:@{@"jobId":model.ID} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            model.isTop = @"1";
            [self.tableView reloadData];
        }
        [LJTools showText:responseObject[@"msg"] delay:1.5];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForDelete:(LMJobListModel *)model
{
    [NetworkingTool postWithUrl:kMarketJobDeleteURL params:@{@"id":model.ID} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self.dataArray removeObject:model];
            [self.tableView reloadData];
        }
        [LJTools showText:responseObject[@"msg"] delay:1.5];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LMJobListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LMJobListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"LMJobListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    LMJobListModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    [cell.cityButton setTitle:model.workCity forState:UIControlStateNormal];
    cell.callButton.hidden = YES;
    cell.managerView.hidden = NO;
    if (self.status==1) {
        cell.blueEditButton.hidden = cell.downButton.hidden = NO;
        cell.whiteEditButton.hidden = cell.upButton.hidden = cell.deleteButton.hidden = YES;
    }else {
        cell.blueEditButton.hidden = cell.downButton.hidden = YES;
        cell.whiteEditButton.hidden = cell.upButton.hidden = cell.deleteButton.hidden = NO;
    }
    WeakSelf
    [cell setOnEditButtonClick:^{
        [weakSelf pushToEditController:model];
    }];
    [cell setOnDownOrUpButtonClick:^{
        [weakSelf requestForDownOrUp:model];
    }];
    [cell setOnTopButtonClick:^{
        [weakSelf requestForTop:model];
    }];
    [cell setOnDeleteButtonClick:^{
        [UIAlertController alertViewNormalWithTitle:@"温馨提示" message:@"确定要删除吗？" titlesArry:@[@"确定"] indexBlock:^(NSInteger index, id obj) {
            [weakSelf requestForDelete:model];
        } okColor:MainColor cancleColor:[UIColor lightGrayColor] isHaveCancel:YES];
    }];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LMJobListModel *model = self.dataArray[indexPath.row];
    MyPublishJobDetailController *vc = [[MyPublishJobDetailController alloc] init];
    vc.jobId = model.ID;
    vc.status = self.status;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Function
- (void)pushToEditController:(LMJobListModel *)model
{
    LMPublishJobController *vc = [[LMPublishJobController alloc] init];
    vc.jobId = model.ID;
    [self.navigationController pushViewController:vc animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
