//
//  MyPublishJobDetailController.h
//  PPK
//
//  Created by null on 2022/3/15.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyPublishJobDetailController : BaseViewController

@property (nonatomic, strong) NSString *jobId;
@property (nonatomic, assign) NSInteger status;

@end

NS_ASSUME_NONNULL_END
