//
//  MyPublishJobDetailController.m
//  PPK
//
//  Created by null on 2022/3/15.
//

#import "MyPublishJobDetailController.h"
#import "LMJobDetailCell.h"
#import "LMPublishJobController.h"

@interface MyPublishJobDetailController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *managerView;
@property (weak, nonatomic) IBOutlet UIButton *blueEditButton;
@property (weak, nonatomic) IBOutlet UIButton *whiteEditButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;
@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) LMJobDetailModel *detailModel;

@end

@implementation MyPublishJobDetailController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"发布详情";
    if (self.status==1) {
        self.blueEditButton.hidden = self.downButton.hidden = NO;
        self.whiteEditButton.hidden = self.upButton.hidden = YES;
    }else {
        self.blueEditButton.hidden = self.downButton.hidden = YES;
        self.whiteEditButton.hidden = self.upButton.hidden = NO;
    }
    [self requestForJobDetail];
}

#pragma mark - Network
- (void)requestForJobDetail
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"jobId"] = self.jobId;
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度
    [NetworkingTool getWithUrl:kMarketJobDetailURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.detailModel = [[LMJobDetailModel alloc] initWithDictionary:dataDic];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForDownOrUp
{
    [NetworkingTool postWithUrl:kMarketJobUpOrDownURL params:@{@"jobId":self.jobId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        [LJTools showText:responseObject[@"msg"] delay:1.5];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LMJobDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LMJobDetailCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"LMJobDetailCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.tipView.hidden = YES;
    cell.callButton.hidden = YES;
    cell.certifyStackViewRight.constant=12;
    cell.model = self.detailModel;
    return cell;
}


#pragma mark - Function
- (IBAction)editButtonAction:(id)sender {
    LMPublishJobController *vc = [[LMPublishJobController alloc] init];
    vc.jobId = self.jobId;
    WeakSelf
    [vc setOnEditSuccess:^{
        [weakSelf requestForJobDetail];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)downOrUpButtonAction:(UIButton *)sender {
    [self requestForDownOrUp];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
