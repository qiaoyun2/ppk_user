//
//  PayViewController.m
//  PPK
//
//  Created by null on 2022/3/10.
//

#import "PayViewController.h"
#import "WXApiManager.h"
#import "TOResultController.h"
#import "RWReserveResultController.h"

@interface PayViewController () <WXApiManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UIButton *wxPayButton;
@property (weak, nonatomic) IBOutlet UIButton *aliPayButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *payButtonBottom;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (nonatomic, strong) UIButton *lastButton;

@end

@implementation PayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"支付";
    self.view.backgroundColor = UIColorFromRGB(0xF7F8FC);
    
    self.payButtonBottom.constant = 28+bottomBarH;
    
    self.priceLabel.attributedText = [LJTools attributedString:[NSString stringWithFormat:@"%.2lf",[self.price doubleValue]] color:UIColorFromRGB(0xFA2033) oneHeight:12 andTColor:UIColorFromRGB(0xFA2033) twoHeight:18 andThreeTColor:UIColorFromRGB(0xFA2033) threeHeight:18];
    
}

#pragma mark - Network
- (void)requestForAddOrder
{
    NSDictionary * parmars = @{
        @"order_type":@"16",
        //        @"order_money":self.money,
        //        @"payable_money":self.money,
        //        @"pay_type":bottomSelectModel.pay_type,
        //        @"level_id":self.level_id,
        @"is_wholesale":@"0"
    };
    [NetworkingTool postWithUrl:@"" params:parmars success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"]intValue]==SUCCESS) {
            NSString *order_sn = responseObject[@"data"][@"order_sn"];
            self.order_sn= order_sn;
            if (self.wxPayButton.selected) {
                [self requestForWxPay];
            } else {
                [self requestForAliPay];
            }
        }else{
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForWxPay
{
    [NetworkingTool postWithUrl:@"" params:@{@"order_sn":self.order_sn} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            [self wechatPayWithDic:dataDic];
        }else{
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForAliPay
{
    [NetworkingTool postWithUrl:@"" params:@{@"order_sn":self.order_sn} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self alipayWithMessage:responseObject[@"data"]];
        }else{
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - WXApiManagerDelegate
- (void)managerDidRecvPaymentResponse:(PayResp *)response{
    if (response.errCode==0) {
        User *user = [User getUser];
        [User saveUser:user];
    }
}

#pragma mark - Function
// 唤起微信支付
- (void)wechatPayWithDic:(NSDictionary *)payDic
{
    PayReq *request = [[PayReq alloc] init];
    request.openID = payDic[@"appid"];//公众账号ID
    request.partnerId = payDic[@"partnerid"];//微信支付分配的商户号
    request.prepayId = payDic[@"prepayid"];//预支付交易会话ID
    request.nonceStr = payDic[@"noncestr"];//随机字符串
    request.timeStamp = [payDic[@"timestamp"] intValue];//时间戳
    request.package = payDic[@"package"];//扩展字段(默认 Sign=WXPay)
    request.sign = payDic[@"sign"];//签名
    [WXApiManager sharedManager].delegate=self;
    ///发起支付
    [WXApi sendReq:request completion:^(BOOL success) {
        if (success) {
            [self paySuccessAction];
            
        }
        else{
            [LJTools showNOHud:@"发起支付失败" delay:1.0];
        }
    }];
}

// 唤起支付宝支付
- (void)alipayWithMessage:(NSString *)msg
{
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"vipPay"];
    
    [[AlipaySDK defaultService] payOrder:msg fromScheme:AlipayScheme callback:^(NSDictionary *resultDic) {
        NSLog(@"-------%@", resultDic);
        //        NSString *Memo = [resultDic objectForKey:@"memo"];
        NSString *resultStatus = [resultDic objectForKey:@"resultStatus"];
        if ([resultStatus isEqualToString:@"9000"])
        {
            
            [self paySuccessAction];
            
            //            PaySuccessfulVC *vc=[[PaySuccessfulVC alloc] init];
            //            vc.isVip=1;
            //            vc.payType=@"支付宝支付";
            ////            [self getUserInfo];
            //            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [LJTools showText:@"支付失败" delay:1.5];
        }
    }];
}
#pragma mark - 支付成功后的操作
- (void) paySuccessAction {
    [LJTools showText:@"支付成功" delay:1.5];
    if (self.type == 1) {
        
        RWReserveResultController *vc = [RWReserveResultController new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark - XibFunction
- (IBAction)wxPayButtonAction:(UIButton *)sender {
    sender.selected = YES;
    self.aliPayButton.selected = NO;
}

- (IBAction)aliPayButtonAction:(UIButton *)sender {
    sender.selected = YES;
    self.wxPayButton.selected = NO;
}

- (IBAction)bottomButtonAction:(id)sender {
    TOResultController *vc = [[TOResultController alloc] init];
    [vc.detailButton setTitle:@"查看订单" forState:UIControlStateNormal];
    vc.payTypeLabel.text = @"支付方式：微信支付";
    vc.payTimeLabel.text = @"支付时间：2019.12.06 12:00";
    [self.navigationController pushViewController:vc animated:YES];
}


@end
