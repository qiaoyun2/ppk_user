//
//  PayViewController.h
//  PPK
//
//  Created by null on 2022/3/10.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PayViewController : BaseViewController
@property (nonatomic ,copy) NSString *price;
@property (nonatomic ,copy) NSString *order_sn;
///类型  0 外卖订单支付 1,维修支付 
@property (nonatomic ,assign) NSInteger  type;

@end

NS_ASSUME_NONNULL_END
