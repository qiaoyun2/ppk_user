//
//  ImageCell.m
//  ZZR
//
//  Created by null on 2019/12/28.
//  Copyright © 2019 null. All rights reserved.
//

#import "ImageCell.h"
#import <Photos/Photos.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <libkern/OSAtomic.h>

static CGFloat kMaxPixel = 4096.0;


@implementation ImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.auxiliaryLabel.layer.masksToBounds = YES;
    self.thumbImageV.layer.masksToBounds = YES;
}

- (void)setData:(id)data {
    _data = data;

    self.thumbImageV.image = nil;
    CGFloat padding = 5, imageViewLength = ([UIScreen mainScreen].bounds.size.width - padding * 2) / 3 - 10, scale = [UIScreen mainScreen].scale;
    CGSize imageViewSize = CGSizeMake(imageViewLength * scale, imageViewLength * scale);
    
    if ([data isKindOfClass:PHAsset.class]) {
        
        PHAsset *phAsset = (PHAsset *)data;
        PHImageRequestOptions *options = [PHImageRequestOptions new];
        options.synchronous = NO;
        options.resizeMode = PHImageRequestOptionsResizeModeFast;
        [[PHImageManager defaultManager] requestImageForAsset:phAsset targetSize:CGSizeMake(200, 200) contentMode:PHImageContentModeAspectFit options:options resultHandler:^(UIImage *result, NSDictionary *info){
            BOOL downloadFinined = ![[info objectForKey:PHImageCancelledKey] boolValue] && ![info objectForKey:PHImageErrorKey] && ![[info objectForKey:PHImageResultIsDegradedKey] boolValue];
            if (downloadFinined && result) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.data == data) self.thumbImageV.image = result;
                });
            }
        }];
        
        if (phAsset.mediaType == PHAssetMediaTypeVideo) {
            self.auxiliaryImageView.hidden = NO;
            self.auxiliaryLabel.hidden = YES;

        } else {
            self.auxiliaryImageView.hidden = YES;
            self.auxiliaryLabel.hidden = YES;
        }
        
    } else if ([data isKindOfClass:NSString.class]) {
        
        NSString *imageStr = (NSString *)data;
        __block BOOL isBigImage = NO, isLongImage = NO;
        
        if ([imageStr hasSuffix:@".mp4"]) {
            
            AVURLAsset *avAsset = nil;
            if ([imageStr hasPrefix:@"http"]) {
                avAsset = [AVURLAsset assetWithURL:[NSURL URLWithString:imageStr]];
            } else {
                NSString *path = [[NSBundle mainBundle] pathForResource:imageStr.stringByDeletingPathExtension ofType:imageStr.pathExtension];
                avAsset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:path]];
            }
            
            if (avAsset) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    AVAssetImageGenerator *generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:avAsset];
                    generator.appliesPreferredTrackTransform = YES;
                    generator.maximumSize = imageViewSize;
                    NSError *error = nil;
                    CGImageRef cgImage = [generator copyCGImageAtTime:CMTimeMake(0, 1) actualTime:NULL error:&error];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self.data == data) self.thumbImageV.image = [UIImage imageWithCGImage:cgImage];
                    });
                });
            }
            
        } else if ([imageStr hasPrefix:@"http"]) {
            
            [self.thumbImageV sd_setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:DefaultImgWidth];
            
        } else {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSString *type = imageStr.pathExtension;
                NSString *resource = imageStr.stringByDeletingPathExtension;
                NSString *filePath = [[NSBundle mainBundle] pathForResource:resource ofType:type];
                NSData *nsData = [NSData dataWithContentsOfFile:filePath];
                UIImage *image = [UIImage imageWithData:nsData];
                
                if (image.size.width * image.scale * image.size.height * image.scale > kMaxPixel * kMaxPixel) isBigImage = YES;
                if (image.size.width * image.scale > kMaxPixel || image.size.height * image.scale > kMaxPixel) isLongImage = YES;
                
                if (isBigImage) {
                    CGSize size = CGSizeMake(imageViewSize.width, image.size.height / image.size.width * imageViewSize.width);
                    UIGraphicsBeginImageContext(size);
                    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
                    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self.data == data) self.thumbImageV.image = scaledImage;
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self.data == data) self.thumbImageV.image = DefaultImgWidth;
                    });
                }
            });
        }
        
        if ([imageStr hasSuffix:@".gif"]) {
            self.auxiliaryImageView.hidden = YES;
            self.auxiliaryLabel.hidden = NO;
            self.auxiliaryLabel.text = @" GIF ";
        } else if (isBigImage) {
            self.auxiliaryImageView.hidden = YES;
            self.auxiliaryLabel.hidden = NO;
            self.auxiliaryLabel.text = @" 高清图 ";
        } else if (isLongImage) {
            self.auxiliaryImageView.hidden = YES;
            self.auxiliaryLabel.hidden = NO;
            self.auxiliaryLabel.text = @" 长图 ";
        } else {
            self.auxiliaryImageView.hidden = YES;
            self.auxiliaryLabel.hidden = YES;
        }
        
    } else {
        self.thumbImageV.image = nil;
        self.auxiliaryImageView.hidden = YES;
        self.auxiliaryLabel.hidden = YES;
    }
}

- (IBAction)errorBtnClick:(id)sender {
    self.deleteBlock();
}

@end
