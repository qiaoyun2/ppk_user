//
//  ImageCell.h
//  ZZR
//
//  Created by null on 2019/12/28.
//  Copyright © 2019 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbImageV;
@property (weak, nonatomic) IBOutlet UIImageView *auxiliaryImageView;
@property (weak, nonatomic) IBOutlet UILabel *auxiliaryLabel;
@property (weak, nonatomic) IBOutlet UIButton *errorBtn;

@property (nonatomic, strong) id data;

@property (nonatomic, copy) void(^deleteBlock)(void);
@end

NS_ASSUME_NONNULL_END
