//
//  LMPublishJobController.h
//  PPK
//
//  Created by null on 2022/3/9.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMPublishJobController : BaseViewController

@property (nonatomic, copy) void(^onEditSuccess)(void);
@property (nonatomic, strong) NSString *jobId;

@end

NS_ASSUME_NONNULL_END
