//
//  LMWorkerDetailCell.h
//  PPK
//
//  Created by null on 2022/3/9.
//

#import <UIKit/UIKit.h>
#import "LMWorkerDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMWorkerDetailCell : UITableViewCell

@property (nonatomic, strong) LMWorkerDetailModel *model;
@property (weak, nonatomic) IBOutlet UIView *tipView;

@end

NS_ASSUME_NONNULL_END
