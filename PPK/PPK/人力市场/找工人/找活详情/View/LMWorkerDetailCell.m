//
//  LMWorkerDetailCell.m
//  PPK
//
//  Created by null on 2022/3/9.
//

#import "LMWorkerDetailCell.h"
#import "WorkerTagsView.h"
#import "FoundListImage9TypographyView.h"
#import "MyPublishWorkerController.h"
#import <SDCycleScrollView/SDCycleScrollView.h>
#import "LMMapViewController.h"

@interface LMWorkerDetailCell () <SDCycleScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIView *blueView;
@property (weak, nonatomic) IBOutlet UIImageView *urgentImageView;
@property (weak, nonatomic) IBOutlet UIImageView *myUrgentImageView;

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sexLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *realNameTipImageView;
@property (weak, nonatomic) IBOutlet UIImageView *enterpriseTipImageView;


@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *nationalLabel;
@property (weak, nonatomic) IBOutlet UILabel *workYearsLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (weak, nonatomic) IBOutlet UILabel *introduceLabel;

@property (weak, nonatomic) IBOutlet WorkerTagsView *tagsView;
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;

@property (weak, nonatomic) IBOutlet UILabel *guideLabel;
@property (weak, nonatomic) IBOutlet UIView *adView;

@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;

@end

@implementation LMWorkerDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.tagsView.width = SCREEN_WIDTH - 56;
    self.imgsView.is_tap = YES;
//    self.imgsView.imageArray= @[@"girl",@"girl2",@"girl3",@"girl2",@"girl3"];
//    self.imgsView.hidden = YES;
//    self.tagsView.dataArray = @[@"焊工/水电/泥工",@"钳工/司机/力工"];
    
    self.shadowView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    self.shadowView.layer.shadowOffset = CGSizeMake(0,1.5);
    self.shadowView.layer.shadowRadius = 6;
    self.shadowView.layer.shadowOpacity = 1;
    self.shadowView.layer.cornerRadius = 12;
    
    [self.blueView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(12, 12) viewRect:CGRectMake(0, 0, SCREEN_WIDTH-32, 80)];
    
    NSString *guideStr = @"《防骗指南》：此信息由“拼拼看”用户提供，但联系时仍需注意识别信息真伪";
    NSMutableAttributedString *guideAttrStr = [[NSMutableAttributedString alloc] initWithString:guideStr];
    [guideAttrStr addAttributes:@{NSForegroundColorAttributeName: MainColor} range:NSMakeRange(0, 6)];
    [self.guideLabel setAttributedText:guideAttrStr];
}

#pragma mark – SDCycleScrollViewDelegate
/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    NSLog(@"=====> %zd", index);
    WKWebViewController *vc = [[WKWebViewController alloc] init];
    vc.titleStr = _model.adTitle;
    vc.contentStr = _model.adContent;
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

#pragma mark – Function
- (void)setModel:(LMWorkerDetailModel *)model
{
    _model = model;
    if ([model.isTop integerValue]==1) {
        self.urgentImageView.hidden = NO;
        self.myUrgentImageView.hidden = NO;
    }else {
        self.urgentImageView.hidden = YES;
        self.myUrgentImageView.hidden = YES;
    }
    
    self.realNameTipImageView.image = [model.authTypePersonal integerValue] == 1 ? [UIImage imageNamed:@"组 50697"] : [UIImage imageNamed:@"组 50910"];
    self.enterpriseTipImageView.hidden = [model.authTypeCompany integerValue] != 1;
    
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.avatar)] placeholderImage:DefaultImgHeader];
    self.nameLabel.text = model.contactsName;
    // TODO:缺少个人公司字段
    self.sexLabel.text = [model.sex integerValue]==1?@"女":@"男";
//    self.personalLabel.text = @"";
    self.ageLabel.text = model.age;
    
    self.tagsView.dataArray = model.keywords;
    
    self.mobileLabel.text = model.encryptMobile;
    // TODO:缺少工龄 民族字段
    self.cityLabel.text = model.workCity;
//    self.workYearsLabel.text = model.;
//    self.nationalLabel.text = @"";
    self.addressLabel.text = model.liveDetailAddress;
    
    self.introduceLabel.text = model.selfIntroduce;
    if (model.videoPath.length) {
        self.imgsView.videoPath = _model.videoPath;
        self.imgsView.imageArray= @[_model.videoPicture];
    }
    else {
        if (model.picture.length>0) {
            NSArray *images = [model.picture componentsSeparatedByString:@","];
            self.imgsView.imageArray= images;
        }
    }
    
    self.cycleScrollView.imageURLStringsGroup = @[kImageUrl(model.adPicture)];
}

#pragma mark – Lazy
- (SDCycleScrollView *)cycleScrollView {
    if (!_cycleScrollView) {
        _cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-32, (SCREEN_WIDTH-32)*108/343) delegate:self placeholderImage:DefaultImgWidth];
        _cycleScrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        _cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
        _cycleScrollView.clipsToBounds = YES;
        [self.adView addSubview:_cycleScrollView];
    }
    return _cycleScrollView;
}

#pragma mark - XibFunction
- (IBAction)myUrgentImageViewTap:(id)sender {
    MyPublishWorkerController *vc = [[MyPublishWorkerController alloc] init];
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

- (IBAction)freeViewButtonAction:(id)sender {
    self.mobileLabel.text = _model.contactsMobile;
}

- (IBAction)addressViewTap:(id)sender {
    LMMapViewController *vc = [[LMMapViewController alloc] init];
    vc.coordinate = CLLocationCoordinate2DMake([_model.latitude floatValue], [_model.longitude floatValue]);
    vc.city = _model.liveCity;
//    vc.district = _model
    vc.address = _model.liveDetailAddress;
    vc.distance = _model.distance;
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

@end
