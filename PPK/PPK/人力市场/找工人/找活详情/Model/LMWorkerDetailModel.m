//
//  LMWorkerDetailModel.m
//  PPK
//
//  Created by null on 2022/3/14.
//

#import "LMWorkerDetailModel.h"

@implementation LMWorkerDetailModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
    if ([key isEqualToString:@"recommendList"]) {
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *obj in value) {
            LMWorkerListModel *model = [[LMWorkerListModel alloc] initWithDictionary:obj];
            [tempArray addObject:model];
        }
        self.recommendList = [NSArray arrayWithArray:tempArray];
    }
}

@end
