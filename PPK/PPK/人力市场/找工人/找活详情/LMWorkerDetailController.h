//
//  LMWorkerDetailController.h
//  PPK
//
//  Created by null on 2022/3/9.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMWorkerDetailController : BaseViewController

@property (nonatomic, strong) NSString *workerId;

@end

NS_ASSUME_NONNULL_END
