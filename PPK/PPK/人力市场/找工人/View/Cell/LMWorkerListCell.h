//
//  LMWorkerListCell.h
//  PPK
//
//  Created by null on 2022/3/7.
//

#import <UIKit/UIKit.h>
#import "LMWorkerListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMWorkerListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (weak, nonatomic) IBOutlet UIView *managerView;
@property (weak, nonatomic) IBOutlet UIButton *blueEditButton;
@property (weak, nonatomic) IBOutlet UIButton *whiteEditButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;
@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIButton *topButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;


@property (weak, nonatomic) IBOutlet UIButton *viewNumButton;

@property (nonatomic, strong) LMWorkerListModel *model;

@property (nonatomic, copy) void(^onSelectedButtonClick)(BOOL isSelected);
@property (nonatomic, copy) void(^onEditButtonClick)(void);
@property (nonatomic, copy) void(^onDownOrUpButtonClick)(void);
@property (nonatomic, copy) void(^onTopButtonClick)(void);
@property (nonatomic, copy) void(^onDeleteButtonClick)(void);

@end

NS_ASSUME_NONNULL_END
