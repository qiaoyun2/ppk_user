//
//  LMWorkerListCell.m
//  PPK
//
//  Created by null on 2022/3/7.
//

#import "LMWorkerListCell.h"
#import "FoundListImage9TypographyView.h"
#import "WorkerTagsView.h"
#import "MyPublishWorkerController.h"

@interface LMWorkerListCell ()


@property (weak, nonatomic) IBOutlet WorkerTagsView *tagsView;
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;
@property (weak, nonatomic) IBOutlet UILabel *urgentLabel; // 急找
@property (weak, nonatomic) IBOutlet UILabel *myUrgentLabel; // 我要急找
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *sexLabel;

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *realNameTipImageView; // 实名认证
@property (weak, nonatomic) IBOutlet UIImageView *enterpriseTipImageView; // 企业
@property (weak, nonatomic) IBOutlet UILabel *workCityLabel;


@property (weak, nonatomic) IBOutlet UIButton *cityButton;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end

@implementation LMWorkerListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.imgsView.is_tap = YES;
}

- (void)setModel:(LMWorkerListModel *)model
{
    _model = model;
    if ([model.isTop integerValue]==1) {
        self.urgentLabel.hidden = NO;
        self.myUrgentLabel.hidden = NO;
    }else {
        self.urgentLabel.hidden = YES;
        self.myUrgentLabel.hidden = YES;
    }
    // TODO:缺少参数
    self.titleLabel.text = model.nickname;
//    self.sexLabel.text = @"男 47岁";
    self.contentLabel.text = model.selfIntroduce;
    self.workCityLabel.text = model.workCity;
    if (model.searchStr.length>0) {
        if ([model.nickname containsString:model.searchStr]) {
            NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:model.nickname];
            NSRange range = [model.nickname rangeOfString:model.searchStr];
            // 改变文字颜色
            [noteStr addAttribute:NSForegroundColorAttributeName value:MainColor range:range];
            // 为label添加Attributed
            [self.titleLabel setAttributedText:noteStr];
        }
        if ([model.selfIntroduce containsString:model.searchStr]) {
            NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:model.selfIntroduce];
            NSRange range = [model.selfIntroduce rangeOfString:model.searchStr];
            // 改变文字颜色
            [noteStr addAttribute:NSForegroundColorAttributeName value:MainColor range:range];
            // 为label添加Attributed
            [self.contentLabel setAttributedText:noteStr];
        }
    }
    
    self.realNameTipImageView.image = [model.authTypePersonal integerValue] == 1 ? [UIImage imageNamed:@"组 50697"] : [UIImage imageNamed:@"组 50910"];
    self.enterpriseTipImageView.hidden = [model.authTypeCompany integerValue] != 1;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.avatar)] placeholderImage:DefaultImgHeader];
    [self.cityButton setTitle:[NSString stringWithFormat:@"%@ %@km",model.liveCity,model.distance] forState:UIControlStateNormal];
    self.timeLabel.text = model.createTime;
    [self.viewNumButton setTitle:[NSString stringWithFormat:@"%@人看过",model.watchNumber] forState:UIControlStateNormal];
    self.selectButton.selected = model.selected;
    if (model.videoPath.length) {
        self.imgsView.videoPath = _model.videoPath;
        self.imgsView.imageArray= @[_model.videoPicture];
    }
    else {
        if (model.picture.length>0) {
            NSArray *images = [model.picture componentsSeparatedByString:@","];
            self.imgsView.imageArray= images;
        }
    }
    self.tagsView.width = SCREEN_WIDTH-86;
    self.tagsView.dataArray = model.keywords;
    self.topButton.hidden = [model.isTop integerValue] == 1;
}

- (IBAction)myUrgentLabelTap:(id)sender {
    MyPublishWorkerController *vc = [[MyPublishWorkerController alloc] init];
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

- (IBAction)selectButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.onSelectedButtonClick) {
        self.onSelectedButtonClick(sender.selected);
    }
}

//- (IBAction)callButtonAction:(UIButton *)sender {
//    
//}

- (IBAction)editButtonAction:(id)sender {
    if (self.onEditButtonClick) {
        self.onEditButtonClick();
    }
}

- (IBAction)downOrUpButtonAction:(UIButton *)sender {
    if (self.onDownOrUpButtonClick) {
        self.onDownOrUpButtonClick();
    }
}

- (IBAction)topButtonAction:(UIButton *)sender {
    if (self.onTopButtonClick) {
        self.onTopButtonClick();
    }
}

- (IBAction)deleteButtonAction:(id)sender {
    if (self.onDeleteButtonClick) {
        self.onDeleteButtonClick();
    }
}

@end
