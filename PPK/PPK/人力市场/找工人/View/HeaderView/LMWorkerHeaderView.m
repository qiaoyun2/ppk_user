//
//  LMWorkerHeaderView.m
//  PPK
//
//  Created by null on 2022/3/7.
//

#import "LMWorkerHeaderView.h"
#import "ScrollNoticeCell.h"
#import <SDCycleScrollView.h>

@interface LMWorkerHeaderView ()<SDCycleScrollViewDelegate>

@property (nonatomic, strong) SDCycleScrollView *noticeView;
@property (nonatomic, strong) NSArray *noticeArray;

@end

@implementation LMWorkerHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];

    _noticeView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, (SCREEN_WIDTH-56), 32) delegate:self placeholderImage:nil];
    _noticeView.scrollDirection = UICollectionViewScrollDirectionVertical;
    _noticeView.localizationImageNamesGroup = @[@"",@""];
    _noticeView.delegate = self;
    _noticeView.showPageControl = NO;
    _noticeView.autoScrollTimeInterval = 3;
    _noticeView.backgroundColor = [UIColor clearColor];
    [_noticeView disableScrollGesture];
    [self.noticeBgView addSubview:_noticeView];
    
}

/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    if (self.noticeArray.count) {
        LMJobHeaderNoticeModel *model = self.noticeArray[index];
        WKWebViewController *vc = [[WKWebViewController alloc] init];
        vc.contentStr = model.content;
        vc.titleStr = model.title;
        [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
    }
}

/** 图片滚动回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index
{
    
}

/** 如果你需要自定义cell样式，请在实现此代理方法返回你的自定义cell的Nib。 */
- (UINib *)customCollectionViewCellNibForCycleScrollView:(SDCycleScrollView *)view
{
    if (view != self.noticeView) {
        return nil;
    }
    return [UINib nibWithNibName:@"ScrollNoticeCell" bundle:nil];
}

/** 如果你自定义了cell样式，请在实现此代理方法为你的cell填充数据以及其它一系列设置 */
- (void)setupCustomCell:(UICollectionViewCell *)cell forIndex:(NSInteger)index cycleScrollView:(SDCycleScrollView *)view
{
    ScrollNoticeCell *myCell = (ScrollNoticeCell *)cell;
    myCell.backgroundColor = [UIColor whiteColor];
    if (self.noticeArray.count) {
        LMJobHeaderNoticeModel *model = self.noticeArray[index];
        myCell.contentLabel.text = model.title;
    }
}


- (void)setModel:(LMJobHeaderModel *)model
{
    _model = model;
    self.mobileLabel.text = model.officialMobile;
    
    self.noticeArray = [NSArray arrayWithArray:_model.notices];
    NSMutableArray *tempAray = [NSMutableArray array];
    for (int i = 0; i<self.noticeArray.count; i++) {
        [tempAray addObject:@"矩形 16382"];
    }
    self.noticeView.imageURLStringsGroup = tempAray;
}

- (IBAction)callButtonAction:(id)sender {
    [LJTools call:_model.officialMobile];
}

@end
