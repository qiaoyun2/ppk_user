//
//  LMWorkerHeaderView.h
//  PPK
//
//  Created by null on 2022/3/7.
//

#import <UIKit/UIKit.h>
#import "ZScrollLabel.h"
#import "LMJobHeaderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMWorkerHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIView *noticeBgView;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (nonatomic, strong) LMJobHeaderModel *model;

@end

NS_ASSUME_NONNULL_END
