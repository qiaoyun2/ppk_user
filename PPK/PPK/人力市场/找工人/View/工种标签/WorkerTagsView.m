//
//  WorkerTagsView.m
//  PPK
//
//  Created by null on 2022/3/8.
//

#import "WorkerTagsView.h"
#import <Masonry.h>

@interface WorkerTagsView ()

//左边的间距
@property (nonatomic, assign) CGFloat leftSpace;
/** 列间距 */
@property (nonatomic, assign) CGFloat rowSpace;
/** 行间距 */
@property (nonatomic, assign) CGFloat sectionSpace;

@property (nonatomic, assign) CGFloat viewHeight;

@end

@implementation WorkerTagsView

// 1、代码加载方式
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}


// 2.xib 加载方式
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if(self = [super initWithCoder:aDecoder]){
    }
    return self;
}

- (void)setDataArray:(NSArray *)dataArray
{
    _dataArray = dataArray;
    [self relaodSubViews];
}

- (void)relaodSubViews
{
    [self removeAllSubviews];
    
//    CGFloat left = 16;
    CGFloat x = 0;
    CGFloat y = 0;
    
    CGFloat lineSpace = 8;
    CGFloat colSpace = 8;
    CGFloat height = 24;

    for (int i = 0; i<self.dataArray.count; i++) {
        CGFloat width  = [[NSString stringWithFormat:@"%@",self.dataArray[i]] commonStringWidthForFont:11]+24;
        if (width > self.bounds.size.width) {
            width = self.bounds.size.width;
        }
        if (x + width + colSpace > (self.frame.size.width)) {
            x = 0;
            y += height + lineSpace;
        }
        UILabel *label = [[UILabel alloc]init];
        label.cornerRadius = 12;
        label.font = [UIFont systemFontOfSize:11 weight:UIFontWeightRegular];
        label.textColor = UIColorFromRGB(0x333333);
        label.textAlignment = NSTextAlignmentCenter;
        label.text = self.dataArray[i];
        label.backgroundColor = UIColorFromRGB(0xF5F6F9);
        [self addSubview:label];

        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(y);
            make.left.mas_equalTo(x);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            if (i==self.dataArray.count-1) {
                make.bottom.equalTo(self);
            }
        }];
//        label.frame = CGRectMake(x, y, width, height);
        NSLog(@"------%@------",NSStringFromCGRect(label.frame));
        x += width + colSpace;
    }
    self.viewHeight = y + height;
    NSLog(@"%f-----------------",self.viewHeight);
}


@end
