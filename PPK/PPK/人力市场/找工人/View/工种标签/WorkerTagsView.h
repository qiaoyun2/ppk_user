//
//  WorkerTagsView.h
//  PPK
//
//  Created by null on 2022/3/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WorkerTagsView : UIView

@property (nonatomic, strong) NSArray *dataArray;

@end

NS_ASSUME_NONNULL_END
