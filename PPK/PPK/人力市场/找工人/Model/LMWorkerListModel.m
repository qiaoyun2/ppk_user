//
//  LMWorkerListModel.m
//  PPK
//
//  Created by null on 2022/3/13.
//

#import "LMWorkerListModel.h"

@implementation LMWorkerListModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
}


@end
