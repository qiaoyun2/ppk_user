//
//  LMWorkerListController.m
//  PPK
//
//  Created by null on 2022/3/7.
//

#import "LMWorkerListController.h"

#import "LMWorkerHeaderView.h"
#import "LMJobSectionHeaderView.h"
#import "LMWorkerListCell.h"
#import "LMJobSectionHeaderView.h"
#import "CityDropDownView.h"
#import "JobTypeDownView.h"
#import "SortDownView.h"
#import "LMWorkerDetailController.h"
#import "LMPublishJobController.h"
#import "LMPublishApplyJobController.h"
#import "LMJobHeaderModel.h"
#import "LMSearchViewController.h"
#import "LMAdvertiseCell.h"

@interface LMWorkerListController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *searchField;

@property (nonatomic, strong) LMWorkerHeaderView *headerView;
@property (nonatomic, strong) LMJobSectionHeaderView *sectionHeaderView;
@property (nonatomic, strong) CityDropDownView *cityView;
@property (nonatomic, strong) JobTypeDownView *jobTypeView;
@property (nonatomic, strong) SortDownView *sortView;

@property (nonatomic, strong) LMJobHeaderModel *headerModel;
@property (nonatomic, strong) LMJobTypeModel *lastTypeModel;
@property (nonatomic, strong) NSArray *subTypeModels;

@property (nonatomic, strong) NSString *sortType;
@property (nonatomic, strong) NSString *city;

@property (nonatomic, assign) NSInteger page;

@end

@implementation LMWorkerListController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    WeakSelf
    self.topViewHeight.constant = NavAndStatusHight;
    self.sortType = @"1";
    self.city = [LJTools getAppDelegate].city;
    [self setupHeaderView];
    [self setupSectionHeaderView];
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationChangeSuccess:) name:LocationChangeSuccess object:nil];
    [self requestForHeaderInfo];
    [self refresh];
}

#pragma mark - UI
- (void)setupHeaderView
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 104)];
    self.tableView.tableHeaderView = headerView;

    self.headerView = [[[NSBundle mainBundle] loadNibNamed:@"LMWorkerHeaderView" owner:nil options:nil] firstObject];
    [headerView addSubview:self.headerView];
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(headerView);
    }];
}

- (void)setupSectionHeaderView
{
    self.sectionHeaderView = [[LMJobSectionHeaderView alloc] init];

    WeakSelf
    [self.sectionHeaderView setOnTitleButtonsClick:^(UIButton * _Nonnull sender) {
        if (!sender.selected) {
            [weakSelf hiddenAllDropDownView];
            CGFloat time = 0.0;
            if (weakSelf.tableView.contentOffset.y<104) {
                [weakSelf.tableView setContentOffset:CGPointMake(0,104) animated:YES];
                time = 0.25;
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(time * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (sender.tag==10) {
                    [weakSelf showCityDropDownView];
    //                [weakSelf.jobTypeView hideDropDown];
    //                [weakSelf.sortView hideDropDown];
                }
                else if (sender.tag==11) {
                    [weakSelf showJobTypeDropDownView];
                }
                else {
                    [weakSelf showSortDropDownView];
                }
            });
            sender.selected = !sender.selected;
        }
        else {
            [weakSelf hiddenAllDropDownView];
        }
    }];
}

#pragma mark - Network
- (void)requestForHeaderInfo
{
    [NetworkingTool getWithUrl:kMarketTopInfoURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            LMJobHeaderModel *headerModel = [[LMJobHeaderModel alloc] initWithDictionary:responseObject[@"data"]];
            self.headerView.model = headerModel;
            self.headerModel = headerModel;
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(15);
    params[@"cityName"] = self.city;
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度
//    params[@"longitude"] = @(113.6401); // 经度
//    params[@"latitude"] = @(34.72468); // 纬度
    
    if (self.lastTypeModel) {
        params[@"workTypeIdFirst"] = self.lastTypeModel.ID; // 一级分类id
    }
    
    if (self.subTypeModels.count) {
        NSMutableArray *array = [NSMutableArray array];
        for (LMJobTypeModel *subModel in self.subTypeModels) {
            [array addObject:subModel.ID];
        }
        NSString *workTypeIds = [array componentsJoinedByString:@","];
        params[@"workTypeIds"] = workTypeIds; // 二级分类id,最多三个
    }
    params[@"type"] = self.sortType; // 1:最新排序；2:距离排序；3:实名认证；4:企业认证
    WeakSelf
    [NetworkingTool getWithUrl:kMarketWorkerListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                LMWorkerListModel *model = [[LMWorkerListModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView withY:250];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LMWorkerListModel *model = self.dataArray[indexPath.row];
    if (model.adPicture.length>0) {
        LMAdvertiseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LMAdvertiseCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"LMAdvertiseCell" owner:nil options:nil] firstObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [cell.adImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.adPicture)] placeholderImage:DefaultImgWidth];
        return cell;
    }
    
    LMWorkerListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LMWorkerListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"LMWorkerListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.model = model;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 43;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.sectionHeaderView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![LJTools panduanLoginWithViewContorller:self isHidden:NO]) {
        return;
    }
    LMWorkerListModel *model = self.dataArray[indexPath.row];
    if (model.adPicture.length>0) {
        WKWebViewController *vc = [[WKWebViewController alloc] init];
        vc.titleStr = model.adTitle;
        vc.contentStr = model.adContent;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    LMWorkerDetailController *vc = [[LMWorkerDetailController alloc] init];
    vc.workerId = model.ID;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Function
- (void)hiddenAllDropDownView
{
    [self.cityView hideDropDown];
    [self.jobTypeView hideDropDown];
    [self.sortView hideDropDown];
    self.sectionHeaderView.cityButton.selected = NO;
    self.sectionHeaderView.jobTypeButton.selected = NO;
    self.sectionHeaderView.sortButton.selected = NO;
}

// 选择城市
- (void)showCityDropDownView
{
    if (!self.cityView) {
        self.cityView = [[[NSBundle mainBundle] loadNibNamed:@"CityDropDownView" owner:nil options:nil] firstObject];
        [self.view addSubview:self.cityView];
    }
    [self.cityView showDropDown];
    WeakSelf
    [self.cityView setDidSelectBlock:^(NSString * _Nonnull city, NSString * _Nonnull district) {
        weakSelf.sectionHeaderView.cityButton.label.text = city;
        weakSelf.city = city;
        [weakSelf refresh];
    }];
    [self.cityView setHiddenBlock:^{
        weakSelf.sectionHeaderView.cityButton.selected = NO;
    }];
}

// 选择工种
- (void)showJobTypeDropDownView
{
    if (!self.jobTypeView) {
        self.jobTypeView = [[[NSBundle mainBundle] loadNibNamed:@"JobTypeDownView" owner:nil options:nil] firstObject];
        [self.view addSubview:self.jobTypeView];
    }

//    self
    self.jobTypeView.seletedSubModels = [NSMutableArray arrayWithArray:self.subTypeModels];
    if (self.lastTypeModel) {
        self.jobTypeView.ID = self.lastTypeModel.ID;
    }
    [self.jobTypeView showDropDown];
    WeakSelf
    [self.jobTypeView setDidSelectedType:^(LMJobTypeModel * _Nonnull typeModel, NSArray * _Nonnull childModels) {
        weakSelf.lastTypeModel = typeModel;
        weakSelf.subTypeModels = [NSArray arrayWithArray:childModels];
        weakSelf.sectionHeaderView.jobTypeButton.label.text = typeModel.workName;
        [weakSelf refresh];
    }];
    
//    [self.jobTypeView setDidReset:^{
//        weakSelf.lastTypeModel = nil;
//        weakSelf.subTypeModels = [NSArray array];
//        weakSelf.sectionHeaderView.jobTypeButton.label.text = @"全部工种";
//        [weakSelf refresh];
//    }];
    
    [self.jobTypeView setHiddenBlock:^{
        weakSelf.sectionHeaderView.jobTypeButton.selected = NO;
    }];
}

// 排序
- (void)showSortDropDownView
{
    if (!self.sortView) {
        self.sortView = [[[NSBundle mainBundle] loadNibNamed:@"SortDownView" owner:nil options:nil] firstObject];
        [self.view addSubview:self.sortView];
    }

    [self.sortView showDropDown];
    WeakSelf
    [self.sortView setDidSelectBlock:^(NSInteger index, NSString * _Nonnull selectedStr) {
        weakSelf.sectionHeaderView.sortButton.label.text = selectedStr;
        weakSelf.sortType = [NSString stringWithFormat:@"%ld",index+1];
        [weakSelf refresh];
    }];
    [self.sortView setHiddenBlock:^{
        weakSelf.sectionHeaderView.sortButton.selected = NO;
    }];
}

- (void)locationChangeSuccess:(NSNotification *)noti
{
    self.city = [LJTools getAppDelegate].city;
    self.sectionHeaderView.cityButton.label.text = self.city;
    [self refresh];
}

#pragma mark - XibFunction
- (IBAction)publishJobButtonAction:(UIButton *)sender {
    if (![LJTools panduanLoginWithViewContorller:self isHidden:NO]) {
        return;
    }
    LMPublishJobController *vc = [[LMPublishJobController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)publishWorkerButtonAction:(UIButton *)sender {
    if (![LJTools panduanLoginWithViewContorller:self isHidden:NO]) {
        return;
    }
    LMPublishApplyJobController *vc = [[LMPublishApplyJobController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)searchButtonAction:(UIButton *)sender {
    LMSearchViewController *vc = [[LMSearchViewController alloc] init];
    vc.type = @"2";
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)topButtonAction:(id)sender {
    [self.tableView setContentOffset:CGPointMake(0,0) animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
