//
//  PersonCertificationController.m
//  PPK
//
//  Created by null on 2022/3/12.
//

#import "PersonCertificationController.h"
#import "LJImagePicker.h"
#import "UploadManager.h"

@interface PersonCertificationController ()

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UIButton *sexButton;
@property (weak, nonatomic) IBOutlet UITextField *mobileField;
@property (weak, nonatomic) IBOutlet UITextField *cardNumField;
@property (weak, nonatomic) IBOutlet UITextField *workYearsField;
@property (weak, nonatomic) IBOutlet UITextField *ageField;
@property (weak, nonatomic) IBOutlet UIButton *frontButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *completeButton;

@property (nonatomic, strong) NSString *frontStr;
@property (nonatomic, strong) NSString *backStr;
@property (nonatomic, assign) NSInteger sex; // 性别 0男1女

@end

@implementation PersonCertificationController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"实名认证";
    self.backButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.frontButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    User *user = [User getUser];
    if ([user.authStatus integerValue]==2) {
        self.nameField.text = user.user_name;
        [self.sexButton setTitle:[user.sex integerValue]==1?@"女":@"男" forState:UIControlStateNormal];
        self.sexButton.selected = YES;
        self.mobileField.text = user.authMobile;
        self.cardNumField.text = user.cardId;
        [self.frontButton sd_setImageWithURL:[NSURL URLWithString:kImageUrl(user.frontCardId)] forState:UIControlStateNormal placeholderImage:DefaultImgWidth];
        [self.backButton sd_setImageWithURL:[NSURL URLWithString:kImageUrl(user.backCardId)] forState:UIControlStateNormal placeholderImage:DefaultImgWidth];
        self.completeButton.alpha = 0.5;
        [self.completeButton setTitle:@"认证已通过" forState:1.5];
    }
}

- (void)requestForCertification
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"username"] = self.nameField.text;
    params[@"sex"] = @(self.sex); // 性别 0男1女
    params[@"authMobile"] = self.mobileField.text;
    params[@"cardId"] =  self.cardNumField.text;
    params[@"workYear"] =  self.workYearsField.text;
    params[@"age"] =  self.ageField.text;
    params[@"frontCardId"] =  self.frontStr;
    params[@"backCardId"] =  self.backStr;
    [NetworkingTool postWithUrl:kAuthenticationPersonURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - XibFunction
- (IBAction)sexButtonAction:(UIButton *)sender {
    WeakSelf
    NSArray *array = @[@"男",@"女"];
    [UIAlertController actionSheetWithTitle:@"选择性别" message:nil titlesArry:array indexBlock:^(NSInteger index, id obj) {
        NSLog(@"%ld",index);
        weakSelf.sex = index;
        [sender setTitle:array[index] forState:UIControlStateNormal];
        sender.selected = YES;
    }];
}

- (IBAction)frontButtonAction:(UIButton *)sender {
    WeakSelf
    [LJImagePicker showImagePickerFromViewController:self allowsEditing:YES finishAction:^(UIImage *image) {
        if (image) {
            [UploadManager uploadImageArray:@[image] block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
//                NSLog(@"")
                weakSelf.frontStr = imageUrl;
                [sender sd_setImageWithURL:[NSURL URLWithString:kImageUrl(imageUrl)] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"组 51102"]];
            }];
        }
    }];
}

- (IBAction)backButtonAction:(id)sender {
    WeakSelf
    [LJImagePicker showImagePickerFromViewController:self allowsEditing:YES finishAction:^(UIImage *image) {
        if (image) {
            [UploadManager uploadImageArray:@[image] block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
    //                NSLog(@"")
                weakSelf.backStr = imageUrl;
                [sender sd_setImageWithURL:[NSURL URLWithString:kImageUrl(imageUrl)] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"组 51102"]];
            }];
        }
    }];
}

- (IBAction)finishButtonAction:(id)sender {
    if (self.nameField.text.length==0) {
        [LJTools showText:@"请输入姓名" delay:1.5];
        return;
    }
    if (!self.sexButton.selected) {
        [LJTools showText:@"请选择性别" delay:1.5];
        return;
    }
    if (self.mobileField.text.length==0) {
        [LJTools showText:@"请输入联系方式" delay:1.5];
        return;
    }
    if (![self.mobileField.text isTelephone]) {
        [LJTools showText:@"请输入正确的联系方式" delay:1.5];
        return;
    }
    if (self.cardNumField.text.length==0) {
        [LJTools showText:@"请输入身份证号" delay:1.5];
        return;
    }
    if (![self.cardNumField.text isidentityCard]) {
        [LJTools showText:@"请输入正确的身份证号" delay:1.5];
        return;
    }
    if (self.workYearsField.text.length==0) {
        [LJTools showText:@"请输入工作年限" delay:1.5];
        return;
    }
    if (self.ageField.text.length==0) {
        [LJTools showText:@"请输入年领" delay:1.5];
        return;
    }
    if (![self.frontStr length]) {
        [LJTools showText:@"请上传正面手持证件照" delay:1.5];
        return;
    }
    if (![self.backStr length]) {
        [LJTools showText:@"请上传反面手持证件照" delay:1.5];
        return;
    }
    [self requestForCertification];
}


@end
