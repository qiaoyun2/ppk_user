//
//  CertificationController.m
//  PPK
//
//  Created by null on 2022/3/12.
//

#import "CertificationController.h"
#import "CompanyCertificationController.h"
#import "PersonCertificationController.h"
#import "CertificationController.h"

@interface CertificationController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;

@end

@implementation CertificationController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.topViewHeight.constant = NavAndStatusHight;
    [self setNavigationRightBarButtonWithTitle:@"认证"];
}

- (IBAction)backButtonAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)companyButtonAction:(UIButton *)sender {
    User *user = [User getUser];
    if ([user.authCompanyStatus integerValue]==1) {
        [LJTools showText:@"您已提交认证，请耐心等待" delay:2];
        return;
    }
    
    CompanyCertificationController *vc = [[CompanyCertificationController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)personButtonAction:(UIButton *)sender {

    User *user = [User getUser];
    if ([user.authStatus integerValue]==1) {
        [LJTools showText:@"您已提交认证，请耐心等待" delay:2];
        return;
    }
    PersonCertificationController *vc = [[PersonCertificationController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
