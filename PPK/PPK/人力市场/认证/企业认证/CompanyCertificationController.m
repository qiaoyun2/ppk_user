//
//  CompanyCertificationController.m
//  PPK
//
//  Created by null on 2022/3/12.
//

#import "CompanyCertificationController.h"
#import "LJImagePicker.h"
#import "UploadManager.h"
#import "SelectAddressViewController.h"

@interface CompanyCertificationController ()

@property (weak, nonatomic) IBOutlet UITextField *companyField;
@property (weak, nonatomic) IBOutlet UITextField *staffNumField;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *mobileField;
@property (weak, nonatomic) IBOutlet UITextField *cardNumField;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UIButton *licenseButton;
@property (weak, nonatomic) IBOutlet UIButton *completeButton;

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *license;


@end

@implementation CompanyCertificationController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"公司认证";
    self.licenseButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    User *user = [User getUser];
    if ([user.authCompanyStatus integerValue]==2) {
        self.view.userInteractionEnabled = NO;
        self.companyField.text = user.companyName;
        self.staffNumField.text = user.companyScale;
        self.mobileField.text = user.authMobile;
        self.nameField.text = user.companyLegal;
        self.cardNumField.text = user.companyCreditCode;

        [self.locationButton setTitle:user.companyAddress forState:UIControlStateNormal];
        self.locationButton.selected = YES;
        [self.licenseButton sd_setImageWithURL:[NSURL URLWithString:kImageUrl(user.companyLicense)] forState:UIControlStateNormal placeholderImage:DefaultImgWidth];
        self.completeButton.alpha = 0.5;
        [self.completeButton setTitle:@"认证已通过" forState:1.5];
    }
}

- (void)requestForCertification
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"companyName"] = self.companyField.text;
    params[@"companyScale"] = self.staffNumField.text;
    params[@"companyLegal"] = self.nameField.text;
    params[@"authMobile"] =  self.mobileField.text;
    params[@"companyCreditCode"] =  self.cardNumField.text;
    params[@"companyAddress"] =  self.address;
    params[@"companyLicense"] =  self.license;
    [NetworkingTool postWithUrl:kAuthenticationCompanyURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (IBAction)locationButtonAction:(UIButton *)sender {
    WeakSelf
    SelectAddressViewController *vc = [[SelectAddressViewController alloc] init];
    vc.Block = ^(AMapPOI * _Nonnull model,NSString *detailAddress) {
        [sender setTitle:detailAddress forState:UIControlStateNormal];
        sender.selected = YES;
        weakSelf.address = detailAddress;
//        weakSelf.cityLabel.text = string;
//        weakSelf.cityLabel.textColor = UIColorFromRGB(0x333333);
//        weakSelf.cityButton.selected = YES;
//        weakSelf.longitude = @(model.location.longitude).stringValue;
//        weakSelf.latitude = @(model.location.latitude).stringValue;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)licenseButtonAction:(UIButton *)sender {
    WeakSelf
    [LJImagePicker showImagePickerFromViewController:self allowsEditing:YES finishAction:^(UIImage *image) {
        if (image) {
            [UploadManager uploadImageArray:@[image] block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
    //                NSLog(@"")
                weakSelf.license = imageUrl;
                [sender sd_setImageWithURL:[NSURL URLWithString:kImageUrl(imageUrl)] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"组 51102"]];
            }];
        }
    }];
}

- (IBAction)finishButtonAction:(id)sender {
    
    if (self.companyField.text.length==0) {
        [LJTools showText:@"请输入公司名称" delay:1.5];
        return;
    }
    if (self.staffNumField.text.length==0) {
        [LJTools showText:@"请输入公司规模" delay:1.5];
        return;
    }
    
    if (self.nameField.text.length==0) {
        [LJTools showText:@"请输入负责人名称" delay:1.5];
        return;
    }

    if (self.mobileField.text.length==0) {
        [LJTools showText:@"请输入联系方式" delay:1.5];
        return;
    }
    if (![self.mobileField.text isTelephone]) {
        [LJTools showText:@"请输入正确的联系方式" delay:1.5];
        return;
    }
    if (self.cardNumField.text.length==0) {
        [LJTools showText:@"请输入统一社会信用代码" delay:1.5];
        return;
    }

    if (![self.address length]) {
        [LJTools showText:@"请选择公司位置" delay:1.5];
        return;
    }
    if (![self.license length]) {
        [LJTools showText:@"请上传营业执照" delay:1.5];
        return;
    }
    [self requestForCertification];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
