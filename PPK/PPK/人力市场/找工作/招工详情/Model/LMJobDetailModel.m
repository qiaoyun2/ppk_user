//
//  LMJobDetailModel.m
//  PPK
//
//  Created by null on 2022/3/13.
//

#import "LMJobDetailModel.h"

@implementation LMJobDetailModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
    if ([key isEqualToString:@"recommendList"]) {
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *obj in value) {
            LMJobListModel *model = [[LMJobListModel alloc] initWithDictionary:obj];
            [tempArray addObject:model];
        }
        self.recommendList = [NSArray arrayWithArray:tempArray];
    }
}


@end
