//
//  LMJobDetailController.m
//  PPK
//
//  Created by null on 2022/3/6.
//

#import "LMJobDetailController.h"
#import "LMJobListCell.h"
#import "LMJobDetailCell.h"
#import "ShareView.h"


@interface LMJobDetailController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet ReLayoutButton *collectionButton;
@property (nonatomic, strong) LMJobDetailModel *detailModel;



@end

@implementation LMJobDetailController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"招工详情";
    [self requestForJobDetail];
//    self.bottomView.layer.shadowColor = r;
}

#pragma mark - Network
- (void)requestForJobDetail
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"jobId"] = self.jobId;
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度
    [NetworkingTool getWithUrl:kMarketJobDetailURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.detailModel = [[LMJobDetailModel alloc] initWithDictionary:dataDic];
            self.collectionButton.selected = [self.detailModel.isCollection integerValue];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForCollection
{
    [NetworkingTool postWithUrl:kMarketColletionURL params:@{@"jobWorkerId":self.jobId,@"type":@"1"} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            self.collectionButton.selected = !self.collectionButton.selected;
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 1;
    }
    return self.detailModel.recommendList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        LMJobDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LMJobDetailCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"LMJobDetailCell" owner:nil options:nil] firstObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.model = self.detailModel;
        return cell;
    }
    
    LMJobListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LMJobListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"LMJobListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSArray *dataArray = self.detailModel.recommendList;
    LMJobListModel *model = dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==1) {
        NSArray *dataArray = self.detailModel.recommendList;
        LMJobListModel *model = dataArray[indexPath.row];
        LMJobDetailController *vc = [[LMJobDetailController alloc] init];
        vc.jobId = model.ID;
        [self.navigationController pushViewController:vc animated:YES];
 
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==0) {
        return [[UIView alloc] init];
    }
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 52)];
    headerView.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, SCREEN_WIDTH-32, 52)];
    label.text = @"附近的工作";
    label.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
    label.textColor = UIColorFromRGB(0x333333);
    [headerView addSubview:label];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0) {
        return CGFLOAT_MIN;
    }
    return 52;
}

#pragma mark - Function
- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType title:(NSString*)title content:(NSString*)content ImageUrl:(NSString*)imageUrl url:(NSString *)url
{
    //创建分享消息对象
    UMSocialMessageObject*messageObject =[UMSocialMessageObject messageObject];
//    NSData *dateImg = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
    
    //创建网页内容对象
    UMShareWebpageObject*shareObject =[UMShareWebpageObject shareObjectWithTitle:title descr:content thumImage:[UIImage imageNamed:@"ppklogo"]];
    //设置网页地址
    shareObject.webpageUrl =url;
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data,NSError*error){
        if(error){
            NSLog(@"************Share fail with error %@*********",error);
        }else{
            NSLog(@"response data is %@",data);
        }
    }];
}

#pragma mark - XibFunction
- (IBAction)shareButtonAction:(UIButton *)sender {
    ShareView *shareView = [[[NSBundle mainBundle] loadNibNamed:@"ShareView" owner:nil options:nil] firstObject];
    shareView.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].keyWindow addSubview:shareView];
    [shareView show];
    WeakSelf
    [shareView setOnPlatformDidSelected:^(NSInteger tag) {
        [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"downLoadUrl"} success:^(NSURLSessionDataTask *task, id responseObject) {
            [LJTools hideHud];
            if ([responseObject[@"code"] intValue] == SUCCESS) {
                NSString *downLoadUrl = (NSString *)responseObject[@"data"];
                [weakSelf shareWebPageToPlatformType:tag+1 title:@"拼拼看" content:@"推荐给好友" ImageUrl:nil url:downLoadUrl];
            } else {
                [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            [LJTools showNOHud:RequestServerError delay:1.0];
        } IsNeedHub:YES];
        
        
    }];
}

- (IBAction)collectionButtonAction:(UIButton *)sender {
    [self requestForCollection];
}

- (IBAction)contactButtonAction:(id)sender {
    [LJTools call:self.detailModel.contactsMobile];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
