//
//  LMJobDetailController.h
//  PPK
//
//  Created by null on 2022/3/6.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMJobDetailController : BaseViewController

@property (nonatomic, strong) NSString *jobId;

@end

NS_ASSUME_NONNULL_END
