//
//  LMJobDetailCell.h
//  PPK
//
//  Created by null on 2022/3/6.
//

#import <UIKit/UIKit.h>
#import "LMJobDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMJobDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *tipView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *certifyStackViewRight;
@property (weak, nonatomic) IBOutlet UIButton *callButton;

@property (nonatomic, strong) LMJobDetailModel *model;

@end

NS_ASSUME_NONNULL_END
