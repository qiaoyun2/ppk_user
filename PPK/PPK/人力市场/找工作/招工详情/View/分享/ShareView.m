//
//  ShareView.m
//  DanMu
//
//  Created by null on 2021/11/26.
//

#import "ShareView.h"

@implementation ShareView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.contentViewHeight.constant = 160+bottomBarH;
    self.contentViewBottom.constant = - self.contentViewHeight.constant;
    [self.contentView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(12, 12) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, self.contentViewHeight.constant)];
}

- (void)show
{
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0.4);
        self.contentViewBottom.constant = 0;
        [self layoutIfNeeded];
    }];
}

- (void)dismiss
{
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0);
        self.contentViewBottom.constant = - self.contentViewHeight.constant;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}
- (IBAction)itemButtonsAction:(UIButton *)sender {
    if (self.onPlatformDidSelected) {
        self.onPlatformDidSelected(sender.tag);
    }
    [self dismiss];
}

- (IBAction)onCloseButtonClick:(id)sender {
    [self dismiss];
}

@end
