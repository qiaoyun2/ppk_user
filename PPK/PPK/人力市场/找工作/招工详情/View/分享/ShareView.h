//
//  ShareView.h
//  DanMu
//
//  Created by null on 2021/11/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShareView : UIView

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@property (nonatomic, copy) void(^onPlatformDidSelected)(NSInteger tag);

- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
