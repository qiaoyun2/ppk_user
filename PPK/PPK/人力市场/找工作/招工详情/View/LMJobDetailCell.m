//
//  LMJobDetailCell.m
//  PPK
//
//  Created by null on 2022/3/6.
//

#import "LMJobDetailCell.h"
#import "FoundListImage9TypographyView.h"
#import <SDCycleScrollView/SDCycleScrollView.h>
#import "JobTagsView.h"
#import "MyPublishJobController.h"
#import "LMMapViewController.h"

@interface LMJobDetailCell ()<SDCycleScrollViewDelegate>

@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *urgentImageView; // 急招
@property (weak, nonatomic) IBOutlet UILabel *myUrgentLabel; // 我要急招

@property (weak, nonatomic) IBOutlet UIView *bossInfoView;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *realNameTipImageView; // 实名认证
@property (weak, nonatomic) IBOutlet UIImageView *enterpriseTipImageView; // 企业
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *callTipLabel;

@property (weak, nonatomic) IBOutlet JobTagsView *keywordsView;

@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;


@property (weak, nonatomic) IBOutlet UILabel *guideLabel;
@property (weak, nonatomic) IBOutlet UIView *bannerBgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *keywordsViewHeight;

@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;

@end

@implementation LMJobDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.keywordsView.dataArray = @[@"塔吊/吊车/指挥/维修",@"升降机/电梯司机/安装/维修",@"渣车/货车/挂车司机"];
//    self.imgsView.imageArray = @[@"girl", @"girl2", @"girl3",@"girl", @"girl2", @"girl3",@"girl", @"girl2", @"girl3"];
    self.cycleScrollView.imageURLStringsGroup = @[@"girl", @"girl2", @"girl3"].copy;
//    self.cycleScrollView.imageURLStringsGroup = @[];
    self.keywordsView.width = SCREEN_WIDTH;
    self.bossInfoView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    self.bossInfoView.layer.shadowOffset = CGSizeMake(0,1.5);
    self.bossInfoView.layer.shadowRadius = 6;
    self.bossInfoView.layer.shadowOpacity = 1;
    self.bossInfoView.layer.cornerRadius = 8;
//    self.bossInfoView.layer.borderColor = UIColorFromRGB(0xBFBFBF).CGColor;
//    self.bossInfoView.layer.borderWidth = 0.8;
    
    NSString *callTiStr = @"联系我时请说明是在“拼拼看”上发现的！";
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:callTiStr];
    [string addAttributes:@{NSForegroundColorAttributeName: MainColor} range:NSMakeRange(10, 3)];
    [self.callTipLabel setAttributedText:string];
    
    NSString *guideStr = @"《防骗指南》：此信息由“拼拼看”用户提供，但联系时仍需注意识别信息真伪";
    NSMutableAttributedString *guideAttrStr = [[NSMutableAttributedString alloc] initWithString:guideStr];
    [guideAttrStr addAttributes:@{NSForegroundColorAttributeName: MainColor} range:NSMakeRange(0, 6)];
    [self.guideLabel setAttributedText:guideAttrStr];
}

#pragma mark – SDCycleScrollViewDelegate

/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    NSLog(@"=====> %zd", index);
    WKWebViewController *vc = [[WKWebViewController alloc] init];
    vc.titleStr = _model.adTitle;
    vc.contentStr = _model.adContent;
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

#pragma mark - Lazy
- (SDCycleScrollView *)cycleScrollView {
    if (!_cycleScrollView) {
        _cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-32, (SCREEN_WIDTH-32)*108/343) delegate:self placeholderImage:DefaultImgWidth];
        _cycleScrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        _cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
        _cycleScrollView.clipsToBounds = YES;
        [self.bannerBgView addSubview:_cycleScrollView];
    }
    return _cycleScrollView;
}

#pragma mark - Setter
- (void)setModel:(LMJobDetailModel *)model
{
    _model = model;
    if ([model.isTop integerValue]==1) {
        self.urgentImageView.hidden = NO;
        self.myUrgentLabel.hidden = NO;
    }else {
        self.urgentImageView.hidden = YES;
        self.myUrgentLabel.hidden = YES;
    }
    self.titleLabel.text = model.workTitle;
    self.timeLabel.text = [NSString stringWithFormat:@"发布时间：%@",model.createTime];

    
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.avatar)] placeholderImage:DefaultImgHeader];
    self.nameLabel.text = model.contactsName;
    self.mobileLabel.text = model.encryptMobile;
    self.realNameTipImageView.image = [model.authTypePersonal integerValue] == 1 ? [UIImage imageNamed:@"组 50697"] : [UIImage imageNamed:@"组 50910"];
    self.enterpriseTipImageView.hidden = [model.authTypeCompany integerValue] != 1;

    self.keywordsView.dataArray = model.keywords;
    
    self.detailLabel.text = model.workContent;
    if (model.videoPath.length) {
        self.imgsView.videoPath = _model.videoPath;
        self.imgsView.imageArray= @[_model.videoPicture];
    }
    else {
        if (model.picture.length>0) {
            NSArray *images = [model.picture componentsSeparatedByString:@","];
            self.imgsView.imageArray= images;
        }
    }
    
    self.addressLabel.text = model.workDetailAddress;
    self.distanceLabel.text = [NSString stringWithFormat:@"%@km",model.distance];
    
    self.cycleScrollView.imageURLStringsGroup = @[kImageUrl(model.adPicture)];
}

#pragma mark - XibFunction
- (IBAction)myUrgentLabelTap:(id)sender {
    MyPublishJobController *vc = [[MyPublishJobController alloc] init];
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

- (IBAction)addressViewTap:(id)sender {
    LMMapViewController *vc = [[LMMapViewController alloc] init];
    vc.coordinate = CLLocationCoordinate2DMake([_model.latitude floatValue], [_model.longitude floatValue]);
    vc.city = _model.workCity;
//    vc.district = _model
    vc.address = _model.workDetailAddress;
    vc.distance = _model.distance;
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

- (IBAction)callButtonAction:(id)sender {
    [LJTools call:_model.contactsMobile];
}

@end
