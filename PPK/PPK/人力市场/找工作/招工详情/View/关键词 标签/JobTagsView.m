//
//  DramaTagsView.m
//  SuWenUser
//
//  Created by null on 2020/9/20.
//  Copyright © 2020 null. All rights reserved.
//

#import "JobTagsView.h"
#import <Masonry.h>
@interface JobTagsView ()
//左边的间距
@property (nonatomic, assign) CGFloat leftSpace;
/** 列间距 */
@property (nonatomic, assign) CGFloat rowSpace;
/** 行间距 */
@property (nonatomic, assign) CGFloat sectionSpace;

@property (nonatomic, assign) CGFloat viewHeight;


@end

@implementation JobTagsView

// 1、代码加载方式
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}


// 2.xib 加载方式
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if(self = [super initWithCoder:aDecoder]){
    }
    return self;
}

- (void)setDataArray:(NSArray *)dataArray
{
    _dataArray = dataArray;
    [self relaodSubViews];
}

- (void)relaodSubViews
{    
    [self removeAllSubviews];
    
//    CGFloat left = 16;
    CGFloat x = 16;
    CGFloat y = 0;
    
    CGFloat lineSpace = 12;
    CGFloat colSpace = 12;
    CGFloat height = 26;

    for (int i = 0; i<self.dataArray.count; i++) {
        CGFloat width  = [[NSString stringWithFormat:@"%@",self.dataArray[i]] commonStringWidthForFont:12]+24;
        if (width > self.bounds.size.width) {
            width = self.bounds.size.width;
        }
        if (x + width + colSpace > (self.bounds.size.width)) {
            x = 16;
            y += height + lineSpace;
        }
        UILabel *label = [[UILabel alloc]init];
        label.cornerRadius = 13;
        label.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        label.textColor = MainColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.text = self.dataArray[i];
        label.backgroundColor = RGBA(0, 162, 234, 0.1);
        [self addSubview:label];

        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(y);
            make.left.mas_equalTo(x);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            if (i==self.dataArray.count-1) {
                make.bottom.equalTo(self);
            }
        }];
//        label.frame = CGRectMake(x, y, width, height);
        NSLog(@"------%@------",NSStringFromCGRect(label.frame));
        x += width + colSpace;
    }
    self.viewHeight = y + height;
    NSLog(@"%f-----------------",self.viewHeight);
}

- (CGFloat)getViewHeight
{
    return self.viewHeight;
}

@end
