//
//  JobTagsView.h
//  SuWenUser
//
//  Created by null on 2020/9/20.
//  Copyright © 2020 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JobTagsView : UIView

@property (nonatomic, strong) NSArray *dataArray;

- (CGFloat)getViewHeight;

@end

NS_ASSUME_NONNULL_END
