//
//  CityDropDownView.h
//  PPK
//
//  Created by null on 2022/3/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CityDropDownView : UIView

@property (nonatomic, copy) void(^didSelectBlock)(NSString *city, NSString *district);/**<选择的返回类型回调 */
@property (nonatomic, copy) void(^hiddenBlock)(void);

@property (nonatomic, assign) BOOL isShowArea;
@property (nonatomic, assign) BOOL hiddenTabbar;
@property (nonatomic, strong) NSString *selectedStr;

- (void)showDropDown; // 显示下拉菜单
- (void)hideDropDown; // 隐藏下拉菜单
- (void)hideDropDownAnimation:(BOOL)animation; // 隐藏下拉菜单

@end

NS_ASSUME_NONNULL_END
