//
//  LMJobSectionHeaderView.h
//  PPK
//
//  Created by null on 2022/3/4.
//

#import <UIKit/UIKit.h>
#import "LMFiltrateButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMJobSectionHeaderView : UIView

@property (nonatomic, strong) LMFiltrateButton *cityButton;
@property (nonatomic, strong) LMFiltrateButton *jobTypeButton;
@property (nonatomic, strong) LMFiltrateButton *sortButton;


@property (nonatomic, copy) void(^onTitleButtonsClick)(UIButton *sender);
- (void)hiddenCityButton;


@end

NS_ASSUME_NONNULL_END
