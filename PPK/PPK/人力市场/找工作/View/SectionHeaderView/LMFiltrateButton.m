//
//  LMFiltrateButton.m
//  PPK
//
//  Created by null on 2022/3/5.
//

#import "LMFiltrateButton.h"

@implementation LMFiltrateButton

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    if (selected) {
        self.label.textColor = MainColor;
        self.iconImageView.image = [UIImage imageNamed:@"组 50692-1"];
    }else {
        self.label.textColor = UIColorFromRGB(0x333333);
        self.iconImageView.image = [UIImage imageNamed:@"组 50692"];
    }
}



@end
