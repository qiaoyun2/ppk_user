//
//  LMJobSectionHeaderView.m
//  PPK
//
//  Created by null on 2022/3/4.
//

#import "LMJobSectionHeaderView.h"

@implementation LMJobSectionHeaderView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        LMFiltrateButton *cityButton = [[[NSBundle mainBundle] loadNibNamed:@"LMFiltrateButton" owner:nil options:nil]firstObject];
        cityButton.label.text = [LJTools getAppDelegate].city;
        cityButton.tag = 10;
        [cityButton addTarget:self action:@selector(buttonsAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:cityButton];
        [cityButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(16);
            make.top.bottom.equalTo(self);
            make.width.mas_greaterThanOrEqualTo(50);
        }];
        self.cityButton = cityButton;
        
        LMFiltrateButton *jobTypeButton = [[[NSBundle mainBundle] loadNibNamed:@"LMFiltrateButton" owner:nil options:nil]firstObject];
        jobTypeButton.label.text = @"选择工种";
        jobTypeButton.tag = 11;
        [jobTypeButton addTarget:self action:@selector(buttonsAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:jobTypeButton];
        [jobTypeButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.centerY.centerX.equalTo(self);
            make.width.mas_lessThanOrEqualTo(120);
        }];
        self.jobTypeButton = jobTypeButton;

        
        LMFiltrateButton *sortButton = [[[NSBundle mainBundle] loadNibNamed:@"LMFiltrateButton" owner:nil options:nil]firstObject];
        sortButton.label.text = @"最新排序";
        sortButton.tag = 12;
        [sortButton addTarget:self action:@selector(buttonsAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:sortButton];
        [sortButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.mas_equalTo(-16);
            make.top.bottom.equalTo(self);
            make.width.mas_greaterThanOrEqualTo(65);
        }];
        self.sortButton = sortButton;

        
        UIView *line = [[UIView alloc] init];
        [self addSubview:line];
        line.backgroundColor = UIColorFromRGB(0xF5F6F9);
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.right.equalTo(self);
            make.height.mas_equalTo(0.5);
        }];

    }
    return self;
}


- (void)hiddenCityButton
{
    self.cityButton.hidden = YES;
    [self.jobTypeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.bottom.equalTo(self);
        make.width.mas_greaterThanOrEqualTo(40);
    }];
    
}


- (void)buttonsAction:(LMFiltrateButton *)sender
{
    if (self.onTitleButtonsClick) {
        self.onTitleButtonsClick(sender);
    }
}


@end
