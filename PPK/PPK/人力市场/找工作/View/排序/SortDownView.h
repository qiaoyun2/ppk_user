//
//  SortDownView.h
//  PPK
//
//  Created by null on 2022/3/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SortDownView : UIView

@property (nonatomic, copy) void(^didSelectBlock)(NSInteger index, NSString *selectedStr);/**<选择的返回类型回调 */
@property (nonatomic, copy) void(^hiddenBlock)(void);

@property (nonatomic, assign) BOOL hiddenTabbar;

- (void)showDropDown; // 显示下拉菜单
- (void)hideDropDown; // 隐藏下拉菜单

@end

NS_ASSUME_NONNULL_END
