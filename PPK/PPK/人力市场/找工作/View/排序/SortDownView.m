//
//  SortDownView.m
//  PPK
//
//  Created by null on 2022/3/6.
//

#import "SortDownView.h"
#import "ProvinceCell.h"

@interface SortDownView () <UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *sortTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTop;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, assign) CGFloat contentViewHeight;
@property (nonatomic, assign) NSInteger sortIndex;
@property (nonatomic, strong) NSArray *dataArray;
@end

@implementation SortDownView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.clipsToBounds = YES;
    
    CGFloat top = NavAndStatusHight + 43;
    self.contentViewHeight = SCREEN_HEIGHT - top - TAB_BAR_HEIGHT;
    self.frame = CGRectMake(0, top, SCREEN_WIDTH, self.contentViewHeight);
    self.contentViewTop.constant = - self.contentViewHeight;

    self.sortTableView.delegate = self;
    self.sortTableView.dataSource = self;

    self.sortIndex = 0;
    self.dataArray = @[@"最新排序",@"距离排序",@"实名认证",@"企业认证"];

//    [self addTapGestureRecognizerToSelf];
}

- (void)setHiddenTabbar:(BOOL)hiddenTabbar
{
    _hiddenTabbar = hiddenTabbar;
    CGFloat top = NavAndStatusHight + 43;
    if (hiddenTabbar) {
        self.contentViewHeight = SCREEN_HEIGHT - top;
    }else {
        self.contentViewHeight = SCREEN_HEIGHT - top - TAB_BAR_HEIGHT;
    }
    self.frame = CGRectMake(0, top, SCREEN_WIDTH, self.contentViewHeight);
    self.contentViewTop.constant = - self.contentViewHeight;
}

#pragma mark - UIGestureRecognizerDelegate
/*
 解决didSelectRowAtIndexPath不能响应事件：
 UITapGestureRecognizer吞掉了touch事件，导致didSelectRowAtIndexPath方法无法响应。
 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch  {
    // 输出点击的view的类名
    NSLog(@"%@", NSStringFromClass([touch.view class]));
    // 若为UITableViewCellContentView（即点击了tableViewCell），则不截获Touch事件
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    return  YES;
}

#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProvinceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProvinceCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ProvinceCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.nameLabel.textAlignment = NSTextAlignmentLeft;
    cell.nameLabel.text = self.dataArray[indexPath.row];
    cell.nameLabel.textColor = indexPath.row == self.sortIndex ? MainColor : UIColorFromRGB(0x333333);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.sortIndex = indexPath.row;
    [self.sortTableView reloadData];
    [self hideDropDown];
    if (self.didSelectBlock) {
        self.didSelectBlock(indexPath.row, self.dataArray[indexPath.row]);
    }
}

#pragma mark - Function
//添加手势
- (void)addTapGestureRecognizerToSelf {
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapMaskView:)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
}

- (void)tapMaskView: (UITapGestureRecognizer *)tap {
    NSLog(@"收起");
    [self hideDropDown];
}

- (void)showDropDown {
    self.backgroundColor = [UIColor clearColor];
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        self.contentViewTop.constant = 0;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

- (void)hideDropDown {
    if (self.hiddenBlock) {
        self.hiddenBlock();
    }
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [UIColor clearColor];
        self.contentViewTop.constant = -self.contentViewHeight;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

#pragma mark - XibFunction


@end
