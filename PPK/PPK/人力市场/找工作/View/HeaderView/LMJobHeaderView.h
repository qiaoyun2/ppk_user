//
//  LMJobHeaderView.h
//  PPK
//
//  Created by null on 2022/3/4.
//

#import <UIKit/UIKit.h>
#import "ZScrollLabel.h"
#import "LMJobHeaderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMJobHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIView *cateBgView;
//@property (weak, nonatomic) IBOutlet UIView *noticeView;
//@property (weak, nonatomic) IBOutlet ZScrollLabel *noticeLabel;
@property (weak, nonatomic) IBOutlet UIView *noticeBgView;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;

@property (nonatomic, copy) void(^onCateButtonsAction)(LMJobTypeModel *typeModel);
@property (nonatomic, strong) LMJobHeaderModel *model;

@end

NS_ASSUME_NONNULL_END
