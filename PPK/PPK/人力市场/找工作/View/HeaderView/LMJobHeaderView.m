//
//  LMJobHeaderView.m
//  PPK
//
//  Created by null on 2022/3/4.
//

#import "LMJobHeaderView.h"
#import "LMJobCateButton.h"
#import <SDCycleScrollView.h>
#import "ScrollNoticeCell.h"
#import "WKWebViewController.h"

@interface LMJobHeaderView ()<SDCycleScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) SDCycleScrollView *noticeView;
@property (nonatomic, strong) NSArray *noticeArray;



@end

@implementation LMJobHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
//    self.noticeLabel.labelAlignment = ZScrollLabelAlignmentLeft;
//    self.noticeLabel.textColor = MainColor;
//    self.noticeLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
    
    _noticeView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, (SCREEN_WIDTH-56), 32) delegate:self placeholderImage:nil];
    _noticeView.scrollDirection = UICollectionViewScrollDirectionVertical;
    _noticeView.localizationImageNamesGroup = @[@"",@""];
    _noticeView.delegate = self;
    _noticeView.showPageControl = NO;
    _noticeView.autoScrollTimeInterval = 3;
    _noticeView.backgroundColor = [UIColor clearColor];
    [_noticeView disableScrollGesture];
    [self.noticeBgView addSubview:_noticeView];
}

/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    if (self.noticeArray.count) {
        LMJobHeaderNoticeModel *model = self.noticeArray[index];
        WKWebViewController *vc = [[WKWebViewController alloc] init];
        vc.contentStr = model.content;
        vc.titleStr = model.title;
        [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
    }
}

/** 图片滚动回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index
{
    
}

/** 如果你需要自定义cell样式，请在实现此代理方法返回你的自定义cell的Nib。 */
- (UINib *)customCollectionViewCellNibForCycleScrollView:(SDCycleScrollView *)view
{
    if (view != self.noticeView) {
        return nil;
    }
    return [UINib nibWithNibName:@"ScrollNoticeCell" bundle:nil];
}

/** 如果你自定义了cell样式，请在实现此代理方法为你的cell填充数据以及其它一系列设置 */
- (void)setupCustomCell:(UICollectionViewCell *)cell forIndex:(NSInteger)index cycleScrollView:(SDCycleScrollView *)view
{
    ScrollNoticeCell *myCell = (ScrollNoticeCell *)cell;
    myCell.backgroundColor = [UIColor whiteColor];
    if (self.noticeArray.count) {
        LMJobHeaderNoticeModel *model = self.noticeArray[index];
        myCell.contentLabel.text = model.title;
    }
}


#pragma mark - Function
- (void)setModel:(LMJobHeaderModel *)model
{
    _model = model;
    [self updateSubviews];
}

- (void)updateSubviews
{
    self.mobileLabel.text = _model.officialMobile;
//    self.noticeLabel.text = _model.noticeModel.content;
    self.noticeArray = [NSArray arrayWithArray:_model.notices];
    
    NSMutableArray *tempAray = [NSMutableArray array];
    for (int i = 0; i<self.noticeArray.count; i++) {
        [tempAray addObject:@"矩形 16382"];
    }
    self.noticeView.imageURLStringsGroup = tempAray;

    if (!self.scrollView) {
        UIScrollView *scrollView = [[UIScrollView alloc] init];
        scrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 172);
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.pagingEnabled = YES;
        [self.cateBgView addSubview:scrollView];
        self.scrollView = scrollView;
    }

    [self.scrollView removeAllSubviews];
    NSArray *cates = _model.types;
    CGFloat width = 64;
    CGFloat height = 65;
    CGFloat leading = 10;
    CGFloat top = 12;
    CGFloat hornterval = (SCREEN_WIDTH - width*5 - leading*2)/4;
    CGFloat vernterval = 16;
    for (int i = 0; i<cates.count; i++) {
        NSInteger row = i%10 < 5 ? 0 : 1;
        NSInteger column = i%5;
        NSInteger page = (i+1)%10 == 0 ? (i+1)/10 : ((i+1)/10+1);
        LMJobCateButton *button = [[[NSBundle mainBundle] loadNibNamed:@"LMJobCateButton" owner:nil options:nil] firstObject];
        button.frame = CGRectMake(leading + (width+hornterval)*column + (page-1)*SCREEN_WIDTH, top+(vernterval+height)*row, width, height);
        NSLog(@"%@",NSStringFromCGRect(button.frame));
        button.tag = 1000+i;
        LMJobTypeModel *typeModel = cates[i];
        [button.iconImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(typeModel.picture)] placeholderImage:DefaultImgWidth];
        button.nameLabel.text = typeModel.showHomeName;
        [button addTarget:self action:@selector(buttonsAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:button];
    }
    NSInteger totalPage = cates.count%10 == 0 ? cates.count/10 : (cates.count/10+1);
    self.scrollView.contentSize = CGSizeMake(totalPage *SCREEN_WIDTH,0);
}

- (void)buttonsAction:(UIButton *)sender
{
    if (self.onCateButtonsAction) {
        self.onCateButtonsAction(_model.types[sender.tag-1000]);
    }
}

#pragma mark - XibFunction
- (IBAction)callButtonAction:(id)sender {
    [LJTools call:_model.officialMobile];
}


@end
