//
//  JobTypeDownView.m
//  PPK
//
//  Created by null on 2022/3/6.
//

#import "JobTypeDownView.h"
#import "ProvinceCell.h"
#import "LMJobTypeModel.h"
#import "FeedbackViewController.h"

@interface JobTypeDownView ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *cateTableView;
@property (weak, nonatomic) IBOutlet UITableView *subCateTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTop;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, assign) CGFloat contentViewHeight;
@property (nonatomic, assign) NSInteger cateIndex;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation JobTypeDownView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.clipsToBounds = YES;
    CGFloat top = NavAndStatusHight + 43;
    self.contentViewHeight = SCREEN_HEIGHT - top  - TAB_BAR_HEIGHT;
    self.frame = CGRectMake(0, top, SCREEN_WIDTH, self.contentViewHeight);
    self.contentViewTop.constant = - self.contentViewHeight;

    self.cateTableView.delegate = self;
    self.cateTableView.dataSource = self;
    
    self.subCateTableView.delegate = self;
    self.subCateTableView.dataSource = self;
    
    self.seletedSubModels = [NSMutableArray array];
    self.cateIndex = 0;
    
//    [self addTapGestureRecognizerToSelf];
    [self requestForWorkType];
}

- (void)setHiddenTabbar:(BOOL)hiddenTabbar
{
    _hiddenTabbar = hiddenTabbar;
    CGFloat top = NavAndStatusHight + 43;
    if (hiddenTabbar) {
        self.contentViewHeight = SCREEN_HEIGHT - top;
    }else {
        self.contentViewHeight = SCREEN_HEIGHT - top - TAB_BAR_HEIGHT;
    }
    self.frame = CGRectMake(0, top, SCREEN_WIDTH, self.contentViewHeight);
    self.contentViewTop.constant = - self.contentViewHeight;
}

- (void)requestForWorkType
{
    [NetworkingTool getWithUrl:kWorkTypeURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        self.dataArray = [NSMutableArray array];
        if ([responseObject[@"code"] integerValue]==1) {
            NSArray *dataArray = responseObject[@"data"];
            [self insertAllMenu];
            [dataArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                LMJobTypeModel *model = [[LMJobTypeModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
                if ([model.ID isEqual:self.ID]) {
                    self.cateIndex = idx;
                }
            }];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.cateTableView reloadData];
        [self.subCateTableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - UIGestureRecognizerDelegate
/*
 解决didSelectRowAtIndexPath不能响应事件：
 UITapGestureRecognizer吞掉了touch事件，导致didSelectRowAtIndexPath方法无法响应。
 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch  {
    // 输出点击的view的类名
    NSLog(@"%@", NSStringFromClass([touch.view class]));
    // 若为UITableViewCellContentView（即点击了tableViewCell），则不截获Touch事件
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    return  YES;
}

#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.dataArray.count==0) {
        return 0;
    }
    if (tableView==self.cateTableView) {
        return self.dataArray.count;
    }
    LMJobTypeModel *model = self.dataArray[self.cateIndex];
    return model.childList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProvinceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProvinceCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ProvinceCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.nameLabel.textAlignment = NSTextAlignmentLeft;
    
    // 一级分类
    if (tableView == self.cateTableView) {
        LMJobTypeModel *model = self.dataArray[indexPath.row];
        cell.nameLabel.text = model.workName;
        if (indexPath.row == self.cateIndex) {
            cell.lineImageView.hidden = NO;
            cell.nameLabel.textColor = MainColor;
            cell.contentView.backgroundColor = UIColorFromRGB(0xF6F7F9);
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }else {
            cell.lineImageView.hidden = YES;
            cell.nameLabel.textColor = UIColorFromRGB(0x333333);
            cell.contentView.backgroundColor = UIColorFromRGB(0xF6F7F9);
        }
        return cell;
    }
    
    // 二级分类
    LMJobTypeModel *model = self.dataArray[self.cateIndex];
    LMJobTypeModel *subModel = model.childList[indexPath.row];
    if (tableView == self.subCateTableView) {
        cell.nameLabel.text = subModel.workName;
        if ([self.seletedSubModels containsObject:subModel]) {
            cell.nameLabel.textColor = MainColor;
        }else {
            cell.nameLabel.textColor = UIColorFromRGB(0x333333);
        }
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 一级分类
    if (tableView == self.cateTableView) {
        if (indexPath.row != self.cateIndex) {
            self.cateIndex = indexPath.row;
            // 切换1级分类是否重置选择
            [self.seletedSubModels removeAllObjects];
            [self.cateTableView reloadData];
            [self.subCateTableView reloadData];
        }
        return;
    }
    
    // 二级分类
    LMJobTypeModel *model = self.dataArray[self.cateIndex];
    LMJobTypeModel *subModel = model.childList[indexPath.row];
    
    if ([self.seletedSubModels containsObject:subModel]) {
        [self.seletedSubModels removeObject:subModel];
    }else {
        [self.seletedSubModels addObject:subModel];
        if (self.seletedSubModels.count>3) {
            [self.seletedSubModels removeObjectAtIndex:0];
        }
    }
    [self.subCateTableView reloadData];
    
    // 切换1级分类是否重置选择
//    if ([self.lastModel isEqual:model]) {
//        if ([self.seletedSubModels containsObject:subModel]) {
//            [self.seletedSubModels removeObject:subModel];
//        }else {
//            [self.seletedSubModels addObject:subModel];
//            if (self.seletedSubModels.count>3) {
//                [self.seletedSubModels removeObjectAtIndex:0];
//            }
//        }
//    }else {
//        self.lastModel = model;
//        [self.seletedSubModels removeAllObjects];
//        [self.seletedSubModels addObject:subModel];
//    }
}

#pragma mark - Function
// 添加手势
- (void)addTapGestureRecognizerToSelf {
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapMaskView:)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
}

// 手势点击事件
- (void)tapMaskView:(UITapGestureRecognizer *)tap {
    NSLog(@"收起");
    [self hideDropDown];
}

// 显示下拉框
- (void)showDropDown {
    self.backgroundColor = [UIColor clearColor];
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        self.contentViewTop.constant = 0;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

// 收起下拉框
- (void)hideDropDown {
    if (self.hiddenBlock) {
        self.hiddenBlock();
    }
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [UIColor clearColor];
        self.contentViewTop.constant = -self.contentViewHeight;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

// 收起下拉框
- (void)hideDropDownaImmediately {
    if (self.hiddenBlock) {
        self.hiddenBlock();
    }
    self.backgroundColor = [UIColor clearColor];
    self.contentViewTop.constant = -self.contentViewHeight;
    self.hidden = YES;
}

#pragma mark - Setter
- (void)setID:(NSString *)ID
{
    _ID = ID;
    if (!self.dataArray.count) {
        return;
    }
    [self.dataArray enumerateObjectsUsingBlock:^(LMJobTypeModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([model.ID isEqual:ID]) {
            self.cateIndex = idx;
//            [self.seletedSubModels removeAllObjects];
            [self.cateTableView reloadData];
            [self.subCateTableView reloadData];
        }
    }];
}

// 插入全部按钮
- (void)insertAllMenu
{
    LMJobTypeModel *subModel = [[LMJobTypeModel alloc] init];
    subModel.ID = @"";
//    subModel.pid = @"";
    subModel.workName = @"全部";
    
    LMJobTypeModel *model = [[LMJobTypeModel alloc] init];
    model.ID = @"";
    model.workName = @"全部分类";
    model.childList = @[subModel];
    [self.dataArray addObject:model];
}

#pragma mark - XibFunction
// 希望添加的岗位
- (IBAction)addJobButtonAction:(UIButton *)sender {
    [self hideDropDownaImmediately];    
    FeedbackViewController *vc = [[FeedbackViewController alloc] init];
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

// 重置
- (IBAction)resetButtonAction:(id)sender {
    [self.seletedSubModels removeAllObjects];
    [self.subCateTableView reloadData];
}

// 确认
- (IBAction)confirmButtonAction:(UIButton *)sender {
    LMJobTypeModel *model = self.dataArray[self.cateIndex];
    if (self.didSelectedType) {
        self.didSelectedType(model, self.seletedSubModels);
    }
 
    // 切换1级分类是否重置选择
//    if (self.seletedSubModels.count>0) {
//        if (self.didSelectedType) {
//            self.didSelectedType(model, self.seletedSubModels);
//        }
//    }
//    else {
//        if (self.didReset) {
//            self.didReset();
//        }
//    }
    [self hideDropDown];
}

@end
