//
//  JobTypeDownView.h
//  PPK
//
//  Created by null on 2022/3/6.
//

#import <UIKit/UIKit.h>
#import "LMJobTypeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface JobTypeDownView : UIView

@property (nonatomic, copy) void(^didSelectedType)(LMJobTypeModel *typeModel,NSArray *childModels);
@property (nonatomic, copy) void(^didReset)(void);

@property (nonatomic, copy) void(^hiddenBlock)(void);

@property (nonatomic, strong) NSString *ID; // 一级分类id
@property (nonatomic, strong) NSMutableArray *seletedSubModels; // 已选择二级分类


@property (nonatomic, assign) BOOL hiddenTabbar;


- (void)showDropDown; // 显示下拉菜单
- (void)hideDropDown; // 隐藏下拉菜单

@end

NS_ASSUME_NONNULL_END
