//
//  LMJobListCell.h
//  PPK
//
//  Created by null on 2022/3/4.
//

#import <UIKit/UIKit.h>
#import "LMJobListModel.h"
#import "FoundListImage9TypographyView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMJobListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (weak, nonatomic) IBOutlet UIView *managerView;
@property (weak, nonatomic) IBOutlet UIButton *blueEditButton;
@property (weak, nonatomic) IBOutlet UIButton *whiteEditButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;
@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIButton *topButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;
@property (weak, nonatomic) IBOutlet UIImageView *urgentImageView; // 急招
@property (weak, nonatomic) IBOutlet UILabel *myUrgentLabel; // 我要急招
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *realNameTipImageView; // 实名认证
@property (weak, nonatomic) IBOutlet UIImageView *enterpriseTipImageView; // 企业
@property (weak, nonatomic) IBOutlet UIButton *cityButton;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *callButton;


@property (nonatomic, strong) LMJobListModel *model;
@property (nonatomic, copy) void(^onSelectedButtonClick)(BOOL isSelected);
@property (nonatomic, copy) void(^onEditButtonClick)(void);
@property (nonatomic, copy) void(^onDownOrUpButtonClick)(void);
@property (nonatomic, copy) void(^onTopButtonClick)(void);
@property (nonatomic, copy) void(^onDeleteButtonClick)(void);


@end

NS_ASSUME_NONNULL_END
