//
//  LMJobListCell.m
//  PPK
//
//  Created by null on 2022/3/4.
//

#import "LMJobListCell.h"
#import "MyPublishJobController.h"


@interface LMJobListCell ()




@end

@implementation LMJobListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.imgsView.is_tap = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



- (void)setModel:(LMJobListModel *)model
{
    _model = model;
    if ([model.isTop integerValue]==1) {
        self.urgentImageView.hidden = NO;
        self.myUrgentLabel.hidden = NO;
    }else {
        self.urgentImageView.hidden = YES;
        self.myUrgentLabel.hidden = YES;
    }
    
    self.titleLabel.text = model.workTitle;
    self.contentLabel.text = model.workContent;
    if (model.searchStr.length>0) {
        if ([model.workTitle containsString:model.searchStr]) {
            NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:model.workTitle];
            NSRange range = [model.workTitle rangeOfString:model.searchStr];
            // 改变文字颜色
            [noteStr addAttribute:NSForegroundColorAttributeName value:MainColor range:range];
            // 为label添加Attributed
            [self.titleLabel setAttributedText:noteStr];
        }
        if ([model.workContent containsString:model.searchStr]) {
            NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:model.workContent];
            NSRange range = [model.workContent rangeOfString:model.searchStr];
            // 改变文字颜色
            [noteStr addAttribute:NSForegroundColorAttributeName value:MainColor range:range];
            // 为label添加Attributed
            [self.contentLabel setAttributedText:noteStr];
        }
    }

    self.realNameTipImageView.image = [model.authTypePersonal integerValue] == 1 ? [UIImage imageNamed:@"组 50697"] : [UIImage imageNamed:@"组 50910"];
    self.enterpriseTipImageView.hidden = [model.authTypeCompany integerValue] != 1;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.avatar)] placeholderImage:DefaultImgHeader];
    [self.cityButton setTitle:[NSString stringWithFormat:@"%@ %@km",model.workCity,model.distance] forState:UIControlStateNormal];
    self.timeLabel.text = model.createTime;
    self.selectButton.selected = model.selected;
    if (model.videoPath.length) {
        self.imgsView.videoPath = _model.videoPath;
        self.imgsView.imageArray= @[_model.videoPicture];
    }
    else {
        if (model.picture.length>0) {
            NSArray *images = [model.picture componentsSeparatedByString:@","];
            self.imgsView.imageArray= images;
        }
    }
    self.topButton.hidden = [model.isTop integerValue] == 1;
}

- (IBAction)myUrgentLabelTap:(id)sender {
    MyPublishJobController *vc = [[MyPublishJobController alloc] init];
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

- (IBAction)callButtonAction:(UIButton *)sender {
    [LJTools call:_model.contactsMobile];
}

- (IBAction)selectButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.onSelectedButtonClick) {
        self.onSelectedButtonClick(sender.selected);
    }
}

- (IBAction)editButtonAction:(id)sender {
    if (self.onEditButtonClick) {
        self.onEditButtonClick();
    }
}

- (IBAction)downOrUpButtonAction:(UIButton *)sender {
    if (self.onDownOrUpButtonClick) {
        self.onDownOrUpButtonClick();
    }
}

- (IBAction)topButtonAction:(UIButton *)sender {
    if (self.onTopButtonClick) {
        self.onTopButtonClick();
    }    
}

- (IBAction)deleteButtonAction:(id)sender {
    if (self.onDeleteButtonClick) {
        self.onDeleteButtonClick();
    }
}

@end
