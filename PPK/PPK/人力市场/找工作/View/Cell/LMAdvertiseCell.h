//
//  LMAdvertiseCell.h
//  PPK
//
//  Created by null on 2022/3/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LMAdvertiseCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *adImageView;

@end

NS_ASSUME_NONNULL_END
