//
//  OpinoinCollectionViewCell.h
//  null
//
//  Created by null on 2019/10/25.
//  Copyright © 2019 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpinoinCollectionViewCell : UICollectionViewCell

///图片
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *addImageView;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (nonatomic, copy) void(^deletePictureBlock)(void);

@end

NS_ASSUME_NONNULL_END
