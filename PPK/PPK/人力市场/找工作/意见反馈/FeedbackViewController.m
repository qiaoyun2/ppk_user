//
//  FeedbackViewController.m
//  ZZR
//
//  Created by null on 2020/12/3.
//

#import "FeedbackViewController.h"
#import "TZImagePickerController.h"
#import "LJTools.h"
#import "LeePlaceholderTextView.h"
#import "OpinoinCollectionViewCell.h"
#import "UploadManager.h"

static NSString *const imgCellId = @"imgCellId";

@interface FeedbackViewController ()<TZImagePickerControllerDelegate>

///反馈内容
@property (weak, nonatomic) IBOutlet LeePlaceholderTextView *textView;

///总view的高度
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeight;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;
@property (weak, nonatomic) IBOutlet UITextField *phoneText;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@property (nonatomic, strong) NSMutableArray *assestArray;
@property (nonatomic, strong) NSMutableArray *photosArray;
///是否是原始s图片
@property (assign, nonatomic) BOOL isSelectOriginalPhoto;

@property (nonatomic, copy) NSString *headUrl;

@end

@implementation FeedbackViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"留言建议";
    [self.textView setPlaceholder:@"问题描述的越详细，有助于我们更快的解决问题"];
    [self initCollectionView];
    [_phoneText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

#pragma mark-- View

- (void)initCollectionView {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake((SCREEN_WIDTH - 32 - 11) / 3.0, (SCREEN_WIDTH - 32 - 11) / 3.0);
    layout.minimumLineSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(0, 16, 0, 16);
    self.collectionView.collectionViewLayout = layout;
    [self.collectionView registerNib:[UINib nibWithNibName:@"OpinoinCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:imgCellId];
}

/// 重置高度
- (void)resetHeight {
    if (SCREEN_HEIGHT - NavAndStatusHight - bottomBarH > 596.5 - 140 + _collectionViewHeight.constant - 10) {
        _viewHeight.constant = SCREEN_HEIGHT - NavAndStatusHight - bottomBarH;
    } else {
        self.viewHeight.constant = 596.5 - 140 + _collectionViewHeight.constant - 10;
    }
}

#pragma mark – TZImagePickerControllerDelegate
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    
    [self.photosArray removeAllObjects];
    [self.assestArray removeAllObjects];
    
    [self.photosArray addObjectsFromArray:photos];
    [self.assestArray addObjectsFromArray:assets];
    _isSelectOriginalPhoto = isSelectOriginalPhoto;
    
    [_collectionView reloadData];
    _collectionViewHeight.constant = _collectionView.collectionViewLayout.collectionViewContentSize.height;
    
    [self resetHeight];
}

#pragma mark – UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (_photosArray.count >= 6) {
        return 6;
    }
    return _photosArray.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    OpinoinCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:imgCellId forIndexPath:indexPath];
    [cell.deleteButton setTag:indexPath.row+1];
    
    @weakify(self);
    cell.deletePictureBlock = ^{
        @strongify(self);
        [self deletePhotoWithIndex:indexPath.row];
    };
    
    if (indexPath.row == _photosArray.count) {
        if (indexPath.row == 6) {
            cell.photoImageView.image = _photosArray[indexPath.row];
            cell.addImageView.hidden = YES;
            [cell.deleteButton setHidden:NO];
        } else {
            cell.photoImageView.image = nil;
            cell.addImageView.hidden = NO;
            [cell.deleteButton setHidden:YES];
        }
    } else {
        cell.photoImageView.image = _photosArray[indexPath.row];
        cell.addImageView.hidden = YES;
        [cell.deleteButton setHidden:NO];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self checkLocalPhoto];
}

#pragma mark – Fonction

/// 输入框观察者事件
- (void)textFieldDidChange:(UITextField *)textField {
    if (textField == _phoneText && _phoneText.text.length > 20) {
        _phoneText.text = [_phoneText.text substringToIndex:20];
    }
}

//选择照片
- (void)checkLocalPhoto {
    TZImagePickerController *imagePicker = [[TZImagePickerController alloc] initWithMaxImagesCount:6 delegate:self];
    imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
    [imagePicker setSortAscendingByModificationDate:NO];
    imagePicker.isSelectOriginalPhoto = _isSelectOriginalPhoto;
    imagePicker.selectedAssets = _assestArray;
    imagePicker.allowPickingVideo = NO;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

//删除照片
- (void)deletePhotoWithIndex:(NSInteger)index {
    
    [_photosArray removeObjectAtIndex:index];
    [_assestArray removeObjectAtIndex:index];
    
    [_collectionView reloadData];
    _collectionViewHeight.constant = _collectionView.collectionViewLayout.collectionViewContentSize.height;
    
    [self resetHeight];
}

- (CGSize)sizeForString:(NSString *)string withSize:(CGSize)fitsize withFontSize:(NSInteger)fontSize{
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]};//指定字号
    CGRect rect = [string boundingRectWithSize:fitsize/*计算高度要先指定宽度*/ options:NSStringDrawingTruncatesLastVisibleLine |
                   NSStringDrawingUsesLineFragmentOrigin |
                   NSStringDrawingUsesFontLeading attributes:dic context:nil];
    return rect.size;
}

- (IBAction)commit:(id)sender {
    [self.view endEditing:YES];
    
    if (self.textView.text.length == 0) {
        [LJTools showNOHud:@"请输入留言建议" delay:1];
        return;
    }
    if (self.phoneText.text.length == 0) {
        [LJTools showNOHud:@"请输入联系方式" delay:1];
        return;
    }
    if ([self.phoneText.text isTelephone] || [self.phoneText.text isEmail] || [self.phoneText.text validateQQ]) {} else {
        [LJTools showNOHud:@"请输入有效的联系方式" delay:1];
        return;
    }
    if (self.photosArray.count <= 0) {
        [self submitQuestion];
    } else {
        [self uploadImage:self.photosArray];
    }
}

///提交问题
- (void)submitQuestion {
    if ([self.textView.text isBlankString]) {
        [LJTools showNOHud:@"请输入留言建议" delay:1];
        return;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"content"] = _textView.text;
    parameters[@"imgArray"] = _headUrl;
    parameters[@"contact"] = _phoneText.text;
    
    [NetworkingTool postWithUrl:kFeedbackURL params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [super backItemClicked];
            [LJTools showOKHud:responseObject[@"msg"] delay:1.0];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

#pragma mark - 上传图片

- (void)uploadImage:(NSArray *)imageList {
    if (!imageList) {
        return;
    }
    @weakify(self);
    [UploadManager uploadImageArray:imageList block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
        @strongify(self);
        self.headUrl = imageUrl;
        [self submitQuestion];
    }];
}

#pragma mark – 懒加载

- (NSMutableArray *)photosArray {
    if (!_photosArray) {
        _photosArray = [NSMutableArray array];
    }
    return _photosArray;
}

- (NSMutableArray *)assestArray {
    if (!_assestArray) {
        _assestArray = [NSMutableArray array];
    }
    return _assestArray;
}

@end
