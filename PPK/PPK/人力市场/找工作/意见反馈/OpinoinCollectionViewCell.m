//
//  OpinoinCollectionViewCell.m
//  null
//
//  Created by null on 2019/10/25.
//  Copyright © 2019 null. All rights reserved.
//

#import "OpinoinCollectionViewCell.h"

@implementation OpinoinCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)deleteAction:(id)sender {
    if (_deletePictureBlock) {
        _deletePictureBlock();
    }
}

@end
