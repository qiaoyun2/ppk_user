//
//  LMJobHeaderModel.m
//  PPK
//
//  Created by null on 2022/3/12.
//

#import "LMJobHeaderModel.h"



@implementation LMJobHeaderNoticeModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
}

@end

//@implementation LMJobHeaderTypeModel
//
//- (void)setValue:(id)value forKey:(NSString *)key
//{
//    [super setValue:value forKey:key];
//    if ([key isEqualToString:@"id"]) {
//        self.ID = [NSString stringWithFormat:@"%@",value];
//    }
//}
//
//@end

@implementation LMJobHeaderModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"WorkTypeVOList"]) {
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *obj in value) {
            LMJobTypeModel *model = [[LMJobTypeModel alloc] initWithDictionary:obj];
            [tempArray addObject:model];
        }
        self.types = [NSArray arrayWithArray:tempArray];
    }
    if ([key isEqualToString:@"messageVO"]) {
        
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *obj in value) {
            LMJobHeaderNoticeModel *model = [[LMJobHeaderNoticeModel alloc] initWithDictionary:obj];
            [tempArray addObject:model];
        }
        self.notices = [NSArray arrayWithArray:tempArray];
    }
}

@end
