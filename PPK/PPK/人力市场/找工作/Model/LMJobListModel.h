//
//  LMJobListModel.h
//  PPK
//
//  Created by null on 2022/3/13.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMJobListModel : BaseModel

@property (nonatomic, strong) NSString *authStatus; // 实名认证状态：0未认证；1认证中；2已完成
@property (nonatomic, strong) NSString *authTypePersonal; // 个人是否实名认证0否1是
@property (nonatomic, strong) NSString *authTypeCompany; // 公司是否实名认证0否1是
@property (nonatomic, strong) NSString *avatar; // 用户头像
@property (nonatomic, strong) NSString *contactsMobile; // 联系人电话

@property (nonatomic, strong) NSString *createTime; // 创建时间
@property (nonatomic, strong) NSString *distance; // 距离
@property (nonatomic, strong) NSString *encryptMobile; // 加密电话
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *isTop; // 是否置顶0否1是
@property (nonatomic, strong) NSString *isVideo; // 是否是视频，0否1是
@property (nonatomic, strong) NSString *latitude; // 纬度
@property (nonatomic, strong) NSString *longitude; // 经度
@property (nonatomic, strong) NSString *picture; // 图片
@property (nonatomic, strong) NSString *videoPicture; // 视频封面
@property (nonatomic, strong) NSString *videoPath; // 视频路径
@property (nonatomic, strong) NSString *workCity; // 
@property (nonatomic, strong) NSString *workContent; // 招工详情
@property (nonatomic, strong) NSString *workTitle; // 招工标题


@property (nonatomic, strong) NSString *adContent; //
@property (nonatomic, strong) NSString *adPicture; //
@property (nonatomic, strong) NSString *adTitle; // 

@property (nonatomic, strong) NSString *searchStr; //

@property (nonatomic, assign) BOOL selected;

@end

NS_ASSUME_NONNULL_END
