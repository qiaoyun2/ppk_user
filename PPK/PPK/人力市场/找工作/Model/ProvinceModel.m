//
//  ProvinceModel.m
//  PPK
//
//  Created by null on 2022/3/13.
//

#import "ProvinceModel.h"

@implementation AreaModel


@end

@implementation CityModel

- (instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [super initWithDictionary:dic];
    if (self) {
        NSArray *citys = dic[@"area"];
        NSMutableArray *array = [NSMutableArray array];
        AreaModel *model = [[AreaModel alloc] init];
        model.name = @"全部";
        model.pname = dic[@"name"];
        [array addObject:model];
        
        for (NSString *str in citys) {
            AreaModel *model = [[AreaModel alloc] init];
            model.name = str;
            model.pname = dic[@"name"];
            [array addObject:model];
        }
        self.areas = [NSArray arrayWithArray:array];
    }
    return self;
}

@end

@implementation ProvinceModel

- (instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [super initWithDictionary:dic];
    if (self) {
        NSArray *citys = dic[@"city"];
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *obj in citys) {
            CityModel *model = [[CityModel alloc] initWithDictionary:obj];
            model.pname = dic[@"name"];
            [array addObject:model];
        }
        self.citys = [NSArray arrayWithArray:array];
    }
    return self;
}

//- (void)setValue:(id)value forKey:(NSString *)key
//{
//    [super setValue:value forKey:key];
//    if ([key isEqualToString:@"city"]) {
//        NSMutableArray *tempArray = [NSMutableArray array];
//        for (NSDictionary *obj in value) {
//            CityModel *model = [[CityModel alloc] initWithDictionary:obj];
//            model
//            [tempArray addObject:model];
//        }
//        self.citys = [NSArray arrayWithArray:tempArray];
//    }
//}

@end
