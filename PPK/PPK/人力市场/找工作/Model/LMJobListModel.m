//
//  LMJobListModel.m
//  PPK
//
//  Created by null on 2022/3/13.
//

#import "LMJobListModel.h"

@implementation LMJobListModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
}

@end
