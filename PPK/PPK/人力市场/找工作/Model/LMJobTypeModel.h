//
//  LMJobTypeModel.h
//  PPK
//
//  Created by null on 2022/3/13.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMJobTypeModel : BaseModel

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *picture;
@property (nonatomic, strong) NSString *workName;
@property (nonatomic, strong) NSString *showHomeName;

@property (nonatomic, strong) NSArray *childList;
@property (nonatomic, strong) NSString *pid;
//@property (nonatomic, assign) BOOL selected;

@end

NS_ASSUME_NONNULL_END
