//
//  LMJobTypeModel.m
//  PPK
//
//  Created by null on 2022/3/13.
//

#import "LMJobTypeModel.h"

@implementation LMJobTypeModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
    if ([key isEqualToString:@"childList"]) {
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *obj in value) {
            LMJobTypeModel *model = [[LMJobTypeModel alloc] initWithDictionary:obj];
            [array addObject:model];
        }
        self.childList = [NSArray arrayWithArray:array];
    }
}

@end
