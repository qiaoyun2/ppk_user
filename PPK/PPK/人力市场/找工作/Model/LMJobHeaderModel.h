//
//  LMJobHeaderModel.h
//  PPK
//
//  Created by null on 2022/3/12.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"
#import "LMJobTypeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMJobHeaderNoticeModel : BaseModel

@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *picture;
@property (nonatomic, strong) NSString *title;

@end

//@interface LMJobHeaderTypeModel : BaseModel
//
//@property (nonatomic, strong) NSString *ID;
//@property (nonatomic, strong) NSString *picture;
//@property (nonatomic, strong) NSString *workName;
//
//@end



@interface LMJobHeaderModel : BaseModel

@property (nonatomic, strong) NSArray *notices;
@property (nonatomic, strong) NSString *officialMobile;
@property (nonatomic, strong) NSArray *types;


@end

NS_ASSUME_NONNULL_END
