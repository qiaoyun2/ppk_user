//
//  HomeSearchManager.h
//  ZhiNengShiBie
//
//  Created by null on 2019/8/29.
//  Copyright © 2019 null. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeSearchManager : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *headerUrl;
@property (nonatomic, copy) void(^refreshOrderBlock)(void);
@property (nonatomic, copy) void(^refreshHomeBlock)(void);
@property (nonatomic, copy) void(^numberBlock)(NSString *count);
@property (nonatomic, copy) void(^cityBlock)(NSString *city,NSString *area);

//官方推荐的的建立单例类的方法,多线程下也保证安全,避免多次初始化
+(instancetype)shareManager;
//更新数据，如果有就更新数据，没有就插入数据
-(void)saveOrUpdateByColumnName:(NSString*)columnName type:(NSString *)type;
//读取所有数据
-(NSMutableArray *)getAllhistoryMessageInfoType:(NSString *)type;
-(void)deleteAllDataType:(NSString *)type;

//聊天列表
//更新数据，如果有就更新数据，没有就插入数据
/*
-(void)saveOrUpdateFriendsByColumnName:(NSString*)columnName headerUrl:(NSString *)headerUrl userId:(NSString *)userId;
//读取某个人的昵称头像
-(id)getMessageInfoUserId:(NSString *)userId;
 */
@end

NS_ASSUME_NONNULL_END
