//
//  HomeSearchManager.m
//  ZhiNengShiBie
//
//  Created by null on 2019/8/29.
//  Copyright © 2019 null. All rights reserved.
//

#import "HomeSearchManager.h"
#import "FMDB.h"
#define HomeDirectoryDocumentFilePath [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)/*这是获取沙盒目录*/ objectAtIndex:0]
@implementation HomeSearchManager
static HomeSearchManager *manager=nil;
static FMDatabase*db=nil;
//官方推荐的的建立单例类的方法,多线程下也保证安全,避免多次初始化
+(instancetype)shareManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (manager==nil)
        {
            manager=[[HomeSearchManager alloc] initWith];
        }
    });
    return manager;
}
-(id)initWith
{
    if (self=[super init])
    {
        db=[[FMDatabase alloc]initWithPath:[self GetShareStoreDataBaseDocumentFilePath]];
        [db setShouldCacheStatements:YES];
        [self createTable];
    }
    return self;
}
//创建表格
-(void)createTable
{
    [db open];
    
    //创建数据库和表，如果以上路径有数据库就创建表，如果该表存在就不创建表
    NSString *CREATESQL = @"CREATE TABLE IF NOT EXISTS searchHistory (id Integer PRIMARY KEY AUTOINCREMENT,name TEXT,type TEXT);";
    
    BOOL IfSuccess=  [db executeUpdate:CREATESQL];
    NSLog(@"%@",IfSuccess?@"历史记录表创建成功":@"历史记录表创建失败");
    
    //创建数据库和表，如果以上路径有数据库就创建表，如果该表存在就不创建表
    NSString *CREATESQL2 = @"CREATE TABLE IF NOT EXISTS friendSQL (id Integer PRIMARY KEY AUTOINCREMENT,name TEXT,headerUrl TEXT,userId TEXT);";
    
    BOOL IfSuccess2=  [db executeUpdate:CREATESQL2];
    NSLog(@"%@",IfSuccess2?@"聊天列表创建成功":@"聊天列表创建失败");
    
    [db close];
}
//更新数据，如果有就更新数据，没有就插入数据
-(void)saveOrUpdateByColumnName:(NSString*)columnName type:(NSString *)type
{
    if ([[columnName safeString] isEqualToString:@""])
    {
        return;
    }
    [db open];
    FMResultSet *rset=[db executeQuery:@"SELECT *FROM searchHistory WHERE name=? AND type=?",columnName,type];
    
    if (![rset next])
    {
        BOOL IfSuccess= [db executeUpdate:@"INSERT INTO searchHistory (name,type) VALUES (?,?)",[columnName safeString],type];
        NSLog(@"%@",IfSuccess?@"历史记录表插入成功":@"历史记录表插入失败");
        
        
    }
    else
    {
        BOOL IfSuccess= [db executeUpdate:@"UPDATE searchHistory SET name=? WHERE name=? AND type=?",[columnName safeString],[columnName safeString],type];
        NSLog(@"%@",IfSuccess?@"历史记录表更新成功":@"历史记录表更新失败");
    }
    [rset close];
    [db close];
    
    
}
//更新数据，如果有就更新数据，没有就插入数据
-(void)saveOrUpdateFriendsByColumnName:(NSString*)columnName headerUrl:(NSString *)headerUrl userId:(NSString *)userId
{
    if ([[userId safeString] isEqualToString:@""])
    {
        return;
    }
    [db open];
    
    FMResultSet *rset=[db executeQuery:@"SELECT *FROM friendSQL WHERE userId=?",userId];
    
    if (![rset next])
    {
        BOOL IfSuccess= [db executeUpdate:@"INSERT INTO friendSQL (name,headerUrl,userId) VALUES (?,?,?)",columnName,headerUrl,userId];
        NSLog(@"%@",IfSuccess?@"聊天列表插入成功":@"聊天列表插入失败");
    }
    else
    {
        BOOL IfSuccess= [db executeUpdate:@"UPDATE friendSQL SET name=?,headerUrl=? WHERE userId=?",columnName,headerUrl,userId];
        NSLog(@"%@",IfSuccess?@"聊天列表更新成功":@"聊天列表更新失败");
    }
    [rset close];
    [db close];
    
    
}
//读取某个人的昵称头像
-(id)getMessageInfoUserId:(NSString *)userId
{
    [db open];
    FMResultSet *rset=[db executeQuery:@"SELECT *FROM friendSQL WHERE userId=?",userId];
    if ([rset next])
    {
        NSString *name=[rset stringForColumn:@"name"];
        NSString *headerUrl=[rset stringForColumn:@"headerUrl"];
        [self setName:name];
        [self setHeaderUrl:headerUrl];
    }
    else
    {
        NSLog(@"聊天列表用户查询信息失败");
        [self setName:@""];
        [self setHeaderUrl:@""];
    }
    [rset close];
    [db close];
    return self;
}

//读取所有数据
-(NSMutableArray *)getAllhistoryMessageInfoType:(NSString *)type
{
    [db open];
    FMResultSet *rset=[db executeQuery:@"SELECT *FROM searchHistory WHERE type=?",type];
    NSMutableArray *dataArray=[[NSMutableArray alloc] init];
    while ([rset next])
    {
        NSString *string=[rset stringForColumn:@"name"];
        [dataArray addObject:string];
    }
    [rset close];
    [db close];
    return dataArray;
}
-(void)deleteAllDataType:(NSString *)type
{
    if ([db open]) {
        BOOL IfSuccess = [db executeUpdate:@"DELETE FROM searchHistory WHERE type=?",type];
        NSLog(@"%@",IfSuccess?@"历史记录表删除成功":@"历史记录表删除失败");
        [db close];
    }
}
-(NSString*)GetShareStoreDataBaseDocumentFilePath
{
    /**
     首先判断该文件夹是否存在，如果存在就直接返回数据库的名字，
     不存在就先创建文件夹，然后在返回数据的库的名字
     */
    NSFileManager *fileManager =[NSFileManager defaultManager];
    NSError *error;
    if([fileManager fileExistsAtPath:[self GetIphoneStoreDocumentFilePath]])
    {
        return  [self GetIphoneStoreDocumentFilePath];
    }
    else
    {
        if([fileManager createFileAtPath:[self GetIphoneStoreDocumentFilePath] contents:nil attributes:nil])
        {
            NSLog(@"历史记录表创建文件成功:%@", [self GetIphoneStoreDocumentFilePath]);
            return  [self GetIphoneStoreDocumentFilePath];
        }
        else
        {
            return [NSString stringWithFormat:@"历史记录表找不到数据库文件:%@",error];
        }
    }
}

/**
 获取iphone 沙盒目录下的数据库文件
 */
-(NSString*)GetIphoneStoreDocumentFilePath
{
    return [HomeDirectoryDocumentFilePath stringByAppendingPathComponent:@"searchHistory.sqlite"];
}

@end
