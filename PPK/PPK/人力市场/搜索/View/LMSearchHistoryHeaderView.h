//
//  LMSearchHistoryHeaderView.h
//  ZhiNengShiBie
//
//  Created by null on 2019/8/29.
//  Copyright © 2019 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LMSearchHistoryHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@end

NS_ASSUME_NONNULL_END
