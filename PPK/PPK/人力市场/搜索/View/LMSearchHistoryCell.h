//
//  LMSearchHistoryCell.h
//  ZhiNengShiBie
//
//  Created by null on 2019/8/29.
//  Copyright © 2019 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LMSearchHistoryCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
