//
//  LMSearchViewController.m
//  ZhiNengShiBie
//
//  Created by null on 2019/8/29.
//  Copyright © 2019 null. All rights reserved.
//

#import "LMSearchViewController.h"
#import "LMSearchHistoryCell.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "LMSearchHistoryHeaderView.h"
#import "HomeSearchManager.h"
#import "HLSearchResultController.h"// 搜索结果
#import "LMSearchResultController.h"// 搜索结果

@interface LMSearchViewController () <UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITextField *textTF;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navHeight;
@property (nonatomic, strong) NSMutableArray *hotArray;
@property (nonatomic, assign) NSInteger page;

@end

@implementation LMSearchViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navHeight.constant = NavAndStatusHight;
    [self initUI];
    [self requestForHotSearch];
}

- (void)initUI {
    UICollectionViewLeftAlignedLayout *layout = [[UICollectionViewLeftAlignedLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    [self.collectionView registerNib:[UINib nibWithNibName:@"LMSearchHistoryCell" bundle:nil] forCellWithReuseIdentifier:@"LMSearchHistoryCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"LMSearchHistoryHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"section"];
    self.collectionView.collectionViewLayout = layout;
    self.collectionView.showsVerticalScrollIndicator = NO;
    [self.collectionView setBounces:NO];
    
    /// 读取本地缓存数据
    [self.dataArray setArray:[[HomeSearchManager shareManager] getAllhistoryMessageInfoType:self.type]];
    // 倒叙插入数据到数组最前方
    NSMutableIndexSet  *indexes = [NSMutableIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.dataArray.count)];
    NSArray *reversedArray = [[self.dataArray reverseObjectEnumerator] allObjects];
    [self.dataArray removeAllObjects];
    [self.dataArray insertObjects:reversedArray atIndexes:indexes];
    
}

- (void)requestForDeleteHistory
{

//    [JBRequest POST:kHistorySearchDeleteURL parameters:@{} success:^(id  _Nonnull responseObject) {
//        if ([responseObject[@"code"] intValue] == SUCCESS) {
//            [self.dataArray removeAllObjects];
//            [self.collectionView reloadData];
//        }
//        else{
//            JBHUD_hide_Message(responseObject[@"msg"]);
//        }
//    } failure:^(NSError * _Nonnull error) {
//        JBHUD_hide_Message(RequestServerError);
//    }];
}

- (void)requestForHotSearch
{
    [NetworkingTool getWithUrl:kSearchKeywordsURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        self.hotArray = [NSMutableArray array];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSArray *dataArray = responseObject[@"data"];
            for (NSDictionary *obj in dataArray) {
                [self.hotArray addObject:obj[@"name"]];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.collectionView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

-(void)refresh
{
    
}

#pragma mark - textFeildDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self saveSearchKeyword];
    return YES;
}

#pragma mark - inputViewDelegate
- (void)saveSearchKeyword {
    NSString *key = self.textTF.text;
    [self.textTF endEditing:YES];
    [self.view endEditing:YES];
    if (key.length == 0) {
        [LJTools showText:@"请输入内容后再搜索" delay:1.5];
        return;
    }
    if (key.length > 8) {
        [LJTools showText:@"输入内容最多8个字符" delay:1.5];
        return;
    }
    
    /// 本地缓存
    [[HomeSearchManager shareManager] saveOrUpdateByColumnName:key type:self.type];
    [self.dataArray setArray:[[HomeSearchManager shareManager] getAllhistoryMessageInfoType:self.type]];
    // 倒叙插入数据到数组最前方
    NSMutableIndexSet  *indexes = [NSMutableIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.dataArray.count)];
    NSArray *reversedArray = [[self.dataArray reverseObjectEnumerator] allObjects];
    [self.dataArray removeAllObjects];
    [self.dataArray insertObjects:reversedArray atIndexes:indexes];
    [self.collectionView reloadData];

    if (self.type.integerValue==12 || self.type.integerValue==11) {
        HLSearchResultController *vc = [[HLSearchResultController alloc] init];
        vc.type = self.type;
        vc.city = self.city;
        vc.searchStr = key;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    LMSearchResultController *vc = [[LMSearchResultController alloc] init];
    vc.type = self.type;
    vc.searchStr = key;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - CollectionDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (self.type.integerValue==12 || self.type.integerValue==11) {
        return 1;
    }
    return 2;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    switch (section) {
        case 0:
        {
            return self.dataArray.count;
        }
            break;
        default:
        {
            return self.hotArray.count;
        }
            break;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LMSearchHistoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LMSearchHistoryCell" forIndexPath:indexPath];
    switch (indexPath.section) {
        case 0:
        {
            NSString *string = self.dataArray[indexPath.item];
            [cell.titleLabel setText:string];
          
        }
            break;
        default:
        {
            [cell.titleLabel setText:self.hotArray[indexPath.item]];
        }
            break;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *searchStr = @"";
    if (indexPath.section==0) {
        searchStr = self.dataArray[indexPath.row];
    }else{
        searchStr = self.hotArray[indexPath.item];
    }

    if (self.type.integerValue==12 || self.type.integerValue==11) {
        HLSearchResultController *vc = [[HLSearchResultController alloc] init];
        vc.type = self.type;
        vc.city = self.city;
        vc.searchStr = searchStr;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    LMSearchResultController *vc = [[LMSearchResultController alloc] init];
    vc.type = self.type;
    vc.searchStr = searchStr;
    [self.navigationController pushViewController:vc animated:YES];
    
}

//区头高度
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(SCREEN_WIDTH, 50);
}

//区尾高度
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeZero;
}

//区头
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *view;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        LMSearchHistoryHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"section" forIndexPath:indexPath];
        [header.deleteButton addTarget:self action:@selector(deleteMethod) forControlEvents:UIControlEventTouchUpInside];
        switch (indexPath.section) {
            case 0: {
                [header.contentLabel setText:@"历史搜索"];
                [header.deleteButton setHidden:NO];
                [header.deleteButton addTarget:self action:@selector(deleteMethod) forControlEvents:UIControlEventTouchUpInside];
                
            } break;
                
            case 1: {
                [header.contentLabel setText:@"搜索发现"];
                [header.deleteButton setHidden:YES];
            } break;
        }
        return header;
    }
    return view;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            NSString *string = self.dataArray[indexPath.item];
            CGFloat width = [NSString getWidthWithTitle:string fontOfSize:12.0];
            return CGSizeMake(width + 40, 28);
        }
            break;
        default:
        {
            NSString *string = self.hotArray[indexPath.item];
            CGFloat width = [NSString getWidthWithTitle:string fontOfSize:12.0];
            if (width > SCREEN_WIDTH-32-40) {
                width = SCREEN_WIDTH-32-40;
            }
            return CGSizeMake(width + 40, 28);
        }
            break;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 12.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 12.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 16.0, 6, 16.0);
}

#pragma mark - Function
- (void)deleteMethod {
    WeakSelf
    [UIAlertController alertViewNormalWithTitle:@"确定删除历史记录" message:nil titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
        /// 清除本地缓存
        [[HomeSearchManager shareManager] deleteAllDataType:self.type];
        [weakSelf.dataArray removeAllObjects];
        [weakSelf.collectionView reloadData];
    } okColor:UIColorFromRGB(0xF64032) cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];

    /// 清除接口缓存
    //    [self requestForDeleteHistory];
}

#pragma mark - XibFunction
- (IBAction)btn:(UIButton *)sender {
    [self.view endEditing:YES];
    switch (sender.tag) {
        case 1://返回
        {
            [self backItemClicked];
        }
            break;
        case 2://搜索
        {
            [self saveSearchKeyword];
        }
            break;
    }
}


@end
