//
//  LMSearchResultController.h
//  PPK
//
//  Created by null on 2022/3/17.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMSearchResultController : BaseViewController
@property (nonatomic, copy) NSString *type; // 1 找工作  2找工人
@property (nonatomic, copy) NSString *searchStr;



@end

NS_ASSUME_NONNULL_END
