//
//  LMSearchResultController.m
//  PPK
//
//  Created by null on 2022/3/17.
//

#import "LMSearchResultController.h"

#import "LMJobSectionHeaderView.h"
#import "CityDropDownView.h"
#import "JobTypeDownView.h"
#import "SortDownView.h"

#import "LMJobListCell.h"
#import "LMWorkerListCell.h"
#import "LMJobDetailController.h"
#import "LMWorkerDetailController.h"

#import "HomeSearchManager.h"

@interface LMSearchResultController ()<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *textTF;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navHeight;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (nonatomic, strong) LMJobSectionHeaderView *sectionHeaderView;
@property (nonatomic, strong) CityDropDownView *cityView;
@property (nonatomic, strong) JobTypeDownView *jobTypeView;
@property (nonatomic, strong) SortDownView *sortView;

@property (nonatomic, strong) LMJobTypeModel *lastTypeModel;
@property (nonatomic, strong) NSArray *subTypeModels;

@property (nonatomic, strong) NSString *sortType;
@property (nonatomic, strong) NSString *city;

@property (nonatomic, assign) NSInteger page;



@end

@implementation LMSearchResultController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navHeight.constant = NavAndStatusHight;
    self.textTF.text = self.searchStr;
    self.sortType = @"1";
    self.city = [LJTools getAppDelegate].city;
    [self setupSectionHeaderView];
    
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];

    [self refresh];
}

- (void)setupSectionHeaderView
{
    self.sectionHeaderView = [[LMJobSectionHeaderView alloc] init];
    [self.sectionHeaderView hiddenCityButton];
    WeakSelf
    [self.sectionHeaderView setOnTitleButtonsClick:^(UIButton * _Nonnull sender) {
        if (!sender.selected) {
            [weakSelf hiddenAllDropDownView];
            CGFloat time = 0.0;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(time * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (sender.tag==10) {
                    [weakSelf showCityDropDownView];
                }
                else if (sender.tag==11) {
                    [weakSelf showJobTypeDropDownView];
                }
                else {
                    [weakSelf showSortDropDownView];
                }
            });
            sender.selected = !sender.selected;
        }
        else {
            [weakSelf hiddenAllDropDownView];
        }
    }];
}


- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(15);
//    params[@"cityName"] = self.city;
    params[@"cityName"] = @"";
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度
    if (self.lastTypeModel) {
        params[@"workTypeIdFirst"] = self.lastTypeModel.ID; // 一级分类id
    }
    if (self.subTypeModels.count) {
        NSMutableArray *array = [NSMutableArray array];
        for (LMJobTypeModel *subModel in self.subTypeModels) {
            [array addObject:subModel.ID];
        }
        NSString *workTypeIds = [array componentsJoinedByString:@","];
        params[@"workTypeIds"] = workTypeIds; // 二级分类id,最多三个
    }
    params[@"type"] = self.sortType; // 1:最新排序；2:距离排序；3:实名认证；4:企业认证
    params[@"keyword"] = self.searchStr;
    NSString *url = [self.type integerValue] == 1 ? kMarketJobListURL : kMarketWorkerListURL;
    WeakSelf
    [NetworkingTool getWithUrl:url params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                if ([self.type integerValue]==1) {
                    LMJobListModel *model = [[LMJobListModel alloc] initWithDictionary:obj];
                    model.searchStr = self.searchStr;
                    [self.dataArray addObject:model];
                }else {
                    LMWorkerListModel *model = [[LMWorkerListModel alloc] initWithDictionary:obj];
                    model.searchStr = self.searchStr;
                    [self.dataArray addObject:model];
                }
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self saveSearchKeyword];
    return YES;
}


#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.type integerValue]==1) {
        LMJobListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LMJobListCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"LMJobListCell" owner:nil options:nil] firstObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        LMJobListModel *model = self.dataArray[indexPath.row];
        cell.model = model;
        return cell;
    }
    
    LMWorkerListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LMWorkerListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"LMWorkerListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    LMWorkerListModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 43;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.sectionHeaderView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.type integerValue]==1) {
        LMJobListModel *model = self.dataArray[indexPath.row];
        LMJobDetailController *vc = [[LMJobDetailController alloc] init];
        vc.jobId = model.ID;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    LMWorkerListModel *model = self.dataArray[indexPath.row];
    LMWorkerDetailController *vc = [[LMWorkerDetailController alloc] init];
    vc.workerId = model.ID;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Function
// 将所有弹窗消失
- (void)hiddenAllDropDownView
{
    [self.cityView hideDropDown];
    [self.jobTypeView hideDropDown];
    [self.sortView hideDropDown];
    self.sectionHeaderView.cityButton.selected = NO;
    self.sectionHeaderView.jobTypeButton.selected = NO;
    self.sectionHeaderView.sortButton.selected = NO;
}

// 选择城市
- (void)showCityDropDownView
{
    if (!self.cityView) {
        self.cityView = [[[NSBundle mainBundle] loadNibNamed:@"CityDropDownView" owner:nil options:nil] firstObject];
        self.cityView.hiddenTabbar = YES;
        [self.view addSubview:self.cityView];
    }
    [self.cityView showDropDown];
    WeakSelf
    [self.cityView setDidSelectBlock:^(NSString * _Nonnull city, NSString * _Nonnull district) {
        weakSelf.sectionHeaderView.cityButton.label.text = city;
        weakSelf.city = city;
        [weakSelf refresh];
    }];
    [self.cityView setHiddenBlock:^{
        weakSelf.sectionHeaderView.cityButton.selected = NO;
    }];
}

// 选择工种
- (void)showJobTypeDropDownView
{
    if (!self.jobTypeView) {
        self.jobTypeView = [[[NSBundle mainBundle] loadNibNamed:@"JobTypeDownView" owner:nil options:nil] firstObject];
        self.jobTypeView.hiddenTabbar = YES;
        [self.view addSubview:self.jobTypeView];
    }

//    self
    self.jobTypeView.seletedSubModels = [NSMutableArray arrayWithArray:self.subTypeModels];
    if (self.lastTypeModel) {
        self.jobTypeView.ID = self.lastTypeModel.ID;
    }
    [self.jobTypeView showDropDown];
    WeakSelf
    [self.jobTypeView setDidSelectedType:^(LMJobTypeModel * _Nonnull typeModel, NSArray * _Nonnull childModels) {
        weakSelf.lastTypeModel = typeModel;
        weakSelf.subTypeModels = [NSArray arrayWithArray:childModels];
        weakSelf.sectionHeaderView.jobTypeButton.label.text = typeModel.workName;
        [weakSelf refresh];
    }];
    
//    [self.jobTypeView setDidReset:^{
//        weakSelf.lastTypeModel = nil;
//        weakSelf.subTypeModels = [NSArray array];
//        weakSelf.sectionHeaderView.jobTypeButton.label.text = @"全部工种";
//        [weakSelf refresh];
//    }];
    
    [self.jobTypeView setHiddenBlock:^{
        weakSelf.sectionHeaderView.jobTypeButton.selected = NO;
    }];
}

// 排序
- (void)showSortDropDownView
{
    if (!self.sortView) {
        self.sortView = [[[NSBundle mainBundle] loadNibNamed:@"SortDownView" owner:nil options:nil] firstObject];
        self.sortView.hiddenTabbar = YES;
        [self.view addSubview:self.sortView];
    }

    [self.sortView showDropDown];
    WeakSelf
    [self.sortView setDidSelectBlock:^(NSInteger index, NSString * _Nonnull selectedStr) {
        weakSelf.sectionHeaderView.sortButton.label.text = selectedStr;
        weakSelf.sortType = [NSString stringWithFormat:@"%ld",index+1];
        [weakSelf refresh];
    }];
    [self.sortView setHiddenBlock:^{
        weakSelf.sectionHeaderView.sortButton.selected = NO;
    }];
}

- (void)saveSearchKeyword {
    NSString *key = self.textTF.text;
    [self.textTF endEditing:YES];
    [self.view endEditing:YES];
    if (key.length == 0) {
        [LJTools showText:@"请输入内容后再搜索" delay:1.5];
        return;
    }
    if (key.length > 8) {
        [LJTools showText:@"输入内容最多8个字符" delay:1.5];
        return;
    }
    
//    /// 本地缓存
    [[HomeSearchManager shareManager] saveOrUpdateByColumnName:key type:self.type];
    
    self.searchStr = key;
    [self refresh];
}



#pragma mark - UI
- (IBAction)btn:(UIButton *)sender {
    [self.view endEditing:YES];
    switch (sender.tag) {
        case 1://返回
        {
            [self backItemClicked];
        }
            break;
        case 2://搜索
        {
            [self saveSearchKeyword];
        }
            break;
    }
}

@end
