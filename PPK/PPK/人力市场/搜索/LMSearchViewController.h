//
//  LMSearchViewController.h
//  ZhiNengShiBie
//
//  Created by null on 2019/8/29.
//  Copyright © 2019 null. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
#pragma mark - 搜索
@interface LMSearchViewController : BaseViewController
@property (nonatomic, copy) NSString *type; // 1 找工作  2找工人  11房源  12寻房
@property (nonatomic, copy) NSString *city;

@end

NS_ASSUME_NONNULL_END
