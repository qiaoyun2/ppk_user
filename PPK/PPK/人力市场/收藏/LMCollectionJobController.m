//
//  LMCollectionJobController.m
//  PPK
//
//  Created by null on 2022/3/10.
//

#import "LMCollectionJobController.h"
#import "LMJobListCell.h"
#import "LMJobDetailController.h"

@interface LMCollectionJobController () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign) BOOL isEdit;
@property (nonatomic, assign) NSInteger page;
@property (weak, nonatomic) IBOutlet UIView *managerView;
@property (weak, nonatomic) IBOutlet UIButton *allButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@end

@implementation LMCollectionJobController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = false;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.managerView.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(collectionEditNoti:) name:@"LMCollectionEdit" object:nil];
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    [self refresh];
}

#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度
//    params[@"longitude"] = @(113.6401); // 经度
//    params[@"latitude"] = @(34.72468); // 纬度
    params[@"type"] = @"1"; // 1工作收藏；2工人收藏
    WeakSelf
    [NetworkingTool getWithUrl:kMarketColletionListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                LMJobListModel *model = [[LMJobListModel alloc] initWithDictionary:obj];
                if (self.allButton.selected) {
                    model.selected = YES;
                }else {
                    model.selected = NO;
                }
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

- (void)requestForDeleteCollection
{
//    params[@"type"] = @"1"; // 1工作收藏；2工人收藏
    
    NSMutableArray *modelArray = [NSMutableArray array];
    NSMutableArray *idArray = [NSMutableArray array];
    for (LMJobListModel *model in self.dataArray) {
        if (model.selected) {
            [idArray addObject:model.ID];
            [modelArray addObject:model];
        }
    }
    if (!modelArray.count) {
        [LJTools showText:@"请先选择" delay:1.5];
        return;
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"jobWorkerIds"] = [idArray componentsJoinedByString:@","];
    params[@"type"] = @"1"; // 1工作收藏；2工人收藏
    [NetworkingTool getWithUrl:kDeleteMarketColletionURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self.dataArray removeObjectsInArray:modelArray];
            [self.tableView reloadData];
            [LJTools showText:@"删除成功" delay:1.5];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LMJobListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LMJobListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"LMJobListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.selectButton.hidden = !self.isEdit;
    LMJobListModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    WeakSelf
    [cell setOnSelectedButtonClick:^(BOOL selected) {
        model.selected = selected;
        [weakSelf checkAllSelected];
    }];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LMJobDetailController *vc = [[LMJobDetailController alloc] init];
    LMJobListModel *model = self.dataArray[indexPath.row];
    vc.jobId = model.ID;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Notification
- (void)collectionEditNoti:(NSNotification *)noti
{
    NSDictionary *obj = noti.userInfo;
    NSInteger isEdit = [obj[@"isEdit"] integerValue];
    [self reload:isEdit];
}

#pragma mark - Function
- (void)reload:(BOOL)isEdit
{
    self.isEdit = isEdit;
    self.managerView.hidden = !isEdit;
    [self.tableView reloadData];
}

- (void)checkAllSelected
{
    for (LMJobListModel *model in self.dataArray) {
        if (!model.selected) {
            self.allButton.selected = NO;
            return;
        }
    }
    self.allButton.selected = YES;
}

#pragma mark - XibFunction
- (IBAction)allButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    for (LMJobListModel *model in self.dataArray) {
        model.selected = sender.selected ? YES : NO;
    }
    [self.tableView reloadData];
}

- (IBAction)deleteButtonAction:(UIButton *)sender {
    [self requestForDeleteCollection];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
