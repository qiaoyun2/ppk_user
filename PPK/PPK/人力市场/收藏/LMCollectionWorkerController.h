//
//  LMCollectionWorkerController.h
//  PPK
//
//  Created by null on 2022/3/10.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMCollectionWorkerController : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void)reload:(BOOL)isEdit;

@end

NS_ASSUME_NONNULL_END
