//
//  LMCollectionController.m
//  PPK
//
//  Created by null on 2022/3/10.
//

#import "LMCollectionController.h"
#import "YNPageViewController.h"
#import "UIView+YNPageExtend.h"
#import "ReLayoutButton.h"
#import "UIImage+Category.h"
#import "LMCollectionWorkerController.h"
#import "LMCollectionJobController.h"
#import "LMCollectionBottomView.h"

@interface LMCollectionController ()<YNPageViewControllerDataSource, YNPageViewControllerDelegate>

@property (nonatomic, strong) YNPageViewController *pageController;
@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) YNPageConfigration *configration;
@property (nonatomic, strong) LMCollectionBottomView *bottomView;

@property (nonatomic, assign) BOOL isEdit;

@end

@implementation LMCollectionController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"我的收藏";
    self.titles = @[@"工作收藏",@"工人收藏"];
    [self setNavigationRightBarButtonWithTitle:@"编辑"];
    [self setupPageVC];
//    [self setupBottomView];
    WeakSelf
    /** 更改header高度 */
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        self.headerView.height = 500;
//        [self.pageController reloadData];
//    });
}

- (void)setupBottomView
{
    self.bottomView = [[[NSBundle mainBundle] loadNibNamed:@"LMCollectionBottomView" owner:nil options:nil] firstObject];
    self.bottomView.hidden = YES;
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(45);
    }];
    [self.bottomView.allButton addTarget:self action:@selector(allButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView.deleteButton addTarget:self action:@selector(deleteButtonClick) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupPageVC {
    
    YNPageConfigration *configration = [YNPageConfigration defaultConfig];
    configration.pageStyle = YNPageStyleTop;

    /// 控制tabbar 和 nav
    configration.showTabbar = YES;
    configration.showNavigation = YES;
    //scrollMenu = NO,aligmentModeCenter = NO 会变成平分
    configration.scrollMenu = NO;
    configration.aligmentModeCenter = NO;
    configration.showBottomLine = NO;
    configration.showScrollLine = NO;
//    configration.bottomLineBgColor = RGBA(218, 98, 57, 1);
//    configration.scrollViewBackgroundColor = UIColorFromRGB(0xF5F5F5);
    configration.menuHeight = 50;
    configration.normalItemColor = RGBA(51, 51, 51, 1);
    configration.selectedItemColor = RGBA(51, 51, 51, 1);
    configration.itemFont = [UIFont systemFontOfSize:16];
    configration.selectedItemFont = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    configration.cutOutHeight = 0;
    self.configration = configration;
    
    NSMutableArray *buttonArrayM = [NSMutableArray array];
    for (int i = 0; i<self.titles.count; i++) {
        ReLayoutButton *button = [ReLayoutButton buttonWithType:UIButtonTypeCustom];
        button.layoutType = 3;
        button.margin = 5;
        UIImage *selectImage = [UIImage imageNamed:@"矩形 1884"];
        [button setImage:[UIImage imageNamed:@"矩形 1884-1"] forState:UIControlStateNormal];
        [button setImage:selectImage forState:UIControlStateSelected];
        [buttonArrayM addObject:button];
    }
    configration.buttonArray = buttonArrayM;
    
    YNPageViewController *vc = [YNPageViewController pageViewControllerWithControllers:self.getArrayVCs titles:self.titles  config:configration];
    vc.dataSource = self;
    vc.delegate = self;
    /// 指定默认选择index 页面
    vc.pageIndex = 0;
    /// 作为自控制器加入到当前控制器
    [vc addSelfToParentViewController:self];
    self.pageController = vc;
}

- (NSArray *)getArrayVCs {
    NSMutableArray *tempArray = [NSMutableArray array];
    LMCollectionJobController *vc1 = [[LMCollectionJobController alloc]init];
    [tempArray addObject:vc1];
    LMCollectionWorkerController *vc2 = [[LMCollectionWorkerController alloc]init];
    [tempArray addObject:vc2];
    return [tempArray copy];
}

- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender
{
    self.isEdit= !self.isEdit;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LMCollectionEdit" object:nil userInfo:@{@"isEdit":@(self.isEdit)}];
    if (self.isEdit) {
        [self setNavigationRightBarButtonWithTitle:@"返回"];
//        self.configration.cutOutHeight = 45;
//        [self.pageController reloadData];
//        self.bottomView.hidden = NO;
    }else {
        [self setNavigationRightBarButtonWithTitle:@"编辑"];
//        self.configration.cutOutHeight = 0;
//        [self.pageController reloadData];
//        self.bottomView.hidden = YES;
    }
}

- (void)allButtonClick:(UIButton *)sender
{
    
}

- (void)deleteButtonClick
{
    
}

#pragma mark - YNPageViewControllerDataSource
- (UIScrollView *)pageViewController:(YNPageViewController *)pageViewController pageForIndex:(NSInteger)index {
    UIViewController *vc = pageViewController.controllersM[index];
    if (index==0) {
        return [(LMCollectionJobController *)vc tableView];
    }
    return [(LMCollectionWorkerController *)vc tableView];
}

#pragma mark - YNPageViewControllerDelegate
- (void)pageViewController:(YNPageViewController *)pageViewController
            contentOffsetY:(CGFloat)contentOffset
                  progress:(CGFloat)progress {
}

- (void)pageViewController:(YNPageViewController *)pageViewController
                 didScroll:(UIScrollView *)scrollView
                  progress:(CGFloat)progress
                 formIndex:(NSInteger)fromIndex
                   toIndex:(NSInteger)toIndex
{
    UIViewController *vc = pageViewController.controllersM[toIndex];
    if (toIndex==0) {
        [(LMCollectionJobController *)vc reload:self.isEdit];
    }else {
        [(LMCollectionWorkerController *)vc reload:self.isEdit];
    }

}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
