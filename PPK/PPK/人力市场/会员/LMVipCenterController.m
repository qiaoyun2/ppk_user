//
//  LMVipCenterController.m
//  PPK
//
//  Created by null on 2022/3/12.
//

#import "LMVipCenterController.h"
#import "PurchaseLogsController.h"
#import "MyPublishJobController.h"
#import "MyPublishWorkerController.h"
#import "CertificationController.h"
#import "OpenVipView.h"
#import "PersonDataController.h"

@interface LMVipCenterController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *publishVipTypeLabel; // 发布季卡VIP
@property (weak, nonatomic) IBOutlet UILabel *topVipTypeLabel; // 置顶季卡VIP

@property (weak, nonatomic) IBOutlet UILabel *publishRemainTimeLabel; // 发布会员剩余时间:999天
@property (weak, nonatomic) IBOutlet UILabel *topRemainTimeLabel; // 置顶会员剩余时间:999天
@property (weak, nonatomic) IBOutlet UIImageView *cerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *enterpriseImageView;
@property (weak, nonatomic) IBOutlet UIImageView *noCerImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;

@property (nonatomic, strong) OpenVipView *vipView;

@end

@implementation LMVipCenterController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    if ([LJTools islogin]) {
        [self requestForUserInfo];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.topViewHeight.constant = NavAndStatusHight;
    self.noCerImageView.hidden = self.cerImageView.hidden = self.enterpriseImageView.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySuccess:) name:@"PaySuccess" object:nil];
    
}

- (void)requestForUserInfo
{
    [NetworkingTool getWithUrl:kGetUserInfoURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            User *user = [User mj_objectWithKeyValues:responseObject[@"data"]];
            [User saveUser:user];
            [self updateSubviews];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

#pragma mark - Function
- (void)updateSubviews
{
    User *user = [User getUser];
    self.nameLabel.text = user.user_nickname;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(user.avatar)] placeholderImage:DefaultImgHeader];
//    self.cerImageView.image = [user.authTypePersonal integerValue]
//    if (<#condition#>) {
//        <#statements#>
//    }
    
    self.noCerImageView.hidden = self.cerImageView.hidden = self.enterpriseImageView.hidden = YES;
    if ([user.authTypeCompany integerValue]==1) {
        // 如果提交了企业认证
        self.cerImageView.hidden = NO;
        if ([user.authStatus integerValue]==2) {
            self.enterpriseImageView.hidden = NO;
        }
    }
    else {
        // 未提交企业认证
        if ([user.authTypePersonal integerValue]==1) {
            // 提交了实名认证
            if ([user.authStatus integerValue]==2) {
                self.cerImageView.hidden = NO;
            }
        }else {
            // 未提交实名认证
            self.noCerImageView.hidden = NO;
        }
    }
    
    if ([user.isReleaseVip integerValue]>0) {
        self.publishVipTypeLabel.text = [NSString stringWithFormat:@"%@  免费发布",user.vipReleaseTypeName];
        if ([user.vipReleaseLastDay integerValue]>0) {
            self.publishRemainTimeLabel.text = [NSString stringWithFormat:@"%@剩余时间：%@天",user.vipReleaseTypeName,user.vipReleaseLastDay];
        }
        if ([user.releaseNumber integerValue]>0) {
            self.publishRemainTimeLabel.text = [NSString stringWithFormat:@"%@剩余次数：%@次",user.vipReleaseTypeName,user.releaseNumber];
        }
    }
    else {
        self.publishVipTypeLabel.text = @"当前未开通发布会员";
        self.publishRemainTimeLabel.text = @"开通会员享更多优惠";
    }
    
    if ([user.isTopVip integerValue]>0) {
        self.topVipTypeLabel.text = [NSString stringWithFormat:@"%@  免费发布",user.vipTopTypeName];
        self.topRemainTimeLabel.hidden = NO;
        if ([user.vipTopLastDay integerValue]>0) {
            self.topRemainTimeLabel.text = [NSString stringWithFormat:@"%@剩余时间：%@天",user.vipTopTypeName,user.vipTopLastDay];
        }
        if ([user.topNumber integerValue]>0) {
            self.topRemainTimeLabel.text = [NSString stringWithFormat:@"%@剩余次数：%@次",user.vipTopTypeName,user.topNumber];
        }
    }else {
        self.topVipTypeLabel.text = @"当前未开通置顶会员";
        self.topRemainTimeLabel.text = @"开通会员享更多优惠";
        self.topRemainTimeLabel.hidden = YES;
    }
}

- (void)paySuccess:(NSNotification *)noti
{
    [self requestForUserInfo];
}

#pragma mark - XibFunction
// 返回
- (IBAction)backButtonAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

// 记录
- (IBAction)logButtonAction:(id)sender {
    PurchaseLogsController *vc = [[PurchaseLogsController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

// 开通vip
- (IBAction)openButtonAction:(id)sender {
    if (!self.vipView) {
        OpenVipView *view = [[[NSBundle mainBundle] loadNibNamed:@"OpenVipView" owner:nil options:nil] firstObject];
        view.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:view];
        self.vipView = view;
    }
    [self.vipView show];
}

// 发布的工作
- (IBAction)jobButtonAction:(id)sender {
    MyPublishJobController *vc = [[MyPublishJobController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

// 发布的找活
- (IBAction)workerButtonAction:(id)sender {
    MyPublishWorkerController *vc = [[MyPublishWorkerController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)certificationButtonAction:(id)sender
{
    [self.navigationController pushViewController:[[CertificationController alloc] init] animated:YES];
}

- (IBAction)headImageViewTap:(id)sender {
    PersonDataController *vc = [[PersonDataController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
