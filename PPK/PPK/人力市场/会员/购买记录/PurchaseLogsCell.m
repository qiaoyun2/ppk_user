//
//  PurchaseLogsCell.m
//  PPK
//
//  Created by null on 2022/3/12.
//

#import "PurchaseLogsCell.h"

@implementation PurchaseLogsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(PurchaseLogsModel *)model
{
    _model = model;
    self.titleLabel.text = [NSString stringWithFormat:@"%@会员",model.vipTypeName];
    self.timeLabel.text = model.createTime;
    self.coinLabel.text = [NSString stringWithFormat:@"- %@",model.price];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
