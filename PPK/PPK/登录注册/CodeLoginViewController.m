//
//  CodeLoginViewController.m
//  ZZR
//
//  Copyright © 2019 null. All rights reserved.
//

#import "CodeLoginViewController.h"
#import "UIButton+Code.h"
#import "CodeInputView.h"
#import "LMTabBarController.h"
#import "RWTabbarController.h"


@interface CodeLoginViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoImageViewTop;

@property (weak, nonatomic) IBOutlet UIButton *codeButton;
@property (weak, nonatomic) IBOutlet UILabel *resendLabel;
@property (weak, nonatomic) IBOutlet UIView *codeBgView;


@property(nonatomic, strong) CodeInputView *codeView;
@property(nonatomic, copy) NSString *code;



@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end


static dispatch_source_t _timer;

@implementation CodeLoginViewController

#pragma mark - Life Cycle Methods
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    [self.codeBtn cancelCountdownWithEndString:@"获取验证码"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.logoImageViewTop.constant = 40+StatusHight;
    [self.codeBgView addSubview:self.codeView];
    [self startTimer];
}
#pragma mark – UI
#pragma mark - Network
- (void)requestForCodeLogin
{
    [self.view endEditing:YES];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"mobile"] = self.mobile;
    parameters[@"captcha"] = self.code;
    parameters[@"event"] = @"login";
    [NetworkingTool postWithUrl:kCodeLoginURL params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSDictionary *userDic = responseObject[@"data"][@"user"];
            User *user = [User mj_objectWithKeyValues:userDic];
            [User saveUser:user];
            [UIApplication sharedApplication].keyWindow.rootViewController = [[LMTabBarController alloc] init];
//            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            
        }
        [LJTools showOKHud:responseObject[@"msg"] delay:1.0];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
#pragma mark - Delegate
#pragma mark - UITableViewDelgate
#pragma mark – Lazy
- (CodeInputView *)codeView {
    __weak typeof(self) weakSelf = self;
    if (!_codeView) {
        _codeView = [[CodeInputView alloc]initWithFrame:CGRectMake(28, 0, CGRectGetWidth(self.view.frame)-56, 56) inputType:4 selectCodeBlock:^(NSString * code) {
            NSLog(@"code === %@",code);
            weakSelf.code = code;
            if (code.length==4) {
                [weakSelf requestForCodeLogin];
            }
        }];
    }
    return _codeView;
}


#pragma mark – Function
- (void)startTimer
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0);
    //每秒执行
    __block int timeout = 60;
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置（倒计时结束后调用）
                [self.codeButton setTitle:@"重新发送验证码" forState:UIControlStateNormal];
                self.resendLabel.hidden = YES;
            });
        }else{
            
            int seconds = timeout;
            NSString *strTime = [NSString stringWithFormat:@"%02d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.codeButton setTitle:[NSString stringWithFormat:@"00:%@",strTime] forState:UIControlStateNormal];
                self.codeButton.userInteractionEnabled = NO;
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

#pragma mark – XibFunction
/// 获取验证码
- (IBAction)codeButtonAction:(UIButton *)sender {
    [self.view endEditing:YES];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = self.mobile;
    params[@"event"] = @"login";
    [NetworkingTool postWithUrl:kGetCodeURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self startTimer];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - Lazy Loads

@end
