//
//  ForgetPasswordViewController.h
//  ZZR
//
//  Created by null on 2018/12/24.
//  Copyright © 2018 null. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ForgetPasswordViewController : BaseViewController

@property (nonatomic, copy) void(^block)(NSString *mobile, NSString *password);

@property (nonatomic, assign) NSInteger type; // 1 忘记密码  2、绑定手机号

@end

NS_ASSUME_NONNULL_END
