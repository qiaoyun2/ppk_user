//
//  TOClassifyLeftCell.m
//  ZZR
//
//  Created by null on 2019/12/25.
//  Copyright © 2019 null. All rights reserved.
//

#import "TOClassifyLeftCell.h"

@implementation TOClassifyLeftCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (TOClassifyLeftCell *)cellWithTableView:(UITableView *)tableView
{
    TOClassifyLeftCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TOClassifyLeftCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"TOClassifyLeftCell" owner:nil options:nil]lastObject];
    }
    return cell;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
//    if (selected) {
//        self.indicatorLabel.hidden = NO;
//        self.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
//    }
//    else{
//        self.indicatorLabel.hidden = YES;
//        self.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
//    }
    // Configure the view for the selected state
}

@end
