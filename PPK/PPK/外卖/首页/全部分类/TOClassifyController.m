//
//  TOClassifyController.m
//  ZZR
//
//  Created by null on 2019/12/25.
//  Copyright © 2019 null. All rights reserved.
//

#import "TOClassifyController.h"
#import "TOClassifyLeftCell.h"
#import "TOClassifyRightCell.h"
#import "ClassifyRightReusableView.h"
//#import "ClassifySortController.h"
#import "JJCollectionViewRoundFlowLayout.h"
#import "TOCategoryModel.h"

@interface TOClassifyController ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,JJCollectionViewDelegateRoundFlowLayout>
{
    NSInteger _selectIndex;
    BOOL _isScrollDown;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;

@property (nonatomic, strong) NSMutableArray *cateModels;
@property (nonatomic, strong) NSMutableArray *subCateModels;
//@property (nonatomic, assign) NSInteger cateIndex;


@end

@implementation TOClassifyController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = nil;
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    JJCollectionViewRoundFlowLayout *layout = [[JJCollectionViewRoundFlowLayout alloc]init];
    layout.isCalculateHeader = YES;
    layout.isCalculateFooter = YES;
    [self.collectionView setCollectionViewLayout:layout];
    [self.collectionView registerNib:[UINib nibWithNibName:@"TOClassifyRightCell" bundle:nil] forCellWithReuseIdentifier:@"TOClassifyRightCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ClassifyRightReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ClassifyRightReusableView"];
//    [collectionView registerClass:[MyCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([MyCollectionReusableView class])];
//
//    [collectionView registerClass:[MyCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass([MyCollectionReusableView class])];
//
//    collectionView.translatesAutoresizingMaskIntoConstraints = NO;

    self.cateModels = [NSMutableArray array];
    self.subCateModels = [NSMutableArray array];
    self.topViewHeight.constant = NavAndStatusHight;
    _selectIndex = 0;
    _isScrollDown = YES;
    [self requestForCate];
}

- (void)requestForCate
{
    [NetworkingTool getWithUrl:kTOShopCategoryURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.cateModels removeAllObjects];
        if ([responseObject[@"code"] integerValue]==1) {
            NSArray *dataArray = responseObject[@"data"];
            for (NSDictionary *obj in dataArray) {
                TOCategoryModel *model = [[TOCategoryModel alloc] initWithDictionary:obj];
                [self.cateModels addObject:model];
            }
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.tableView reloadData];
        [self requestForSubCate];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {

    } IsNeedHub:NO];
}

- (void)requestForSubCate
{
    TOCategoryModel *model = self.cateModels[_selectIndex];
    [NetworkingTool getWithUrl:kTOShopChildCategoryURL params:@{@"pid":model.ID} success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.subCateModels removeAllObjects];
        if ([responseObject[@"code"] integerValue]==1) {
            NSArray *dataArray = responseObject[@"data"];
            for (NSDictionary *obj in dataArray) {
                TOCategoryModel *model = [[TOCategoryModel alloc] initWithDictionary:obj];
                [self.subCateModels addObject:model];
            }
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.collectionView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {

    } IsNeedHub:NO];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cateModels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TOClassifyLeftCell *cell = [TOClassifyLeftCell cellWithTableView:tableView];
    TOCategoryModel *model = self.cateModels[indexPath.row];
    cell.titleLabel.text = [NSString stringWithFormat:@"%@",model.name];
    if (indexPath.row == _selectIndex) {
        cell.indicatorLabel.hidden = NO;
        cell.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    }
    else{
        cell.indicatorLabel.hidden = YES;
        cell.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectIndex = indexPath.row;
    [self.tableView reloadData];
    [self requestForSubCate];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:(UICollectionViewScrollPositionTop) animated:YES];
//    [self scrollToTopOfSection:_selectIndex animated:YES];
//    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_selectIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    
}

- (void)tableView:(UITableView *)tableView didEndDisplayingHeaderView:(UIView *)view forSection:(NSInteger)section
{
    
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.subCateModels.count;
//    NSDictionary *dataDic = self.dataArray[section];
//    if (dataDic[@"child"] && [dataDic[@"child"] isKindOfClass:[NSArray class]]) {
//        NSArray *array = dataDic[@"child"];
//        NSLog(@"%ld",array.count);
//        return array.count;
//    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TOClassifyRightCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TOClassifyRightCell" forIndexPath:indexPath];
    TOCategoryModel *model = self.subCateModels[indexPath.row];
    [cell.thumbImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.picture)] placeholderImage:DefaultImgWidth];
//    NSDictionary *dataDic = self.dataArray[indexPath.section];
//    if (dataDic[@"child"] && [dataDic[@"child"] isKindOfClass:[NSArray class]]) {
//        NSArray *array = dataDic[@"child"];
//        if (array.count) {
//            NSDictionary *cateInfoDic = array[indexPath.row];
//            cell.nameLabel.text = cateInfoDic[@"name"];
//            [cell.thumbImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",cateInfoDic[@"thumb"]]] placeholderImage:DefaultImgWidth];
//
//        }
//
//    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    id = 66;
    name = "华为手机";
    pid = 42;
    thumb = "http://zb.sh
     */
//    NSDictionary *dataDic = self.dataArray[indexPath.section];
//    if (dataDic[@"child"] && [dataDic[@"child"] isKindOfClass:[NSArray class]]) {
//        NSArray *array = dataDic[@"child"];
//        if (array.count) {
//
//        }else{
//
//        }
//    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader])
    {
        ClassifyRightReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"ClassifyRightReusableView" forIndexPath:indexPath];
        if (self.cateModels.count) {
            TOCategoryModel *model = self.cateModels[_selectIndex];
            view.titleLabel.text = [NSString stringWithFormat:@"%@",model.name];
        }
        return view;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(SCREEN_WIDTH, 52);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH-100-40-50-12)/3,(SCREEN_WIDTH-100-40-50-12)/3+25);
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 20, 24, 32);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 16.f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 24.f;
}


// CollectionView分区标题即将展示
- (void)collectionView:(UICollectionView *)collectionView willDisplaySupplementaryView:(UICollectionReusableView *)view forElementKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath
{
    // 当前CollectionView滚动的方向向上，CollectionView是用户拖拽而产生滚动的（主要是判断CollectionView是用户拖拽而滚动的，还是点击TableView而滚动的）
    if (!_isScrollDown && (collectionView.dragging || collectionView.decelerating))
    {
        [self selectRowAtIndexPath:indexPath.section];
    }
}

// CollectionView分区标题展示结束
- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingSupplementaryView:(nonnull UICollectionReusableView *)view forElementOfKind:(nonnull NSString *)elementKind atIndexPath:(nonnull NSIndexPath *)indexPath
{
    // 当前CollectionView滚动的方向向下，CollectionView是用户拖拽而产生滚动的（主要是判断CollectionView是用户拖拽而滚动的，还是点击TableView而滚动的）
    if (_isScrollDown && (collectionView.dragging || collectionView.decelerating))
    {
        [self selectRowAtIndexPath:indexPath.section + 1];
    }
}

#pragma mark - JJCollectionViewDelegateRoundFlowLayout
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout borderEdgeInsertsForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0.f, 0.f, 8.f, 12.f);
}

- (JJCollectionViewRoundConfigModel *)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout configModelForSectionAtIndex:(NSInteger)section{
    JJCollectionViewRoundConfigModel *model = [[JJCollectionViewRoundConfigModel alloc]init];
    model.backgroundColor = [UIColor whiteColor];
    model.cornerRadius = 8;
    return model;
}

#pragma mark - UIScrollView Delegate
// 标记一下CollectionView的滚动方向，是向上还是向下
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    static float lastOffsetY = 0;

    if (self.collectionView == scrollView)
    {
        _isScrollDown = lastOffsetY < scrollView.contentOffset.y;
        lastOffsetY = scrollView.contentOffset.y;
    }
}

#pragma mark - 解决点击 TableView 后 CollectionView 的 Header 遮挡问题
- (void)scrollToTopOfSection:(NSInteger)section animated:(BOOL)animated
{
    CGRect headerRect = [self frameForHeaderForSection:section];
    CGPoint topOfHeader = CGPointMake(0, headerRect.origin.y - _collectionView.contentInset.top);
    [self.collectionView setContentOffset:topOfHeader animated:animated];
}

- (CGRect)frameForHeaderForSection:(NSInteger)section
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:section];
    UICollectionViewLayoutAttributes *attributes = [self.collectionView.collectionViewLayout layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader atIndexPath:indexPath];
    return attributes.frame;
}

// 当拖动CollectionView的时候，处理TableView
- (void)selectRowAtIndexPath:(NSInteger)index
{
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
}

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)searchButtonAction:(UIButton *)sender {
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
