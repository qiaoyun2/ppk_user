//
//  ClassifyRightReusableView.m
//  ZZR
//
//  Created by null on 2019/12/26.
//  Copyright © 2019 null. All rights reserved.
//

#import "ClassifyRightReusableView.h"

@implementation ClassifyRightReusableView

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (IBAction)moreButtonAction:(id)sender {
    if (self.onMoreButtonClick) {
        self.onMoreButtonClick();
    }
}
@end
