//
//  TOClassifyLeftCell.h
//  ZZR
//
//  Created by null on 2019/12/25.
//  Copyright © 2019 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TOClassifyLeftCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *indicatorLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomLineLabel;

+ (TOClassifyLeftCell *)cellWithTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
