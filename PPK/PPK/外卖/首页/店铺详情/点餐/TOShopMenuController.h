//
//  TOShopMenuController.h
//  PPK
//
//  Created by null on 2022/7/25.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TOShopMenuController : BaseViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@property (weak, nonatomic) IBOutlet UITableView *goodsTableView;




@end

NS_ASSUME_NONNULL_END
