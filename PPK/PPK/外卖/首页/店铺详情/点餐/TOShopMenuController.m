//
//  TOShopMenuController.m
//  PPK
//
//  Created by null on 2022/7/25.
//

#import "TOShopMenuController.h"
#import "UIViewController+YNPageExtend.h"

@interface TOShopMenuController () <UITableViewDelegate,UITableViewDataSource>
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuTableViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@end

@implementation TOShopMenuController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the viexfxsw from its nib.
    [self.goodsTableView reloadData];
    
    self.contentViewHeight.constant = 48*20;
//    self.contentViewHeight.constant = 48*10;
//    self.yn_pageViewController.conte
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.menuTableView) {
        return 1;
    }
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.menuTableView) {
        return 20;
    }
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.menuTableView) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuTableView"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"menuTableView"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.textLabel.text = [NSString stringWithFormat:@"%ld行",indexPath.row];
        return cell;
    }

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"goodsTableView"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"goodsTableView"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = [NSString stringWithFormat:@"goods%ld区%ld行",indexPath.section,indexPath.row];
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
