//
//  TOShopDetailHeaderView.m
//  PPK
//
//  Created by null on 2022/7/21.
//

#import "TOShopDetailHeaderView.h"

@implementation TOShopDetailHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.shadowView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    self.shadowView.layer.shadowOffset = CGSizeMake(0,1.5);
    self.shadowView.layer.shadowRadius = 6;
    self.shadowView.layer.shadowOpacity = 1;
    self.shadowView.layer.cornerRadius = 12;
}

- (IBAction)moreButtonAction:(UIButton *)sender {
    
}

@end
