//
//  TOShopDetailNavBar.h
//  PPK
//
//  Created by null on 2022/7/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TOShopDetailNavBar : UIView
@property (weak, nonatomic) IBOutlet UIView *clearView;
@property (weak, nonatomic) IBOutlet UIView *whiteView;

@end

NS_ASSUME_NONNULL_END
