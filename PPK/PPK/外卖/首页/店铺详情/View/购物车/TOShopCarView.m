//
//  TOShopCarView.m
//  PPK
//
//  Created by null on 2022/7/30.
//

#import "TOShopCarView.h"
#import "TOShopCarCell.h"

@interface TOShopCarView () <UITableViewDelegate,UITableViewDataSource>

@end

@implementation TOShopCarView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.contentViewHeight.constant = 450;
    self.contentViewBottom.constant = - self.contentViewHeight.constant;
    [self.contentView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(12, 12) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, self.contentViewHeight.constant)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TOShopCarCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TOShopCarCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TOShopCarCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - Function
- (void)show
{
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0.4);
        self.contentViewBottom.constant = 0;
        [self layoutIfNeeded];
    }];
}

- (void)dismiss
{
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0);
        self.contentViewBottom.constant = - self.contentViewHeight.constant;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

- (IBAction)clearButtonAction:(UIButton *)sender {
    
}

- (IBAction)onCloseButtonClick:(id)sender {
    [self dismiss];
}

@end
