//
//  TOShopCarView.h
//  PPK
//
//  Created by null on 2022/7/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TOShopCarView : UIView

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *packingFeeLabel;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@property (nonatomic, copy) void(^onPlatformDidSelected)(NSInteger tag);

- (void)show;
- (void)dismiss;


@end

NS_ASSUME_NONNULL_END
