//
//  TOShopDetailController.m
//  PPK
//
//  Created by null on 2022/7/21.
//

#import "TOShopDetailController.h"
#import "YNPageViewController.h"
#import "UIView+YNPageExtend.h"
#import "TOShopMenuController.h"
#import "TOShopDetailHeaderView.h"
#import "TOShopDetailNavBar.h"

@interface TOShopDetailController ()<YNPageViewControllerDataSource, YNPageViewControllerDelegate>

@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) TOShopDetailHeaderView *headerView;
@property (nonatomic, strong) TOShopDetailNavBar *navBar;
@property (nonatomic, assign) BOOL lightStatusBar;



@end

@implementation TOShopDetailController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}


/*
3. 当触发某个条件需要改变状态栏颜色时在 UIViewController 中调用
然后在 - (UIStatusBarStyle)preferredStatusBarStyle; 中判断你的条件是否满足改变颜色
*/
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return self.lightStatusBar ? UIStatusBarStyleLightContent : UIStatusBarStyleDefault;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单";
    self.lightStatusBar = YES;
    self.headerView = [[[NSBundle mainBundle] loadNibNamed:@"TOShopDetailHeaderView" owner:nil options:nil] firstObject];
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 230);
    self.titles = @[@"点餐",@"评价",@"商家"];
    [self setupPageVC];
    [self setupNavBar];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)setupNavBar
{
    self.navBar = [[[NSBundle mainBundle] loadNibNamed:@"TOShopDetailNavBar" owner:nil options:nil] firstObject];
    self.navBar.frame = CGRectMake(0, 0, SCREEN_WIDTH, NavAndStatusHight);
    [self.view addSubview:self.navBar];
}

- (void)setupPageVC {
    YNPageConfigration *configration = [YNPageConfigration defaultConfig];
    configration.pageStyle = YNPageStyleSuspensionCenter;
    /// 控制tabbar 和 nav
    configration.showTabbar = NO;
    configration.showNavigation = NO;
    //scrollMenu = NO,aligmentModeCenter = NO 会变成平分
    configration.scrollMenu = YES;
    configration.aligmentModeCenter = NO;
    configration.showBottomLine = NO;
    configration.showScrollLine = NO;
    configration.itemMargin = 50;
    configration.menuHeight = 55;
    configration.normalItemColor = RGBA(51, 51, 51, 1);
    configration.selectedItemColor = RGBA(51, 51, 51, 1);
    configration.itemFont = [UIFont systemFontOfSize:16];
    configration.selectedItemFont = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    configration.cutOutHeight = 0;
    configration.suspenOffsetY = NavAndStatusHight;
    
    NSMutableArray *buttonArrayM = [NSMutableArray array];
    for (int i = 0; i<self.titles.count; i++) {
        ReLayoutButton *button = [ReLayoutButton buttonWithType:UIButtonTypeCustom];
        button.layoutType = 3;
        button.margin = 3;
        UIImage *selectImage = [UIImage imageNamed:@"矩形 1884"];
        [button setImage:[UIImage imageNamed:@"矩形 1884-1"] forState:UIControlStateNormal];
        [button setImage:selectImage forState:UIControlStateSelected];
        [buttonArrayM addObject:button];
    }
    configration.buttonArray = buttonArrayM;
    
    YNPageViewController *vc = [YNPageViewController pageViewControllerWithControllers:self.getArrayVCs titles:self.titles  config:configration];
    vc.headerView = self.headerView;
    vc.dataSource = self;
    vc.delegate = self;
    /// 指定默认选择index 页面
    vc.pageIndex = 0;
    /// 作为自控制器加入到当前控制器
    [vc addSelfToParentViewController:self];
}

- (NSArray *)getArrayVCs {
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSInteger i = 0; i<self.titles.count; i++) {
        TOShopMenuController *vc = [[TOShopMenuController alloc] init];
//        vc.status = 0;
        [tempArray addObject:vc];
    }
    return [tempArray copy];
}

#pragma mark - YNPageViewControllerDataSource
- (UIScrollView *)pageViewController:(YNPageViewController *)pageViewController pageForIndex:(NSInteger)index {
    UIViewController *vc = pageViewController.controllersM[index];
    return [(TOShopMenuController *)vc scrollView];
}

#pragma mark - YNPageViewControllerDelegate
- (void)pageViewController:(YNPageViewController *)pageViewController
            contentOffsetY:(CGFloat)contentOffset
                  progress:(CGFloat)progress {
    NSLog(@"%f-----%f",contentOffset,progress);
    TOShopMenuController *vc = pageViewController.controllersM[0];
    if (progress < 1) {
        self.navBar.clearView.alpha = 1-progress;
        self.navBar.whiteView.alpha = progress;
        self.lightStatusBar = YES;
    }else {
        self.navBar.clearView.alpha = 0;
        self.navBar.whiteView.alpha = 1;
        self.lightStatusBar = NO;
    }
    
    if (contentOffset<=0) {
        if (contentOffset+StatusHight+55<0) {
            vc.menuTableView.scrollEnabled = NO;
            vc.goodsTableView.scrollEnabled = NO;
            vc.scrollView.scrollEnabled = YES;
        }else {
            vc.menuTableView.scrollEnabled = YES;
            vc.goodsTableView.scrollEnabled = YES;
            vc.scrollView.scrollEnabled = NO;
        }
    }else {
        vc.menuTableView.scrollEnabled = YES;
        vc.goodsTableView.scrollEnabled = YES;
        vc.scrollView.scrollEnabled = NO;
    }
    NSLog(@"%f",-contentOffset);
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)pageViewController:(YNPageViewController *)pageViewController
                 didScroll:(UIScrollView *)scrollView
                  progress:(CGFloat)progress
                 formIndex:(NSInteger)fromIndex
                   toIndex:(NSInteger)toIndex
{

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
