//
//  TOHomeController.m
//  PPK
//
//  Created by null on 2022/7/11.
//

#import "TOHomeController.h"
#import "TOHomeHeaderCell.h"
#import "TOShopListCell.h"
#import "SYTypeButtonView.h"
#import "TOShopDetailController.h"
#import "TOSortDownView.h"



@interface TOHomeController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;


@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) TOSortDownView *sortView;

@property (nonatomic, strong) NSMutableArray *bannerModels;
@property (nonatomic, strong) NSMutableArray *cateModels;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) dispatch_group_t group;

@property (nonatomic, strong) NSString *sortType;


@end

@implementation TOHomeController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.topViewHeight.constant = NavAndStatusHight;
    self.bannerModels = [NSMutableArray array];
    self.cateModels = [NSMutableArray array];
    
    [self initSectionHeaderView];
    [self dispatch_group_network];
    [self refresh];
}

#pragma mark - UI
- (void)initSectionHeaderView
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 67)];
    headerView.backgroundColor = UIColorFromRGB(0xF5F6F9);
    self.headerView = headerView;

    SYTypeButtonView *buttonView = [[SYTypeButtonView alloc] initWithFrame:CGRectMake(16, 13, SCREEN_WIDTH-32, 43) view:headerView];
    [headerView addSubview:buttonView];
    buttonView.backgroundColor = [UIColor whiteColor];
    buttonView.cornerRadius = 8;
    WeakSelf
    buttonView.buttonClick = ^(NSInteger index, BOOL isDescending){
        NSLog(@"click index %ld, isDescending %d", index, isDescending);
        
//        if (selected) {
//            [weakSelf.sortView hideDropDown];
//            CGFloat time = 0.0;
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(time * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [weakSelf.sortView showDropDown];
//
//            });
//        }else {
//            [weakSelf.sortView hideDropDown];
//
//        }

        
//        NSString *orderStr = isDescending?@"desc":@"asc";
//        self.is_new = NO;
//        if (index==0) {
//            //综合
//            self.sort = @"1";
//            self.order = @"asc";
//        }else if (index==1){
//            //新品
//            self.sort = @"1";
//            self.order = @"asc";
//            self.is_new = YES;
//        }else if (index==2){
//            //价格
//            self.sort = @"3";
//            self.order =orderStr;
//        }else{
//            //销量
//            self.sort = @"2";
//            self.order =@"desc";
//        }
//        self.page = 1;
//        [self requestForGoodsListPage:self.page];
    };
    buttonView.titleColorNormal = UIColorFromRGB(0x333333);
    buttonView.titleColorSelected = UIColorFromRGB(0x333333);
    buttonView.titles = @[@"热门店铺", @"综合排序",@"销量",@"距离"];
    buttonView.enableTitles = @[@"销量",@"距离"];
    buttonView.titleFont = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
    buttonView.titleFontSelected = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];

    NSDictionary *dict01 = [NSDictionary dictionary];
    NSDictionary *dict02 = [NSDictionary dictionaryWithObjectsAndKeys:[UIImage imageNamed:@"sort_single_down"], keyImageNormal, [UIImage imageNamed:@"sort_single_up"], keyImageSelected, nil];
    NSDictionary *dict03 = [NSDictionary dictionaryWithObjectsAndKeys:[UIImage imageNamed:@"sort_normal"], keyImageNormal, [UIImage imageNamed:@"sort_up"], keyImageSelectedDouble, [UIImage imageNamed:@"sort_down"], keyImageSelected, nil];
    NSDictionary *dict04 = [NSDictionary dictionaryWithObjectsAndKeys:[UIImage imageNamed:@"sort_normal"], keyImageNormal, [UIImage imageNamed:@"sort_up"], keyImageSelectedDouble, [UIImage imageNamed:@"sort_down"], keyImageSelected, nil];
    buttonView.imageTypeArray = @[dict01, dict02, dict03,dict04];
    
}

#pragma mark - Network
- (void)dispatch_group_network
{
    // 请求多个网络请求, 并监听所有网络请求都结束.
    // 创建一个组
    self.group = dispatch_group_create();
    dispatch_queue_t serialQueue = dispatch_queue_create("io.dcloud.pinpinkan", DISPATCH_QUEUE_SERIAL);
    // 网络请求 1
    {
        // 进入组
        dispatch_group_enter(self.group);
        dispatch_group_async(self.group, serialQueue, ^{
            [self requestForBanner];
            NSLog(@"网络请求 1 结束 %@", [NSThread currentThread]);
        });
    }
    
    // 网络请求 2
    {
        // 进入组
        dispatch_group_enter(self.group);
        // 假装这里有一个网络请求
         dispatch_group_async(self.group, serialQueue, ^{
            [self requestForCate];
            NSLog(@"网络请求 2 结束 %@", [NSThread currentThread]);
        });
    }
    
    // 所有子线程任务全部完成, 并回到主线程
    dispatch_group_notify(self.group, dispatch_get_main_queue(), ^{
        NSLog(@"所有请求结束 %@", [NSThread currentThread]);
        [self.tableView reloadData];
    });
}


// banner
- (void)requestForBanner
{
    [NetworkingTool getWithUrl:kTOBannerListURL params:@{@"position":@"1"} success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.bannerModels removeAllObjects];
        if ([responseObject[@"code"] integerValue]==1) {
            NSArray *dataArray = responseObject[@"data"];
            for (NSDictionary *obj in dataArray) {
                TOBannerModel *model = [[TOBannerModel alloc] initWithDictionary:obj];
                [self.bannerModels addObject:model];
            }
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        // 任务结束, 退出组
        dispatch_group_leave(self.group);
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        // 任务结束, 退出组
        dispatch_group_leave(self.group);
    } IsNeedHub:NO];
}

- (void)requestForCate
{
    [NetworkingTool getWithUrl:kTOShopCategoryURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.cateModels removeAllObjects];
        if ([responseObject[@"code"] integerValue]==1) {
            NSArray *dataArray = responseObject[@"data"];
            for (NSDictionary *obj in dataArray) {
                TOCategoryModel *model = [[TOCategoryModel alloc] initWithDictionary:obj];
                [self.cateModels addObject:model];
            }
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        // 任务结束, 退出组
        dispatch_group_leave(self.group);
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        // 任务结束, 退出组
        dispatch_group_leave(self.group);
    } IsNeedHub:NO];
}

- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    if (self.searchField.text.length) {
        params[@"searchTxt"] = self.searchField.text;
    }
    
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(15);
    
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度
    
    params[@"sort"] = @""; // 综合排序：0-默认;1-人均价最低;2-派送费最低;3-好评最高
    params[@"sales"] = @""; // 销量：0-默认; 1-倒序;2-升序
    params[@"distance"] = @""; // 距离：0-默认，1-倒序;2-升序
    params[@"price"] = @""; // 价格：0-默认，1-倒序;2-升序
    WeakSelf
    [NetworkingTool getWithUrl:kTOShopListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"shopList"];
            for (NSDictionary *obj in dataArray) {
                TOHomeShopListModel *model = [[TOHomeShopListModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView withY:200];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}



#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 1;
    }
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        TOHomeHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TOHomeHeaderCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"TOHomeHeaderCell" owner:nil options:nil] firstObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [cell updateHeaderView:self.bannerModels cate:self.cateModels];

        return cell;
    }
    
    TOShopListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TOShopListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TOShopListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    TOHomeShopListModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TOShopDetailController *vc = [[TOShopDetailController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==0) {
        return [[UIView alloc] init];
    }
    return self.headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0) {
        return CGFLOAT_MIN;
    }
    return 67;
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}

#pragma mark - Network

#pragma mark - Function
// 排序
- (void)showSortDropDownView
{
    if (!self.sortView) {
        self.sortView = [[[NSBundle mainBundle] loadNibNamed:@"SortDownView" owner:nil options:nil] firstObject];
        self.sortView.hiddenTabbar = YES;
        [self.view addSubview:self.sortView];
    }

    [self.sortView showDropDown];
    WeakSelf
    [self.sortView setDidSelectBlock:^(NSInteger index, NSString * _Nonnull selectedStr) {
        weakSelf.sortType = [NSString stringWithFormat:@"%ld",index+1];
        [weakSelf refresh];
    }];
    [self.sortView setHiddenBlock:^{

    }];
}

#pragma mark - XibFunction
- (IBAction)backButtonAction:(UIButton *)sender {
}
- (IBAction)messageButtonAction:(UIButton *)sender {
}
- (IBAction)cityViewTap:(id)sender {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
