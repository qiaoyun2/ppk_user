//
//  TOShopQualificationController.m
//  PPK
//
//  Created by null on 2022/8/6.
//

#import "TOShopQualificationController.h"
#import "TOShopQualificationCell.h"
#import "YBImageBrowser.h"

@interface TOShopQualificationController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation TOShopQualificationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 10;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TOShopQualificationCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TOShopQualificationCell" forIndexPath:indexPath];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH-32-10)/2, (SCREEN_WIDTH-32-10)*110/2/167);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self showBrowserWithIndex:indexPath.row];
}


- (void)showBrowserWithIndex:(NSInteger)index
{
    WeakSelf;
    NSMutableArray *datas = [NSMutableArray array];

//    [self.imageArray enumerateObjectsUsingBlock:^(NSString *_Nonnull imageStr, NSUInteger idx, BOOL * _Nonnull stop) {
//        // 网络图片
//        YBIBImageData *data = [YBIBImageData new];
//        data.imageURL = [NSURL URLWithString:kImageUrl(imageStr)];
//        data.projectiveView = [weakSelf getImageV:idx];
//        [datas addObject:data];
//    }];

    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = datas;
    browser.currentPage = index;
    // 只有一个保存操作的时候，可以直接右上角显示保存按钮
    browser.defaultToolViewHandler.topView.operationType = YBIBTopViewOperationTypeSave;
    [browser show];
}

- (UIView *)getImageV:(NSInteger)index {
    TOShopQualificationCell *cell = (TOShopQualificationCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathWithIndex:index]];
    return cell.thumbImageView ? : cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
