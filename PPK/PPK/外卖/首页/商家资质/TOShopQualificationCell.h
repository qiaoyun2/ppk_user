//
//  TOShopQualificationCell.h
//  PPK
//
//  Created by null on 2022/8/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TOShopQualificationCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbImageView;

@end

NS_ASSUME_NONNULL_END
