//
//  TOBannerModel.h
//  PPK
//
//  Created by null on 2022/8/1.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TOBannerModel : BaseModel

@property (nonatomic, strong) NSString *foodId; //商品id
@property (nonatomic, strong) NSString *linkType; //类型：1-跳转商家详情;2-跳转平台公告;3-跳转外部链接;4-商品详情
@property (nonatomic, strong) NSString *messageId; //平台公告id
@property (nonatomic, strong) NSString *picture; //
@property (nonatomic, strong) NSString *shopId; //商家id
@property (nonatomic, strong) NSString *url; //外部链接



@end

NS_ASSUME_NONNULL_END
