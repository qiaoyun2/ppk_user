//
//  TOHomeShopListModel.h
//  PPK
//
//  Created by null on 2022/8/1.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TOHomeShopFoodsModel : BaseModel

@property (nonatomic, strong) NSString *picture; //图片
@property (nonatomic, strong) NSString *sellPrice; //售价


@end


@interface TOHomeShopListModel : BaseModel

@property (nonatomic, strong) NSString *averagePrice; //人均价
@property (nonatomic, strong) NSString *deliveryTime; //送达时长
@property (nonatomic, strong) NSString *distance; //距离
@property (nonatomic, strong) NSString *monthSales; //店铺月销量
@property (nonatomic, strong) NSString *score; //平均评分
@property (nonatomic, strong) NSString *sendCost; //配送费用
@property (nonatomic, strong) NSString *shopId; //商铺id
@property (nonatomic, strong) NSString *shopName; //商铺名称
@property (nonatomic, strong) NSString *startDelivery; //起送费用
@property (nonatomic, strong) NSString *logoImg;
@property (nonatomic, strong) NSArray *foods;



@end

NS_ASSUME_NONNULL_END
