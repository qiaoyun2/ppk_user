//
//  TOHomeShopListModel.m
//  PPK
//
//  Created by null on 2022/8/1.
//

#import "TOHomeShopListModel.h"

@implementation TOHomeShopFoodsModel


@end

@implementation TOHomeShopListModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"foodList"]) {
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *obj in value) {
            TOHomeShopFoodsModel *vc = [[TOHomeShopFoodsModel alloc] initWithDictionary:obj];
            [array addObject:vc];
        }
        self.foods = [NSMutableArray arrayWithArray:array];
    }
}


@end
