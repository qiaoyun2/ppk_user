//
//  TOCategoryModel.m
//  PPK
//
//  Created by null on 2022/8/1.
//

#import "TOCategoryModel.h"

@implementation TOCategoryModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
}

@end
