//
//  TOCategoryModel.h
//  PPK
//
//  Created by null on 2022/8/1.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TOCategoryModel : BaseModel

@property (nonatomic, strong) NSString *ID; //分类id
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *picture;

@end

NS_ASSUME_NONNULL_END
