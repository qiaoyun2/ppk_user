//
//  TOShopListCell.m
//  PPK
//
//  Created by null on 2022/7/13.
//

#import "TOShopListCell.h"
#import "TOShopListGoodsItem.h"

@interface TOShopListCell ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>



@end

@implementation TOShopListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"TOShopListGoodsItem" bundle:nil] forCellWithReuseIdentifier:@"TOShopListGoodsItem"];
    self.collectionViewHeight.constant = (SCREEN_WIDTH-32-82-12-8*2)/3.0+24;
}
/**
 @property (weak, nonatomic) IBOutlet UIImageView *thumbImageView;
 @property (weak, nonatomic) IBOutlet UILabel *nameLabel;
 @property (weak, nonatomic) IBOutlet UILabel *scoreLabel; // 评分
 @property (weak, nonatomic) IBOutlet UILabel *salesLabel; // 月售
 @property (weak, nonatomic) IBOutlet UILabel *percapitaLabel; // 人均消费
 @property (weak, nonatomic) IBOutlet UILabel *deliveryFeeLabel; // 配送费
 @property (weak, nonatomic) IBOutlet UILabel *sendLimitLabel; // 20起送
 @property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
 @property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;

 
 */

- (void)setModel:(TOHomeShopListModel *)model
{
    _model = model;
    [self.thumbImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.logoImg)] placeholderImage:DefaultImgWidth];
    self.nameLabel.text = model.shopName;
    self.scoreLabel.text = [NSString stringWithFormat:@"%@分",model.score];
    self.salesLabel.text = [NSString stringWithFormat:@"月售%@",model.monthSales];
    self.percapitaLabel.text = [NSString stringWithFormat:@"人均%@",model.averagePrice];
    self.sendLimitLabel.text = [NSString stringWithFormat:@"起送%@",model.startDelivery];
    self.deliveryFeeLabel.text = [NSString stringWithFormat:@"配送%@",model.sendCost];
    self.sendTimeLabel.text = [NSString stringWithFormat:@"%@分钟",model.score];
    self.distanceLabel.text = [NSString stringWithFormat:@"%@.km",model.distance];

    [self.collectionView reloadData];

}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.model.foods.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TOShopListGoodsItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TOShopListGoodsItem" forIndexPath:indexPath];
    TOHomeShopFoodsModel *model = self.model.foods[indexPath.row];
    [cell.thumbImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.picture)] placeholderImage:DefaultImgWidth];
    cell.priceLabel.text = [NSString stringWithFormat:@"¥%@",model.sellPrice];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = self.collectionViewHeight.constant;
    return CGSizeMake(height-24, height);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
