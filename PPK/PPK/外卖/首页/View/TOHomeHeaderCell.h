//
//  TOHomeHeaderCell.h
//  PPK
//
//  Created by null on 2022/7/11.
//

#import <UIKit/UIKit.h>
#import "TOBannerModel.h"
#import "TOCategoryModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TOHomeHeaderCell : UITableViewCell

//@property (nonatomic, copy) void(^)

- (void)updateHeaderView:(NSArray *)banners cate:(NSArray *)cates;


@end

NS_ASSUME_NONNULL_END
