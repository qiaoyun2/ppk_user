//
//  TOHomeHeaderCell.m
//  PPK
//
//  Created by null on 2022/7/11.
//

#import "TOHomeHeaderCell.h"
#import "KJBannerHeader.h"
#import "LMJobCateButton.h"
#import "TOClassifyController.h"
#import "WKWebViewController.h"

@interface TOHomeHeaderCell ()<KJBannerViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *bannerBgView;
@property (weak, nonatomic) IBOutlet UIView *cateView;

@property (nonatomic,strong) KJBannerView *bannerView;
@property (nonatomic, strong) NSMutableArray *bannerModels;
@property (nonatomic, strong) NSMutableArray *cateModels;

@end

@implementation TOHomeHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _bannerView = [[KJBannerView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 158)];
    _bannerView.pageControl.hidden = YES;
    _bannerView.imgCornerRadius = 12;
    _bannerView.autoScrollTimeInterval = 2.5;
    _bannerView.isZoom = YES;
    _bannerView.itemSpace = 0;
    _bannerView.itemWidth = SCREEN_WIDTH-32;
    _bannerView.delegate = self;
    //    _adView.item
    _bannerView.imageType = KJBannerViewImageTypeMix;
    [_bannerBgView addSubview:_bannerView];
//    _bannerView.imageDatas = @[@"defaultImgWidth",@"defaultImgWidth",@"defaultImgWidth"];
}

- (void)updateCateView
{
    [self.cateView removeAllSubviews];
    CGFloat width = 64;
    CGFloat height = 70;
    CGFloat leading = 10;
    CGFloat top = 0;
    CGFloat hornterval = (SCREEN_WIDTH - width*5 - leading*2)/4;
    CGFloat vernterval = 12;
    NSInteger totalNum = self.cateModels.count <= 7 ? self.cateModels.count : 7;
    for (int i = 0; i<totalNum+1; i++) {
        NSInteger row = i%10 < 5 ? 0 : 1;
        NSInteger column = i%5;
//        NSInteger page = (i+1)%10 == 0 ? (i+1)/10 : ((i+1)/10+1);
        LMJobCateButton *button = [[[NSBundle mainBundle] loadNibNamed:@"LMJobCateButton" owner:nil options:nil] firstObject];
        button.backgroundColor = [UIColor clearColor];
        button.tag = 1000+i;
        [self.cateView addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(leading + (width+hornterval)*column));
            make.top.equalTo(@(top+(vernterval+height)*row));
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            if (i==totalNum) {
                make.bottom.equalTo(@(0));
            }
        }];
        if (i==totalNum) {
            button.iconImageView.image = [UIImage imageNamed:@"to_home_all"];
            button.nameLabel.text = @"全部";
        }else {
            TOCategoryModel *cateModel = self.cateModels[i];
            [button.iconImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(cateModel.picture)] placeholderImage:DefaultImgWidth];
            button.nameLabel.text = cateModel.name;
        }
        [button addTarget:self action:@selector(buttonsAction:) forControlEvents:UIControlEventTouchUpInside];
    }
}


- (void)buttonsAction:(UIButton *)sender
{
    NSInteger index = sender.tag - 1000;
    TOClassifyController *vc = [[TOClassifyController alloc] init];
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

- (void)updateHeaderView:(NSArray *)banners cate:(NSArray *)cates
{
    self.bannerModels = [NSMutableArray arrayWithArray:banners];
    self.cateModels = [NSMutableArray arrayWithArray:cates];
    
    NSMutableArray *pictures = [NSMutableArray array];
    
    for (TOBannerModel *model in banners) {
        [pictures addObject:kImageUrl(model.picture)];
    }
    self.bannerView.imageDatas = pictures;
    [self.bannerView setKSelectBlock:^(KJBannerView * _Nonnull banner, NSInteger idx) {
        //类型：1-跳转商家详情;2-跳转平台公告;3-跳转外部链接;4-商品详情
        TOBannerModel *model = banners[idx];
        if (model.linkType.integerValue==1) {
            
        }else if (model.linkType.integerValue==2) {
            
        }else if (model.linkType.integerValue==3) {
            
        }else if (model.linkType.integerValue==4) {
            WKWebViewController *vc = [[WKWebViewController alloc] init];
            vc.urlStr = model.url;
            [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
        }
    }];
    
    if (cates.count) {
        [self updateCateView];
    }
}


/** 点击图片回调 */
- (void)kj_BannerView:(KJBannerView *)banner SelectIndex:(NSInteger)index;
{
    
}



@end
