//
//  TOShopListCell.h
//  PPK
//
//  Created by null on 2022/7/13.
//

#import <UIKit/UIKit.h>
#import "TOHomeShopListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TOShopListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel; // 评分
@property (weak, nonatomic) IBOutlet UILabel *salesLabel; // 月售
@property (weak, nonatomic) IBOutlet UILabel *percapitaLabel; // 人均消费
@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeLabel; // 配送费
@property (weak, nonatomic) IBOutlet UILabel *sendLimitLabel; // 20起送
@property (weak, nonatomic) IBOutlet UILabel *sendTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;

@property (nonatomic, strong) TOHomeShopListModel *model;

@end

NS_ASSUME_NONNULL_END
