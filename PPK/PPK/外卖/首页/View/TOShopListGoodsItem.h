//
//  TOShopListGoodsItem.h
//  PPK
//
//  Created by null on 2022/7/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TOShopListGoodsItem : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbImageView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;


@end

NS_ASSUME_NONNULL_END
