//
//  TOGoodsDetailController.m
//  PPK
//
//  Created by null on 2022/8/6.
//

#import "TOGoodsDetailController.h"
#import <SDCycleScrollView/SDCycleScrollView.h>

@interface TOGoodsDetailController ()<SDCycleScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *bannerView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *saleNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *linePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *packagingFeeLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UIStackView *numStackView;
@property (weak, nonatomic) IBOutlet UIButton *skuButton;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;

@end

@implementation TOGoodsDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.cycleScrollView.imageURLStringsGroup = @[kImageUrl(model.adPicture)];
    self.cycleScrollView.imageURLStringsGroup = @[@"girl", @"girl2", @"girl3"].copy;
}

#pragma mark – SDCycleScrollViewDelegate

/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    NSLog(@"=====> %zd", index);
//    WKWebViewController *vc = [[WKWebViewController alloc] init];
//    vc.titleStr = _model.adTitle;
//    vc.contentStr = _model.adContent;
//    [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
}

#pragma mark - Lazy
- (SDCycleScrollView *)cycleScrollView {
    if (!_cycleScrollView) {
        _cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH*277/375) delegate:self placeholderImage:DefaultImgWidth];
        _cycleScrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        _cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
        _cycleScrollView.clipsToBounds = YES;
        [self.bannerView addSubview:_cycleScrollView];
    }
    return _cycleScrollView;
}

#pragma mark - XibFunction
- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)shareButtonAction:(id)sender {
    
}

- (IBAction)addButtonAction:(UIButton *)sender {
    
}

- (IBAction)deleteButtonAction:(UIButton *)sender {
    
}

- (IBAction)skuButtonButtonAction:(UIButton *)sender {
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
