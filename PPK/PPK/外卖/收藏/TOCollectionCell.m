//
//  TOCollectionCell.m
//  PPK
//
//  Created by null on 2022/7/29.
//

#import "TOCollectionCell.h"

@implementation TOCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentViewLeading.constant = 16;
    self.selectButton.hidden = YES;
}

- (void)setModel:(TOCollectionModel *)model
{
    _model = model;
    if (model.isEdit) {
        self.contentViewLeading.constant = 50;
        self.selectButton.hidden = NO;
    }else {
        self.contentViewLeading.constant = 16;
        self.selectButton.hidden = YES;
    }
    self.selectButton.selected = model.isSelected;
}

- (IBAction)selectButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.onSelectButtonClick) {
        self.onSelectButtonClick(sender.selected);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
