//
//  TOCollectionCell.h
//  PPK
//
//  Created by null on 2022/7/29.
//

#import <UIKit/UIKit.h>
#import "TOCollectionModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TOCollectionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel; // 评分
@property (weak, nonatomic) IBOutlet UILabel *salesLabel; // 月售
@property (weak, nonatomic) IBOutlet UILabel *percapitaLabel; // 人均消费
@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeLabel; // 配送费
@property (weak, nonatomic) IBOutlet UILabel *sendLimitLabel; // 20起送


@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewLeading;


@property (nonatomic, strong) TOCollectionModel *model;

@property (nonatomic, copy) void(^onSelectButtonClick)(BOOL selected);

@end

NS_ASSUME_NONNULL_END
