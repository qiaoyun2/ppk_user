//
//  TOCollectionController.m
//  PPK
//
//  Created by null on 2022/7/29.
//

#import "TOCollectionController.h"
#import "TOCollectionCell.h"


@interface TOCollectionController () <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *managerView;
@property (weak, nonatomic) IBOutlet UIButton *allButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (nonatomic, assign) BOOL isEdit;
@property (nonatomic, assign) NSInteger page;

@end

@implementation TOCollectionController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
//    [self refresh];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"收藏";
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 10, 0);
    [self setNavigationRightBarButtonWithTitle:@"编辑" color:MainColor];

    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    for (int i = 0; i<5; i++) {
        TOCollectionModel *model = [[TOCollectionModel alloc] init];
        [self.dataArray addObject:model];
    }
    [self.tableView reloadData];
}

- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(15);
    WeakSelf
    [NetworkingTool getWithUrl:kHLCollectionListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                TOCollectionModel *model = [[TOCollectionModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

- (void)requestForDeleteCollection
{
//    NSMutableArray *selectedArray = [NSMutableArray array];
//    NSMutableArray *idArray = [NSMutableArray array];
//    for (TOCollectionModel *model in self.dataArray) {
//        if (model.isSelected) {
//            [selectedArray addObject:model];
//            [idArray addObject:model.favoritesId];
//        }
//    }
//    if (!selectedArray.count) {
//        [LJTools showText:@"请先勾选" delay:1.5];
//        return;
//    }
//    [NetworkingTool postWithUrl:kHLDeleteCollectionURL params:@{@"ids":[idArray componentsJoinedByString:@","]} success:^(NSURLSessionDataTask *task, id responseObject) {
//        if ([responseObject[@"code"] integerValue] ==1) {
//            [self.dataArray removeObjectsInArray:selectedArray];
//            [self.tableView reloadData];
//            [LJTools showText:@"删除成功" delay:1.5];
//        }else {
//            [LJTools showText:responseObject[@"msg"] delay:1.5];
//        }
//    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
//        
//    } IsNeedHub:YES];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TOCollectionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TOCollectionCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TOCollectionCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    TOCollectionModel *model = self.dataArray[indexPath.row];
    model.isEdit = self.isEdit;
    cell.model = model;
    WeakSelf
    [cell setOnSelectButtonClick:^(BOOL selected) {
        model.isSelected = selected;
        [weakSelf checkAllSelected];
    }];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    TOCollectionModel *model = self.dataArray[indexPath.row];
//    HLHouseDetailController *vc = [[HLHouseDetailController alloc] init];
//    vc.houseId = model.ID;
//    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - Function
- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender
{
    self.isEdit = !self.isEdit;
    if (self.isEdit) {
        [self setNavigationRightBarButtonWithTitle:@"完成" color:MainColor];
        self.managerView.hidden = NO;
    }else {
        [self setNavigationRightBarButtonWithTitle:@"编辑" color:MainColor];
        self.managerView.hidden = YES;
    }
    [self.tableView reloadData];
}

- (void)checkAllSelected
{
    for (TOCollectionModel *model in self.dataArray) {
        if (!model.isSelected) {
            self.allButton.selected = NO;
            return;
        }
    }
    self.allButton.selected = YES;
}

#pragma mark - XibFunction
- (IBAction)allButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    for (TOCollectionModel *model in self.dataArray) {
        model.isSelected = sender.selected ? YES : NO;
    }
    [self.tableView reloadData];
}

- (IBAction)deleteButtonAction:(UIButton *)sender {
//    [self requestForDeleteCollection];
    WeakSelf
    [UIAlertController alertViewNormalWithTitle:@"提示" message:@"删除后不能恢复" titlesArry:@[@"立即删除"] indexBlock:^(NSInteger index, id obj) {
        [weakSelf requestForDeleteCollection];
    } okColor:UIColorFromRGB(0xFF6464) cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
}

@end
