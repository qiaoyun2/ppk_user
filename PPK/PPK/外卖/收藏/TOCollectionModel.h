//
//  TOCollectionModel.h
//  PPK
//
//  Created by null on 2022/7/29.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TOCollectionModel : BaseModel

@property (nonatomic, strong) NSString *distance; //
@property (nonatomic, strong) NSString *logoImg; //距离
@property (nonatomic, strong) NSString *monthSales; //月销量
@property (nonatomic, strong) NSString *score; //评价
@property (nonatomic, strong) NSString *sendCost; //配送费
@property (nonatomic, strong) NSString *shopId; //
@property (nonatomic, strong) NSString *collectId; //收藏id
@property (nonatomic, strong) NSString *startDelivery; //起送费

@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL isEdit;

@end

NS_ASSUME_NONNULL_END
