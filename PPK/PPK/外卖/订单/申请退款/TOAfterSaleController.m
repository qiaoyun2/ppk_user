//
//  TOAfterSaleController.m
//  PPK
//
//  Created by null on 2022/7/28.
//

#import "TOAfterSaleController.h"
#import "HXPhotoView.h"
#import "HXAssetManager.h"
#import "UploadManager.h"

@interface TOAfterSaleController ()<HXPhotoViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *reasonLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet HXPhotoView *photoView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoViewHeight;

@property (nonatomic, assign) BOOL isVideo;
@property (nonatomic, strong) NSMutableArray<UIImage *> *photos;
@property (nonatomic, strong) HXPhotoModel *videoModel;
@property (nonatomic, strong) NSString *videoPath;
@property (nonatomic, strong) NSMutableArray *imagePathArray;
@property (nonatomic, strong) HXPhotoManager *manager;

@property (nonatomic, strong) NSString *typeId; // 举报类型
@property (nonatomic, strong) NSArray *reportArray; // 举报类型

@end

@implementation TOAfterSaleController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"申请退款";
    self.photos = [NSMutableArray array];
    self.imagePathArray = [NSMutableArray array];
    
    self.photoView.spacing = 10.f;
    self.photoView.delegate = self;
    self.photoView.deleteCellShowAlert = YES;
    self.photoView.outerCamera = YES;
    self.photoView.previewShowDeleteButton = YES;
    self.photoView.addImageName = @"组 51192";
    self.photoView.lineCount = 4;
    self.photoView.manager = self.manager;
}

- (void)requestForReasonList
{
    [NetworkingTool getWithUrl:kHLReportReasonURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSArray *dataArray = responseObject[@"data"];
            self.reportArray = [NSArray arrayWithArray:dataArray];
            NSMutableArray *titleArray = [NSMutableArray array];
            for (NSDictionary *obj in dataArray) {
                [titleArray addObject:obj[@"title"]];
            }
            WeakSelf
            [ZJNormalPickerView zj_showStringPickerWithTitle:@"举报理由" dataSource:titleArray defaultSelValue:nil isAutoSelect:NO resultBlock:^(id selectValue, NSInteger index) {
                NSDictionary *obj = dataArray[index];
                weakSelf.typeId = [NSString stringWithFormat:@"%@",obj[@"id"]];
                weakSelf.reasonLabel.text = selectValue;
                weakSelf.reasonLabel.textColor = UIColorFromRGB(0x333333);
            } cancelBlock:^{
                
            }];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForSubmit
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (self.imagePathArray.count) {
        params[@"picture"] = [self.imagePathArray componentsJoinedByString:@","];
    }
    [NetworkingTool postWithUrl:@"" params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForUploadImages
{
    [self.imagePathArray removeAllObjects];
    WeakSelf
    [UploadManager uploadImageArray:self.photos block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
        NSArray *array = [imageUrl componentsSeparatedByString:@","];
        [weakSelf.imagePathArray addObjectsFromArray:array];
        [weakSelf requestForSubmit];
    }];
}

#pragma mark - HXPhotoViewDelegate
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {
    if (videos.count>0) {
        self.isVideo = YES;
        self.videoModel = [videos firstObject];
        [self.photos removeAllObjects];
        [self.imagePathArray removeAllObjects];
        [self.videoModel getImageWithSuccess:^(UIImage * _Nullable image, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
            [self.photos addObject:image];
        } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
            
        }];
        NSLog(@"%@",self.videoModel.videoURL);
    }
    else {
        self.isVideo = NO;
        self.videoModel = nil;
        [self.photos removeAllObjects];
        [self.imagePathArray removeAllObjects];
        for (HXPhotoModel *model in photos) {
            [self.photos addObject:[HXAssetManager originImageForAsset:model.asset]];
        }
    }
}
// 拿到view变化的高度
- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame {
    self.photoViewHeight.constant = frame.size.height;
}

- (IBAction)typeButtonAction:(UIButton *)sender {
    [ZJNormalPickerView zj_showStringPickerWithTitle:@"退款类型" dataSource:@[@"全部退款",@"部分退款"] defaultSelValue:@"" resultBlock:^(id selectValue, NSInteger index) {
            
    }];
}

- (IBAction)reasonButtonAction:(UIButton *)sender {
    [ZJNormalPickerView zj_showStringPickerWithTitle:@"退款原因" dataSource:@[@"不好吃",@"少产品",@"送完了",@"其他原因"] defaultSelValue:@"" resultBlock:^(id selectValue, NSInteger index) {
            
    }];
}

- (IBAction)submitButtonAction:(UIButton *)sender {
    if (!self.typeId.length) {
        [LJTools showText:@"请选择举报原因" delay:1.5];
        return;
    }
    if (!self.textView.text.length) {
        [LJTools showText:@"请输入举报描述" delay:1.5];
        return;
    }
    if (self.photos.count) {
        [self requestForUploadImages];
    }else {
        [self requestForSubmit];
    }
}

#pragma mark - Lazy Load
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _manager.configuration.type = HXConfigurationTypeWXChat;
        _manager.configuration.singleSelected = NO;
        _manager.configuration.lookGifPhoto = NO;
        _manager.configuration.useWxPhotoEdit = YES;
        _manager.configuration.selectTogether=NO;
        _manager.configuration.photoEditConfigur.onlyCliping = YES;
        _manager.configuration.showOriginalBytes = NO;
        _manager.configuration.videoMaxNum=1;
        _manager.configuration.photoMaxNum=9;
        _manager.configuration.requestImageAfterFinishingSelection = YES;
        _manager.configuration.photoEditConfigur.aspectRatio = HXPhotoEditAspectRatioType_None;
    }
    return _manager;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
