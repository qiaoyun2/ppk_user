//
//  TOSettlementController.m
//  PPK
//
//  Created by null on 2022/7/29.
//

#import "TOSettlementController.h"
#import "TOOrderDetailGoodsCell.h"
#import "TOTimePickerView.h"
#import "AddressListView.h"
#import "PayViewController.h"

@interface TOSettlementController ()

@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *pickButton;

/**配送地址*/
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *receiveTimeLabel;

/**自提信息*/
@property (weak, nonatomic) IBOutlet UIView *pickView;
@property (weak, nonatomic) IBOutlet UILabel *pickAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickMobileLabel;
@property (weak, nonatomic) IBOutlet UIButton *agreeButton;

/**商品信息*/
@property (weak, nonatomic) IBOutlet UILabel *shopNameLabel; // 店铺名字
@property (weak, nonatomic) IBOutlet UITableView *tableView; // 商品列表
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *packagingFeeLabel; // 打包费
@property (weak, nonatomic) IBOutlet UILabel *sendFeeLabel; // 配送费
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLabel; // 总金额

@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (weak, nonatomic) IBOutlet UIView *shadowView;
//@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *payMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;

@property (nonatomic, strong) TOTimePickerView *receriveTimeView;
@property (nonatomic, strong) TOTimePickerView *pickTimeView;
@property (nonatomic, strong) AddressListView *addressListView;

@end

@implementation TOSettlementController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = false;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"结算";
    self.view.backgroundColor = UIColorFromRGB(0xF6F7F9);
    [self typeButtonsAction:self.sendButton];
    self.tableViewHeight.constant = 82*3;
    
    self.shadowView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    self.shadowView.layer.shadowOffset = CGSizeMake(0,1.5);
    self.shadowView.layer.shadowRadius = 6;
    self.shadowView.layer.shadowOpacity = 1;
    self.shadowView.layer.cornerRadius = 27.5;
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TOOrderDetailGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TOOrderDetailGoodsCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TOOrderDetailGoodsCell" owner:nil options:nil] firstObject];
    }
    return cell;
}

#pragma mark - XibFunction
- (IBAction)typeButtonsAction:(UIButton *)sender {
    sender.selected = YES;
    sender.backgroundColor = MainColor;
    if (sender == self.sendButton) {
        self.pickButton.selected = NO;
        self.pickButton.backgroundColor = [UIColor whiteColor];
        self.addressView.hidden = NO;
        self.pickView.hidden = YES;
    }else {
        self.sendButton.selected = NO;
        self.sendButton.backgroundColor = [UIColor whiteColor];
        self.addressView.hidden = YES;
        self.pickView.hidden = NO;
    }
}

- (IBAction)addressButtonAction:(UIButton *)sender {
    if (!self.addressListView) {
        self.addressListView = [[[NSBundle mainBundle] loadNibNamed:@"AddressListView" owner:nil options:nil] firstObject];
        self.addressListView.frame = [UIScreen mainScreen].bounds;
        self.addressListView.titleLabel.text = @"配送至";
        [[UIApplication sharedApplication].keyWindow addSubview:self.addressListView];
    }
    [self.addressListView show];
    WeakSelf
    [self.addressListView setOnSelectedAddress:^(AddressModel * _Nonnull addressModel) {
//        weakSelf.addressModel = addressModel;
//        weakSelf.codeField.text = @"";
//        weakSelf.codeView.hidden = [addressModel.telphone isEqual:[User getUser].mobile];
//        [weakSelf updateAddressView];
    }];
    
}

- (IBAction)receiveTimeButtonAction:(id)sender {
    if (!self.receriveTimeView) {
        self.receriveTimeView = [[[NSBundle mainBundle] loadNibNamed:@"TOTimePickerView" owner:nil options:nil] firstObject];
        self.receriveTimeView.frame = [UIScreen mainScreen].bounds;
        self.receriveTimeView.titleLabel.text = @"选择配送时间";
        [[UIApplication sharedApplication].keyWindow addSubview:self.receriveTimeView];
    }
    [self.receriveTimeView show];
    WeakSelf
    [self.receriveTimeView setDidSelectBlock:^(NSString * _Nonnull selectedStr) {
//        weakSelf.reserveTime = selectedStr;
//        weakSelf.timeLabel.text = selectedStr;
//        weakSelf.timeLabel.textColor = UIColorFromRGB(0x333333);
    }];
}

- (IBAction)pickTimeButtonAction:(UIButton *)sender {
    if (!self.pickTimeView) {
        self.pickTimeView = [[[NSBundle mainBundle] loadNibNamed:@"TOTimePickerView" owner:nil options:nil] firstObject];
        self.pickTimeView.frame = [UIScreen mainScreen].bounds;
        self.pickTimeView.titleLabel.text = @"选择自提时间";
        [[UIApplication sharedApplication].keyWindow addSubview:self.pickTimeView];
    }
    [self.pickTimeView show];
    WeakSelf
    [self.pickTimeView setDidSelectBlock:^(NSString * _Nonnull selectedStr) {
//        weakSelf.reserveTime = selectedStr;
//        weakSelf.timeLabel.text = selectedStr;
//        weakSelf.timeLabel.textColor = UIColorFromRGB(0x333333);
    }];
}
/**
 搬运：19+地图 78
 维修：25+地图  72
 */



- (IBAction)pickMobileButtonAction:(UIButton *)sender {
    
}

- (IBAction)agreementButtonAction:(UIButton *)sender {
    
}

- (IBAction)agreeButtonAction:(UIButton *)sender {
    
}

- (IBAction)submitButtonAction:(UIButton *)sender {
    PayViewController *vc = [[PayViewController alloc] init];
    vc.type = 0;
    vc.order_sn = @"9999";
    vc.price = @"88.99";
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
