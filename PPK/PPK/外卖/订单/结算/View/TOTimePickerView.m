//
//  TOTimePickerView.m
//  PPK
//
//  Created by null on 2022/7/29.
//

#import "TOTimePickerView.h"
#import "ProvinceCell.h"

@interface TOTimePickerView ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@property (weak, nonatomic) IBOutlet UITableView *dateTableView;
@property (weak, nonatomic) IBOutlet UITableView *timeTableView;


@property (nonatomic, strong) NSArray *dateArray;
@property (nonatomic, strong) NSMutableArray *subDateArray;

@property (nonatomic, strong) NSDate *lastDate;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) NSInteger subIndex;

@end

@implementation TOTimePickerView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.hidden = YES;
    self.index = 0;
    self.subIndex = -1;

    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:currentDate];
    NSDate *date1 = [calendar dateFromComponents:components];
    NSDate *date2 = [NSDate jk_offsetDays:1 fromDate:date1];
    NSDate *date3 = [NSDate jk_offsetDays:2 fromDate:date1];
    self.dateArray = @[date1,date2,date3];
    self.lastDate = date1;
    
    self.contentViewHeight.constant = 339+bottomBarH;
    self.contentViewBottom.constant = - self.contentViewHeight.constant;
    [self.contentView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(12, 12) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, self.contentViewHeight.constant)];
    
    self.dateTableView.delegate = self;
    self.dateTableView.dataSource = self;
    
    self.timeTableView.delegate = self;
    self.timeTableView.dataSource = self;
    [self requestForDateList];
}

- (void)requestForDateList
{
    [NetworkingTool getWithUrl:kRWReserveDateListURL params:@{@"code":@"recovey_time"} success:^(NSURLSessionDataTask *task, id responseObject) {
        self.subDateArray = [NSMutableArray array];
        if ([responseObject[@"code"] integerValue]==1) {
            for (NSDictionary *obj in responseObject[@"data"]) {
                [self.subDateArray addObject:obj[@"text"]];
            }
        }
        [self.timeTableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.dateTableView) {
        return self.dateArray.count;
    }
    return self.subDateArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProvinceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProvinceCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ProvinceCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.badgeView.hidden = YES;
    cell.lineImageView.hidden = YES;
    cell.nameLabel.font = [UIFont systemFontOfSize:14];
    if (tableView == self.dateTableView) {
        NSDate *date = self.dateArray[indexPath.row];
        cell.nameLabel.text = [NSString stringWithFormat:@"%@（%@)",[date jk_dateWithFormat:@"yyyy.MM.dd"],[date jk_dayFromWeekday]];
        if (indexPath.row == self.index) {
            cell.lineImageView.hidden = NO;
            cell.nameLabel.textColor = MainColor;
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }
        else {
            cell.lineImageView.hidden = YES;
            cell.nameLabel.textColor = UIColorFromRGB(0x333333);
            cell.contentView.backgroundColor = UIColorFromRGB(0xF6F7F9);
        }
        return cell;
    }
    
    cell.nameLabel.text = self.subDateArray[indexPath.row];
    cell.nameLabel.textColor = indexPath.row == self.subIndex ? MainColor : UIColorFromRGB(0x333333);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.dateTableView) {
        self.lastDate = self.dateArray[indexPath.row];
        self.index = indexPath.row;
        [self.dateTableView reloadData];
        return;
    }
    
    self.subIndex = indexPath.row;
    [self.timeTableView reloadData];
}


#pragma mark - Function
- (void)show
{
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0.4);
        self.contentViewBottom.constant = 0;
        [self layoutIfNeeded];
    }];
}

- (void)dismiss
{
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0);
        self.contentViewBottom.constant = - self.contentViewHeight.constant;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

#pragma mark - XibFunction
- (IBAction)confirmButtonClick:(UIButton *)sender {
    if (self.subIndex < 0) {
        [LJTools showText:self.titleLabel.text delay:1.5];
        return;
    }
    if (self.didSelectBlock) {
        NSString *str = [NSString stringWithFormat:@"%@ %@",[self.lastDate jk_formatYMD],self.subDateArray[self.subIndex]];
        self.didSelectBlock(str);
    }
    [self dismiss];
}

- (IBAction)closeButtonClick:(id)sender {
    [self dismiss];
}

@end
