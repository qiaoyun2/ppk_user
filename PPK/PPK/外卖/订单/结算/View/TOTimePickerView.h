//
//  TOTimePickerView.h
//  PPK
//
//  Created by null on 2022/7/29.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TOTimePickerView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) NSString *selectedStr;
@property (nonatomic, copy) void(^didSelectBlock)(NSString *selectedStr);

- (void)show;
- (void)dismiss;


@end

NS_ASSUME_NONNULL_END
