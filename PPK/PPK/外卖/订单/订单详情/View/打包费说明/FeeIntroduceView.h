//
//  FeeIntroduceView.h
//  PPK
//
//  Created by null on 2022/7/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FeeIntroduceView : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *introduceLabel;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
