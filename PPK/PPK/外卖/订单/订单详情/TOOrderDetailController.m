//
//  TOOrderDetailController.m
//  PPK
//
//  Created by null on 2022/7/28.
//

#import "TOOrderDetailController.h"
#import "FoundListImage9TypographyView.h"
#import "TOOrderDetailGoodsCell.h"
#import "FeeIntroduceView.h"
#import "TOAfterSaleController.h"
#import "TOEvaluationController.h"

@interface TOOrderDetailController () <UITableViewDelegate,UITableViewDataSource>

/**订单状态*/
@property (weak, nonatomic) IBOutlet UILabel *statusLabel; // 待付款

@property (weak, nonatomic) IBOutlet UIView *subStatusView;
@property (weak, nonatomic) IBOutlet UILabel *subStatusLabel; // 剩余支付时间
@property (weak, nonatomic) IBOutlet UILabel *countDownLabel; // 14:30

@property (weak, nonatomic) IBOutlet UIView *managerView;
@property (weak, nonatomic) IBOutlet UIStackView *managerStackView;

/**退款信息*/
@property (weak, nonatomic) IBOutlet UIView *refundInfoView;

@property (weak, nonatomic) IBOutlet UIView *refundMoneyView; // 退款金额  退款账户 退款时间
@property (weak, nonatomic) IBOutlet UILabel *refundMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *refundAccountLabel;
@property (weak, nonatomic) IBOutlet UILabel *refundTimeLabel;

@property (weak, nonatomic) IBOutlet UIStackView *refundReasonView; // 服务类型  退款原因 申请时间 退款编号 退款凭证
@property (weak, nonatomic) IBOutlet UILabel *refundTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *refundReasonLabel;
@property (weak, nonatomic) IBOutlet UILabel *applyTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *refundNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *refundRemarkLabel;
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;

/**配送信息*/
@property (weak, nonatomic) IBOutlet UIView *sendInfoView;
@property (weak, nonatomic) IBOutlet UILabel *sendTimeLabel; // 配送时间
@property (weak, nonatomic) IBOutlet UILabel *sendAddressLabel; // 配送地址

/**自提信息*/
@property (weak, nonatomic) IBOutlet UIView *pickInfoView;
@property (weak, nonatomic) IBOutlet UILabel *pickTimeLabel; // 自提时间
@property (weak, nonatomic) IBOutlet UILabel *pickAddressLabel; // 自提地址
@property (weak, nonatomic) IBOutlet UILabel *pickMobileLabel; // 自提人电话

@property (weak, nonatomic) IBOutlet UIView *goodsInfoView;
@property (weak, nonatomic) IBOutlet UILabel *shopNameLabel; // 店铺名字
@property (weak, nonatomic) IBOutlet UITableView *tableView; // 商品列表
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *packagingFeeLabel; // 打包费
@property (weak, nonatomic) IBOutlet UILabel *sendFeeLabel; // 配送费
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLabel; // 总金额

/**订单信息*/
@property (weak, nonatomic) IBOutlet UIView *orderInfoView;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel; // 备注
@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel; // 订单号
@property (weak, nonatomic) IBOutlet UILabel *createTimeLabel; // 下单时间
@property (weak, nonatomic) IBOutlet UILabel *payTypeLabel;  // 支付方式
@property (weak, nonatomic) IBOutlet UILabel *payMoneyLabel; // 支付金额

/**取消*/
@property (weak, nonatomic) IBOutlet UIView *cancelView;


@end

@implementation TOOrderDetailController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单详情";
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    self.tableViewHeight.constant = 82*3;
    [self.managerStackView.arrangedSubviews enumerateObjectsUsingBlock:^(__kindof UIButton * _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([button.titleLabel.text isEqualToString:@"取消订单"] || [button.titleLabel.text isEqualToString:@"评价"]) {
            button.hidden = NO;
        }else {
            button.hidden = YES;
        }
    }];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TOOrderDetailGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TOOrderDetailGoodsCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TOOrderDetailGoodsCell" owner:nil options:nil] firstObject];
    }
    return cell;
}

#pragma mark - XibFunction
- (IBAction)contactButtonAction:(id)sender {
    
}

- (IBAction)callButtonAction:(id)sender {
    
}

- (IBAction)questionButtonAction:(UIButton *)sender {
    FeeIntroduceView *view = [[[NSBundle mainBundle] loadNibNamed:@"FeeIntroduceView" owner:nil options:nil] firstObject];
    view.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].keyWindow addSubview:view];
    [view show];
}

- (IBAction)copyButtonAction:(UIButton *)sender {
    
}

- (IBAction)cancelButtonAction:(UIButton *)sender {
    
}

- (IBAction)managerButtonsAction:(UIButton *)sender {
    NSString *title = sender.titleLabel.text;
    
    if ([title isEqualToString:@"取消订单"]) {
        [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认要取消订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
            
        } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        return;
    }
    
    if ([title isEqualToString:@"撤销申请"]) {
        [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认撤销申请？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
            
        } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        return;
    }
    
    if ([title isEqualToString:@"申请退款"]) {
        TOAfterSaleController *vc = [[TOAfterSaleController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    if ([title isEqualToString:@"评价"]) {
        TOEvaluationController *vc = [[TOEvaluationController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
}

@end
