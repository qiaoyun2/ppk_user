//
//  TOOrderListCell.h
//  PPK
//
//  Created by null on 2022/7/27.
//

#import <UIKit/UIKit.h>
#import "TOOrderListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TOOrderListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *shopNameLabel; // 店铺名
@property (weak, nonatomic) IBOutlet UILabel *statusLabel; // 状态
@property (weak, nonatomic) IBOutlet UILabel *shippingTypeLabel;  // 商家配送 自提
@property (weak, nonatomic) IBOutlet UILabel *countDownLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIStackView *managerStackView;

@property (nonatomic, strong) TOOrderListModel *model;


@end

NS_ASSUME_NONNULL_END
