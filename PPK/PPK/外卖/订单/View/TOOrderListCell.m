//
//  TOOrderListCell.m
//  PPK
//
//  Created by null on 2022/7/27.
//

#import "TOOrderListCell.h"
#import "TOShopListGoodsItem.h"

@interface TOOrderListCell () <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation TOOrderListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"TOShopListGoodsItem" bundle:nil] forCellWithReuseIdentifier:@"TOShopListGoodsItem"];
    
    [self.managerStackView.arrangedSubviews enumerateObjectsUsingBlock:^(__kindof UIButton * _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([button.titleLabel.text isEqualToString:@"取消订单"]) {
            button.hidden = NO;
        }else {
            button.hidden = YES;
        }
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TOShopListGoodsItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TOShopListGoodsItem" forIndexPath:indexPath];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(70, 94);
}

- (IBAction)shopNameButtonAction:(UIButton *)sender {
    
    
}

- (IBAction)managerButtonsAction:(UIButton *)sender {
    
}


@end
