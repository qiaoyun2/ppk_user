//
//  TOResultController.h
//  PPK
//
//  Created by null on 2022/7/29.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TOResultController : BaseViewController

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *payTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *payTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;


@end

NS_ASSUME_NONNULL_END
