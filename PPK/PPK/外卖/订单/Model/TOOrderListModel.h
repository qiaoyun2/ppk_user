//
//  TOOrderListModel.h
//  PPK
//
//  Created by null on 2022/7/28.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TOOrderGoodsModel : BaseModel

@property (nonatomic, strong) NSString *foodId; // 商品id
@property (nonatomic, strong) NSString *monthSales; // 月售
@property (nonatomic, strong) NSString *num; // 已选购数量
@property (nonatomic, strong) NSString *originPrice; //
@property (nonatomic, strong) NSString *picture; // 商品封面图(一张)
@property (nonatomic, strong) NSString *sellPrice; //
@property (nonatomic, strong) NSString *skuId; // 商品的skuid
@property (nonatomic, strong) NSString *skuName; // 商品的sku名称
@property (nonatomic, strong) NSString *title; // 商品名称
@property (nonatomic, strong) NSString *carItemId; // 购物车数据id


@end


@interface TOOrderListModel : BaseModel

@property (nonatomic, strong) NSString *boxCost; // 打包费/餐盒费
@property (nonatomic, strong) NSString *sendCost; // 配送费用
@property (nonatomic, strong) NSString *totalOriginPrice; // 总原价
@property (nonatomic, strong) NSString *totalSellPrice; // 总原价
@property (nonatomic, strong) NSArray *itemList;

@end

NS_ASSUME_NONNULL_END
