//
//  TOOrderListModel.m
//  PPK
//
//  Created by null on 2022/7/28.
//

#import "TOOrderListModel.h"

@implementation TOOrderGoodsModel

@end

@implementation TOOrderListModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"itemList"]) {
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *dic in value) {
            TOOrderGoodsModel *model = [[TOOrderGoodsModel alloc] initWithDictionary:dic];
            [array addObject:model];
        }
        self.itemList = [NSArray arrayWithArray:array];
    }
}

@end
