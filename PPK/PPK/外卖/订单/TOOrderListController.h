//
//  TOOrderListController.h
//  PPK
//
//  Created by null on 2022/7/27.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TOOrderListController : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSInteger status;

@end

NS_ASSUME_NONNULL_END
