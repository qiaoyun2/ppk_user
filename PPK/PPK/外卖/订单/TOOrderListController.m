//
//  TOOrderListController.m
//  PPK
//
//  Created by null on 2022/7/27.
//

#import "TOOrderListController.h"
#import "TOOrderListCell.h"
#import "TOOrderDetailController.h"
#import "RWEvaluationController.h"
#import "TOSettlementController.h"

@interface TOOrderListController () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign) NSInteger page;

@end

@implementation TOOrderListController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refresh];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = false;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
//    [self refresh];
}

- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
    params[@"status"] = @(self.status);
    WeakSelf
    [NetworkingTool getWithUrl:kRWOrderListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                TOOrderListModel *model = [[TOOrderListModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

- (void)requestForCancelOrder:(TOOrderListModel *)model
{
//    [DZNetworkingTool postWithUrl:kRWCancelOrderURL params:@{@"orderId":model.orderId, @"cancelId":@""} success:^(NSURLSessionDataTask *task, id responseObject) {
//        if ([responseObject[@"code"] integerValue]==1) {
//            [LJTools showText:@"取消成功" delay:1.5];
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [self refresh];
//            });
//        }else {
//            [LJTools showText:responseObject[@"msg"] delay:1.5];
//        }
//    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
//
//    } IsNeedHub:YES];
}

- (void)requestForDeleteOrder:(TOOrderListModel *)model
{
//    [DZNetworkingTool postWithUrl:kRWDeleteOrderURL params:@{@"orderId":model.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
//        if ([responseObject[@"code"] integerValue]==1) {
//            [LJTools showText:@"删除成功" delay:1.5];
//            [self.dataArray removeObject:model];
//            [self.tableView reloadData];
//        }else {
//            [LJTools showText:responseObject[@"msg"] delay:1.5];
//        }
//    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
//
//    } IsNeedHub:YES];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
//    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TOOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TOOrderListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TOOrderListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    
    
//    TOOrderListModel *model = self.dataArray[indexPath.row];
//    cell.model = model;
//    WeakSelf
//    [cell setOnButtonsClick:^(NSInteger tag) {
//        /// 0:取消  1:联系  2:评论  3:删除
//        if (tag==0) {
//            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认取消订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
//                [weakSelf requestForCancelOrder:model];
//            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
//        }
//        else if (tag==1) {
//            [LJTools call:model.receiveUserTelPhone];
//        }
//        else if (tag==2) {
//            RWEvaluationController *vc = [[RWEvaluationController alloc] init];
//            vc.orderId = model.orderId;
//            [self.navigationController pushViewController:vc animated:YES];
//        }
//        else {
//            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
//                [weakSelf requestForDeleteOrder:model];
//            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
//        }
//    }];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    TOOrderDetailController *vc = [[TOOrderDetailController alloc] init];
    TOSettlementController *vc = [[TOSettlementController alloc] init];
//    TOOrderListModel *model = self.dataArray[indexPath.row];
//    vc.orderId = model.orderId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 24;
}


@end
