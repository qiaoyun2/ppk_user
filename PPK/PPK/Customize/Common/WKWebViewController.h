//
//  WKWebViewController.h
//  DZBase


#import "BaseViewController.h"
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WKWebViewController : BaseViewController <WKNavigationDelegate>

@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, strong) NSString *urlStr;
@property (nonatomic, strong) NSString *htmlStr;
@property (nonatomic, copy) NSString *contentStr;
@end

NS_ASSUME_NONNULL_END
