//
//  User.m
//  HNPartyBuilding
//

#import "User.h"
#import "LJTools.h"

@implementation User

//添加了下面的宏定义
MJExtensionCodingImplementation

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
        @"user_id":@"id",
        @"user_name":@"username",
        @"user_nickname":@"nickname",
        @"user_token":@"token"
    };
}

/* 实现下面的方法，说明哪些属性不需要归档和解档 */
+ (NSArray *)mj_ignoredCodingPropertyNames {
    return @[];
}

/// 服务器获取用户信息
+ (void)getUserFromServer:(void (^)(User *))callBack {
    [self getUserFromServer:[User getUserID] callBack:^(User *user) {
        user.user_token = [User getUserToken];
        [User saveUser:user];
        !callBack ?: callBack(user);
    }];
}

+ (void)getUserFromServer:(NSString *)userId callBack:(void (^)(User *user))callBack {
    [NetworkingTool postWithUrl:kGetUserInfoURL params:@{@"user_id":userId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [LJTools hideHud];
            User *user = [User mj_objectWithKeyValues:responseObject[@"data"]];
            !callBack ?: callBack(user);
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

+ (void)saveUser:(User *)user {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:@"userInfo"];
    if (user.user_token) {
        [defaults setObject:user.user_token forKey:UserToken];
    }
    if (user.user_id) {
        [defaults setObject:user.user_id forKey:UserID];
    }
    [defaults setObject: user.avatar forKey:UserHeader];
    [defaults setObject:user.user_name forKey:UserName];
    [defaults setObject:user.user_nickname forKey:UserNickname];
    [defaults setObject:user.sex forKey:UserSex];
    [defaults setObject:user.mobile forKey:Mobile];
    [defaults synchronize];
}

+ (User *)getUser {
    User *user = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"userInfo"]];
    return user;
}

+ (void)deleUser {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:@"userInfo"];
    [defaults setObject:nil forKey:UserID];
    [defaults setObject:nil forKey:UserToken];
    [defaults setObject:nil forKey:UserHeader];
    [defaults setObject:nil forKey:UserName];
    [defaults setObject:nil forKey:UserNickname];
    [defaults setInteger:0 forKey:UserSex];
    [defaults setObject:nil forKey:Mobile];
    [defaults synchronize];
}

+ (NSString *)getUserID {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:UserID] != nil ) {
        return [defaults objectForKey:UserID];
    }
    return @"";
}

+ (NSString *)getUserToken {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:UserToken];
}

+ (NSString *)getMobile {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:Mobile];
}

+ (NSString *)getMobile2 {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:Mobile2];
}

+ (NSString *)getUserNickname {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:UserNickname];
}

+ (NSString *)getHeader {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:UserHeader];
}

+ (NSInteger)getSex {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults integerForKey:UserSex];
}

@end
