//
//  BaseViewController.m
//  ZZR
//
//  Created by null on 2018/12/13.
//  Copyright © 2018 null. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@property (nonatomic, assign) CGFloat y;

@end

@implementation BaseViewController

- (instancetype)init
{
    if (self = [super init]) {
        self.isDark = YES;

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    //保证在ios13 下也是全屏
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    UIImage *image = [UIImage imageNamed:@"back"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationController.interactivePopGestureRecognizer.delegate=(id)self;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:image  style:UIBarButtonItemStyleDone target:self action:@selector(backItemClicked)];
    
    [self changeNavBarBottomLineColor:HLHex(0xeeeeee)];
    
    if (self.navigationController.viewControllers.count < 2) {
        self.navigationItem.leftBarButtonItem = nil;
    }

    if (@available(iOS 15.0, *)) {
        UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
        appearance.backgroundColor = [UIColor whiteColor];// 背景色
        appearance.backgroundEffect = nil;// 去掉半透明效果
        appearance.titleTextAttributes = @{
            NSForegroundColorAttributeName : UIColorNamed(@"333333"),
            NSFontAttributeName : [UIFont boldSystemFontOfSize:18],
        };// 标题字体颜色及大小
        // 设置导航栏下边界分割线透明
        appearance.shadowImage = [[UIImage alloc] init];
        // 去除导航栏阴影（如果不设置clear，导航栏底下会有一条阴影线）
        appearance.shadowColor = UIColorNamed(@"EEEEEE");
        // standardAppearance：常规状态, 标准外观，iOS15之后不设置的时候，导航栏背景透明
        self.navigationController.navigationBar.standardAppearance = appearance;
        // scrollEdgeAppearance：被scrollview向下拉的状态, 滚动时外观，不设置的时候，使用标准外观
        self.navigationController.navigationBar.scrollEdgeAppearance = appearance;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.navigationController.viewControllers.firstObject == self) {
        self.navigationController.interactivePopGestureRecognizer.enabled = false;
    }else{
        self.navigationController.interactivePopGestureRecognizer.enabled = true;
    }
}


//设置状态栏颜色
- (UIStatusBarStyle)preferredStatusBarStyle {
    if (@available(iOS 13.0, *)) {
        return UIStatusBarStyleDarkContent;
    } else {
        return UIStatusBarStyleDefault;
    }
}

- (void)backItemClicked
{
    if (self.presentingViewController) {
        if (self.navigationController == nil || [self.navigationController.jk_rootViewController isEqual:self]) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }else{
            [self.navigationController popViewControllerAnimated:YES];

        }
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/*设置导航栏右侧的按钮 和点击事件*/
- (void) setNavigationRightBarButtonWithTitle:(NSString *)title color:(UIColor *)color{
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:(UIBarButtonItemStyleDone) target:self action:@selector(rightButtonTouchUpInside:)];
    [rightButtonItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:color} forState:(UIControlStateNormal)];
    [rightButtonItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:color} forState:(UIControlStateHighlighted)];
    [self.navigationItem setRightBarButtonItem:rightButtonItem];
}
- (void)setNavigationRightBarButtonWithTitle:(NSString *)title{
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:(UIBarButtonItemStyleDone) target:self action:@selector(rightButtonTouchUpInside:)];
    [rightButtonItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:UIColorFromRGB(0x333333)} forState:(UIControlStateNormal)];
    [rightButtonItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:UIColorFromRGB(0x333333)} forState:(UIControlStateHighlighted)];
    [self.navigationItem setRightBarButtonItem:rightButtonItem];
}
- (void) setNavigationRightBarButtonWithImage:(NSString *)image{
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:image] imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)] style:(UIBarButtonItemStyleDone) target:self action:@selector(rightButtonTouchUpInside:)];
    [self.navigationItem setRightBarButtonItem:rightButtonItem];
    
}


- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender{
    
}

-(UITableView *)plainTableView{
    if (!_plainTableView) {
        _plainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, P_WIDTH, P_HEIGHT - bottomBarH - NavAndStatusHight) style:UITableViewStylePlain];
        _plainTableView.estimatedSectionHeaderHeight = 0;
        _plainTableView.estimatedSectionFooterHeight = 0;
        _plainTableView.showsVerticalScrollIndicator = NO;
        _plainTableView.delegate = self;
        _plainTableView.dataSource = self;
        _plainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _plainTableView.backgroundColor = [UIColor whiteColor];
        _plainTableView.separatorColor = [UIColor whiteColor];
    }
    return _plainTableView;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)changeNavBarBottomLineColor:(UIColor *)coler{
    
    [self.navigationController.navigationBar setShadowImage:[self imageWithColor:coler size:CGSizeMake([UIScreen mainScreen].bounds.size.width, 0.5)]];
}
#pragma mark - public methods
- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    if (!color || size.width <= 0 || size.height <= 0) return nil;
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
-(NSString*)getYMD{
    NSDateFormatter * formatter  =[[NSDateFormatter alloc]init] ;
    formatter.dateFormat = @"yyyy-MM";
    NSString * dateStr = [formatter stringFromDate:[NSDate date]];
//    dateStr = [dateStr substringWithRange:NSMakeRange(0, 2) ];
    //    NSLog(@"%d",[dateStr intValue]);
//    NSString *final=[NSString stringWithFormat:@"%@-01",dateStr];
    return dateStr;
}
//隐藏底部横线
- (void)hideNavBarBottomLine {
    UIImageView *bottomLine = [self foundNavigationBarBottomLine:self.navigationController.navigationBar];
    if (bottomLine) {
        bottomLine.hidden = YES;
    }
    UIImageView *navLine = [self.navigationController.navigationBar.subviews[0] viewWithTag:5757];
    if (navLine) {
//        navLine.hidden = YES;
    }
}
//显示底部横线
- (void)showNavBarBottomLine {
    
    UIImageView *bottomLine = [self foundNavigationBarBottomLine:self.navigationController.navigationBar];
    if (bottomLine) {
        bottomLine.hidden = NO;
        return;
    }
    UIImageView *navLine = [self.navigationController.navigationBar.subviews[0] viewWithTag:5757];
    if (navLine) {
        navLine.hidden = NO;
        CGRect bottomLineFrame = bottomLine.frame;
        bottomLineFrame.origin.y = CGRectGetMaxY(self.navigationController.navigationBar.frame)-1;
        navLine.frame = bottomLineFrame;
    }else {
        CGRect bottomLineFrame = bottomLine.frame;
        bottomLineFrame.origin.y = CGRectGetMaxY(self.navigationController.navigationBar.frame);
        UIImageView *navLine = [[UIImageView alloc] initWithFrame:bottomLineFrame];
        navLine.tag = 5757;
        navLine.backgroundColor = HLHex(0xeeeeee);
        if (self.navigationController.navigationBar.subviews.count) {
            [self.navigationController.navigationBar.subviews[0] addSubview:navLine];
        }else{
            bottomLine.hidden = NO;
        }
    }
}

//寻找底部横线
- (UIImageView *)foundNavigationBarBottomLine:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self foundNavigationBarBottomLine:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}
//添加空白页面
-(void)addBlankOnView:(UIView *)view{
    [view addSubview:self.noDataView];
//    NSLog(@"%f------%f---",view.frame.size.width,view.width);
    self.noDataView.center = CGPointMake(view.frame.size.width/2, view.frame.size.height/2-50);
}
-(void)addBlankOnView:(UIView *)view withY:(CGFloat)y{
    [view addSubview:self.noDataView];
    CGSize size = self.noDataView.size;
    self.noDataView.center = CGPointMake(view.frame.size.width/2, y+(size.height)/2);
}

#pragma mark - 懒加载
-(NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
#pragma mark -- 无数据view
- (BlankView *)noDataView{
    if (!_noDataView) {
        _noDataView = [[BlankView alloc] initWithFrame:CGRectMake(0, 0, 240, 180)];
    }
    return _noDataView;
}

#pragma mark -- 释放
- (void)dealloc{
    NSString  *ctrl_controller = NSStringFromClass([self class]);
    NSLog(@"释放控制器:%@",ctrl_controller);
}


@end
