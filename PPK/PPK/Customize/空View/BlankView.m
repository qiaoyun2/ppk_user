//
//  BlankView.m
//  Mei
//
//  Created by null on 2019/4/24.
//  Copyright © 2019 null. All rights reserved.
//

#import "BlankView.h"

@implementation BlankView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self = [[[NSBundle mainBundle]loadNibNamed:@"BlankView" owner:nil options:nil] firstObject];
        [self setFrame:frame];
    }
    return self;
}

//- (void)layoutSubviews{
//    [super layoutSubviews];
//    self.bounds = CGRectMake(0, 0, 240, 200);
//}

@end
