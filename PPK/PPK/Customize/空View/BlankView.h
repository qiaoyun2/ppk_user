//
//  BlankView.h
//  Mei
//
//  Created by null on 2019/4/24.
//  Copyright © 2019 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BlankView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;


@end

NS_ASSUME_NONNULL_END
