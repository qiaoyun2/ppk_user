//
//  UIImage+Category.h
//  anjuyi
//
//  Created by null on 2018/5/29.
//  Copyright © 2018年 null. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Category)
//由颜色生成图片
+ (UIImage *) imageWithColor:(UIColor*)color;

//绘图
- (UIImage*)imageChangeColor:(UIColor*)color;
@end
