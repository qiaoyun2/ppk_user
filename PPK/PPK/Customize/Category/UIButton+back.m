//
//  UIButton+back.m
//  Mei
//
//  Created by null on 2019/4/10.
//  Copyright © 2019 null. All rights reserved.
//

#import "UIButton+back.h"

@implementation UIButton (back)

- (void)setBackgroundDefaultGradientColor{
    
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0,0, self.width, self.height);
    gl.startPoint = CGPointMake(0, 0.5);
    gl.endPoint = CGPointMake(1, 0.5);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:150/255.0 green:96/255.0 blue:248/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:231/255.0 green:91/255.0 blue:239/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0.0),@(1.0)];
    [self.layer insertSublayer:gl atIndex:0];
}

@end
