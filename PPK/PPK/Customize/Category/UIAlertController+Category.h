//
//  UIAlertController+Category.h
//  daycareParent
//
//  Created by 钧泰科技 on 2018/2/5.
//  Copyright © 2018年 XueHui. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^indexBlock)(NSInteger index,id obj);
typedef void(^Block)(void);
@interface UIAlertController (Category)

/**
 actionSheet 不带取消按钮
 @param title title
 @param message message
 @param titleArry 列表
 @param indexBlock 选择对象block
 @return actionSheet
 */
+(UIAlertController *)actionSheetWithTitle:(NSString *)title message:(NSString *)message titlesArry:(NSArray *)titleArry indexBlock:(indexBlock)indexBlock;
/**
 actionSheet 标准
 @param title title
 @param message message
 @param titleArry 列表
 @param indexBlock 选择对象block
 @param okColor 确认按钮颜色
 @param cancleColor 取消按钮颜色
 @param isHaveCancel 是否有取消按钮
 @return actionSheet
 */
+(UIAlertController *)actionSheetNormalWithTitle:(NSString *)title message:(NSString *)message titlesArry:(NSArray *)titleArry indexBlock:(indexBlock)indexBlock okColor:(UIColor *)okColor cancleColor:(UIColor *)cancleColor isHaveCancel:(BOOL)isHaveCancel;
/**
 alertview
 @param title title
 @param message message
 @param block 选择block
 @return alertview
 */
+(UIAlertController *)alertViewWithTitle:(NSString *)title message:(NSString *)message block:(Block)block;
/**
 alertview
 @param title title
 @param message message
 @param titleArry 对象数组
 @param indexBlock 选择block
 @param okColor 确认按钮颜色
 @param cancleColor 取消按钮颜色
 @param isHaveCancel 是否有取消按钮
 @return alertview
 */
+(UIAlertController *)alertViewNormalWithTitle:(NSString *)title message:(NSString *)message titlesArry:(NSArray *)titleArry indexBlock:(indexBlock)indexBlock okColor:(UIColor *)okColor cancleColor:(UIColor *)cancleColor isHaveCancel:(BOOL)isHaveCancel;
/**
 alertview
 @param title title
 @param message message
 @param indexBlock 选择对象block
 @return alertview
 */
+(UIAlertController *)addtextFeildWithTitle:(NSString *)title message:(NSString *)message indexBlock:(indexBlock)indexBlock;
+(UIAlertController *)addtextFeildCustomWithTitle:(NSString *)title message:(NSString *)message indexBlock:(indexBlock)indexBlock;
@end
