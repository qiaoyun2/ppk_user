//
//  UILabel+LineSpace.h
//  phonelive
//
//  Created by null on 2018/12/22.
//  Copyright © 2018年 ebz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (LineSpace)
//设置行间距
- (void)setLineSpaceWithValue:(NSString*)str withFont:(UIFont*)font;
//快速创建label
+ (UILabel *)LabelWithFrame:(CGRect)frame fontSize:(int)size textColor:(UIColor *)textColor  textAlient:(NSTextAlignment )alient  numberLines:(int)numLine;
@end
