//
//  UILabel+LineSpace.m
//  phonelive
//
//  Created by null on 2018/12/22.
//  Copyright © 2018年 ebz. All rights reserved.
//

#import "UILabel+LineSpace.h"

@implementation UILabel (LineSpace)

- (void)setLineSpaceWithValue:(NSString*)str withFont:(UIFont*)font
{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    
    paraStyle.alignment = NSTextAlignmentLeft;
    
    paraStyle.lineSpacing = 5; //设置行间距
    
    paraStyle.hyphenationFactor = 1.0;
    
    paraStyle.firstLineHeadIndent = 0.0;
    
    paraStyle.paragraphSpacingBefore = 0.0;
    
    paraStyle.headIndent = 0;
    
    paraStyle.tailIndent = 0;
    
    //设置字间距 NSKernAttributeName:@0.5f
    
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@0.3f
                          };
    
    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
    
    self.attributedText = attributeStr;
    
}
+ (UILabel *)LabelWithFrame:(CGRect)frame fontSize:(int)size textColor:(UIColor *)textColor  textAlient:(NSTextAlignment)alient  numberLines:(int)numLine
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    if (size) {
        label.font =  [UIFont systemFontOfSize:size];
    }
    if (textColor) {
        label.textColor =  textColor;
    }
    if (alient) {
        label.textAlignment =  alient;
    }
    if (numLine) {
        label.numberOfLines =  numLine;
    }
    return label;
}
@end
