//
//  UILabel+xib.m
//  DZBase
//
//  Created by null on 2020/8/4.
//  Copyright © 2020 null. All rights reserved.
//

#import "UILabel+xib.h"
#import "Aspects.h"
#import "LocalizedHelper.h"

@implementation UILabel (xib)

- (void)setMiddleLine:(BOOL)middleLine {
    
    if (middleLine == true) {
        [self setAttributedWirhAttributeName:NSStrikethroughStyleAttributeName];
        __weak typeof (self) weakSelf = self;

        [self aspect_hookSelector:@selector(setText:) withOptions:AspectPositionAfter usingBlock:^(){
            
            [weakSelf setAttributedWirhAttributeName:NSStrikethroughStyleAttributeName];
        } error:nil];
    }
}

- (void)setAttributedWirhAttributeName:(NSString *)key {
    
    NSMutableAttributedString *aText = [[NSMutableAttributedString alloc] initWithString:self.text];
    [aText addAttribute:key value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, aText.length)];
    self.attributedText = aText;
}

- (void)setLocalizedText:(NSString *)localizedText{
    
    if (localizedText.length != 0) {
        [self setText:LocalizedString(localizedText)];
    }
}

@end
