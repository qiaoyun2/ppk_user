//
//  UILabel+xib.h
//  DZBase
//
//  Created by null on 2020/8/4.
//  Copyright © 2020 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (xib)

//中间横线
@property(nonatomic, assign) IBInspectable BOOL middleLine;

//多语言设置
@property(nonatomic, strong) IBInspectable NSString * localizedText;

@end

NS_ASSUME_NONNULL_END
