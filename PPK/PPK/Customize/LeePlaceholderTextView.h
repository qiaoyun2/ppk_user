//
//  LeePlaceholderTextView.h
//  null
//
//  Created by null on 2019/11/5.
//  Copyright © 2019 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeePlaceholderTextView : UITextView
/** 占位文字 */
@property (nonatomic, copy) NSString *placeholder;
/** 占位文字颜色 */
@property (nonatomic, strong) UIColor *placeholderColor;
/** 限制字符*/
@property (nonatomic, assign) NSInteger labelCount;

@end

NS_ASSUME_NONNULL_END
