//
//  LocalizedHelper.h
//  TXLiteAVDemo_Smart
//
//  Created by null on 2019/5/13.
//  Copyright © 2019 Tencent. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
#define LocalizedString(key) key
@interface LocalizedHelper : NSObject

@end

NS_ASSUME_NONNULL_END
