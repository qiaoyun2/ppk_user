//
//  MTOrderDetailModel.m
//  PPK
//
//  Created by Apple on 2022/8/31.
//

#import "MTOrderDetailModel.h"

@implementation MTOrderDetailModel
- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
}
@end
