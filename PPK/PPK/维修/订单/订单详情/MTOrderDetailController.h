//
//  MTOrderDetailController.h
//  PPK
//
//  Created by Apple on 2022/8/30.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTOrderDetailController : BaseViewController
@property (nonatomic, strong) NSString *orderId;
@end

NS_ASSUME_NONNULL_END
