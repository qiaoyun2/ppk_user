//
//  MTOrderDetailController.m
//  PPK
//
//  Created by Apple on 2022/8/30.
//

#import "MTOrderDetailController.h"
#import "FoundListImage9TypographyView.h"
#import "MTOrderDetailModel.h"

@interface MTOrderDetailController ()
@property (weak, nonatomic) IBOutlet UIView *nomalStatusView;
///订单状态
@property (weak, nonatomic) IBOutlet UILabel *nomalStatusLabel;

@property (weak, nonatomic) IBOutlet UIView *refundStatusView;
///订单状态
@property (weak, nonatomic) IBOutlet UILabel *refundStatusLabel;
///支付金额已原路返回
@property (weak, nonatomic) IBOutlet UILabel *refundResonLabel;
///退款金额
@property (weak, nonatomic) IBOutlet UILabel *refundMoneyLabel;
///师傅view
@property (weak, nonatomic) IBOutlet UIView *masterView;
///师傅头像
@property (weak, nonatomic) IBOutlet UIImageView *masterHeadImageView;
///师傅凝成
@property (weak, nonatomic) IBOutlet UILabel *masterNameLabel;
///标签view
@property (weak, nonatomic) IBOutlet UIView *tagView;
///标签view宽度
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagViewWidth;
///评分
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
///评分星星view
@property (weak, nonatomic) IBOutlet UIView *starView;
///所有的单数
@property (weak, nonatomic) IBOutlet UILabel *allNumberLabel;
///线view
@property (weak, nonatomic) IBOutlet UIView *lineMasterView;
///订单线view
@property (weak, nonatomic) IBOutlet UIView *lineOrderView;
///维修类型
@property (weak, nonatomic) IBOutlet UILabel *mtTypeLabel;
///支付金额
@property (weak, nonatomic) IBOutlet UILabel *payPriceLabel;
///备注
@property (weak, nonatomic) IBOutlet UILabel *remarksLabel;
///店铺view
@property (weak, nonatomic) IBOutlet UIView *onerView;
///头像
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
///名字
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
///电话
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
///图片view
@property (weak, nonatomic) IBOutlet UIView *imageView;
///展示图片
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *photoImageView;
///订单编号
@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel;
///预约时间
@property (weak, nonatomic) IBOutlet UILabel *appointTimeLabel;
///下单时间
@property (weak, nonatomic) IBOutlet UILabel *makeTimeLabel;
///接单时间
@property (weak, nonatomic) IBOutlet UILabel *receiveTimeLabel;
///售后时间
@property (weak, nonatomic) IBOutlet UILabel *afterSalesTimeLabel;
///到达时间
@property (weak, nonatomic) IBOutlet UILabel *arriveTimeLabel;
///评价时间
@property (weak, nonatomic) IBOutlet UILabel *judgeTimeLabel;
///接单view
@property (weak, nonatomic) IBOutlet UIView *receiveView;
///售后view
@property (weak, nonatomic) IBOutlet UIView *afterSalesView;
///到达View
@property (weak, nonatomic) IBOutlet UIView *arriveView;
///评价View
@property (weak, nonatomic) IBOutlet UIView *judgeView;
///取消订单
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
///退款申请
@property (weak, nonatomic) IBOutlet UIButton *applyBtn;
///删除按钮
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
///撤销申请
@property (weak, nonatomic) IBOutlet UIButton *revokeBtn;
///评价
@property (weak, nonatomic) IBOutlet UIButton *judgeBtn;
///UIStackView
@property (weak, nonatomic) IBOutlet UIStackView *buttonsStackView;

@property (nonatomic, strong) MTOrderDetailModel *detailModel;


@end

@implementation MTOrderDetailController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self requestForOrderDetail];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单详情";
    
    for (UIButton *button in self.buttonsStackView.arrangedSubviews) {
        button.hidden = YES;
    }
    
    CDKViewGradientTopToBottom(self.lineMasterView, @[UIColorFromRGB(0x81D1F4),UIColorFromRGB(0x00A2EA)]);
    CDKViewGradientTopToBottom(self.lineOrderView, @[UIColorFromRGB(0x81D1F4),UIColorFromRGB(0x00A2EA)]);

}
#pragma mark - Network
- (void)requestForOrderDetail
{
    [NetworkingTool getWithUrl:kRWOrderDetailURL params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.detailModel = [[MTOrderDetailModel alloc] initWithDictionary:dataDic];
            [self updateSubviews];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 更新UI
-(void)updateSubviews{
    
}

#pragma mark - 打电话
- (IBAction)callBtnClick:(id)sender {
//    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"186xxxx6979"];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    
}

#pragma mark -  确认送达
- (IBAction)sureSendBtnClick:(id)sender {
}

#pragma mark - 按钮的点击
/// 1,取消订单 退款申请 删除 撤销 评价按钮的点击
- (IBAction)btnClick:(UIButton *)sender {
    switch (sender.tag) {
        case 1://
            
            break;
        case 2:
            
            break;
        case 3:
            
            break;
        case 4:
            
            break;
        case 5:
            
            break;
            
        default:
            break;
    }
    
}

@end
