//
//  MTOrderListViewController.h
//  PPK
//
//  Created by Apple on 2022/8/30.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTOrderListViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSInteger status;
@end

NS_ASSUME_NONNULL_END
