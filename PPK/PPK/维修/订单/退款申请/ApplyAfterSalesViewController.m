//
//  ApplyAfterSalesViewController.m
//  PPK
//
//  Created by Apple on 2022/8/31.
//

#import "ApplyAfterSalesViewController.h"
#import <HXPhotoPicker/HXPhotoPicker.h>

@interface ApplyAfterSalesViewController ()<HXPhotoViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *reasonView;
///理由
@property (weak, nonatomic) IBOutlet UITextField *reasonText;
///价格
@property (weak, nonatomic) IBOutlet UITextField *priceLabel;
///内容描述
@property (weak, nonatomic) IBOutlet UITextView *destailTextView;
///照片
@property (weak, nonatomic) IBOutlet HXPhotoView *photoView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoViewHeight;

@property (nonatomic, assign) BOOL isVideo;
@property (nonatomic, strong) NSMutableArray<UIImage *> *photos;
@property (nonatomic, strong) HXPhotoModel *videoModel;
@property (nonatomic, strong) NSString *videoPath;
@property (nonatomic, strong) NSMutableArray *imagePathArray;
@property (strong, nonatomic) HXPhotoManager *manager;


@end

@implementation ApplyAfterSalesViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
