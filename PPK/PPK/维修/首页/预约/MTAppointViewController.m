//
//  MTAppointViewController.m
//  PPK
//
//  Created by Apple on 2022/8/30.
//

#import "MTAppointViewController.h"
#import <HXPhotoPicker/HXPhotoPicker.h>
#import "HXAssetManager.h"
#import "UploadManager.h"
#import "RWCateController.h"
#import "RWSubCateView.h"
#import "RWReserveResultController.h"
#import "AddressListView.h"
#import "RWReserveTimeView.h"
#import "PayViewController.h"



@interface MTAppointViewController ()<HXPhotoViewDelegate>
///价格
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
///没有添加分类
@property (weak, nonatomic) IBOutlet UIView *noClassView;
///提示信息
@property (weak, nonatomic) IBOutlet UILabel *tipsLabel;
///分类view
@property (weak, nonatomic) IBOutlet UIView *classView;
///分类1
@property (weak, nonatomic) IBOutlet UIButton *cateButton;
///分类2
@property (weak, nonatomic) IBOutlet UIButton *subCateButton;
///名字
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
///手机号
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
///地址
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
///时间
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
///备注
@property (weak, nonatomic) IBOutlet UITextView *textView;
///是否选择
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@property (weak, nonatomic) IBOutlet HXPhotoView *photoView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoViewHeight;

///确认时间
@property (nonatomic, strong) NSString *reserveTime;
///分类
@property (strong, nonatomic) RWSubCateView *subCateView;
///地址
@property (nonatomic, strong) AddressListView *addressView;
///时间
@property (nonatomic, strong) RWReserveTimeView *timeView;

@property (nonatomic, assign) BOOL isVideo;
@property (nonatomic, strong) NSMutableArray<UIImage *> *photos;
@property (nonatomic, strong) HXPhotoModel *videoModel;
@property (nonatomic, strong) NSString *videoPath;
@property (nonatomic, strong) NSMutableArray *imagePathArray;
@property (strong, nonatomic) HXPhotoManager *manager;
///预约价格
@property (nonatomic, strong) NSString *price;


@end

@implementation MTAppointViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"预约";
    self.textView.jk_placeHolderTextView.text = @"请输入备注内容...";
    [self requestForBailAmount];
   
    [self updateAddressView];
    [self initImageView];
}
#pragma mark -UI
-(void)initImageView{
    self.photos = [NSMutableArray array];
    self.imagePathArray = [NSMutableArray array];
    
    self.photoView.spacing = 10.f;
    self.photoView.delegate = self;
    self.photoView.deleteCellShowAlert = YES;
    self.photoView.outerCamera = YES;
    self.photoView.previewShowDeleteButton = YES;
    self.photoView.addImageName = @"组 50938";
    self.photoView.lineCount = 4;
    self.photoView.manager = self.manager;
    
    self.tipsLabel.attributedText = [LJTools attributString:@"维修须知：" twoStr:@"所有用户在下单时需要支付￥50作为订单保证金，本金额只作为维修员上门时的路费（需用户确认到达后支付），师傅确认出发前双方均可无责退单，师傅出发后需要用户向平台申请退款" color:UIColorFromRGB(0x00A2EA) oneHeight:11 andTColor:UIColorFromRGB(0x666666) twoHeight:11];
   
    if (self.cateModel ) {
        self.noClassView.hidden = YES;
        self.classView.hidden = NO;
        [self.cateButton setTitle:self.cateModel.name forState:UIControlStateNormal];
        [self.subCateButton setTitle:self.subCateModel.name forState:UIControlStateNormal];
    }else{
        self.noClassView.hidden = NO;
        self.classView.hidden = YES;
    }
    
}
#pragma mark -  保证金金额
-(void)requestForBailAmount{
    [NetworkingTool getWithUrl:kMTRepairBailAmountURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS){
            double data = [responseObject[@"data"] doubleValue];
            self.price = responseObject[@"data"];
            self.priceLabel.attributedText = [LJTools attributedString:[NSString stringWithFormat:@"%.2lf",data] color:UIColorFromRGB(0xFA2033) oneHeight:12 andTColor:UIColorFromRGB(0xFA2033) twoHeight:18 andThreeTColor:UIColorFromRGB(0xFA2033) threeHeight:18];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showText:responseObject[@"msg"] delay:1.5];
    } IsNeedHub:NO];
}
#pragma mark -  提交预约
- (void)requestForReserve {

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"firstClassifyId"] = self.cateModel.ID;
    params[@"secondClassifyId"] = self.subCateModel.ID;
    params[@"remark"] = self.textView.text;
    params[@"reserveTime"] = self.reserveTime;
    if (self.videoModel) {//resourceType 资源类型：1-图片；2-视频
        params[@"resourceType"] = @"2";
        params[@"video"] = self.videoPath;
        params[@"videoPicture"] = [self.imagePathArray firstObject];
        params[@"picture"] = @"";
    }else if (self.imagePathArray.count) {
        params[@"resourceType"] = @"1";
        params[@"picture"] = [self.imagePathArray componentsJoinedByString:@","];
        params[@"video"] = @"";
        params[@"videoPicture"] = @"";
    }
    else {
        params[@"resourceType"] = @"";
        params[@"picture"] = @"";
        params[@"video"] = @"";
        params[@"videoPicture"] = @"";
    }
    if (self.addressModel.ID.intValue>0) {
        params[@"addressId"] = self.addressModel.ID;
    }
    else{
        params[@"addressId"] = @"";
        params[@"longitude"] = self.addressModel.longitude;
        params[@"latitude"] = self.addressModel.latitude;
        params[@"country"] = self.addressModel.country;
        params[@"province"] = self.addressModel.province;
        params[@"city"] = self.addressModel.city;
        params[@"area"] = self.addressModel.area;
        params[@"street"] = self.addressModel.street;
        params[@"nickname"] = self.addressModel.receiver;
        params[@"mobile"] = self.addressModel.telphone;
    }

    [NetworkingTool postWithUrl:kMTRepairAddURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSString *order_sn = responseObject[@"data"];
            PayViewController *vc = [[PayViewController alloc] init];
            vc.order_sn = order_sn;
            vc.type = 1;
            vc.price = self.price;
            [self.navigationController pushViewController:vc animated:YES];
            
            NSMutableArray *controllers = [[NSMutableArray alloc]initWithArray:self.navigationController.viewControllers];

            for (UIViewController *vc in controllers) {
                if ([vc isKindOfClass:[MTAppointViewController class]]) {
                    [controllers removeObject:vc];
                    break;
                }
            }
            self.navigationController.viewControllers = controllers;
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 上传视频
- (void)requestForUploadVideo
{
    NSData *data = [NSData dataWithContentsOfURL:self.videoModel.videoURL];
    WeakSelf
    [UploadManager uploadWithVideoData:data block:^(NSString * _Nonnull videoUrl) {
        NSLog(@"%@",videoUrl);
        weakSelf.videoPath = videoUrl;
        [weakSelf requestForUploadImages];
    }];
}
#pragma mark - 上传图片
- (void)requestForUploadImages
{
    WeakSelf
    [UploadManager uploadImageArray:self.photos block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
        NSArray *array = [imageUrl componentsSeparatedByString:@","];
        [weakSelf.imagePathArray addObjectsFromArray:array];
        [weakSelf requestForReserve];

    }];
}

#pragma mark - HXPhotoViewDelegate
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {

    if (videos.count>0) {
        self.isVideo = YES;
        self.videoModel = [videos firstObject];
        [self.photos removeAllObjects];
        [self.imagePathArray removeAllObjects];
        [self.videoModel getImageWithSuccess:^(UIImage * _Nullable image, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
            [self.photos addObject:image];
        } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
            
        }];
        NSLog(@"%@",self.videoModel.videoURL);
    }
    else {
        self.isVideo = NO;
        self.videoModel = nil;
        [self.photos removeAllObjects];
        [self.imagePathArray removeAllObjects];
        for (HXPhotoModel *model in photos) {
            [self.photos addObject:[HXAssetManager originImageForAsset:model.asset]];
        }
//        [self requestForUploadImages];
    }
//    self.selectList = allList;
}
// 拿到view变化的高度
- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame {
    self.photoViewHeight.constant = frame.size.height;
}
#pragma mark - Function
- (void)updateAddressView
{
//    self.nameLabel.text = [NSString stringWithFormat:@"%@(%@)",self.addressModel.receiver,[self.addressModel.gender integerValue]==1?@"男士":@"女士"];
    self.nameLabel.text = [NSString stringWithFormat:@"%@",self.addressModel.receiver];
    self.mobileLabel.text = self.addressModel.telphone;
    self.addressLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@",_addressModel.province,_addressModel.city,_addressModel.area,_addressModel.street];
}

#pragma mark -  地址
- (IBAction)addressViewTap:(id)sender {
    if (!self.addressView) {
        self.addressView = [[[NSBundle mainBundle] loadNibNamed:@"AddressListView" owner:nil options:nil] firstObject];
        self.addressView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.addressView];
    }
    [self.addressView show];
    WeakSelf
    [self.addressView setOnSelectedAddress:^(AddressModel * _Nonnull addressModel) {
        weakSelf.addressModel = addressModel;
        [weakSelf updateAddressView];
    }];

}

#pragma mark -  预约时间
- (IBAction)timeViewTap:(id)sender {
    if (!self.timeView) {
        self.timeView = [[[NSBundle mainBundle] loadNibNamed:@"RWReserveTimeView" owner:nil options:nil] firstObject];
        self.timeView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.timeView];
    }
    [self.timeView show];
    WeakSelf
    [self.timeView setDidSelectBlock:^(NSString * _Nonnull selectedStr) {
        weakSelf.reserveTime = selectedStr;
        weakSelf.timeLabel.text = selectedStr;
        weakSelf.timeLabel.textColor = UIColorFromRGB(0x333333);
    }];

}

#pragma mark -  一级分类
- (IBAction)cateButtonAction:(id)sender {
    RWCateController *vc = [[RWCateController alloc] init];
    vc.type = 1;
    WeakSelf
    [vc setDidSelectBlock:^(RWCateModel * _Nullable cateModel) {
        weakSelf.cateModel = cateModel;
        weakSelf.subCateModel = nil;
        [weakSelf.cateButton setTitle:cateModel.name forState:UIControlStateNormal];
        [weakSelf.subCateButton setTitle:@"请选择二级分类" forState:UIControlStateNormal];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -  二级分类
- (IBAction)subCateButtonAction:(id)sender {
    if (!self.subCateView) {
        self.subCateView = [[[NSBundle mainBundle] loadNibNamed:@"RWSubCateView" owner:nil options:nil] firstObject];
        self.subCateView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.subCateView];
    }
    self.subCateView.type = 1;
    self.subCateView.firstModel = self.cateModel;
    
    [self.subCateView show];
    WeakSelf
    [self.subCateView setDidSelectBlock:^(RWCateModel * _Nullable subModel) {
        weakSelf.subCateModel = subModel;
        [weakSelf.subCateButton setTitle:subModel.name forState:UIControlStateNormal];
    }];
}

#pragma mark -  勾选按钮
- (IBAction)selectButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
}
#pragma mark - 协议
- (IBAction)agreementButtonAction:(UIButton *)sender {
    
    //FIXME: qy - 维修须知暂时没有配置
    [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"recoveryKnow"} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSString *privacy = (NSString *)responseObject[@"data"];
            WKWebViewController *vc = [WKWebViewController new];
            vc.titleStr = @"维修须知";
            vc.contentStr = privacy;
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
    
}

#pragma mark -  预约
- (IBAction)reservationButtonAction:(UIButton *)sender {

    if (!self.subCateModel) {
        [LJTools showText:@"请选择二级分类" delay:1.5];
        return;
    }

    
    if (!self.reserveTime.length) {
        [LJTools showText:@"请选择预约时间" delay:1.5];
        return;
    }
    
    if (!self.selectButton.selected) {
        [LJTools showText:@"请阅读并同意《维修须知》" delay:1.5];
        return;
    }
    
 
    [self requestForReserve];

}

#pragma mark - Lazy Load
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
        _manager.configuration.type = HXConfigurationTypeWXChat;
        _manager.configuration.singleSelected = NO;
        _manager.configuration.lookGifPhoto = NO;
        _manager.configuration.useWxPhotoEdit = YES;
        _manager.configuration.selectTogether=NO;
        _manager.configuration.photoEditConfigur.onlyCliping = YES;
        _manager.configuration.showOriginalBytes = NO;
        _manager.configuration.videoMaxNum=1;
        _manager.configuration.photoMaxNum=9;
        _manager.configuration.requestImageAfterFinishingSelection = YES;
        _manager.configuration.photoEditConfigur.aspectRatio = HXPhotoEditAspectRatioType_None;
    }
    return _manager;
}

@end
