//
//  MTAppointViewController.h
//  PPK
//
//  Created by Apple on 2022/8/30.
//

#import "BaseViewController.h"
#import "AddressModel.h"
#import "RWCateModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTAppointViewController : BaseViewController
@property (nonatomic, strong) AddressModel *addressModel;
@property (nonatomic, strong) RWCateModel *cateModel;
@property (nonatomic, strong) RWCateModel *__nullable subCateModel;

@end

NS_ASSUME_NONNULL_END
